<?php
class Login {
	public static function Index() {
		if (StackValue::hasValue('id_message_connexion')) {
			$message = StaticValue::$message_cle_valeur[StackValue::getValue('id_message_connexion')];
		} else {
			$message = false;
		}
		
		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'message', $message);
		$template_layout->add(array('content'), 'titre_site', StaticValue::$titre_site);
		echo $template_layout->draw();
	}
	
	public static function Deconnexion() {
		Request::setCookieParameter('cookie_connect', '', (time() - StaticDbValue::get('temps_duree_cookie_connexion')));
		StackValue::setValue('id_message_connexion', 'deconnexion');
		self::Index();
	}
	
	private static function Layout() {
		return Loader::get('Template')->set('layout1.tpl', array(
				'head' => Loader::get('Template')->set('head.tpl', array(
						'titre_site' => StaticValue::$titre_site
				)),
				'content' => Loader::get('Template')->set('login.tpl'),
				'script' => Loader::get('Template')->set('script.tpl')
		));
	}
}