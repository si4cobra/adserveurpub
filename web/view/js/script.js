$(document).ready(function() {
	$("input:file").not('[data-no-uniform="true"],#uniform-is-ajax').uniform();
	
	$.datepicker.regional['fr'] = {
			prevText: 'Précédent',
			nextText: 'Suivant',
			monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
			dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
			dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
			dateFormat: 'dd/mm/yy',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['fr']);
		
	$(".datepicker_date").datepicker();
	$(".datepicker_hour").timepicker({
		hourText: 'Heures',
		showMinutes: false,
		showPeriodLabels: false,
		showLeadingZero: true
	});
	
	$("input[type=checkbox][name=periodicite]").click(function () {
		if ($(this).is(':checked')) {
			$("input[type=checkbox][name='periodicite_jour[]']").each(function() {
				$(this).attr('checked', true);
			});
		} else {
			$("input[type=checkbox][name='periodicite_jour[]']").each(function() {
				$(this).removeAttr('checked');
			});
		}
	});
	
	$("input[type=checkbox][name='periodicite_jour[]']").click(function() {
		if ($("input:checked[type=checkbox][name='periodicite_jour[]']").length == 7) {
			$("input[type=checkbox][name=periodicite]").attr('checked', true);
		} else {
			$("input:checked[type=checkbox][name=periodicite]").removeAttr('checked');
		}
	});
	
	$("#heure_fin").change(function() {
		if ($(this).val() == '00' || $(this).val() == '0') {
			$(this).val('23:59:59');
			
			if ($("#heure_debut").val() == '00' || $("#heure_debut").val() == '0') {
				$("#toute_la_journee").attr('checked', true);
			}
		} else {
			$("#toute_la_journee").removeAttr('checked');
		}
	});
	
	$("#heure_debut").change(function() {
		if ($(this).val() == '00' || $(this).val() == '0') {
			if ($("#heure_fin").val() == '23:59:59') {
				$("#toute_la_journee").attr('checked', true);
			}
		} else {
			$("#toute_la_journee").removeAttr('checked');
		}
	});
	
	$("#toute_la_journee").click(function() {
		if ($(this).is(':checked')) {
			$('#heure_debut').val('00');
			$('#heure_fin').val('23:59:59');
		} else {
			$('#heure_debut').val('');
			$('#heure_fin').val('');
		}
	});
	
	$("#non_modification_affiche").click(function() {
		if ($(this).is(':checked')) {
			var fichier_ecran = $("#fichier_ecran");
			var filename = $("#fichier_ecran + .filename");
			fichier_ecran.replaceWith(fichier_ecran = fichier_ecran.clone(true));
			filename.html('Pas de fichier sélectionné');
			$('#oui_modification_affiche').removeAttr('checked');
			$('#formulaire_nouvelle_affiche').hide(500);
		} else {
			$('#oui_modification_affiche').attr('checked', true);
			$('#formulaire_nouvelle_affiche').show(500).css('display', 'inline-block');
		}
	});
	
	$("#oui_modification_affiche").click(function() {
		if ($(this).is(':checked')) {
			$('#non_modification_affiche').removeAttr('checked');
			$('#formulaire_nouvelle_affiche').show(500).css('display', 'inline-block');
		} else {
			$('#non_modification_affiche').attr('checked', true);
			$('#formulaire_nouvelle_affiche').hide(500);
		}
	});
	
	$("#fichier_ecran").change(function() {
		if ($(this).val() != '') {
			$("#oui_modification_affiche").attr('checked', true);
			$("#non_modification_affiche").removeAttr('checked');
		} else {
			$("#oui_modification_affiche").removeAttr('checked');
			$("#non_modification_affiche").attr('checked', true);
		}
	});
	
	$("#bouton_mode_expert_planning").click(function() {
		if ($("#mode_expert_planning").css('display') == 'none') {
			$("#mode_expert_planning").show(500);
			$("#label_bouton_mode_expert_planning").text('Masquer le mode expert');
		} else {
			$("#mode_expert_planning").hide(500);
			$("#label_bouton_mode_expert_planning").text('Affiner la programmation (mode expert)');
		}
	});
});