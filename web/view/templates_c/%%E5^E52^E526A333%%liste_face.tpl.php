<?php /* Smarty version 2.6.9, created on 2018-03-22 15:38:22
         compiled from liste_face.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'liste_face.tpl', 1, false),)), $this); ?>
<?php if (count($this->_tpl_vars['liste_face']) > 0): ?>
<table class="table table-bordered table-striped" style="width:auto;margin-bottom:8px;">
	<thead>
		<tr>
			<th>Ville</th>
			<th>Mobilier</th>
			<th>Adresse</th>
			<th>Visibilité</th>
			<th>Action</th>
		</tr>
	</thead>
	<?php $_from = $this->_tpl_vars['liste_face']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['face']):
?>
	<tr>
		<td><?php echo $this->_tpl_vars['face']['ville']; ?>
</td>
		<td>
			<?php if ($this->_tpl_vars['face']['numero_mobilier'] != ''): ?>
			RIM No <?php echo $this->_tpl_vars['face']['numero_mobilier']; ?>

			<?php endif; ?>
		</td>
		<td><?php echo $this->_tpl_vars['face']['adresse_mobilier']; ?>

		<td><?php echo $this->_tpl_vars['face']['nom_face']; ?>
</td>
		<td style="padding-bottom:0;">
			<a class="btn btn-info" href="index.php?page=selection&face=<?php echo $this->_tpl_vars['face']['id']; ?>
" style="display:block;margin-bottom:8px;">
				<i class="icon-list icon-white"></i>  
				Visualiser
			</a>
		</td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
</table>
<?php endif; ?>