<?php /* Smarty version 2.6.9, created on 2019-09-25 17:53:50
         compiled from liste_reseau_tournant.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'liste_reseau_tournant.tpl', 1, false),)), $this); ?>
<?php if (count($this->_tpl_vars['liste_reseau_tournant']) > 0): ?>
<table class="table table-bordered table-striped" style="width:auto;margin-bottom:8px;">
	<thead>
		<tr>
			<th>Réseau tournant</th>
			<!--<th>Action</th>-->
		</tr>
	</thead>
	<?php $_from = $this->_tpl_vars['liste_reseau_tournant']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['reseau_tournant']):
?>
	<tr>
		<td><?php echo $this->_tpl_vars['reseau_tournant']['nom']; ?>
</td>

	</tr>
	<?php endforeach; endif; unset($_from); ?>
</table>
	<table class="table table-bordered table-striped " style="width:auto;margin-bottom:8px;">
		<tr>
			<td style="padding-bottom:0;">
				<a class="btn btn-info" href="index.php?page=selection&reseau_tournant=<?php echo $this->_tpl_vars['reseau_tournant']['id']; ?>
" style="display:block;margin-bottom:8px;">
					<i class="icon-list icon-white"></i>
					Visualiser la liste des faces sur les réseaux tournants
				</a>
			</td>
		</tr>

	</table>
<?php endif; ?>