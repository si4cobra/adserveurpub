<?php /* Smarty version 2.6.9, created on 2018-03-22 15:39:28
         compiled from liste3.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'liste3.tpl', 2, false),)), $this); ?>
<div style="width:auto;margin-bottom:0;display:inline-block;vertical-align:top;">
	<?php if (count($this->_tpl_vars['plannings']) > 0): ?>
		<div class="span12 well" style="padding:10px;margin-bottom:10px;width:auto;display:inline-block;float:none;">
		Toutes les programmations de visuels pour la FACE <?php echo $this->_tpl_vars['numero_de_face']; ?>

		</div>
		<?php $_from = $this->_tpl_vars['plannings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['planning']):
?>
		<table class="table table-bordered table-striped" style="margin-bottom:10px;width:auto;">
			<tbody>
				<tr>
					<td rowspan="3" style="min-width:40px;">
						<aside id="id_affiche_<?php echo $this->_tpl_vars['planning']['id']; ?>
" class="modal_seb" style="text-align:center;">
							<div>
								<?php if ($this->_tpl_vars['planning']['extension'] != 'swf'): ?>
								<img src="<?php echo $this->_tpl_vars['planning']['image']; ?>
" style="height:100%;"/>
								<?php else: ?>
								<object type="application/x-shockwave-flash" data="<?php echo $this->_tpl_vars['planning']['image']; ?>
" height="100%">
									<param name="movie" value="<?php echo $this->_tpl_vars['planning']['image']; ?>
" />
									<param name="wmode" value="transparent" />
									<p>swf non affichable</p>
								</object>
								<?php endif; ?>
								<a href="#close" title="Close"></a>
							</div>
						</aside>
						<?php if ($this->_tpl_vars['planning']['extension'] != 'swf'): ?>
						<img src="<?php echo $this->_tpl_vars['planning']['image']; ?>
" style="width:40px;" />
						<?php else: ?>
						<object type="application/x-shockwave-flash" data="<?php echo $this->_tpl_vars['planning']['image']; ?>
" width="40" height="143">
							<param name="movie" value="<?php echo $this->_tpl_vars['planning']['image']; ?>
" />
							<param name="wmode" value="transparent" />
							<p>swf non affichable</p>
						</object>
						<?php endif; ?>
					</td>
					<?php if ($this->_tpl_vars['planning']['periodicite'] != 'Tous les jours' || $this->_tpl_vars['planning']['horaire'] != 'Toute la journée'): ?>
					<th colspan="6" style="vertical-align:middle;text-align:center;">
					<?php else: ?>
					<th colspan="4" style="vertical-align:middle;text-align:center;">
					<?php endif; ?>
						<strong>Planning de programmation</strong>
					</th>
				</tr>
				<tr>
					<th style="vertical-align:middle;text-align:center;">Date de début</th>
					<th style="vertical-align:middle;text-align:center;">Date de fin</th>
					<?php if ($this->_tpl_vars['planning']['periodicite'] != 'Tous les jours' || $this->_tpl_vars['planning']['horaire'] != 'Toute la journée'): ?>
					<th style="vertical-align:middle;text-align:center;">Jours</th>
					<th style="vertical-align:middle;text-align:center;">Horaires</th>
					<?php endif; ?>
					<th style="vertical-align:middle;text-align:center;">Statut</th>
					<th style="vertical-align:middle;text-align:center;">Actions</th>
				</tr>
				<tr>
					<td rowspan="2"><?php echo $this->_tpl_vars['planning']['date_debut']; ?>
</td>
					<td rowspan="2"><?php echo $this->_tpl_vars['planning']['date_fin']; ?>
</td>
					<?php if ($this->_tpl_vars['planning']['periodicite'] != 'Tous les jours' || $this->_tpl_vars['planning']['horaire'] != 'Toute la journée'): ?>
					<td rowspan="2"><?php echo $this->_tpl_vars['planning']['periodicite']; ?>
</td>
					<td rowspan="2"><?php echo $this->_tpl_vars['planning']['horaire']; ?>
</td>
					<?php endif; ?>
					<td rowspan="2"><?php if ($this->_tpl_vars['planning']['desactivation'] == 'N'): ?>activ&eacute;<?php else: ?>non activ&eacute;<?php endif; ?></td>
					<td rowspan="2" class="center" style="padding-right:0;">
						<a class="btn btn-info" href="index.php?page=planning&action=modifier&planning=<?php echo $this->_tpl_vars['planning']['id']; ?>
&<?php echo $this->_tpl_vars['selection']; ?>
" style="display:inline-block;margin-bottom:5px;margin-right:5px;">
							<i class="icon-edit icon-white"></i>  
							Modifier
						</a>
						<?php if ($this->_tpl_vars['planning']['desactivation'] == 'N'): ?>
						<a class="btn btn-warning" href="index.php?page=emplacement&action=desactiver_planning&planning=<?php echo $this->_tpl_vars['planning']['id']; ?>
&<?php echo $this->_tpl_vars['selection']; ?>
" style="display:inline-block;margin-bottom:5px;margin-right:5px;" onclick="return confirm('Voulez-vous vraiment désactiver ce planning ?');">
							<i class="icon-ban-circle icon-white"></i>  
							Désactiver
						</a>
						<?php else: ?>
						<a class="btn btn-success" href="index.php?page=emplacement&action=activer_planning&planning=<?php echo $this->_tpl_vars['planning']['id']; ?>
&<?php echo $this->_tpl_vars['selection']; ?>
" style="display:inline-block;margin-bottom:5px;margin-right:5px;" onclick="return confirm('Voulez-vous vraiment activer ce planning ?');">
							<i class="icon-ok icon-white"></i>  
							Activer
						</a>
						<?php endif; ?>
						<a class="btn btn-danger" href="index.php?page=emplacement&action=supprimer_planning&planning=<?php echo $this->_tpl_vars['planning']['id']; ?>
&<?php echo $this->_tpl_vars['selection']; ?>
" style="display:inline-block;margin-bottom:5px;margin-right:5px;" onclick="return confirm('Voulez-vous vraiment supprimer ce planning ?');">
							<i class="icon-remove icon-white"></i>  
							Supprimer
						</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="#id_affiche_<?php echo $this->_tpl_vars['planning']['id']; ?>
">
							<span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
						</a>
					</td>
				</tr>
				</tbody>
		</table>
		<?php endforeach; endif; unset($_from); ?>
	<?php else: ?>
	<div class="span12 well" style="padding:10px;margin-bottom:10px;margin-left:0;width:auto;display:block;float:none;">
		<p style="margin-bottom:0;">Aucune programmation spécifique sur la FACE <?php echo $this->_tpl_vars['numero_de_face']; ?>
 : affiche tampon mise en ligne</p>
	</div>
	<?php endif; ?>
	<div class="span12 well" style="padding:10px;margin:0 0 10px;width:auto;display:inline-block;float:none;">
		<a href="index.php?page=planning&action=creer&emplacement=<?php echo $this->_tpl_vars['id']; ?>
">
			<button class="btn btn-large btn-success" style="padding:2px;">
				<span title=".icon32  .icon-white  .icon-square-plus " class="icon32 icon-white icon-square-plus" style="vertical-align:middle;"></span>
				Programmer un autre visuel
			</button>
		</a>
	</div>
</div>
<div style="clear:both;"></div>