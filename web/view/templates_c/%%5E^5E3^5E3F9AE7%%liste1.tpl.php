<?php /* Smarty version 2.6.9, created on 2018-03-21 19:49:31
         compiled from liste1.tpl */ ?>
<div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
	<div class="box-header well" data-original-title style="padding:5px;height:auto;cursor:auto;">
		<?php if (isset ( $this->_tpl_vars['bouton_retour_url'] )): ?>
		<div class="btn-group pull-right" style="margin-left:10px;">
			<a class="btn dropdown-toggle" href="<?php echo $this->_tpl_vars['bouton_retour_url']; ?>
">
				<i class="icon-arrow-left"></i>
				<span class="hidden-phone"><?php echo $this->_tpl_vars['bouton_retour_libelle']; ?>
</span>
			</a>
		</div>
		<?php endif; ?>
		<?php if (isset ( $this->_tpl_vars['class_icone'] )): ?>
		<i class="<?php echo $this->_tpl_vars['class_icone']; ?>
" style="vertical-align:middle;"></i>
		<?php endif; ?>
		<span style="vertical-align:middle;"><?php echo $this->_tpl_vars['titre']; ?>
</span>
		<div style="clear:both;"></div>
	</div>
	<div class="box-content" style="padding-bottom:0;display:inline-block;">
		<?php echo $this->_tpl_vars['content']; ?>

	</div>
</div>