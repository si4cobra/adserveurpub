<?php /* Smarty version 2.6.9, created on 2018-03-23 10:37:20
         compiled from modifier_planning.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'implode', 'modifier_planning.tpl', 38, false),array('modifier', 'count_characters', 'modifier_planning.tpl', 64, false),array('modifier', 'match', 'modifier_planning.tpl', 84, false),)), $this); ?>
<form action="<?php echo $this->_tpl_vars['formulaire_url']; ?>
" method="POST" class="form-horizontal" style="margin-bottom:0;" enctype="multipart/form-data">
	<fieldset style="text-align:center;">
		<div style="text-align:left;">
			<div style="display:inline-block;vertical-align:top;width:80px;min-width:80px;margin-right:20px;text-align:center;">
				<legend style="margin-bottom:10px;line-height:initial;font-size:16px;">Visuel actuel</legend>
				<div style="margin:0;">
					<?php if ($this->_tpl_vars['formulaire']['extension'] != 'swf'): ?>
					<img src="<?php echo $this->_tpl_vars['formulaire']['image']; ?>
" style="width:80px;"/>
					<?php else: ?>
					<object type="application/x-shockwave-flash" data="<?php echo $this->_tpl_vars['formulaire']['image']; ?>
" width="80" height="285">
						<param name="movie" value="<?php echo $this->_tpl_vars['formulaire']['image']; ?>
" />
						<param name="wmode" value="transparent" />
						<p>swf non affichable</p>
					</object>
					<?php endif; ?>
				</div>
			</div>
			<div style="display:inline-block;">
				<div style="vertical-align:top;">
					<legend style="margin-bottom:10px;">Modifier le visuel:</legend>
					<div style="margin:10px 20px 30px 0">
						<div style="display:inline-block;vertical-align:top;">
							<div class="controls" style="margin-left:10px;">
								<div>
									<input type="radio" name="oui_modification_affiche" id="non_modification_affiche" value="0" <?php if (! isset ( $this->_tpl_vars['formulaire']['oui_modification_affiche'] ) || $this->_tpl_vars['formulaire']['oui_modification_affiche'] != '1'): ?>checked<?php endif; ?>>
									<label style="display:inline;">Non</label>
								</div>
								<div>
									<input type="radio" name="oui_modification_affiche" id="oui_modification_affiche" value="1" <?php if (isset ( $this->_tpl_vars['formulaire']['oui_modification_affiche'] ) && $this->_tpl_vars['formulaire']['oui_modification_affiche'] == '1'): ?>checked<?php endif; ?>>
									<label style="display:inline;">Oui</label>
								</div>
							</div>
						</div>
						<div id="formulaire_nouvelle_affiche" style="display:<?php if (isset ( $this->_tpl_vars['formulaire']['oui_modification_affiche'] ) && $this->_tpl_vars['formulaire']['oui_modification_affiche'] == '1'): ?>inline-block<?php else: ?>none<?php endif; ?>;">
							<label class="control-label" for="fichier_ecran" style="width:auto;margin-left:10px;">Choisir un autre visuel:</label>
							<div class="controls" style="margin-left:10px;">
								<input class="input-file uniform_on" id="fichier_ecran" name="fichier_ecran" type="file" size="25" style="opacity:0;">
								<p>format fichier autorisé (<?php echo ((is_array($_tmp=', ')) ? $this->_run_mod_handler('implode', true, $_tmp, $this->_tpl_vars['tableau_planning_type_mime_autorise']) : implode($_tmp, $this->_tpl_vars['tableau_planning_type_mime_autorise'])); ?>
), dimensions autorisées (<?php echo $this->_tpl_vars['longueur_autorisee_image_pub']; ?>
*<?php echo $this->_tpl_vars['hauteur_autorisee_image_pub']; ?>
), poids fichier autorisé (<?php echo $this->_tpl_vars['taille_maximum_image_pub']; ?>
 Ko)</p>
							</div>
						</div>
					</div>
				</div>
				<div style="width:auto;">
					<legend style="margin-bottom:10px;line-height:initial;">Modifier sa diffusion:</legend>
					<div style="width:auto;text-align:left;margin-left:30px;">
						<legend style="margin-bottom:10px;line-height:initial;">Période d'affichage</legend>
						<div style="margin:10px 0;">
							<div style="display:inline-block;">
								<label class="control-label" for="date_debut" style="width:auto;margin-left:10px;">Date de début:</label>
								<div class="controls" style="margin-left:105px;">
									<input type="text" class="input-xlarge datepicker_date" id="date_debut" name="date_debut" style="margin-left:5px;width:80px;" value="<?php echo $this->_tpl_vars['formulaire']['date_debut']; ?>
">
								</div>
							</div>
							<div style="display:inline-block;">
								<label class="control-label" for="date_fin" style="width:auto;margin-left:10px;">Date de fin:</label>
								<div class="controls" style="margin-left:85px;">
									<input type="text" class="input-xlarge datepicker_date" id="date_fin" name="date_fin" style="margin-left:5px;width:80px;" value="<?php echo $this->_tpl_vars['formulaire']['date_fin']; ?>
">
								</div>
							</div>
						</div>
						<a class="btn btn-warning" id="bouton_mode_expert_planning">
							<i class="icon-edit icon-white"></i>
							<span id="label_bouton_mode_expert_planning">
								<?php if (( ((is_array($_tmp=$this->_tpl_vars['formulaire']['periodicite'])) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) == 7 || ((is_array($_tmp=$this->_tpl_vars['formulaire']['periodicite'])) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) == 0 ) && ( ( $this->_tpl_vars['formulaire']['heure_debut'] == '' && $this->_tpl_vars['formulaire']['heure_fin'] == '' ) || ( $this->_tpl_vars['formulaire']['heure_debut'] == '00' && $this->_tpl_vars['formulaire']['heure_fin'] == '23:59:59' ) )): ?>
								Affiner la programmation (mode expert)
								<?php else: ?>
								Masquer le mode expert
								<?php endif; ?>
							</span>
						</a>
						<div id="mode_expert_planning" <?php if (( ((is_array($_tmp=$this->_tpl_vars['formulaire']['periodicite'])) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) == 7 || ((is_array($_tmp=$this->_tpl_vars['formulaire']['periodicite'])) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) == 0 ) && ( ( $this->_tpl_vars['formulaire']['heure_debut'] == '' && $this->_tpl_vars['formulaire']['heure_fin'] == '' ) || ( $this->_tpl_vars['formulaire']['heure_debut'] == '00' && $this->_tpl_vars['formulaire']['heure_fin'] == '23:59:59' ) )): ?>style="display:none;"<?php endif; ?>>
							<legend style="margin-bottom:10px;line-height:initial;">Jours d'affichage</legend>
							<div style="margin:10px 0">
								<div style="margin:10px;display:inline;">
									<div style="display:inline-block;margin-right:20px;">
										<input type="checkbox" name="periodicite" <?php if (((is_array($_tmp=$this->_tpl_vars['formulaire']['periodicite'])) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) == 7 || ((is_array($_tmp=$this->_tpl_vars['formulaire']['periodicite'])) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) == 0): ?>checked<?php endif; ?>>
										<label style="display:inline;">
											Tous les jours
										</label>
									</div>
									<div style="display:inline-block;">
										<?php $_from = $this->_tpl_vars['tableau_correspondance_indice_jour']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['indice'] => $this->_tpl_vars['intitule']):
?>
										<div style="display:inline-block;margin-left:10px;">
											<input type="checkbox" name="periodicite_jour[]" value="<?php echo $this->_tpl_vars['indice']; ?>
" <?php if (((is_array($_tmp=$this->_tpl_vars['formulaire']['periodicite'])) ? $this->_run_mod_handler('match', true, $_tmp, $this->_tpl_vars['indice']) : smarty_modifier_match($_tmp, $this->_tpl_vars['indice'])) || ((is_array($_tmp=$this->_tpl_vars['formulaire']['periodicite'])) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) == 0): ?>checked<?php endif; ?>>
											<label style="display:inline;margin-left:3px;">
												<?php echo $this->_tpl_vars['intitule']; ?>

											</label>
										</div>
										<?php endforeach; endif; unset($_from); ?>
									</div>
								</div>
							</div>
							<legend style="margin-bottom:10px;line-height:initial;">Horaire d'affichage</legend>
							<div style="margin:10px 0;">
								<div style="display:inline-block;vertical-align:top;margin-bottom:10px;margin-right:20px;">
									<label style="display:inline;">Toute la journée:</label>
									<input type="checkbox" id="toute_la_journee" value="1" <?php if ($this->_tpl_vars['formulaire']['heure_debut'] == '00' && $this->_tpl_vars['formulaire']['heure_fin'] == '23:59:59'): ?>checked<?php endif; ?>>
								</div>
								<div style="display:inline-block;">
									<div style="display:inline-block;margin-right:10px;">
										<label class="control-label" for="heure_debut" style="width:auto;">Heure de début:</label>
										<div class="controls" style="margin-left:105px;">
											<input type="text" class="input-xlarge datepicker_hour" id="heure_debut" name="heure_debut" style="margin-left:5px;width:80px;"
												value="<?php if ($this->_tpl_vars['formulaire']['heure_debut'] != '' || $this->_tpl_vars['formulaire']['heure_fin'] != ''):  echo $this->_tpl_vars['formulaire']['heure_debut'];  else: ?>00<?php endif; ?>">
										</div>
									</div>
									<div style="display:inline-block;">
										<label class="control-label" for="heure_fin" style="width:auto;">Heure de fin:</label>
										<div class="controls" style="margin-left:85px;">
											<input type="text" class="input-xlarge datepicker_hour" id="heure_fin" name="heure_fin" style="margin-left:5px;width:80px;"
										value="<?php if ($this->_tpl_vars['formulaire']['heure_debut'] != '' || $this->_tpl_vars['formulaire']['heure_fin'] != ''):  echo $this->_tpl_vars['formulaire']['heure_fin'];  else: ?>23:59:59<?php endif; ?>">
										</div>
									</div>
								</div>
							</div>
							<legend style="margin-bottom:10px;line-height:initial;">Activation du planning</legend>
							<div style="margin:10px 0">
								<label class="control-label" for="desactivation" style="width:auto;margin-left:10px;">Activer:</label>
								<div class="controls" style="margin-left:70px;">
									<input type="checkbox" name="desactivation" value="1" <?php if ($this->_tpl_vars['formulaire']['desactivation'] == 'N'): ?>checked<?php endif; ?>>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-actions" style="display:inline-block;padding:10px">
			<button type="submit" class="btn btn-primary" name="valider_edition_planning" value="1">Valider</button>
			<a class="btn dropdown-toggle" href="<?php echo $this->_tpl_vars['bouton_annuler_url']; ?>
">
				Annuler
			</a>
		</div>
	</fieldset>
</form>