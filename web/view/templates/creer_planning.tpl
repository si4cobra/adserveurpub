<form action="{$formulaire_url}" method="POST" class="form-horizontal" style="margin-bottom:0;" enctype="multipart/form-data">
	<fieldset style="text-align:center;">
		<div style="vertical-align:top;text-align:left;">
			<legend style="margin-bottom:10px;">Sélectionner un visuel:</legend>
			<div style="margin:10px 20px 30px 0">
				<label class="control-label" for="fichier_ecran" style="width:auto;margin-left:10px;">Visuel:</label>
				<div class="controls" style="margin-left:10px;">
					<input class="input-file uniform_on" id="fichier_ecran" name="fichier_ecran" type="file" size="25" style="opacity:0;">
					<p>format fichier autorisé ({', '|implode:$tableau_planning_type_mime_autorise}), dimensions autorisées ({$longueur_autorisee_image_pub}*{$hauteur_autorisee_image_pub}), poids fichier autorisé ({$taille_maximum_image_pub} Ko)</p>
				</div>
			</div>
		</div>
		<div style="width:auto;text-align:left;">
			<legend style="margin-bottom:10px;line-height:initial;">Plannifier sa diffusion:</legend>
			<div style="width:auto;text-align:left;margin-left:30px;">
				<legend style="margin-bottom:10px;line-height:initial;">Période d'affichage</legend>
				<div style="margin:10px 0;">
					<div style="display:inline-block;">
						<label class="control-label" for="date_debut" style="width:auto;margin-left:10px;">Date de début:</label>
						<div class="controls" style="margin-left:105px;">
							<input type="text" class="input-xlarge datepicker_date" id="date_debut" name="date_debut" style="margin-left:5px;width:80px;" value="{$formulaire.date_debut}">
						</div>
					</div>
					<div style="display:inline-block;">
						<label class="control-label" for="date_fin" style="width:auto;margin-left:10px;">Date de fin:</label>
						<div class="controls" style="margin-left:85px;">
							<input type="text" class="input-xlarge datepicker_date" id="date_fin" name="date_fin" style="margin-left:5px;width:80px;" value="{$formulaire.date_fin}">
						</div>
					</div>
				</div>
				<a class="btn btn-warning" id="bouton_mode_expert_planning">
					<i class="icon-edit icon-white"></i>
					<span id="label_bouton_mode_expert_planning">
						{if ($formulaire.periodicite|count_characters eq 7 or $formulaire.periodicite|count_characters eq 0) and (($formulaire.heure_debut eq '' and $formulaire.heure_fin eq '') or ($formulaire.heure_debut eq '00' and $formulaire.heure_fin eq '23:59:59'))}
						Affiner la programmation (mode expert)
						{else}
						Masquer le mode expert
						{/if}
					</span>
				</a>
				<div id="mode_expert_planning" {if ($formulaire.periodicite|count_characters eq 7 or $formulaire.periodicite|count_characters eq 0) and (($formulaire.heure_debut eq '' and $formulaire.heure_fin eq '') or ($formulaire.heure_debut eq '00' and $formulaire.heure_fin eq '23:59:59'))}style="display:none;"{/if}>
					<legend style="margin-bottom:10px;line-height:initial;">Jours d'affichage</legend>
					<div style="margin:10px 0">
						<div style="margin:10px;display:inline;">
							<div style="display:inline-block;margin-right:20px;">
								<input type="checkbox" name="periodicite" {if $formulaire.periodicite|count_characters eq 7 or $formulaire.periodicite|count_characters eq 0}checked{/if}>
								<label style="display:inline;">
									Tous les jours
								</label>
							</div>
							<div style="display:inline-block;">
								{foreach from=$tableau_correspondance_indice_jour key=indice item=intitule}
								<div style="display:inline-block;margin-left:10px;">
									<input type="checkbox" name="periodicite_jour[]" value="{$indice}" {if $formulaire.periodicite|match:$indice or $formulaire.periodicite|count_characters eq 0}checked{/if}>
									<label style="display:inline;margin-left:3px;">
										{$intitule}
									</label>
								</div>
								{/foreach}
							</div>
						</div>
					</div>
					<legend style="margin-bottom:10px;line-height:initial;">Horaire d'affichage</legend>
					<div style="margin:10px 0;">
						<div style="display:inline-block;vertical-align:top;margin-bottom:10px;margin-right:20px;">
							<label style="display:inline;">Toute la journée:</label>
							<input type="checkbox" id="toute_la_journee" value="1" checked>
						</div>
						<div style="display:inline-block;">
							<div style="display:inline-block;margin-right:10px;">
								<label class="control-label" for="heure_debut" style="width:auto;">Heure de début:</label>
								<div class="controls" style="margin-left:105px;">
									<input type="text" class="input-xlarge datepicker_hour" id="heure_debut" name="heure_debut" style="margin-left:5px;width:80px;"
										value="{if $formulaire.heure_debut neq '' or $formulaire.heure_fin neq ''}{$formulaire.heure_debut}{else}00{/if}">
								</div>
							</div>
							<div style="display:inline-block;">
								<label class="control-label" for="heure_fin" style="width:auto;">Heure de fin:</label>
								<div class="controls" style="margin-left:85px;">
									<input type="text" class="input-xlarge datepicker_hour" id="heure_fin" name="heure_fin" style="margin-left:5px;width:80px;"
										value="{if $formulaire.heure_debut neq '' or $formulaire.heure_fin neq ''}{$formulaire.heure_fin}{else}23:59:59{/if}">
								</div>
							</div>
						</div>
					</div>
					<legend style="margin-bottom:10px;line-height:initial;">Activation du planning</legend>
					<div style="margin:10px 0">
						<label class="control-label" for="desactivation" style="width:auto;margin-left:10px;">Activer:</label>
						<div class="controls" style="margin-left:70px;">
							<input type="checkbox" name="desactivation" value="1" {if $formulaire.desactivation eq 'N'}checked{/if}>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-actions" style="display:inline-block;padding:10px">
			<button type="submit" class="btn btn-primary" name="valider_edition_planning" value="1">Valider</button>
			<a class="btn dropdown-toggle" href="{$bouton_annuler_url}">
				Annuler
			</a>
		</div>
	</fieldset>
</form>