<head>
	<title>{$titre_site}</title>
	<meta charset="utf-8" />
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	{literal}
	<style type="text/css">
		body {
			padding-bottom: 40px;
		}
		.sidebar-nav {
			padding: 9px 0;
		}
	</style>
	{/literal}
	<!-- <link href="view/css/adserver.css" rel="stylesheet"> -->
	<link id="bs-css" href="view/css/bootstrap-cerulean.min.css" rel="stylesheet">
	<link href="view/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link href="view/css/charisma-app.min.css" rel="stylesheet">
	<link href='view/css/chosen.min.css' rel='stylesheet'>
	<link href='view/css/colorbox.min.css' rel='stylesheet'>
	<link href='view/css/elfinder.min.css' rel='stylesheet'>
	<link href='view/css/elfinder.theme.min.css' rel='stylesheet'>
	<link href='view/css/fullcalendar.min.css' rel='stylesheet'>
	<link href='view/css/fullcalendar.print.min.css' rel='stylesheet'  media='print'>
	<link href="view/css/jquery-ui-1.8.21.custom.min.css" rel="stylesheet">
	<link href='view/css/jquery.cleditor.min.css' rel='stylesheet'>
	<link href='view/css/jquery.iphone.toggle.min.css' rel='stylesheet'>
	<link href='view/css/jquery.noty.min.css' rel='stylesheet'>
	<link href='view/css/jquery.ui.timepicker.min.css' rel='stylesheet'>
	<link href='view/css/noty_theme_default.min.css' rel='stylesheet'>
	<link href='view/css/opa-icons.min.css' rel='stylesheet'>
	<link href='view/css/uniform.default.min.css' rel='stylesheet'>
	<link href='view/css/uploadify.min.css' rel='stylesheet'>
	<link href='view/css/experiment.css' rel='stylesheet'>
</head>