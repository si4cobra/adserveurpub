<div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
	<div class="box-header well" data-original-title style="padding:5px;height:auto;cursor:auto;">
		{if isset($bouton_retour_url)}
		<div class="btn-group pull-right" style="margin-left:10px;">
			<a class="btn dropdown-toggle" href="{$bouton_retour_url}">
				<i class="icon-arrow-left"></i>
				<span class="hidden-phone">{$bouton_retour_libelle}</span>
			</a>
		</div>
		{/if}
		{if isset($class_icone)}
		<i class="{$class_icone}" style="vertical-align:middle;"></i>
		{/if}
		<span style="vertical-align:middle;">{$titre}</span>
		<div style="clear:both;"></div>
	</div>
	<div class="box-content" style="padding-bottom:0;display:inline-block;">
		{$content}
	</div>
</div>