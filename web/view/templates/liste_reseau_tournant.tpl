{if $liste_reseau_tournant|@count gt 0}
<table class="table table-bordered table-striped" style="width:auto;margin-bottom:8px;">
	<thead>
		<tr>
			<th>Réseau tournant</th>
			<!--<th>Action</th>-->
		</tr>
	</thead>
	{foreach from=$liste_reseau_tournant item=reseau_tournant}
	<tr>
		<td>{$reseau_tournant.nom}</td>

	</tr>
	{/foreach}
</table>
	<table class="table table-bordered table-striped " style="width:auto;margin-bottom:8px;">
		<tr>
			<td style="padding-bottom:0;">
				<a class="btn btn-info" href="index.php?page=selection&reseau_tournant={$reseau_tournant.id}" style="display:block;margin-bottom:8px;">
					<i class="icon-list icon-white"></i>
					Visualiser la liste des faces sur les réseaux tournants
				</a>
			</td>
		</tr>

	</table>
{/if}