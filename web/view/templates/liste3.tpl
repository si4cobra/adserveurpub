<div style="width:auto;margin-bottom:0;display:inline-block;vertical-align:top;">
	{if $plannings|@count gt 0}
		<div class="span12 well" style="padding:10px;margin-bottom:10px;width:auto;display:inline-block;float:none;">
		Toutes les programmations de visuels pour la FACE {$numero_de_face}
		</div>
		{foreach from=$plannings item=planning}
		<table class="table table-bordered table-striped" style="margin-bottom:10px;width:auto;">
			<tbody>
				<tr>
					<td rowspan="3" style="min-width:40px;">
						<aside id="id_affiche_{$planning.id}" class="modal_seb" style="text-align:center;">
							<div>
								{if $planning.extension neq 'swf'}
								<img src="{$planning.image}" style="height:100%;"/>
								{else}
								<object type="application/x-shockwave-flash" data="{$planning.image}" height="100%">
									<param name="movie" value="{$planning.image}" />
									<param name="wmode" value="transparent" />
									<p>swf non affichable</p>
								</object>
								{/if}
								<a href="#close" title="Close"></a>
							</div>
						</aside>
						{if $planning.extension neq 'swf'}
						<img src="{$planning.image}" style="width:40px;" />
						{else}
						<object type="application/x-shockwave-flash" data="{$planning.image}" width="40" height="143">
							<param name="movie" value="{$planning.image}" />
							<param name="wmode" value="transparent" />
							<p>swf non affichable</p>
						</object>
						{/if}
					</td>
					{if $planning.periodicite neq 'Tous les jours' or $planning.horaire neq 'Toute la journée'}
					<th colspan="6" style="vertical-align:middle;text-align:center;">
					{else}
					<th colspan="4" style="vertical-align:middle;text-align:center;">
					{/if}
						<strong>Planning de programmation</strong>
					</th>
				</tr>
				<tr>
					<th style="vertical-align:middle;text-align:center;">Date de début</th>
					<th style="vertical-align:middle;text-align:center;">Date de fin</th>
					{if $planning.periodicite neq 'Tous les jours' or $planning.horaire neq 'Toute la journée'}
					<th style="vertical-align:middle;text-align:center;">Jours</th>
					<th style="vertical-align:middle;text-align:center;">Horaires</th>
					{/if}
					<th style="vertical-align:middle;text-align:center;">Statut</th>
					<th style="vertical-align:middle;text-align:center;">Actions</th>
				</tr>
				<tr>
					<td rowspan="2">{$planning.date_debut}</td>
					<td rowspan="2">{$planning.date_fin}</td>
					{if $planning.periodicite neq 'Tous les jours' or $planning.horaire neq 'Toute la journée'}
					<td rowspan="2">{$planning.periodicite}</td>
					<td rowspan="2">{$planning.horaire}</td>
					{/if}
					<td rowspan="2">{if $planning.desactivation eq 'N'}activ&eacute;{else}non activ&eacute;{/if}</td>
					<td rowspan="2" class="center" style="padding-right:0;">
						<a class="btn btn-info" href="index.php?page=planning&action=modifier&planning={$planning.id}&{$selection}" style="display:inline-block;margin-bottom:5px;margin-right:5px;">
							<i class="icon-edit icon-white"></i>  
							Modifier
						</a>
						{if $planning.desactivation eq 'N'}
						<a class="btn btn-warning" href="index.php?page=emplacement&action=desactiver_planning&planning={$planning.id}&{$selection}" style="display:inline-block;margin-bottom:5px;margin-right:5px;" onclick="return confirm('Voulez-vous vraiment désactiver ce planning ?');">
							<i class="icon-ban-circle icon-white"></i>  
							Désactiver
						</a>
						{else}
						<a class="btn btn-success" href="index.php?page=emplacement&action=activer_planning&planning={$planning.id}&{$selection}" style="display:inline-block;margin-bottom:5px;margin-right:5px;" onclick="return confirm('Voulez-vous vraiment activer ce planning ?');">
							<i class="icon-ok icon-white"></i>  
							Activer
						</a>
						{/if}
						<a class="btn btn-danger" href="index.php?page=emplacement&action=supprimer_planning&planning={$planning.id}&{$selection}" style="display:inline-block;margin-bottom:5px;margin-right:5px;" onclick="return confirm('Voulez-vous vraiment supprimer ce planning ?');">
							<i class="icon-remove icon-white"></i>  
							Supprimer
						</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="#id_affiche_{$planning.id}">
							<span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
						</a>
					</td>
				</tr>
				</tbody>
		</table>
		{/foreach}
	{else}
	<div class="span12 well" style="padding:10px;margin-bottom:10px;margin-left:0;width:auto;display:block;float:none;">
		<p style="margin-bottom:0;">Aucune programmation spécifique sur la FACE {$numero_de_face} : affiche tampon mise en ligne</p>
	</div>
	{/if}
	<div class="span12 well" style="padding:10px;margin:0 0 10px;width:auto;display:inline-block;float:none;">
		<a href="index.php?page=planning&action=creer&emplacement={$id}">
			<button class="btn btn-large btn-success" style="padding:2px;">
				<span title=".icon32  .icon-white  .icon-square-plus " class="icon32 icon-white icon-square-plus" style="vertical-align:middle;"></span>
				Programmer un autre visuel
			</button>
		</a>
	</div>
</div>
<div style="clear:both;"></div>