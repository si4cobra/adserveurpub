{$header}
<div class="container-fluid" style="padding:0 20px;">
	<div class="row-fluid" style="padding:0;">
		<div id="content" class="span10" style="width:auto;">
			<div class="row-fluid sortable ui-sortable" style="display:inline-block;width:auto;">
				{$content}
			</div>
		</div>
	</div>
</div>