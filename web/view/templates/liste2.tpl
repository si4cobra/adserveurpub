<div class="span12 well" style="padding:10px;margin:0 0 10px 0;width:auto;display:block;float:none;">
	<h3>FACE {$numero_de_face}</h3>
	<table>
		<tr>
			<td style="vertical-align:top;padding-bottom:20px;">
				<i class="icon-stop"></i>
			</td>
			<td style="vertical-align:top;padding-bottom:20px;">
				<p style="margin:0;padding:0;font-size:16px;">Affiche tampon*</p>
			</td>
			<aside id="id_affiche_tampon_{$affiche_tampon.id}" class="modal_seb" style="text-align:center;">
				<div>
					{if $affiche_tampon.extension neq 'swf'}
					<img alt="" src="{$affiche_tampon.image}" style="height:100%;">
					{else}
					<object type="application/x-shockwave-flash" data="{$affiche_tampon.image}" height="100%">
						<param name="movie" value="{$affiche_tampon.image}" />
						<param name="wmode" value="transparent" />
						<p>swf non affichable</p>
					</object>
					{/if}
					<a href="#close" title="Close"></a>
				</div>
			</aside>
			<td style="width:40px;min-width:40px;padding-bottom:20px;">
				{if $affiche_tampon.extension neq 'swf'}
				<img src="{$affiche_tampon.image}" style="width:40px;"/>
				{else}
				<object type="application/x-shockwave-flash" data="{$affiche_tampon.image}" width="40" height="143">
					<param name="movie" value="{$affiche_tampon.image}" />
					<param name="wmode" value="transparent" />
					<p>swf non affichable</p>
				</object>
				{/if}
			</td>
			<td style="padding-bottom:20px;">
				<a href="#id_affiche_tampon_{$affiche_tampon.id}">
					<span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
				</a>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top;padding-bottom:20px;">
				<i class="icon-stop"></i>
			</td>
			<td style="vertical-align:top;padding-bottom:20px;">
				<p style="margin:0 0 10px;padding:0;font-size:16px;">Visuel actuellement diffusé</p>
			</td>
			<td style="width:40px;min-width:40px;padding-bottom:20px;">
				{if isset($planning_actuel)}
				<aside id="id_affiche_actuel_{$planning_actuel.id}" class="modal_seb" style="text-align:center;">
					<div>
						{if $planning_actuel.extension neq 'swf'}
						<img src="{$planning_actuel.image}" style="height:100%;"/>
						{else}
						<object type="application/x-shockwave-flash" data="{$planning_actuel.image}" height="100%">
							<param name="movie" value="{$planning_actuel.image}" />
							<param name="wmode" value="transparent" />
							<p>swf non affichable</p>
						</object>
						{/if}
						<a href="#close" title="Close"></a>
					</div>
				</aside>
				{if $planning_actuel.extension neq 'swf'}
				<img src="{$planning_actuel.image}" style="width:40px;"/>
				{else}
				<object type="application/x-shockwave-flash" data="{$planning_actuel.image}" width="40" height="143">
					<param name="movie" value="{$planning_actuel.image}" />
					<param name="wmode" value="transparent" />
					<p>swf non affichable</p>
				</object>
				{/if}
				{else}
				<aside id="id_affiche_tampon_actuel_{$affiche_tampon.id}" class="modal_seb" style="text-align:center;">
					<div>
						{if $affiche_tampon.extension neq 'swf'}
						<img alt="" src="{$affiche_tampon.image}" style="height:100%;">
						{else}
						<object type="application/x-shockwave-flash" data="{$affiche_tampon.image}" height="100%">
							<param name="movie" value="{$affiche_tampon.image}" />
							<param name="wmode" value="transparent" />
							<p>swf non affichable</p>
						</object>
						{/if}
						<a href="#close" title="Close"></a>
					</div>
				</aside>
				{if $affiche_tampon.extension neq 'swf'}
				<img src="{$affiche_tampon.image}" style="width:40px;"/>
				{else}
				<object type="application/x-shockwave-flash" data="{$affiche_tampon.image}" width="40" height="143">
					<param name="movie" value="{$affiche_tampon.image}" />
					<param name="wmode" value="transparent" />
					<p>swf non affichable</p>
				</object>
				{/if}
				{/if}
			</td>
			<td style="padding-bottom:20px;">
				{if isset($planning_actuel)}
				<a href="#id_affiche_actuel_{$planning_actuel.id}">
					<span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
				</a>
				{else}
				<a href="#id_affiche_tampon_actuel_{$affiche_tampon.id}">
					<span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
				</a>
				{/if}
			</td>
		</tr>
		<tr>
			<td colspan="4" style="vertical-align:top;">
				<div>
					<a href="index.php?page=emplacement&emplacement={$id}">
						<button class="btn btn-large btn-info" style="padding:2px;">
							<span title=".icon32  .icon-white  .icon-copy " class="icon32 icon-white icon-copy" style="vertical-align:middle;"></span>
							Voir toutes vos programmations de visuels
						</button>
					</a>
				</div>
				<p style="margin:0 0 10px;padding:0;font-size:12px;">(campagne promotionnelle ponctuelle ou déclinaisons de visuels)</p>
				<div>
					<a href="index.php?page=planning&action=creer&emplacement={$id}&{$selection}">
						<button class="btn btn-large btn-success" style="padding:2px;">
							<span title=".icon32  .icon-white  .icon-square-plus " class="icon32 icon-white icon-square-plus" style="vertical-align:middle;"></span>
							Programmer un autre visuel
						</button>
					</a>
				</div>
			</td>
		</tr>
	</table>
</div>