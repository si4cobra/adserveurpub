<?php

class mod_form_list
{
    private $db;
    private $host;
    private $username;
    private $password;
    private $database;
    protected $sPrefixeDb;

    public function __construct()
    {
        $this->host = class_fli::get_host();
        $this->username = class_fli::get_username();
        $this->password = class_fli::get_password();
        $this->database = class_fli::get_database();
        $this->sPrefixeDb = class_fli::get_prefixe();

        try {
            $this->db = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->database, $this->username,
                $this->password, array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING

                ));

            $req = $this->db->prepare("SET sql_mode = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'");
            $req->execute();

        } catch( PDOException $e ) {
            die('<h1>Impossible de se connecter &agrave; la base de donn&eacute;e ' . $this->database . ' !!</h1>');
        }
    }

    public function tl_query($sql, $data = array())
    {
        $req = $this->db->prepare($sql);
        $req->execute($data);
        return $req->fetchAll(PDO::FETCH_OBJ);
    }

    public function renvoi_info_requete($sql, $data = array())
    {

            //echo $sql;
            $req = $this->db->prepare($sql);
            $req->execute($data);
            return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Renvoi la liste des table de la base de donnée
     * @return array
     */
    public function renvoi_liste_table()
    {   $aTabRetour =array();
        $sql ="SHOW TABLES";
        $data=array();
        $req = $this->db->prepare($sql);
        $req->execute($data);
        $aTmp= $req->fetchAll(PDO::FETCH_ASSOC);

        $sDatabase = class_fli::get_database();

        if(!empty($aTmp)) {
            foreach( $aTmp as $item ) {
                $aTabRetour[] = $item['Tables_in_'.$sDatabase];
            }
        }

        return $aTabRetour;
    }

    public function renvoi_liste_champ_table($sNomTable){

        $sSqlChamp ="SHOW COLUMNS FROM ".$sNomTable;
        $req = $this->db->prepare($sSqlChamp);
        $req->execute();
        $aTmp= $req->fetchAll(PDO::FETCH_ASSOC);

        return $aTmp;
    }


    public function renvoi_nombreLigne_requete($sql, $data = array())
    {
        $req = $this->db->prepare($sql);
        $req->execute($data);
        return $req->rowCount();
    }

    /**
     * @param $sql
     */
    public function tl_exec($sql)
    {
        $req = $this->db->prepare($sql);
        $req->execute();
    }

    /**
     * Methode qui execute une requete en direct
     * @param $sql
     */
    public function execute_requete($sql)
    {
        //echo $sql."<br>";
        try {
            $req = $this->db->prepare($sql);
            return $req->execute();
        }catch(Exception $e){
            $aTabRetour=array();
            $aTabRetour['result']=false;
            $aTabRetour['message']="Probleme survenu ".$e;
            return $aTabRetour;
        }
    }

    /**
     * Methode qui execute une requete en direct
     * @param $sql
     */
    public function executionRequeteId($sql,$bdd="",$action="",$id="",$commentaire="")
    {
        //echo $sql."<br>";
        $req = $this->db->prepare($sql);
        //echo $sql."<br>"
        $bresult = $req->execute();
        // echo $sql."<br>";exit;
        if(preg_match('/\binsert/i',$sql)){
            $id= $this->db->lastInsertId();
            return $id;
        }
        else
            return $bresult;


    }

    public function insert_update_requete($sql,$aTab=array()){
        $req = $this->db->prepare($sql);
        //echo"<pre>";print_r($aTab);echo"</pre>";
        //echo $sql."<br>";
        if(!empty($aTab)) {
            foreach( $aTab as $key => $value ) {
                $req->bindValue(':' . $key, $value);
            }
        }

        try {
            $bresult = $req->execute();
            if( preg_match('/\binsert/i', $sql) ) {
                $id = $this->db->lastInsertId();
                return $id;
            } else
                return $bresult;
        }catch(Exception $e){
                $aTabRetour=array();
                $aTabRetour['result']=false;

                 $sMessageErreur="";
                 
                 $aTableauerror= $req->errorInfo ();
                 if($aTableauerror[0]=="23000"){
                     $aTabRetour['message'] = "<br> Cette entrée est déjà dans la base";
                 }else{
                     $aTabRetour['message'] = "<br> erreur ".$aTableauerror[2];
                 }
                 
                 //echo"<pre>";print_r($aTableauerror);echo"</pre>";

                return $aTabRetour;
            }
    }

}

?>