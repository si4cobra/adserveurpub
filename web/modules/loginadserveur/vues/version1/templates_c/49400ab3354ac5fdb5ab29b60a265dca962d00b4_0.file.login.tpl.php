<?php
/* Smarty version 3.1.29, created on 2018-03-22 16:07:56
  from "/var/www/html/modules/loginadserveur/vues/version1/templates/login.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ab3c6ccc55e08_33252227',
  'file_dependency' => 
  array (
    '49400ab3354ac5fdb5ab29b60a265dca962d00b4' => 
    array (
      0 => '/var/www/html/modules/loginadserveur/vues/version1/templates/login.tpl',
      1 => 1521731260,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ab3c6ccc55e08_33252227 ($_smarty_tpl) {
?>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="row-fluid">
            <div class="span12 center login-header" style="height:auto;margin:30px auto;padding-top:0;">
                <h2 style="font-family:Verdana;"><?php echo $_smarty_tpl->tpl_vars['titre_site']->value;?>
</h2>
            </div>
        </div>
        <div class="row-fluid">
            <div class="well span5 center login-box" style="padding-bottom:0;">
                <div class="alert alert-info">
                    Merci de vous identifier avec votre nom d'utilisateur et mot de passe.
                </div>
                <form class="form-horizontal" action="" method="post" style="margin:0 0 20px;">
                    <fieldset>
                        <div class="input-prepend" title="Nom d'utilisateur" data-rel="tooltip">
                            <span class="add-on"><i class="icon-user"></i></span><input autofocus class="input-large span10" name="login" id="username" type="text" value="" />
                        </div>
                        <div class="clearfix"></div>
                        <div class="input-prepend" title="Mot de passe" data-rel="tooltip">
                            <span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="password" id="password" type="password" value="" />
                        </div>
                        <div class="clearfix"></div>
                        <p class="center span5">
                            <button type="submit" class="btn btn-primary">Se connecter</button>
                        </p>
                    </fieldset>
                </form>
                <?php if ($_smarty_tpl->tpl_vars['message']->value) {?>
                    <div class="alert alert-block" style="margin-bottom:20px;">
                        <?php echo $_smarty_tpl->tpl_vars['message']->value;?>

                    </div>
                <?php }?>
            </div>
        </div>
    </div>
</div><?php }
}
