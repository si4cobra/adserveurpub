<div class="container">

    <form class="form-signin" method="post">
        {if !empty($erreur)}
            <div class="popupAlert alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                Erreur de connexion !<br/>
                {$erreur}
            </div>
        {/if}
        <h2 class="form-signin-heading">FRAMEWORK</h2>
        <input type="hidden" name="validation" value="ok">
        <label for="inputUsername" class="sr-only">Utilisateur</label>
        <input type="text" id="inputUsername" class="form-control" placeholder="Utilisateur" name="login" required
               autofocus>
        <label for="inputPassword" class="sr-only">Mot de passe</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" name="password"
               required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
    </form>

</div>