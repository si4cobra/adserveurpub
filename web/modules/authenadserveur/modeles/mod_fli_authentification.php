<?php

/**
 * Classe de connexion
 */
class mod_fli_authentification extends mod_form_list
{

    public function get_droits($sRoute,$aSession,$sRequeteGroupes)
    {
        $tabDroits = array();

        $aParam[] = $aSession;
        $aParam[] = $sRoute;

        $sReqDroits = "SELECT `ajout_groupe_route`,`modif_groupe_route`,`suppr_groupe_route`,`visu_groupe_route`,`ajout_user_route`,
        `modif_user_route`,`suppr_user_route`,`visu_user_route`,switch_wizard_groupes_routes,switch_wizard_user_routes FROM ".$this->sPrefixeDb."routes
        LEFT JOIN ".$this->sPrefixeDb."groupes_routes ON ".$this->sPrefixeDb."groupes_routes.guid_route = ".$this->sPrefixeDb."routes.guid_route ".$sRequeteGroupes."
        LEFT JOIN ".$this->sPrefixeDb."users_routes ON ".$this->sPrefixeDb."users_routes.guid_route = ".$this->sPrefixeDb."routes.guid_route AND ".$this->sPrefixeDb."users_routes.guid_user=?
        WHERE ".$this->sPrefixeDb."routes.route_route = ? AND `supplogique_groupe_route`='N'
		";

        //echo $sReqDroits."<br>";
        $tabDroits = $this->renvoi_info_requete($sReqDroits, $aParam);
        
       // echo"<pre>";print_r($tabDroits);echo"</pre>";

        return $tabDroits;
    }

    //Détection utilisateur existant ou non
    public function check_user($aSession)
    {
        $aParam[] = $aSession;


        $sRequete ="SELECT  id_client as id_user,accesdirect_client as guid_user,
          'fr' as langue_user,
          'N' as activtraductionmodif_user 
          FROM client WHERE accesdirect_client = ?";

        //echo $sRequete."<br>";
        //echo"<pre>";print_r($aParam);echo"</pre>";

        $aResultUser = $this->renvoi_info_requete($sRequete, $aParam);

        return $aResultUser;
    }

    //Récupération des groupes de l'utilisateur
    public function get_groupes_user($guid_user){
        if(isset($guid_user)) {
            $aParam[] = $guid_user;
        }else{
            return array();
        }

        $sRequeteGroupesUser = "SELECT ".$this->sPrefixeDb."groupes.nom_groupe,".$this->sPrefixeDb."users_groupes.guid_groupe FROM ".$this->sPrefixeDb."users_groupes
                                INNER JOIN ".$this->sPrefixeDb."groupes on ".$this->sPrefixeDb."groupes.guid_groupe=".$this->sPrefixeDb."users_groupes.guid_groupe
                                WHERE supplogique_user_groupe='N' AND guid_user=?";
        return $this->renvoi_info_requete($sRequeteGroupesUser,$aParam);
    }

}