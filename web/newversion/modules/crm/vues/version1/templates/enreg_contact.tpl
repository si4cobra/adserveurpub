{literal}
    <style>
        .select2-results__option[aria-selected] {
            font-size: 12px;
            color: #000;
            line-height: 12px;
        }
    </style>
{/literal}
<div style="color:#313131;margin: 0 auto;padding: 15px;margin-top: 10px;margin: 0 auto;text-align: center;padding-top: 10px;">
    {if $bDebugRequete}
        <p>
            Requete Select : <br><br>
            {$sDebugRequeteSelect}<br><br>
            Requete Insert/Update : <br><br>
            {$sDebugRequeteInsertUpdate}<br><br>
        </p>
    {/if}

    <div id="page-heading">
        <div style="display:inline-block;border:1px solid #D0D1D5;margin-bottom: 20px;">
            <div class="hidden-xs title-head-search"
                 style="display: inline-block;height: 73px;width: 73px;vertical-align: middle;padding: 20px;">
        <span class="glyphicon glyphicon-link" style="color:#fff;vertical-align: bottom;font-size: 20px;top: 6px;">
        </span>
            </div>
            <h1 style="font-size:30px;margin: 0px;display:inline-block;padding: 20px;vertical-align: bottom;"
                class="text-head-search">
                {$sTitreForm}
            </h1>
        </div>
    </div>


    <form method="POST">
        <div style="display: inline-block;background-color: #fff;padding: 15px;border: 1px solid #D0D1D5;">


            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                                                              data-toggle="tab">FICHE CONTACT</a></li>
                    <li role="presentation"><a href="#suvi" aria-controls="profile" role="tab" data-toggle="tab">HISTORIQUE</a>
                    </li>
                    <li role="presentation"><a href="#prochetape" aria-controls="profile" role="tab" data-toggle="tab">PROCHAINE
                                                                                                                       ETAPE</a>
                    </li>
                    <li role="presentation"><a href="#evolution" aria-controls="profile" role="tab" data-toggle="tab">EVOLUTION</a>
                    </li>
                    </li>
                    <li role="propalcom"><a href="#propalcom" aria-controls="profile" role="tab" data-toggle="tab">PROPAL COMMERCIAL</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        {if isset($bMessageSuccesForm) and $bMessageSuccesForm}
                            <div class="alert alert-success"
                                 role="alert">{if isset($sMessageSuccesForm)}{$sMessageSuccesForm}{/if}</div>
                        {/if}
                        {if isset($bMessageErreurForm) and $bMessageErreurForm}
                            <div class="alert alert-danger"
                                 role="alert"> {if isset($sMessageErreurForm)}{$sMessageErreurForm}{/if}</div>
                        {/if}

                        {if isset($bMessageWarmingForm) and $bMessageWarmingForm}
                            <div class="alert alert-warning"
                                 role="alert"> {if isset($sMessageWarmingForm)}{$sMessageWarmingForm}{/if}</div>
                        {/if}

                        <table border="0" cellpadding="0" cellspacing="0" id="id-form"
                               style="align:left;padding:20px;background-color:#fff;"
                               class="table table-condensed table-hover">
                            <tbody>
                            <tr align="left">
                                <td>
                                    <b>Date de création <br>de la fiche du prospect</b>
                                </td>
                                <td align="left">
                                    <b>{$datecrationfiche}
                                </td>
                            </tr>

                            {foreach from=$aForm item=objForm}
                                {if isset($objForm.type_champ) and ((isset($objForm.aff_form) and $objForm.aff_form eq 'ok') or ($objForm.type_champ eq 'category' or $objForm.type_champ eq 'bouton'))}
                                    {if $objForm.type_champ eq 'text'}
                                        {if $objForm.supprimer_br eq 'ok'}</tr><tr>{/if}
                                        <th valign="top" style="border:0;">{$objForm.text_label} {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}&nbsp;<span style="color:red">*</span>{/if}</th>
                                            <td style="border:0;text-align: left;">
                                                <input
                                                        type="text"
                                                        class="inp-form"
                                                        id="id_{$objForm.nom_variable}"
                                                        name="{$objForm.nom_variable}"
                                                        {if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
                                                    {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                        {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                    {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                {$objForm.fonction_javascript}
                                                {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}required{/if}>

                                            </td>
                                    {elseif $objForm.type_champ eq 'hidden'}

                                                <input
                                                        type="hidden"
                                                        {if isset($objForm.nom_variable)}name="{$objForm.nom_variable}"{/if}
                                                        {if isset($objForm.valeur_variable)}value="{$objForm.valeur_variable}"{/if}
                                                >
                                    {elseif $objForm.type_champ eq 'color'}
                                        {if $objForm.supprimer_br eq 'ok'}</tr><tr>{/if}
                                            <th valign="top" style="border:0;">{$objForm.text_label}</th>
                                            <td style="border:0;text-align: left;">
                                                <input
                                                        type="color"
                                                        class="inp-form"
                                                        id="id_{$objForm.nom_variable}"
                                                        name="{$objForm.nom_variable}"
                                                        {if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
                                                    {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                        {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                    {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                    {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                >
                                                {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                    <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                {/if}
                                            </td>


                                    {elseif $objForm.type_champ eq 'category'}
                                        <tr>
                                            <td colspan="8" >
                                                <b>{$objForm.text_label}</b>
                                            </td>
                                        </tr>
                                    {elseif $objForm.type_champ eq 'date'}
                                        {if $objForm.supprimer_br eq 'ok'}</tr><tr>{/if}
                                            <th valign="top" style="border:0;">{$objForm.text_label}</th>
                                            <td style="border:0;text-align: left;">
                                                <input
                                                        type="date"
                                                        id="id_{$objForm.nom_variable}"
                                                        name="{$objForm.nom_variable}"
                                                        {if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value='{$objForm.valeur_variable|date_format:"%Y-%m-%d"}'{/if}
                                                    {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                        {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                    {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                    {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                >
                                                {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                    <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                {/if}
                                            </td>

                                    {elseif $objForm.type_champ eq 'textarea'}
                                        {if $objForm.supprimer_br eq 'ok'}</tr><tr>{/if}
                                            <th valign="top" style="border:0;">{$objForm.text_label}</th>
                                            <td style="border:0;text-align: left;">
							<textarea
                                    id="id_{$objForm.nom_variable}"
                                    name="{$objForm.nom_variable}"
                                    {if isset($objForm.wysiwyg) and $objForm.wysiwyg eq 'ok'}class="ckeditor"{/if}
                                {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                    {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                    {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                    {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                >{$objForm.valeur_variable}</textarea>
                                                {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                    <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                {/if}
                                            </td>

                                    {elseif $objForm.type_champ eq 'checkbox'}
                                        {if $objForm.supprimer_br eq 'ok'}</tr><tr>{/if}
                                            <th style="vertical-align:top;border:0;">{$objForm.text_label}</th>
                                            <td style="padding-top:5px;border:0;text-align: left;">
                                                {if $objForm.tags eq 'ok'}
                                                    <select name="{$objForm.nom_variable}[]" class="js-example-responsive" multiple="multiple" style="width: 100%;">
                                                        {foreach from=$objForm.lesitem key=valeur_checkbox item=nom_checkbox}
                                                            <option value="{$valeur_checkbox}" {if is_array($objForm.valeur_variable) and in_array($valeur_checkbox, $objForm.valeur_variable)}selected{/if}>{$nom_checkbox}</option>
                                                        {/foreach}
                                                    </select>
                                                {else}
                                                    <table style="display:inline-block;">
                                                        <tbody>
                                                        {foreach from=$objForm.lesitem key=valeur_checkbox item=nom_checkbox}
                                                            <tr>
                                                                <td style="padding-right:10px;">
                                                                    <input
                                                                            type="checkbox"
                                                                            name="{$objForm.nom_variable}[]"
                                                                            value="{$valeur_checkbox}"
                                                                            {if is_array($objForm.valeur_variable) and in_array($valeur_checkbox, $objForm.valeur_variable)}checked{/if}
                                                                    >
                                                                </td>
                                                                <td style="padding-right:20px;">{$nom_checkbox}</td>
                                                            </tr>
                                                        {/foreach}
                                                        </tbody>
                                                    </table>
                                                {/if}
                                                {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                    <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                {/if}
                                            </td>

                                    {elseif $objForm.type_champ eq 'select'}
                                        {if $objForm.supprimer_br eq 'ok'}</tr><tr>{/if}
                                        {if isset($objForm.select_autocomplete) and $objForm.select_autocomplete eq 'ok'}

                                                <th valign="top" style="border:0;">{$objForm.text_label_filtre}</th>
                                                <td style="border:0;text-align: left;">
                                                    <input type='text'
                                                           name='rech_{$objForm.nom_variable}'
                                                           id='id_rech{$objForm.nom_variable}'
                                                           class="inp-form"
                                                    {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                        {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                            {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                        {/foreach}
                                                    {/if}
                                                    onKeyUp="affiche_liste_generique('{$objForm.table_item}','{$objForm.id_table_item}','{$objForm.affichage_table_item}','{$objForm.supplogique_table_item}', '{$objForm.tabfiltre_autocomplete}',this.value,'id_form_{$objForm.nom_variable}');">
                                                </td>

                                        {/if}

                                            <th style="border:0;">{$objForm.text_label} {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}&nbsp;<span style="color:red">*</span>{/if}</th>
                                            <td style="padding-top:8px;border:0;text-align: left;">
                                                <select
                                                        style="padding:5px;"
                                                        id="id_form_{$objForm.nom_variable}"
                                                        name="{$objForm.nom_variable}{if isset($objForm.multiple)}[]{/if}"
                                                        {if isset($objForm.multiple)}multiple{/if}

                                                        {$objForm.fonction_javascript}

                                                        size="{$objForm.size_champ}"
                                                        {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}required{/if}>
                                                    <option value="" {if $objForm.valeur_variable eq ''}selected{/if}></option>
                                                    {if is_array($objForm.lesitem)}
                                                        {foreach from=$objForm.lesitem key=id_valeur_possible item=valeur_possible_bdd}
                                                            <option value="{$id_valeur_possible}" {if $objForm.valeur_variable eq $id_valeur_possible}selected{/if}>
                                                                {$valeur_possible_bdd}
                                                            </option>
                                                        {/foreach}
                                                    {/if}
                                                </select>

                                            </td>

                                    {elseif $objForm.type_champ eq 'radio'}
                                        {if $objForm.supprimer_br eq 'ok'}</tr><tr>{/if}
                                            <th valign="top" style="border:0;">{$objForm.text_label}</th>
                                            <td style="padding-top:8px;border:0;text-align: left;">
                                                <div style="display:inline-block;">
                                                    <table>
                                                        <tbody>
                                                        {foreach from=$objForm.lesitem key=valeur_radio item=nom_radio}
                                                            <tr>
                                                                <td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
                                                                    <input
                                                                            type="radio"
                                                                            name="{$objForm.nom_variable}"
                                                                            value="{$valeur_radio}"
                                                                            {if $valeur_radio eq $objForm.valeur_variable}checked{/if}
                                                                    >
                                                                </td>
                                                                <td style="padding-bottom:0;vertical-align:middle;height:32px;">{$nom_radio}</td>
                                                            </tr>
                                                        {/foreach}
                                                        </tbody>
                                                    </table>
                                                </div>
                                                {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                    <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                {/if}
                                            </td>

                                    {elseif $objForm.type_champ eq 'file'}
                                        {if $objForm.supprimer_br eq 'ok'}</tr><tr>{/if}
                                            <th style="border:0;">{$objForm.text_label}</th>
                                            <td style="border:0;text-align: left;">
                                                <div style="border-radius:6px;border:1px solid #ACACAC;padding:3px;display:inline-block;vertical-align:middle;">
                                                    <input
                                                            type="file"
                                                            id="id_{$objForm.nom_variable}"
                                                            name="{$objForm.nom_variable}"
                                                            {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                            {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                    {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                        {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                            {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                        {/foreach}
                                                    {/if}
                                                    {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                        {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                            {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                        {/foreach}
                                                    {/if}
                                                    >
                                                </div>
                                                {if isset($objForm.file_aff_modif_form) and $objForm.file_aff_modif_form eq 'ok'}
                                                    {if $objForm.valeur_variable neq ''}
                                                        {if preg_match("/\.png$/", $objForm.valeur_variable) or
                                                        preg_match("/\.jpeg$/", $objForm.valeur_variable) or
                                                        preg_match("/\.gif$/", $objForm.valeur_variable)
                                                        }
                                                            <div style="
                                                                    border-radius:6px;
                                                                    border:1px solid #ACACAC;
                                                                    padding:3px;
                                                                    display:inline-block;
                                                                    vertical-align:middle;
                                                                    {if isset($objForm.file_aff_modif_form_couleur_fond)}background-color:{$objForm.file_aff_modif_form_couleur_fond}{/if};"
                                                            >
                                                                <img
                                                                        src="{$objForm.file_visu}{$objForm.valeur_variable}"
                                                                        style="display:inline-block;vertical-align:middle;"
                                                                        {if isset($objForm.file_aff_modif_form_taille)}width="{$objForm.file_aff_modif_form_taille}"{/if}
                                                                >
                                                            </div>
                                                        {elseif $objForm.valeur_variable neq '' and preg_match("/\.swf$/", $objForm.valeur_variable)}
                                                            <div style="
                                                                    border-radius:6px;
                                                                    border:1px solid #ACACAC;
                                                                    padding:3px;
                                                                    display:inline-block;
                                                                    vertical-align:middle;
                                                                    {if isset($objForm.file_aff_modif_form_couleur_fond)}background-color:{$objForm.file_aff_modif_form_couleur_fond}{/if};"
                                                            >
                                                                <object
                                                                        type="application/x-shockwave-flash"
                                                                        data="{$objForm.file_visu}{$objForm.valeur_variable}"
                                                                        {if isset($objForm.file_aff_modif_form_taille)}width="{$objForm.file_aff_modif_form_taille}"{/if}
                                                                >
                                                                    <param name="movie" value="{$objForm.file_visu}{$objForm.valeur_variable}" />
                                                                    <param name="wmode" value="transparent" />

                                                                </object>
                                                            </div>
                                                        {else}
                                                            <a target="_blank" href="{$objForm.file_visu}{$objForm.valeur_variable}">Voir le fichier</a>
                                                        {/if}
                                                    {/if}
                                                {/if}
                                                {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                    <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                {/if}
                                            </td>

                                    {elseif $objForm.type_champ eq 'time'}
                                        <tr>
                                            <th style="border:0;">{$objForm.text_label}</th>
                                            <td style="border:0;text-align: left;">
                                                <input
                                                        type="time"
                                                        class="timepicker inp-form"
                                                        id="id_{$objForm.nom_variable}"
                                                        name="{$objForm.nom_variable}"
                                                        {if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
                                                    {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                        {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                    {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                    {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                >
                                                {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                    <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                {/if}
                                            </td>
                                        </tr>
                                    {elseif $objForm.type_champ eq 'hour'}
                                        <tr>
                                            <th style="border:0;">{$objForm.text_label}</th>
                                            <td style="border:0;text-align: left;">
                                                <input
                                                        type="text"
                                                        class="hourpicker inp-form"
                                                        id="id_{$objForm.nom_variable}"
                                                        name="{$objForm.nom_variable}"
                                                        {if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
                                                    {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                        {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                    {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                    {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                >
                                                {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                    <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                {/if}
                                            </td>
                                        </tr>
                                    {elseif $objForm.type_champ eq 'hidden'}
                                        <tr>
                                            <td style="padding:0;border:0;"></td>
                                            <td style="padding:0;border:0;">
                                                <input
                                                        type="hidden"
                                                        {if isset($objForm.nom_variable)}name="{$objForm.nom_variable}"{/if}
                                                        {if isset($objForm.valeur_variable)}value="{$objForm.valeur_variable}"{/if}
                                                >
                                            </td>
                                        </tr>
                                    {elseif $objForm.type_champ eq 'password'}
                                        {if $objForm.double_password eq 'ok'}
                                            <tr>
                                                <th valign="top">{$objForm.text_label}</th>
                                                <td>
                                                    <input
                                                            type="password"
                                                            class="inp-form"
                                                            id="id_{$objForm.nom_variable}"
                                                            name="{$objForm.nom_variable}"
                                                            {if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
                                                        {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                            {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                    {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                        {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                            {$cle}="{$valeur}"
                                                        {/foreach}
                                                    {/if}
                                                    {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                        {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                            {$cle}="{$valeur}"
                                                        {/foreach}
                                                    {/if}
                                                    >
                                                    {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                        <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                    {/if}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th valign="top">{$objForm.text_label} 2</th>
                                                <td>
                                                    <input
                                                            type="password"
                                                            class="inp-form"
                                                            id="id_{$objForm.nom_variable}_2"
                                                            name="{$objForm.nom_variable}_2"
                                                            {if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
                                                        {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                            {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                    {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                        {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                            {$cle}="{$valeur}"
                                                        {/foreach}
                                                    {/if}
                                                    {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                        {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                            {$cle}="{$valeur}"
                                                        {/foreach}
                                                    {/if}
                                                    >
                                                    {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                        <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                    {/if}
                                                </td>
                                            </tr>
                                        {else}
                                            <tr>
                                                <th valign="top">{$objForm.text_label}</th>
                                                <td>
                                                    <input
                                                            type="password"
                                                            class="inp-form"
                                                            id="id_{$objForm.nom_variable}"
                                                            name="{$objForm.nom_variable}"
                                                            {if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
                                                        {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                            {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                    {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                        {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                            {$cle}="{$valeur}"
                                                        {/foreach}
                                                    {/if}
                                                    {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                        {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                            {$cle}="{$valeur}"
                                                        {/foreach}
                                                    {/if}
                                                    >
                                                    {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                        <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                    {/if}
                                                </td>

                                        {/if}
                                    {elseif $objForm.type_champ eq 'email'}
                                        {if $objForm.supprimer_br eq 'ok'}</tr><tr>{/if}
                                            <th valign="top" style="border:0;">{$objForm.text_label}</th>
                                            <td style="border:0;text-align: left;">
                                                <input
                                                        type="email"
                                                        class="inp-form"
                                                        id="id_{$objForm.nom_variable}"
                                                        name="{$objForm.nom_variable}"
                                                        {if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
                                                    {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                        {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                    {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                    {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                >
                                                {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                    <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                {/if}
                                            </td>
                                        </tr>
                                    {elseif $objForm.type_champ eq 'telephone'}
                                        <tr>
                                            <th style="border:0;" valign="top">{$objForm.text_label}</th>
                                            <td style="border:0;text-align: left;">
                                                <input
                                                        type="tel"
                                                        class="inp-form"
                                                        id="id_{$objForm.nom_variable}"
                                                        name="{$objForm.nom_variable}"
                                                        {if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
                                                    {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                        {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                    {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                    {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                >
                                                {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
                                                    <div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
                                                {/if}
                                            </td>
                                        </tr>
                                    {/if}
                                {/if}
                            {/foreach}
                            </td>
                            </tr>
                            <tr>
                                <td colspan="8" align="left" height="10px">
                                    <b> Suivi et action - commentaires</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <table width="100%" style="border-collapse: separate;border-spacing: 5px 8px;">
                                        <tr>
                                            <td align="left">
                                                <u> Suivi</u>
                                            </td>
                                            <td>

                                            </td>
                                            <td align="left">
                                                Commentaire
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <input type="checkbox" name="checktel" value="ok">&nbsp;Tel
                                            </td>
                                            <td align="left"  valign="top">
                                                <select name="itemchecktel"
                                                        onchange="modal_gestio_item(this.value,'checktel','Item suivi téléphone');">
                                                    <!--modal_gestio_item(this.value)-->
                                                    <option></option>
                                                    {if $bconfig}
                                                        <option value="aj">Ajouter un item</option>
                                                    {/if}
                                                    {if !empty($aTabChecktel)}
                                                    {foreach from=$aTabChecktel item=objChecktel}
                                                        <option value="{$objChecktel.id_item}">{$objChecktel.nom_item}</option>
                                                    {/foreach}
                                                    {/if}
                                                </select>
                                            </td>
                                            <td align="left" colspan="2">
                                                <textarea   name="commentchecktel"  cols="50" rows="5"></textarea>
                                            </td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td height="10px">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <input type="checkbox" name="checkmail" value="ok">&nbsp;Mail
                                            </td>
                                            <td align="left" valign="top">
                                                <select name="itemcheckmail"
                                                        onchange="modal_gestio_item(this.value,'checkmail','Item suivi mail');">
                                                    <!--modal_gestio_item(this.value)-->
                                                    <option></option>
                                                    {if $bconfig}
                                                        <option value="aj">Ajouter un item</option>
                                                    {/if}
                                                    {if !empty($aTabCheckMail)}
                                                    {foreach from=$aTabCheckMail item=objCheckMail}
                                                        <option value="{$objCheckMail.id_item}">{$objCheckMail.nom_item}</option>
                                                    {/foreach}
                                                    {/if}
                                                </select>
                                            </td>
                                            <td align="left" colspan="2">
                                                <textarea   name="commentcheckmail"  cols="50" rows="5"></textarea>
                                            </td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td height="10px">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <input type="checkbox" name="checkbv" value="ok">&nbsp;Visite
                                            </td>
                                            <td align="left" valign="top">
                                                <select name="itemcheckbv"
                                                        onchange="modal_gestio_item(this.value,'checkbv','Item suivi visite BV');">
                                                    <!--modal_gestio_item(this.value)-->
                                                    <option></option>
                                                    {if $bconfig}
                                                        <option value="aj">Ajouter un item</option>
                                                    {/if}
                                                    {if !empty($aTabCheckBv)}
                                                    {foreach from=$aTabCheckBv item=objCheckBv}
                                                        <option value="{$objCheckBv.id_item}">{$objCheckBv.nom_item}</option>
                                                    {/foreach}
                                                    {/if}
                                                </select>
                                            </td>
                                            <td align="left" colspan="2">
                                                <textarea  name="commentcheckbv"  cols="50" rows="5"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colpsan="8">
                                                <u>Prochaine Etape</u>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <input type="radio" name="chexkprochetap" value="0">&nbsp; Refus<br>
                                                <input type="radio" name="chexkprochetap" value="tel">&nbsp; Tel<br>
                                                <input type="radio" name="chexkprochetap" value="mail">&nbsp; Mail<br/>
                                                <input type="radio" name="chexkprochetap" value="rdv">&nbsp; RDV<br/>
                                            </td>
                                            <td>

                                                Date prévision <br><input type="date" size="12" name="dateprevision"
                                                                          id="datepicker" value="{$dateprevision}"><br>
                                                Heure<br><input type="time" size="6" name="heureprevision" id="heure"
                                                                value="">


                                            </td>
                                            <td align="left" colspan="2">
                                                <textarea  name="commentprochaine" cols="50" rows="5"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="8">
                                                <u>Evolution du contact </u>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                Statut
                                            </td>
                                            <td align="left">
                                                <select name="statut" required
                                                        onchange="modal_gestio_item(this.value,'statut','Statut');if(this.value=={$idouvrecauseadandon})document.getElementById('affcause').style.display='block';else document.getElementById('affcause').style.display='none'">
                                                    <option></option>
                                                    {if $bconfig}
                                                        <option value="aj">Ajouter un item</option>
                                                    {/if}
                                                    {if !empty($aTabStatut)}
                                                    {foreach from=$aTabStatut item=objStatut}
                                                        <option value="{$objStatut.id_item}"
                                                               >{$objStatut.nom_item}</option>
                                                    {/foreach}
                                                    {/if}
                                                </select> <br><br>

                                                <div id="affcause" style="display:none">La cause : <select
                                                            name="causeadandon"
                                                            onchange="modal_gestio_item(this.value,'causeadandon','Item Cause Abandon');">
                                                        <option></option>
                                                        <option value="aj">Ajouter un item</option>
                                                        {if !empty($aTabcauseabandon)}
                                                        {foreach from=$aTabcauseabandon item=objcauseabandone}
                                                            <option value="{$objcauseabandone.id_item}">{$objcauseabandone.nom_item}</option>
                                                        {/foreach}
                                                        {/if}
                                                    </select>
                                                </div>
                                            </td>

                                            <td align="left" valign="top">
                                                Phase
                                            </td>
                                            <td align="left" valign="top">
                                                <select name="qualite"
                                                        onchange="modal_gestio_item(this.value,'qualite','Qualité');"
                                                        required>
                                                    <option></option>
                                                    {if $bconfig}
                                                        <option value="aj">Ajouter un item</option>
                                                    {/if}
                                                    {if !empty($aTabQualite)}
                                                    {foreach from=$aTabQualite item=objQualite}
                                                        <option value="{$objQualite.id_item}"
                                                               >{$objQualite.nom_item}</option>
                                                    {/foreach}
                                                    {/if}
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="left">
                                                Ce contact a t'il évolué ?&nbsp;<input type="radio" name="actionevo"
                                                                                       value="ok"
                                                                                       {if $bnouveau}checked="checked"
                                                                                       {/if}required>&nbsp;OUI&nbsp;<input
                                                        type="radio" name="actionevo" value="no" &nbsp; required> NON
                                            </td>
                                        </tr>
                                        {if $bnouveau}
                                            <td colspan="4" align="left" >
                                                Programmation de l'envoi du mail automatique immédiat&nbsp;<input type="radio" name="mailautook" value="ok" required>&nbsp;OUI&nbsp;<input type="radio" name="mailautook" checked="checked" value="no" &nbsp; required> NON
                                            </td>
                                            </tr>
                                        {/if}

                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            {if !empty($aEmailing)}
                                <tr>
                                    <td colspan="4">
                                        <table width="80%" id="emailinprog">
                                            <caption>
                                                <center>Emailing en programmation ou programmé</center>
                                            </caption>
                                            <tr>
                                                <td>
                                                    <b>Nom emailing</b>
                                                </td>
                                                <td>
                                                    <b>Date envoi</b>
                                                </td>
                                                <td>
                                                    <b>Statut</b>
                                                </td>
                                                <td>
                                                    <b></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="5px"></td>
                                            </tr>
                                            {if !empty($aEmailing)}
                                                {foreach from=$aEmailing item=objEmail}
                                                    <tr>
                                                        <td>
                                                            {$objEmail.nom}
                                                        </td>
                                                        <td>
                                                            {$objEmail.date}
                                                        </td>
                                                        <td>
                                                            {$objEmail.statut}
                                                        </td>
                                                        <td>
                                                            {$objEmail.action}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="5px"></td>
                                                    </tr>
                                                {/foreach}
                                            {/if}
                                        </table>

                                    </td>
                                </tr>
                            {/if}

                            <tr>
                                <td align="center" colspan="8" id="idbtel">

                                    </a> {if $bactionmodif}&nbsp;&nbsp;
                                    <button type="submit" class="btn btn-primary">Valider</button>{/if}&nbsp;&nbsp; <a
                                            href="crm-ctrl_contact_crm-fli_contact_crm">
                                        <button type="button" class="btn btn-primary">Retour</button>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--- dic ction - -->
                    <div role="tabpanel" class="tab-pane fade" id="prochetape">

                        {if isset($bMessageSuccesForm) and $bMessageSuccesForm}
                            <div class="alert alert-success"
                                 role="alert">{if isset($sMessageSuccesForm)}{$sMessageSuccesForm}{/if}</div>
                        {/if}
                        {if isset($bMessageErreurForm) and $bMessageErreurForm}
                            <div class="alert alert-danger"
                                 role="alert"> {if isset($sMessageErreurForm)}{$sMessageErreurForm}{/if}</div>
                        {/if}


                        <table class="table table-hover table-condensed table-bordered table-striped">
                            <thead>
                             {*<th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                         <a href="#" style="color:#fff;">Action</a>
                             </th>
                             *}
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Date </a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Type Action</a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                        <a href="#" style="color:#fff;">Categorie Action</a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Commentaire</a>
                            </th>
                            </thead>
                            <tbody>
                            {if !empty($aTabMenuTacheAFaire)}
                            {foreach from=$aTabMenuTacheAFaire item=aTabMenuTacheAFaireLigne}
                                <tr>
                                    {*<td style="text-align:center;vertical-align: middle;" class="dropdown">
                                      <div class="btn-group">
                                                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                    Menu <span class="caret"></span>
                                                  </button>
                                                  <ul class="dropdown-menu dropdown-menu-left" role="menu">

                                                       <li><a href="#"><span class="glyphicon glyphicon-pencil"></span>Modifier</a></li>
                                                       <li><a href="#"><span class="glyphicon glyphicon-pencil"></span>supprimer</a></li>

                                          </ul>
                                      </div>
                                            </td>*}

                                    {foreach from=$aTabMenuTacheAFaireLigne.liste item=aTabMenuTacheAFaireCol}
                                        <td>
                                            {$aTabMenuTacheAFaireCol.nom}
                                        </td>
                                    {/foreach}
                                </tr>
                            {/foreach}
                            {/if}

                            </tbody>
                        </table>

                    </div>
                    <!--     fin   -->

                    <!--- dic ction - -->
                    <div role="tabpanel" class="tab-pane fade" id="suvi">

                        {if isset($bMessageSuccesForm) and $bMessageSuccesForm}
                            <div class="alert alert-success"
                                 role="alert">{if isset($sMessageSuccesForm)}{$sMessageSuccesForm}{/if}</div>
                        {/if}
                        {if isset($bMessageErreurForm) and $bMessageErreurForm}
                            <div class="alert alert-danger"
                                 role="alert"> {if isset($sMessageErreurForm)}{$sMessageErreurForm}{/if}</div>
                        {/if}


                        <table class="table table-hover table-condensed table-bordered table-striped">
                            <thead>
                            {* <th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                         <a href="#" style="color:#fff;">Action</a>
                             </th>*}
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                <a href="#" data-toggle="modal" data-target="#myModal" style="color:#fff;">Date</a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Type Action</a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                        <a href="#" style="color:#fff;">Categorie Action</a>
                            </th>

                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Commentaire</a>
                            </th>
                            </thead>
                            <tbody>
                            {if !empty($aTabSuvitache)}
                            {foreach from=$aTabSuvitache item=aTabSuvieTacheLigne}
                                <tr>

                                    {foreach from=$aTabSuvieTacheLigne.liste item=aTabSuviTacheCol}
                                        <td>
                                            {$aTabSuviTacheCol.nom}
                                        </td>
                                    {/foreach}
                                </tr>
                            {/foreach}
                            {/if}

                            </tbody>
                        </table>

                    </div>
                    <!--     fin   -->
                    <!--- dic ction - -->
                    <div role="tabpanel" class="tab-pane fade" id="evolution">


                        <table class="table table-hover table-condensed table-bordered table-striped">
                            <thead>

                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" data-toggle="modal" data-target="#myModal" style="color:#fff;">Date </a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Statut</a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Phase</a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;"></a>
                            </th>
                            </thead>
                            <tbody>
                            {if !empty($aTabEvolution)}
                            {foreach from=$aTabEvolution item=aTabEvolutionLigne}
                                <tr>
                                    {*<td style="text-align:center;vertical-align: middle;" class="dropdown">
                                      <div class="btn-group">
                                                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                    Menu <span class="caret"></span>
                                                  </button>
                                                  <ul class="dropdown-menu dropdown-menu-left" role="menu">

                                                       <li><a href="#"><span class="glyphicon glyphicon-pencil"></span>Modifier</a></li>
                                                       <li><a href="#"><span class="glyphicon glyphicon-pencil"></span>supprimer</a></li>

                                          </ul>
                                      </div>
                                            </td>*}

                                    {foreach from=$aTabEvolutionLigne.liste item=aTabEvolutioncol}
                                        <td>
                                            {$aTabEvolutioncol.nom}
                                        </td>
                                    {/foreach}
                                </tr>
                            {/foreach}
                            {/if}

                            </tbody>
                        </table>

                    </div>
                    <!--     fin   -->



                    <!--- dic ction - -->
                    <div role="tabpanel" class="tab-pane fade" id="propalcom">


                        <table class="table table-hover table-condensed table-bordered table-striped">
                            <thead>

                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" data-toggle="modal" data-target="#myModal" style="color:#fff;">Date </a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Ville</a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Type Mobilier</a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;"> Mobilier</a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;"> Prix location</a>
                            </th>

                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;"> Frais technique</a>
                            </th>


                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;"> Commentaire</a>
                            </th>
                            <th class="table-header-repeat line-left minwidth-1 title-table-head"
                                style="padding: 5px;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;"></a>
                            </th>
                            </thead>
                            <tbody>
                            {if !empty($aTabPropalaTabPropal)}
                                {foreach from=$aTabPropal item=aTabPropalLigne}
                                    <tr>
                                        {*<td style="text-align:center;vertical-align: middle;" class="dropdown">
                                          <div class="btn-group">
                                                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Menu <span class="caret"></span>
                                                      </button>
                                                      <ul class="dropdown-menu dropdown-menu-left" role="menu">

                                                           <li><a href="#"><span class="glyphicon glyphicon-pencil"></span>Modifier</a></li>
                                                           <li><a href="#"><span class="glyphicon glyphicon-pencil"></span>supprimer</a></li>

                                              </ul>
                                          </div>
                                                </td>*}

                                        {foreach from=$aTabPropalLigne.liste item=aTabPropalLignecol}
                                            <td>
                                                {$aTabPropalLignecol.nom}
                                            </td>
                                        {/foreach}
                                    </tr>
                                {/foreach}
                            {/if}

                            </tbody>
                        </table>

                    </div>
                    <!--     fin   -->




                </div>

            </div>

        </div>
    </form>


    <!------ fin grande dic-->

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>


{literal}
    <script type="text/javascript">

        function open_div(valeur) {

            if (valeur == 1) {
                document.getElementById('iddetail').style.display = "block";
                document.getElementById('idopen').value = "0";
                document.getElementById('plusmoin').innerHTML = "&minus;";
            } else {
                document.getElementById('iddetail').style.display = "none";
                document.getElementById('idopen').value = "1";
                document.getElementById('plusmoin').innerHTML = "&plus;";
            }

        }


        function modal_gestio_item(action, value, nom) {
            //alert(value);

            if (action == "aj")
                document.location.replace('main.php?dir=configlisteitem&identifiant=' + value + '&nomitem=' + nom);

        }

        $(function () {
            $('[data-toggle="popover"]').popover()
        });

        if ($('#id_form_id_ville option:selected').text() == '' && $('#cpville').val() != '' && $('#cpville').val() != 0)
            affiche_liste_generique('ville_cedex', 'id_code_postal', 'ville', 'supplogique_ville', 'tabfiltre[0]=code_postal|%<rech>%|like&', $('#cpville').val(), 'id_form_id_ville');

        //$('#idtel').popover('show');

    </script>
{/literal}