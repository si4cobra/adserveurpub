<div style="color:#313131;margin: 0 auto;padding: 20px;margin-top: -10px;margin: 0 auto;text-align: center;padding-top: 10px;padding-bottom:0;">


    <div style="padding:5px;background-color: #fafafa;border: 1px solid #ddd;margin-left: 5px;margin-right: 5px;">
        <form class="form-inline">
            <input type="hidden" name="dir" value="dashboard">
            <input type="hidden" name="filtrecrm" value="ok">
            <div class="form-group">
                <label class="label-form-upper" style="width: 100%;">Date</label>
                <div class="input-group" style="display: inline-table;vertical-align: middle;border: 0;box-shadow: none;width: auto;">
                    <span class="input-group-addon" id="basic-addon2" style="width:auto;min-width:52px;">début</span>
                    <input class="form-control" type="date" name="tridatedebut" aria-describedby="basic-addon2" required {if !empty($tridatedebut)}value="{$tridatedebut}"{/if}>
                </div>
                <div class="input-group" style="display: inline-table;vertical-align: middle;border: 0;box-shadow: none;width: auto;">
                    <span class="input-group-addon" id="basic-addon2" style="width:auto;">fin</span>
                    <input class="form-control" type="date" name="tridatefin" aria-describedby="basic-addon2" required {if !empty($tridatefin)}value="{$tridatefin}"{/if}>
                </div>
            </div>
            {*<div class="form-group">
                <label>Programmes</label>
                <select name="triprogramme" class="form-control">
                    <option value=""></option>
                    {foreach from=$aTabTriProg item=aTabTriProgParc}
                        <option value="{$aTabTriProgParc.value}" {if $aTabTriProgParc.value eq $triprogramme}selected{/if}>{$aTabTriProgParc.prog}</option>
                    {/foreach}
                </select>
            </div>

            {if $bAdmin}
                <div class="form-group">
                    <label>Commerciaux</label>
                    <select name="tricommercial" class="form-control">
                        <option value=""></option>
                        {foreach from=$aTabTriCom item=aTabTriComParc}
                            <option value="{$aTabTriComParc.value}">{$aTabTriComParc.com}</option>
                        {/foreach}
                    </select>
                </div>
            {/if}
            *}
            <div class="form-group">
                <input class="btn btn-primary" type="submit" name="tri-button" value="Valider">
            </div>
            <div class="form-group">
                <input class="btn btn-primary" type="button" name="tri-button" value="Réinitialiser" onclick="location.href='main.php?dir=dashboard';">
            </div>
        </form>
    </div>


    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding:5px;">
        <div style="background-color: #fafafa;border: 1px solid #ddd;height:304px;padding:10px;overflow-y: auto;">
            <div style="word-wrap: break-word;text-align:left;">
                <div style="margin-bottom: 20px;">
                    <div style="display: inline-block;" class="title-search">
                        <span class="glyphicon glyphicon-pushpin title-search" style="display: inline-block;height: 35px;width: 35px;padding: 10px;vertical-align: top;top:0px;"></span>
                        <h4 style="display: inline-block;margin: 0;padding: 10px;font-size: 14px;" class="text-search">Rendez vous</h4>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-bordered table-striped">
                        <thead>
                        <tr>
                            {*<th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Action</a>
                            </th>*}
                            {foreach from=$aTabMenuTacheAFaireRDVEntete item=aTabMenuTacheAFaireRDVEnteteParc}
                                <th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                    <a href="#" style="color:#fff;">{$aTabMenuTacheAFaireRDVEnteteParc.nom}</a>
                                </th>
                            {/foreach}
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$aTabMenuTacheAFaireRDV item=aTabMenuTacheAFaireLigneRDV}
                            <tr style="cursor:pointer" onclick="location.href='crm-ctrl_contact_crm-fli_contact_crm?guid_contact={$aTabMenuTacheAFaireLigneRDV.id}&action=form';">
                                {*<td style="text-align:center;vertical-align: middle;" class="dropdown">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                          Menu <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-left" role="menu">

                                            <li><a href="#"><span class="glyphicon glyphicon-pencil"></span> Modifier</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-trash"></span> supprimer</a></li>

                                        </ul>
                                    </div>
                                </td>
                                *}

                                {foreach from=$aTabMenuTacheAFaireLigneRDV.liste item=aTabMenuTacheAFaireColRDV}
                                    <td>
                                        {$aTabMenuTacheAFaireColRDV.nom}
                                    </td>
                                {/foreach}
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding:5px;float:right;">
        <div style="background-color: #fafafa;border: 1px solid #ddd;height: 395px;padding:10px;overflow-y: auto;">
            <div style="word-wrap: break-word;text-align:left;">
                <div style="margin-bottom: 20px;">
                    <div style="display: inline-block;" class="title-search">
                        <a href="crm-ctrl_contact_crm-fli_contact_crm?action=form" style="color:#fff;"><span class="glyphicon glyphicon-user title-search" style="display: inline-block;height: 35px;width: 35px;padding: 10px;vertical-align: top;top:0px;"></span>
                            <h4 style="display: inline-block;margin: 0;padding: 10px;font-size: 14px;" class="text-search">Nouveaux contacts</h4></a>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-bordered table-striped">
                        <thead>
                        <tr>
                            {*<th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Action</a>
                            </th>
                            *}

                            {foreach from=$aTabMenuNouveauxContactsEntete item=aTabMenuNouveauxContactsEnteteParc}
                                <th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                    <a href="#" style="color:#fff;">{$aTabMenuNouveauxContactsEnteteParc.nom}</a>
                                </th>
                            {/foreach}
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$aTabMenuNouveauxContacts item=aTabMenuNouveauxContactsLigne}

                            <tr style="cursor:pointer" onclick="location.href='crm-ctrl_contact_crm-fli_contact_crm?guid_contact={$aTabMenuNouveauxContactsLigne.id}&action=form';">
                                {*<td style="text-align:center;vertical-align: middle;" class="dropdown">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                          Menu <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-left" role="menu">
                                            <li><a href="#"><span class="glyphicon glyphicon-pencil"></span> Modifier</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-tasks"></span> Ajouter une tâche</a></li>
                                        </ul>
                                    </div>
                                </td>
                                *}

                                {foreach from=$aTabMenuNouveauxContactsLigne.liste item=aTabMenuNouveauxContactsCol}
                                    <td>
                                        {$aTabMenuNouveauxContactsCol.nom}
                                    </td>
                                {/foreach}
                            </tr>

                        {/foreach}
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding:5px;">
        <div style="background-color: #fafafa;border: 1px solid #ddd;height:172px;padding:10px;overflow-y: auto;">
            <div style="word-wrap: break-word;text-align:left;">
                <div style="margin-bottom: 20px;">
                    <div style="display: inline-block;" class="title-search">
                        <span class="glyphicon glyphicon-pushpin title-search" style="display: inline-block;height: 35px;width: 35px;padding: 10px;vertical-align: top;top:0px;"></span>
                        <h4 style="display: inline-block;margin: 0;padding: 10px;font-size: 14px;" class="text-search">Mails automatiques au départ</h4>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-bordered table-striped">
                        <thead>
                        <tr>
                            {*<th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Action</a>
                            </th>*}
                            {foreach from=$aTabMailsAutomatiquesEntetes item=aTabMailsAutomatiquesEntetesParc}
                                <th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                    <a href="#" style="color:#fff;">{$aTabMailsAutomatiquesEntetesParc.nom}</a>
                                </th>
                            {/foreach}
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$aTabMailsAutomatiques item=aTabMailsAutomatiquesParc}
                            <tr style="cursor:pointer" onclick="location.href='crm-ctrl_contact_crm-fli_contact_crm?guid_contact={$aTabMailsAutomatiquesParc.id}&action=form';">
                                {*<td style="text-align:center;vertical-align: middle;" class="dropdown">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                          Menu <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-left" role="menu">

                                            <li><a href="#"><span class="glyphicon glyphicon-pencil"></span> Modifier</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-trash"></span> supprimer</a></li>

                                        </ul>
                                    </div>
                                </td>
                                *}

                                {foreach from=$aTabMailsAutomatiquesParc.liste item=aTabMailsAutomatiquesParcCol}
                                    <td>
                                        {$aTabMailsAutomatiquesParcCol.nom}
                                    </td>
                                {/foreach}
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding:5px;float:right;">
        <div style="background-color: #fafafa;border: 1px solid #ddd;height: 395px;padding:10px;overflow-y: auto;">
            <div style="word-wrap: break-word;text-align:left;">
                <div style="margin-bottom: 20px;">
                    <div style="display: inline-block;" class="title-search">
                        <span class="glyphicon glyphicon-tasks title-search" style="display: inline-block;height: 35px;width: 35px;padding: 10px;vertical-align: top;top:0px;"></span>
                        <h4 style="display: inline-block;margin: 0;padding: 10px;font-size: 14px;" class="text-search">Tâches en retard</h4>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-bordered table-striped">
                        <thead>
                        <tr>
                            {*<th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Action</a>
                            </th>
                            *}
                            {foreach from=$aTabMenuTacheEnRetardEntete item=aTabMenuTacheEnRetardEnteteParc}
                                <th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                    <a href="#" style="color:#fff;">{$aTabMenuTacheEnRetardEnteteParc.nom}</a>
                                </th>
                            {/foreach}
                        </thead>
                        <tbody>
                        {foreach from=$aTabMenuTacheEnRetard item=aTabMenuTacheEnRetardLigne}
                            <tr style="cursor:pointer" onclick="location.href='crm-ctrl_contact_crm-fli_contact_crm?guid_contact={$aTabMenuTacheEnRetardLigne.id}&action=form';">
                                {*<td style="text-align:center;vertical-align: middle;" class="dropdown">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                          Menu <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-left" role="menu">
                                            <li><a href="#"><span class="glyphicon glyphicon-pencil"></span> texte</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-pencil"></span> texte</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-pencil"></span> texte</a></li>
                                        </ul>
                                    </div>
                                </td>
                                *}

                                {foreach from=$aTabMenuTacheEnRetardLigne.liste item=aTabMenuTacheEnRetardCol}
                                    <td>
                                        {$aTabMenuTacheEnRetardCol.nom}
                                    </td>
                                {/foreach}
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding:5px;">
        <div style="background-color: #fafafa;border: 1px solid #ddd;height:304px;padding:10px;overflow-y: auto;">
            <div style="word-wrap: break-word;text-align:left;">
                <div style="margin-bottom: 20px;">
                    <div style="display: inline-block;" class="title-search">
                        <span class="glyphicon glyphicon-pushpin title-search" style="display: inline-block;height: 35px;width: 35px;padding: 10px;vertical-align: top;top:0px;"></span>
                        <h4 style="display: inline-block;margin: 0;padding: 10px;font-size: 14px;" class="text-search">Relances</h4>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-bordered table-striped">
                        <thead>
                        <tr>
                            {*<th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                <a href="#" style="color:#fff;">Action</a>
                            </th>*}
                            {foreach from=$aTabMenuTacheAFaireEntete item=aTabMenuTacheAFaireEnteteParc}
                                <th class="table-header-repeat line-left minwidth-1 title-table-head" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                                    <a href="#" style="color:#fff;">{$aTabMenuTacheAFaireEnteteParc.nom}</a>
                                </th>
                            {/foreach}
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$aTabMenuTacheAFaire item=aTabMenuTacheAFaireLigne}
                            <tr style="cursor:pointer" onclick="location.href='crm-ctrl_contact_crm-fli_contact_crm?guid_contact={$aTabMenuTacheAFaireLigne.id}&action=form';">
                                {*<td style="text-align:center;vertical-align: middle;" class="dropdown">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                          Menu <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-left" role="menu">

                                            <li><a href="#"><span class="glyphicon glyphicon-pencil"></span> Modifier</a></li>
                                            <li><a href="#"><span class="glyphicon glyphicon-trash"></span> supprimer</a></li>

                                        </ul>
                                    </div>
                                </td>*}

                                {foreach from=$aTabMenuTacheAFaireLigne.liste item=aTabMenuTacheAFaireCol}
                                    <td>
                                        {$aTabMenuTacheAFaireCol.nom}
                                    </td>
                                {/foreach}
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>