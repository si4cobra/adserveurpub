<?php

/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 07/12/2017
 * Time: 16:05
 */
class ctrl_contact_crm  extends class_form_list
{
    public function __construct($objSmartyImport, $sModule)
    {

        parent::class_list($objSmartyImport, $sModule);
        $objImport = new class_import();
        $objImport->import_modele('crm', 'mod_crm');
        $objModoutils = new mod_crm();
        $this->objBdd = $objModoutils;
    }

    private $sTelPrefix = null;

    public function controle_form_textnom(){


        $aTableauRetour['bResult'] =array();
        $aTableauRetour['result'] =true;
        $aTableauRetour['sMMessage '] ="";


        if($this->sEtatForm=="insert") {
            $aVar = array( 'checktel', 'itemchecktel', 'dateinfo', 'commentchecktel', 'checkmail', 'itemcheckmail', 'commentcheckmail', 'telcree', 'Raisonsocial', 'codepastalcree'
            , 'checkbv', 'itemcheckbv', 'commentcheckbv', 'chexkprochetap', 'dateprevision', 'commentprochaine', 'heureprevision', 'statut', 'qualite', 'actionevo', 'affcause', 'villecree'
            , 'rech_id_client', 'id_client', 'pays_client', 'tel_contact', 'email_contact', 'nom_contact', 'id_ville', 'numrue', 'typerue','id_typerue','numero_rue_contact' );
            $aVarVal = class_params::nettoie_get_post($aVar);

            foreach( $aVar as $key => $value ) {
                $$value = $aVarVal[$key];
            }

            if( $telcree == "" )
                $telcree = $tel_contact;

            if( $villecree == "" )
                $villecree = $id_ville;


            if( $Raisonsocial == "" )
                $Raisonsocial = $rech_id_client;

            if( $typerue == "" )
                $typerue = $id_typerue;

            if( $numrue == "" )
                $numrue = $numero_rue_contact;

            if( $id_client == "" ) {
                $objImport = new class_import();
                $objImport->import_modele('client', 'mod_client');

                $objMod = new mod_client();

                $aTabPrefix = $objMod->get_prefixe_pays($pays_client);

                $iPrefixe = $aTabPrefix[0]['prefixe_pays'];


                $sNumber = $telcree;

                $sNumber = preg_replace('/[^0-9]/', '', $sNumber);
                $sNumber = str_replace(' ', '', $sNumber);

                $data = $sNumber;

                if( preg_match('/^(0033\d{2})(\d{3})(\d{4})$/', $data, $matches) ) {

                } else if( preg_match('/^(\d{3})(\d{3})(\d{4})$/', $data, $matches) ) {
                    $data = substr($data, 1);
                    $data = $iPrefixe . $data;
                } else if( preg_match('/^(33\d{2})(\d{3})(\d{4})$/', $data, $matches) ) {
                    $data = "00" . $data;
                }
                $num = $data;
                $this->sTelPrefix = $data;
                $aTabClient = $objMod->get_client_has_number($num, 0);


                if( empty($aTabClient) ) {
                    $aTableauRetour['message'] = "";
                    $aTableauRetour['result'] = true;

                    $aTabParam = array();
                    $aTabParam['nom_client'] = $rech_id_client;
                    $aTabParam['raisonsocial_client'] = $Raisonsocial;
                    $aTabParam['tel_client'] = $telcree;
                    $aTabParam['email_client'] = $email_contact;
                    $aTabParam['contact_client'] = $nom_contact;
                    $aTabParam['mobile_client'] = $telcree;
                    //return array('bResult' => true, 'sSql' => '');
                    $id_client = $objMod->insert_Client($aTabParam);
                    $_POST['id_client'] = $id_client;
                    if( !$id_client ) {
                        $u = 0;
                    } else {
                        $aTabAdresse = array();
                        $aTabAdresse['id_client'] = $id_client;
                        $aTabAdresse['id_typerue'] = 0;
                        $aTabAdresse['ville_adresse'] = $id_client;
                        $aTabAdresse['codepostal_adresse'] = $codepastalcree;
                        $aTabAdresse['numero_adresse'] = $numrue;
                        $aTabAdresse['id_typerue'] = $typerue;
                        $aTabAdresse['adresse_adresse'] = $codepastalcree;
                        $aTabAdresse['idville'] = $villecree;
                        $objMod->insert_ClientAdresseWithIdClient($aTabAdresse);
                    }
                } else {
                    $aTableauRetour['message'] = "Un client existe déjà avec ce numéro ! (" . $aTabClient[0]['raisonsocial_client'] . ")";
                    $aTableauRetour['result'] = false;
                    //return array('bResult' => false, 'sSql' => '');
                }

                //controle des information
            } else {
                $aTableauRetour['result'] = true;
            }
        }


        return $aTableauRetour;


    }


    public function post_form_insert_fli_contact_crm(){

        $aTableauRetour['sMessage_Post']='';
        $aTableauRetour['bResult_Post']=true;

        $aVar = array( 'checktel', 'itemchecktel', 'dateinfo', 'commentchecktel','checkmail','itemcheckmail','commentcheckmail','villemobilier','id_type_mobilier','mobilie_contact'
        ,'prix_location_contact','fraistechnique_contact','strategie_contact','checkbv','itemcheckbv','commentcheckbv','chexkprochetap','dateprevision','commentprochaine',
            'heureprevision','statut','qualite','actionevo','affcause' );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $guid_user= class_fli::get_aData('guid_user');
        //envoi mail auto si ok
       /* if($mailautook=="ok"){
            $aTabaleauMailAuto = $objTest->emailing_programmation_jour($objClassGenerique->id_user,$programme,0);
            //echo"<pre>";print_r($aTabaleauMailAuto);echo"</pre>";
            if(!empty($aTabaleauMailAuto)){
                $objTest->envoi_emailing($objClassFormList->aTabResultInsert['id'],$aTabaleauMailAuto[0]['id_emailing'],date('Y-m-d'));
            }
        }*/

       /*
        * Enregistrement  d'une propal
        */
       if(($prix_location_contact!=0 and $prix_location_contact!="") or ($fraistechnique_contact!=0 and $fraistechnique_contact!="")){

           $aTabParamPropal=array();
           $aTabParamPropal['prix_location_contact']=$prix_location_contact;
           $aTabParamPropal['idville']=$villemobilier;
           $aTabParamPropal['id_type_mobilier']=$id_type_mobilier;
           $aTabParamPropal['fraistechnique_contact']=$fraistechnique_contact;
           $aTabParamPropal['mobilie_contact']=$mobilie_contact;
           $aTabParamPropal['strategie_contact']=$strategie_contact;
           $aTabParamPropal['guid_contact']=$this->aTabResultInsert['guid'];

           $this->objBdd->enregistrement_propal_commercial($aTabParamPropal);

       }

        if($checktel=="ok"){
            $sRequete_insert_action="insert crm_action set date_action='".$dateinfo."',guid_user='".$guid_user."',type_action='tel',
			id_type_action='".$itemchecktel."',guid_contact='".$this->aTabResultInsert['guid']."',commentaire_action='".$commentchecktel."'";
            //echo $sRequete_insert_action."<br>";
            $this->objBdd->executionRequeteId($sRequete_insert_action);
        }
        $dateinfo=date('Y-m-d');
        if($checkmail=="ok"){

            $sRequete_insert_action="insert crm_action set date_action='".$dateinfo."',guid_user='".$guid_user."',type_action='mail',
			id_type_action='".$itemcheckmail."',guid_contact='".$this->aTabResultInsert['guid']."',commentaire_action='".$commentcheckmail."'";

            //echo $sRequete_insert_action."<br>";

            $this->objBdd->executionRequeteId($sRequete_insert_action);


        }

        if($checkbv=="ok"){
            $sRequete_insert_action="insert crm_action set date_action='".$dateinfo."',guid_user='".$guid_user."',type_action='bv',
			id_type_action='".$itemcheckbv."',guid_contact='".$this->aTabResultInsert['guid']."',commentaire_action='".$commentcheckbv."'";
            //echo $sRequete_insert_action."<br>";
            $this->objBdd->executionRequeteId($sRequete_insert_action);


        }

        if($chexkprochetap!="" and $chexkprochetap!="0"){

            $sRequete_insert_action="insert crm_action set date_action='".$dateprevision."',guid_user='".$guid_user."',type_action='".$chexkprochetap."',
			guid_contact='".$this->aTabResultInsert['guid']."',commentaire_action='".$commentprochaine."',programmation_action='a faire',heure_action='".$heureprevision."'";
            //echo $sRequete_insert_action."<br>";
            $this->objBdd->executionRequeteId($sRequete_insert_action);
        }


        //enregistrement de l'evolution du contact
        $sRequete_evolution_contact="insert crm_evolution_contact set date_evolution_contact=now(),id_type_statut='".$statut."',guid_user='".$guid_user."',
		guid_contact='".$this->aTabResultInsert['guid']."',id_type_qualite='".$qualite."',id_type_abandon='".$affcause."',statut_evolution_contact='ouvert'";
        $this->objBdd->executionRequeteId($sRequete_evolution_contact);

        return $aTableauRetour;
}


    public function post_form_update_fli_contact_crm(){

        $aTableauRetour['sMessage_Post']='';
        $aTableauRetour['bResult_Post']=true;

        $aVar = array( 'checktel', 'itemchecktel', 'dateinfo', 'commentchecktel','checkmail','itemcheckmail','commentcheckmail','actionevo'
        ,'checkbv','itemcheckbv','commentcheckbv','chexkprochetap','dateprevision','commentprochaine','heureprevision','statut','qualite','actionevo','affcause' );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $guid_user= class_fli::get_aData('guid_user');



        if(($prix_location_contact!=0 and $prix_location_contact!="") or ($fraistechnique_contact!=0 and $fraistechnique_contact!="")){

            $aTabParamPropal=array();
            $aTabParamPropal['prix_location_contact']=$prix_location_contact;
            $aTabParamPropal['idville']=$villemobilier;
            $aTabParamPropal['id_type_mobilier']=$id_type_mobilier;
            $aTabParamPropal['fraistechnique_contact']=$fraistechnique_contact;
            $aTabParamPropal['mobilie_contact']=$mobilie_contact;
            $aTabParamPropal['strategie_contact']=$strategie_contact;
            $aTabParamPropal['guid_contact']=$this->aTabResultInsert['guid'];

            $this->objBdd->enregistrement_propal_commercial($aTabParamPropal);

        }

        $dateinfo=date('Y-m-d');
        if($checktel=="ok"){
            $sRequete_insert_action="insert crm_action set date_action='".$dateinfo."',guid_user='".$guid_user."',type_action='tel',
			id_type_action='".$itemchecktel."',guid_contact='".$this->aTabResultUpdate['id']."',commentaire_action='".$commentchecktel."'";
            //echo $sRequete_insert_action."<br>";
            $this->objBdd->executionRequeteId($sRequete_insert_action);
        }

        if($checkmail=="ok"){

            $sRequete_insert_action="insert crm_action set date_action='".$dateinfo."',guid_user='".$guid_user."',type_action='mail',
			id_type_action='".$itemcheckmail."',guid_contact='".$this->aTabResultUpdate['id']."',commentaire_action='".$commentcheckmail."'";

            //echo $sRequete_insert_action."<br>";

            $this->objBdd->executionRequeteId($sRequete_insert_action);


        }

        if($checkbv=="ok"){
            $sRequete_insert_action="insert crm_action set date_action='".$dateinfo."',guid_user='".$guid_user."',type_action='bv',
			id_type_action='".$itemcheckbv."',guid_contact='".$this->aTabResultUpdate['id']."',commentaire_action='".$commentcheckbv."'";
            //echo $sRequete_insert_action."<br>";
            $this->objBdd->executionRequeteId($sRequete_insert_action);


        }

        if($chexkprochetap!="" and $chexkprochetap!="0"){

            $sRquete_update="update crm_action set programmation_action='fait',datemaj_action=now() where programmation_action='a faire' 
			 and date_action<='".date('Y-m-d')."' and guid_contact='".$this->aTabResultUpdate['id']."'";
            //echo $sRquete_update."<br>";
            $this->objBdd->executionRequeteId($sRquete_update);


            $sRequete_insert_action="insert crm_action set date_action='".$dateprevision."',guid_user='".$guid_user."',type_action='".$chexkprochetap."',
			guid_contact='".$this->aTabResultUpdate['id']."',commentaire_action='".$commentprochaine."',programmation_action='a faire',heure_action='".$heureprevision."'";
            //echo $sRequete_insert_action."<br>";
            $this->objBdd->executionRequeteId($sRequete_insert_action);
        }


        if($actionevo=="ok"){
            //*********************cloture toutes les anciennes evolutions
            $sRequete_update_cloture_evolution="update crm_evolution_contact set statut_evolution_contact='fermer' 
             where guid_contact='".$this->aTabResultUpdate['id']."'";
            $this->objBdd->executionRequeteId($sRequete_update_cloture_evolution);

            //energsitrement de l'evolution du contact
            $sRequete_evolution_contact="insert crm_evolution_contact set date_evolution_contact=now(),id_type_statut='".$statut."',guid_user='".$guid_user."',
			guid_contact='".$this->aTabResultUpdate['id']."',id_type_qualite='".$qualite."',id_type_abandon='".$affcause."',statut_evolution_contact='ouvert'";
            $this->objBdd->executionRequeteId($sRequete_evolution_contact);
        }

        return $aTableauRetour;
    }

    public function renvoi_affiche_cellule_textstrategie($objElement,$objValeurBddListe){

        $sMessage="";
        $sMessage=$this->objBdd->renvoi_dernier_stategie( $objValeurBddListe['guid_contact']['value']);
        $objValeurBddListe[$objElement['mapping_champ']]['value']=$sMessage;
        $aTableRetour['keys'] = $objElement['nom_variable'];
        //echo"";print_r($objValeurBddListe);echo"";
        $aTableRetour['value']=$objValeurBddListe[$objElement['mapping_champ']];
        return $aTableRetour;
    }

    public function renvoi_affiche_cellule_textetape($objElement,$objValeurBddListe){

        $sMessage="";
        $sMessage=$this->objBdd->renvoi_prochiane_etape( $objValeurBddListe['guid_contact']['value']);
        $objValeurBddListe[$objElement['mapping_champ']]['value']=$sMessage;
        $aTableRetour['keys'] = $objElement['nom_variable'];
        //echo"";print_r($objValeurBddListe);echo"";
        $aTableRetour['value']=$objValeurBddListe[$objElement['mapping_champ']];
        return $aTableRetour;
    }


    public function fli_contact_crm(){

        $aVar = array( 'checktel', 'itemchecktel', 'dateinfo', 'commentchecktel','checkmail','itemcheckmail','commentcheckmail','validation'
        ,'checkbv','itemcheckbv','commentcheckbv','chexkprochetap','dateprevision','commentprochaine','heureprevision','statut','qualite','actionevo','affcause',
            'dateprevision','dateinfo','datecrationfiche','id_contrat','id_produit',"bdebug");
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }
        $guid_user= class_fli::get_aData('guid_user');


        //BDD
        $this->sNomTable='client_contact';                            //string Nom de la table liée
        $this->sChampId='id_contact';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid='guid_contact';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique='supplogique_contact';                    //string Nom du champ supplogique dans la table liée
        //Booléen
        $this->bTraiteConnexion=true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bKeyMultiple=false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bDebugRequete=false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffDup=false;                            //bool bAffDup Permet de dupliquer une ligne
        $this->bAffTitre=true;                          //bool Permet d'afficher le titre de la liste
        $this->bAffFiche=false;                         //bool Permet d'afficher la liste (si aucun des autres droits vaut true)
        $this->bLabelCreationElem=true;                 //bool Permet d'afficher le bouton d'ajout
        $this->bAffMod=true;                           //bool Permet la modification
        $this->bAffRecapLigne=false;                     //bool  bAffRecapLigne Permet d'affiche dans unenouvelle fenetre le recap de la ligne
        $this->bAffSupp=true;                          //bool Permet la suppression
        $this->bBtnRetour=false;                        //bool Permet d'afficher le bouton 'Retour' de la liste
        $this->bPagination=true;                       //bool Permet l'affichage de la pagination
        $this->bAffAnnuler=true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->bAffListeQuandPasRecherche=true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->bRetourSpecifique=false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->bUseDelete=false;                       //bool Permet l'affichage de la pagination
        $this->bAffNombreResult=true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable=false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->bAffPrintBtn=false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug=false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bActiveFormSelect=false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup=false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->bRadioSelect=false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->bCheckboxSelect=false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire)
        $this->bCsv=false;                              //bool Permet d'afficher le bouton d'export en csv
        //Entier
        $this->iNombrePage=50;                          //int Nombre de lignes désirées par page
        //String
        $this->sFiltreliste='';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND')
        $this->sFiltrelisteOrderBy='';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->sTitreCreationForm='Enregistrement d\'un';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm='Enregistement réussie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sMessageDuplicationSucces='Duplication de la ligne reussie';                           // sMessageDuplicationSucces Message affiché lorsquela duplication de la ligne s'est déroulé avec succès
        $this->sMessageDuplicationError='Erreur pendant la duplication de la ligne';                          //string sMessageDuplicationError Message affiché lorsque la duplication de la ligne s'est déroulé avec uen erreur
        $this->sTitreModificationForm='Modification d\'un thème';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm='Modification reussie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->sMessageSupprElem='Suppression réussie';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm='Problème survenue pendant l\'enregistrement';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem='Aj';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche='Rechercher un';                      //string Titre de la section recherche
        $this->sTitreListe='Liste des contacts prospect';                          //string Titre de la liste
        $this->sTitreForm='Contact prospect';                           //string Titre du formulaire
        $this->sLabelNbrLigne='nombre(s) sur votre sélection';                       //string Texte à coté du nombre de lignes
        $this->sLabelRetourListe='';                    //string Texte du bouton 'Retour' sur la liste
        $this->sLabelFileRetourElem='';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->sUrlRetourConnexion='';                  //string Module-controleur-fonction spécifié pour le module de connexion à utiliser
        $this->sDirRetour='';                           //string Module-controleur-fonction spécifié pour le bouton 'Retour'
        $this->sRetourElemUrl='';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sDirpagination='';                       //string Le module-controleur-fonction pour la pagination quand l'url est spécifique
        $this->sUrlRetourSpecifique='';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sTypeFichier='';                         //string Le type de fichier attendu
        $this->sLienLigne='';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert='';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate='';              //string Script executé après un UPDATE
        $this->sSuiteRequeteInsert='';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate='';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sListeTpl='liste.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sFormulaireTpl='enreg_contact.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTplSortie='principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)
        $this->sListeFilsTpl='liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->sListeFitreNopDuplicate='';              //string sListeFitreNopDuplicate renvoi la liste des champs qu'on veut exclure de la duplication séparé par un point virgule
        //Array
        $this->aElement=array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->itemBoutonsForm=array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->aListeFils=array();                      //array Tableau contenant les données de la liste fils
        $this->aSelectSqlSuppl=array();                 //array Tableau Permettant de rajouter des champs dans le SELECT de la liste
        //Wizard
        $this->sParametreWizardListe='';                //string Les paramètres de la liste du wizard
        $this->sCategorieListe='';                      //string La catégorie de la liste lors du wizard
        $this->itemBoutonsListe=array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->aParametreWizardListe=array();           //array Tableau contenant les paramètres du wizard
        $this->aParamsNonSuivi=array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        //Peu utilisés
        $this->sDebugRequeteSelect='';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate='';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->sRetourValidationForm='liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne='';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm='liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sRetourAnnulationFormExterne='';         //string Permet de spécifier l'url de l'annulation
        $this->sRetourSuppressionFormExterne='';        //string Permet de spécifier l'url de retour après la suppression
        $this->sUrlRetourSuppressionFormExterne='';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sRetourListe=null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        //inforamtion sur le menu automatique
        $this->sMenuIntitule='Gestion Contact';
        $this->sMenuVisibile=true;
        $this->sMenuTarge=false;

        $this->chargement("crm","version1");                      //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module
        $_GET['guid_user']=$guid_user;

        $this->ajout_champ(array(
        'type_champ' => 'hidden',
        'mapping_champ' => 'guid_user', // le nom du champ dans la table selectionnee
        'nom_variable' => 'guid_user', // la valeur de l attribut "name" dans le formulaire
        'text_label' => '', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        'aff_form' => 'ok',
        'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|'
        'transfert_inter_module'=>'ok',
        'titre_inter_module'=>'',//si a ok active la récriture du titre
        'bbd_titre_inter_module'=>'',//le nom de la base de donné ou on va chercher l'info
        'champfiltre_titre_inter_module'=>'',// le chmap sur lequel on fait le filtre
        'champaffiche_titre_inter_module'=>'',//le champ ou la composition de champ a afficher
        'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
         ));

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'id_client', // le nom du champ dans la table selectionnee
            'nom_variable' => 'id_client', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Nom une société',
            'text_label' => 'Societé', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'alias_champ' => '', //alias du champ dans la table selectionnee
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'text_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'actif_jointure' => '', // cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche  'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '5', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => 'client', // la table liee pour ce champ
            'id_table_item' => 'id_client', // le champ de la table liee qui sert de cle primaire
            'alias_table_item' => 'alias_client', //Alias du de la table dans la jointure ;
            'affichage_table_item' => 'nom_client', // le champ de la table liee qui sert de label d affichage
            'alias_champ_item' => '',  //alias du champ item dans la base la requete générale pour champ select et ou selectdist
            'supplogique_table_item' => 'supplogique_client',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_pere_fils' => '', // si la liste deroulante a une hierachie le nom du champ père
            'table_item_aff_liste' => 'nop', // si la liste deroulante a une hierachie le nom du champ père
            'select_value_pere_base' => '', // si la liste deroulante a une hierachie valeurpar defaut du pere
            'select_autocomplete' => 'ok', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => 'tabfiltre[0]=nom_client|%<rech>%|like&', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'supprimer_br' => 'ok',
            'bdebug' => ""
        ));

        /* --------- $this->Ajout_champ() ici --------- */
        $aTmpList = array();
        $aTmpList["F"] = "Madame";
        $aTmpList["H"] ="Monsieur";

        $this->ajout_champ(array(
        	'type_champ' => 'select',
        	'mapping_champ' => 'civilite_contact', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'civilite_contact', // la valeur de l attribut "name" dans le formulaire
        	'text_label_filtre' => 'Saisir un nom',
        	'text_label' => 'Civilité', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'supprimer_br' => 'ok',
        	'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        	'lesitem'=>$aTmpList,
        	'bdebug' => ""

        ));



        $this->ajout_champ(array(
        	'type_champ' => 'text',
        	'mapping_champ' => 'nom_contact', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'nom_contact', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'Nom contact', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '40', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'html_editable_td'=>'',
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        	'supprimer_br' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'textnom',
            'mapping_champ' => 'nom_contact', // le nom du champ dans la table selectionnee
            'nom_variable' => 'nom_contact2', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Nom contact', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '40', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td'=>'',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => '', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'supprimer_br' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        $this->ajout_champ(array(
        	'type_champ' => 'text',
        	'mapping_champ' => 'prenom_contact', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'prenom_contact', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'Prenom', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '40', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'html_editable_td'=>'',
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
        	'type_champ' => 'text',
        	'mapping_champ' => 'tel_contact', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'tel_contact', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'Tel', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'html_editable_td'=>'',
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'supprimer_br' => 'ok',
        	'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'email_contact', // le nom du champ dans la table selectionnee
            'nom_variable' => 'email_contact', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Email', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '60', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td'=>'',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'supprimer_br' => '',
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
        	'type_champ' => 'text',
        	'mapping_champ' => 'code_postal', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'code_postal', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'Code postal', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'html_editable_td'=>'ok',
        	'fonction_javascript' => 'onkeyup="affiche_liste_generique(\'ville_cedex\',\'id_code_postal\',\'ville\',\'supplogique_ville\', \'tabfiltre[0]=code_postal|%<rech>%|like&tabfiltre[1]=ville|%<rech>%|like\',this.value,\'id_form_id_ville\');"', // ajout du javascript sur le champ
            'supprimer_br' => 'ok',
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'id_ville', // le nom du champ dans la table selectionnee
            'nom_variable' => 'id_ville', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => '',
            'text_label' => 'Ville', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'alias_champ' => '', //alias du champ dans la table selectionnee
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'actif_jointure' => '', // cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche  'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '5', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => 'ville_cedex', // la table liee pour ce champ
            'id_table_item' => 'id_code_postal', // le champ de la table liee qui sert de cle primaire
            'alias_table_item' => 'alias_ville', //Alias du de la table dans la jointure ;
            'affichage_table_item' => 'ville', // le champ de la table liee qui sert de label d affichage
            'alias_champ_item' => '',  //alias du champ item dans la base la requete générale pour champ select et ou selectdist
            'supplogique_table_item' => 'supplogique_ville',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_pere_fils' => '', // si la liste deroulante a une hierachie le nom du champ père
            'table_item_aff_liste' => 'nop', // si la liste deroulante a une hierachie le nom du champ père
            'select_value_pere_base' => '', // si la liste deroulante a une hierachie valeurpar defaut du pere
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'bdebug' => ""
        ));
        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'guid_user', // le nom du champ dans la table selectionnee
            'nom_variable' => 'guid_user', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => '',
            'text_label' => 'Créateur du contact', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => '', // faut il afficher le champ dans le formulaire 'ok'|''
            'alias_champ' => '', //alias du champ dans la table selectionnee
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'actif_jointure' => '', // cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche  'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => $this->sPrefixeDb .'users', // la table liee pour ce champ
            'id_table_item' => 'guid_user', // le champ de la table liee qui sert de cle primaire
            'alias_table_item' => 'alias_user', //Alias du de la table dans la jointure ;
            'affichage_table_item' => 'nom_user', // le champ de la table liee qui sert de label d affichage
            'alias_champ_item' => '',  //alias du champ item dans la base la requete générale pour champ select et ou selectdist
            'supplogique_table_item' => 'supplogique_user',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_pere_fils' => '', // si la liste deroulante a une hierachie le nom du champ père
            'select_value_pere_base' => '', // si la liste deroulante a une hierachie valeurpar defaut du pere
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'bdebug' => ""
        ));

        $this->ajout_champ(array(
        	'type_champ' => 'text',
        	'mapping_champ' => 'numero_rue_contact', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'numero_rue_contact', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'Numéro', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'html_editable_td'=>'ok',
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'supprimer_br' => 'ok',
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

       $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'id_typerue', // le nom du champ dans la table selectionnee
            'nom_variable' => 'id_typerue', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => '',
            'text_label' => 'Type voie', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'alias_champ' => '', //alias du champ dans la table selectionnee
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'actif_jointure' => '', // cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche  'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => 'typerue', // la table liee pour ce champ
            'id_table_item' => 'id_typerue', // le champ de la table liee qui sert de cle primaire
            'alias_table_item' => 'alias_typerueenreg', //Alias du de la table dans la jointure ;
            'affichage_table_item' => 'nom_typerue', // le champ de la table liee qui sert de label d affichage
            'alias_champ_item' => '',  //alias du champ item dans la base la requete générale pour champ select et ou selectdist
            'supplogique_table_item' => 'supplogique_typerue',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_pere_fils' => '', // si la liste deroulante a une hierachie le nom du champ père
            'select_value_pere_base' => '', // si la liste deroulante a une hierachie valeurpar defaut du pere
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'bdebug' => ""
        ));


          $this->ajout_champ(array(
                  'type_champ' => 'textarea',
                  'mapping_champ' => 'adresse_contact', // le nom du champ dans la table selectionnee
                  'nom_variable' => 'adresse_contact', // la valeur de l attribut "name" dans le formulaire
                  'text_label' => 'Adresse', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
                  'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
                  'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
                  'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
                  'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
                  'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
                  'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
                  'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
                  'size_champ' => '', // taille max du champ
                  'style' => '', // ajout du style sur le champ
                  'tableau_attribut' => array( 'cols' => '40', 'rows' => '5' ), // ajout d attributs sur le champ
                  'fonction_javascript' => '', // ajout du javascript sur le champ
                  'mess_erreur' => '', // message d erreur lorsque le controle n est pas valide
                  'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification 'ok'|''
                  'supprimer_br' => 'ok',
                  'wysiwyg' => '' // si le textarea est utilise en wysiwyg dans le formulaire
              ));



       if($this->sEtatForm=="insert") {

           $this->ajout_champ(array(
               'type_champ' => 'category',
               'mapping_champ' => 'categoryII', // le nom du champ dans la table selectionnee
               'nom_variable' => 'categoryII', // la valeur de l attribut "name" dans le formulaire
               'text_label' => 'Information sur la socièté si elle n\'existe pas ', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
               'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
               'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
               'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
               'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
               'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
               'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
               'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
               'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
               'size_champ' => '', // taille max du champ
               'style' => '', // ajout du style sur le champ
               'tableau_attribut' => '', // ajout d attributs sur le champ
               'html_editable_td' => 'ok',
               'fonction_javascript' => '', // ajout du javascript sur le champ
               'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
               'supprimer_br' => 'ok',
               'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
           ));


           $this->ajout_champ(array(
               'type_champ' => 'category',
               'mapping_champ' => 'categoryIIII', // le nom du champ dans la table selectionnee
               'nom_variable' => 'categoryIIII', // la valeur de l attribut "name" dans le formulaire
               'text_label' => '', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
               'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
               'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
               'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
               'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
               'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
               'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
               'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
               'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
               'size_champ' => '', // taille max du champ
               'style' => '', // ajout du style sur le champ
               'tableau_attribut' => '', // ajout d attributs sur le champ
               'html_editable_td' => 'ok',
               'fonction_javascript' => '', // ajout du javascript sur le champ
               'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
               'supprimer_br' => 'ok',
               'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
           ));

           $this->ajout_champ(array(
               'type_champ' => 'text',
               'mapping_champ' => 'Raisonsocial', // le nom du champ dans la table selectionnee
               'nom_variable' => 'Raisonsocial', // la valeur de l attribut "name" dans le formulaire
               'text_label' => 'Raison social si socièté n\'existe pas ', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
               'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
               'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
               'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
               'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
               'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
               'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
               'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
               'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
               'size_champ' => '', // taille max du champ
               'style' => '', // ajout du style sur le champ
               'tableau_attribut' => '', // ajout d attributs sur le champ
               'html_editable_td' => 'ok',
               'fonction_javascript' => '', // ajout du javascript sur le champ
               'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
               'supprimer_br' => 'ok',
               'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
           ));

           $this->ajout_champ(array(
               'type_champ' => 'text',
               'mapping_champ' => 'codepastalcree', // le nom du champ dans la table selectionnee
               'nom_variable' => 'codepastalcree', // la valeur de l attribut "name" dans le formulaire
               'text_label' => 'Code postal si socièté n\'existe pas ', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
               'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
               'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
               'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
               'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
               'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
               'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
               'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
               'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
               'size_champ' => '', // taille max du champ
               'style' => '', // ajout du style sur le champ
               'tableau_attribut' => '', // ajout d attributs sur le champ
               'html_editable_td' => 'ok',
               'fonction_javascript' => 'onkeyup="affiche_liste_generique(\'ville_cedex\',\'id_code_postal\',\'ville\',\'supplogique_ville\', \'tabfiltre[0]=code_postal|%<rech>%|like&tabfiltre[1]=ville|%<rech>%|like\',this.value,\'id_form_villecree\');"', // ajout du javascript sur le champ
               'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
               'supprimer_br' => '',
               'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
           ));

           $this->ajout_champ(array(
               'type_champ' => 'select',
               'mapping_champ' => 'villecree', // le nom du champ dans la table selectionnee
               'nom_variable' => 'villecree', // la valeur de l attribut "name" dans le formulaire
               'text_label_filtre' => '',
               'text_label' => 'Ville si la société n\'existe pas', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
               'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
               'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
               'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
               'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
               'alias_champ' => '', //alias du champ dans la table selectionnee
               'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
               'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
               'actif_jointure' => '', // cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche  'ok'|''
               'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
               'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
               'size_champ' => '5', // taille max du champ
               'style' => '', // ajout du style sur le champ
               'tableau_attribut' => '', // ajout d attributs sur le champ
               'fonction_javascript' => '', // ajout du javascript sur le champ
               'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
               'traite_sql' => '', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
               'table_item' => 'ville_cedex', // la table liee pour ce champ
               'id_table_item' => 'id_code_postal', // le champ de la table liee qui sert de cle primaire
               'alias_table_item' => 'alias_ville', //Alias du de la table dans la jointure ;
               'affichage_table_item' => 'ville', // le champ de la table liee qui sert de label d affichage
               'alias_champ_item' => '',  //alias du champ item dans la base la requete générale pour champ select et ou selectdist
               'supplogique_table_item' => 'supplogique_ville',
               'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
               'select_pere_fils' => '', // si la liste deroulante a une hierachie le nom du champ père
               'table_item_aff_liste' => 'nop', // si la liste deroulante a une hierachie le nom du champ père
               'select_value_pere_base' => '', // si la liste deroulante a une hierachie valeurpar defaut du pere
               'select_autocomplete' => '', //si on fait de l'autocompletion
               'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
               'supprimer_br' => 'ok',
               'bdebug' => ""
           ));
           $this->ajout_champ(array(
               'type_champ' => 'select',
               'mapping_champ' => 'pays_client', // le nom du champ dans la table selectionnee
               'nom_variable' => 'pays_client', // la valeur de l attribut "name" dans le formulaire
               'text_label_filtre' => 'Pays',
               'text_label' => 'Pays', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
               'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
               'valeur_variable' => '73', // la valeur par defaut dans le formulaire lors de la creation
               'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
               'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
               'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
               'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
               'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
               'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
               'size_champ' => '', // taille max du champ
               'style' => '', // ajout du style sur le champ
               'tableau_attribut' => '', // ajout d attributs sur le champ
               'fonction_javascript' => '', // ajout du javascript sur le champ
               'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
               'traite_sql' => '', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
               'table_item' => 'pays_prefixe', // la table liee pour ce champ
               'id_table_item' => 'id_pays', // le champ de la table liee qui sert de cle primaire
               'affichage_table_item' => 'nom_pays', // le champ de la table liee qui sert de label d affichage
               'supplogique_table_item' => 'supplogique_pays',
               'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
               'select_autocomplete' => '', //si on fait de l'autocompletion
               'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
               'bdebug' => ""

           ));


           $this->ajout_champ(array(
               'type_champ' => 'text',
               'mapping_champ' => 'telcree', // le nom du champ dans la table selectionnee
               'nom_variable' => 'telcree', // la valeur de l attribut "name" dans le formulaire
               'text_label' => 'Tel si socièté n\'existe pas ', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
               'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
               'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
               'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
               'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
               'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
               'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
               'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
               'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
               'size_champ' => '', // taille max du champ
               'style' => '', // ajout du style sur le champ
               'tableau_attribut' => '', // ajout d attributs sur le champ
               'html_editable_td' => 'ok',
               'fonction_javascript' => '', // ajout du javascript sur le champ
               'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
               'supprimer_br' => 'ok',
               'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
           ));

          $this->ajout_champ(array(
                    'type_champ' => 'select',
                    'mapping_champ' => 'typerue', // le nom du champ dans la table selectionnee
                    'nom_variable' => 'typerue', // la valeur de l attribut "name" dans le formulaire
                    'text_label_filtre' => '',
                    'text_label' => 'Type voie', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
                    'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
                    'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
                    'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
                    'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
                    'alias_champ' => '', //alias du champ dans la table selectionnee
                    'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
                    'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
                    'actif_jointure' => '', // cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche  'ok'|''
                    'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
                    'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
                    'size_champ' => '', // taille max du champ
                    'style' => '', // ajout du style sur le champ
                    'tableau_attribut' => '', // ajout d attributs sur le champ
                    'fonction_javascript' => '', // ajout du javascript sur le champ
                    'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
                    'traite_sql' => '', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
                    'table_item' => 'typerue', // la table liee pour ce champ
                    'id_table_item' => 'id_typerue', // le champ de la table liee qui sert de cle primaire
                    'alias_table_item' => 'alias_typerue', //Alias du de la table dans la jointure ;
                    'affichage_table_item' => 'nom_typerue', // le champ de la table liee qui sert de label d affichage
                    'alias_champ_item' => '',  //alias du champ item dans la base la requete générale pour champ select et ou selectdist
                    'supplogique_table_item' => 'supplogique_typerue',
                    'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
                    'select_pere_fils' => '', // si la liste deroulante a une hierachie le nom du champ père
                    'select_value_pere_base' => '', // si la liste deroulante a une hierachie valeurpar defaut du pere
                    'select_autocomplete' => '', //si on fait de l'autocompletion
                    'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
                    'bdebug' => ""
                ));


            $this->ajout_champ(array(
            	'type_champ' => 'text',
            	'mapping_champ' => 'numrue', // le nom du champ dans la table selectionnee
            	'nom_variable' => 'numrue', // la valeur de l attribut "name" dans le formulaire
            	'text_label' => 'Numéro ', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            	'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            	'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            	'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            	'size_champ' => '4', // taille max du champ
            	'style' => '', // ajout du style sur le champ
            	'tableau_attribut' => '', // ajout d attributs sur le champ
            	'html_editable_td'=>'ok',
            	'fonction_javascript' => '', // ajout du javascript sur le champ
            	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
                'supprimer_br' => 'ok',
            	'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            ));


           $this->ajout_champ(array(
               'type_champ' => 'textarea',
               'mapping_champ' => 'adressecree', // le nom du champ dans la table selectionnee
               'nom_variable' => 'adressecree', // la valeur de l attribut "name" dans le formulaire
               'text_label' => 'adresse si la société n\'existe pas', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
               'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
               'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
               'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
               'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
               'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
               'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
               'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
               'size_champ' => '', // taille max du champ
               'style' => '', // ajout du style sur le champ
               'tableau_attribut' => array( 'cols' => '60', 'rows' => '5' ), // ajout d attributs sur le champ
               'fonction_javascript' => '', // ajout du javascript sur le champ
               'mess_erreur' => '', // message d erreur lorsque le controle n est pas valide
               'traite_sql' => '', // faut il traite le champ dans les divers requetes sql selection, insertion, modification 'ok'|''
               'wysiwyg' => '' // si le textarea est utilise en wysiwyg dans le formulaire
           ));

           $this->ajout_champ(array(
               'type_champ' => 'category',
               'mapping_champ' => 'categoryIII', // le nom du champ dans la table selectionnee
               'nom_variable' => 'categoryIII', // la valeur de l attribut "name" dans le formulaire
               'text_label' => '', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
               'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
               'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
               'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
               'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
               'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
               'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
               'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
               'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
               'size_champ' => '', // taille max du champ
               'style' => '', // ajout du style sur le champ
               'tableau_attribut' => '', // ajout d attributs sur le champ
               'html_editable_td' => 'ok',
               'fonction_javascript' => '', // ajout du javascript sur le champ
               'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
               'supprimer_br' => 'ok',
               'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
           ));
       }

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'id_nature_contact', // le nom du champ dans la table selectionnee
            'nom_variable' => 'id_nature_contact', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => '',
            'text_label' => 'Nature 1er contact', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'alias_champ' => '', //alias du champ dans la table selectionnee
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'actif_jointure' => '', // cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche  'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => 'crm_item', // la table liee pour ce champ
            'id_table_item' => 'id_item', // le champ de la table liee qui sert de cle primaire
            'alias_table_item' => 'alias_nature', //Alias du de la table dans la jointure ;
            'affichage_table_item' => 'nom_item', // le champ de la table liee qui sert de label d affichage
            'alias_champ_item' => '',  //alias du champ item dans la base la requete générale pour champ select et ou selectdist
            'supplogique_table_item' => 'filtre_item=\'naturecontact\' and supplogique_item',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_pere_fils' => '', // si la liste deroulante a une hierachie le nom du champ père
            'table_item_aff_liste' => '', // si la liste deroulante a une hierachie le nom du champ père
            'select_value_pere_base' => '', // si la liste deroulante a une hierachie valeurpar defaut du pere
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '  ', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'supprimer_br' => 'ok',
            'bdebug' => ""
        ));

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'id_operation_contact', // le nom du champ dans la table selectionnee
            'nom_variable' => 'id_operation_contact', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => '',
            'text_label' => 'Opération', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'alias_champ' => '', //alias du champ dans la table selectionnee
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'actif_jointure' => '', // cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche  'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => 'crm_item', // la table liee pour ce champ
            'id_table_item' => 'id_item', // le champ de la table liee qui sert de cle primaire
            'alias_table_item' => 'alias_operation', //Alias du de la table dans la jointure ;
            'affichage_table_item' => 'nom_item', // le champ de la table liee qui sert de label d affichage
            'alias_champ_item' => '',  //alias du champ item dans la base la requete générale pour champ select et ou selectdist
            'supplogique_table_item' => 'filtre_item=\'operation\' and supplogique_item',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_pere_fils' => '', // si la liste deroulante a une hierachie le nom du champ père
            'table_item_aff_liste' => '', // si la liste deroulante a une hierachie le nom du champ père
            'select_value_pere_base' => '', // si la liste deroulante a une hierachie valeurpar defaut du pere
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'supprimer_br' => '',
            'bdebug' => ""
        ));


          $this->ajout_champ(array(
              'type_champ' => 'textarea',
              'mapping_champ' => 'qcpq_contact', // le nom du champ dans la table selectionnee
              'nom_variable' => 'qcpq_contact', // la valeur de l attribut "name" dans le formulaire
              'text_label' => 'QQQOCCP', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
              'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
              'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
              'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
              'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
              'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
              'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
              'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
              'size_champ' => '', // taille max du champ
              'style' => '', // ajout du style sur le champ
              'tableau_attribut' => array( 'cols' => '40', 'rows' => '10' ), // ajout d attributs sur le champ
              'fonction_javascript' => '', // ajout du javascript sur le champ
              'mess_erreur' => '', // message d erreur lorsque le controle n est pas valide
              'traite_sql' => '', // faut il traite le champ dans les divers requetes sql selection, insertion, modification 'ok'|''
              'supprimer_br' => 'ok',
              'wysiwyg' => '' // si le textarea est utilise en wysiwyg dans le formulaire
          ));
        $this->ajout_champ(array(
            'type_champ' => 'category',
            'mapping_champ' => 'propalII', // le nom du champ dans la table selectionnee
            'nom_variable' => 'propalII', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Proposition commercial', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td' => 'ok',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'supprimer_br' => 'ok',
            'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        $this->ajout_champ(array(
            'type_champ' => 'category',
            'mapping_champ' => 'propalI', // le nom du champ dans la table selectionnee
            'nom_variable' => 'propalI', // la valeur de l attribut "name" dans le formulaire
            'text_label' => '', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td' => 'ok',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'supprimer_br' => 'ok',
            'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'recheville', // le nom du champ dans la table selectionnee
            'nom_variable' => 'codepastalcree', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'rechercher ville mobilier ', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td' => 'ok',
            'fonction_javascript' => 'onkeyup="affiche_liste_generique(\'ville_cedex\',\'id_code_postal\',\'ville\',\'supplogique_ville\', \'tabfiltre[0]=code_postal|%<rech>%|like&tabfiltre[1]=ville|%<rech>%|like\',this.value,\'id_form_villemobilier\');"', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'supprimer_br' => '',
            'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'villemobilier', // le nom du champ dans la table selectionnee
            'nom_variable' => 'villemobilier', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => '',
            'text_label' => 'Ville mobilier', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'alias_champ' => '', //alias du champ dans la table selectionnee
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'actif_jointure' => '', // cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche  'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '5', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => '', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => 'ville_cedex', // la table liee pour ce champ
            'id_table_item' => 'id_code_postal', // le champ de la table liee qui sert de cle primaire
            'alias_table_item' => 'alias_ville', //Alias du de la table dans la jointure ;
            'affichage_table_item' => 'ville', // le champ de la table liee qui sert de label d affichage
            'alias_champ_item' => '',  //alias du champ item dans la base la requete générale pour champ select et ou selectdist
            'supplogique_table_item' => 'supplogique_ville',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_pere_fils' => '', // si la liste deroulante a une hierachie le nom du champ père
            'table_item_aff_liste' => 'nop', // si la liste deroulante a une hierachie le nom du champ père
            'select_value_pere_base' => '', // si la liste deroulante a une hierachie valeurpar defaut du pere
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'supprimer_br' => '',
            'bdebug' => ""
        ));



        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'id_type_mobilier', // le nom du champ dans la table selectionnee
            'nom_variable' => 'id_type_mobilier', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => '',
            'text_label' => 'Type Mobilier', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'alias_champ' => '', //alias du champ dans la table selectionnee
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'actif_jointure' => '', // cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche  'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => '', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => 'ric_mobilier_type', // la table liee pour ce champ
            'id_table_item' => 'id_mobilier_type', // le champ de la table liee qui sert de cle primaire
            'alias_table_item' => 'alias_typemobilier', //Alias du de la table dans la jointure ;
            'affichage_table_item' => 'libelle_type_mobilier', // le champ de la table liee qui sert de label d affichage
            'alias_champ_item' => '',  //alias du champ item dans la base la requete générale pour champ select et ou selectdist
            'supplogique_table_item' => ' supplogique_mobilier_type',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_pere_fils' => '', // si la liste deroulante a une hierachie le nom du champ père
            'table_item_aff_liste' => '', // si la liste deroulante a une hierachie le nom du champ père
            'select_value_pere_base' => '', // si la liste deroulante a une hierachie valeurpar defaut du pere
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'supprimer_br' => 'ok',
            'bdebug' => ""
        ));
        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'mobilie_contact', // le nom du champ dans la table selectionnee
            'nom_variable' => 'mobilie_contact', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'N° mobilier', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td'=>'ok',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'supprimer_br' => '',
            'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'prix_location_contact', // le nom du champ dans la table selectionnee
            'nom_variable' => 'prix_location_contact', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Prix location <br>(si différent de zéro on crée la propal', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td'=>'ok',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'supprimer_br' => 'ok',
            'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
        	'type_champ' => 'text',
        	'mapping_champ' => 'fraistechnique_contact', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'fraistechnique_contact', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'Frais technique<br> (si différent de zéro on crée la propal)', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'html_editable_td'=>'ok',
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'supprimer_br' => 'ok',
        	'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));



          $this->ajout_champ(array(
              'type_champ' => 'textarea',
              'mapping_champ' => 'strategie_contact', // le nom du champ dans la table selectionnee
              'nom_variable' => 'strategie_contact', // la valeur de l attribut "name" dans le formulaire
              'text_label' => 'Commentaire', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
              'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
              'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
              'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
              'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
              'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
              'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
              'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
              'size_champ' => '', // taille max du champ
              'style' => '', // ajout du style sur le champ
              'tableau_attribut' => array( 'cols' => '60', 'rows' => '5' ), // ajout d attributs sur le champ
              'fonction_javascript' => '', // ajout du javascript sur le champ
              'mess_erreur' => '', // message d erreur lorsque le controle n est pas valide
              'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification 'ok'|''
              'wysiwyg' => '' // si le textarea est utilise en wysiwyg dans le formulaire
          ));


          $this->ajout_champ(array(
          	'type_champ' => 'textstrategie',
          	'mapping_champ' => 'id_type_mobilier', // le nom du champ dans la table selectionnee
          	'nom_variable' => 'id_stategie', // la valeur de l attribut "name" dans le formulaire
          	'text_label' => 'Stratégie', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
          	'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
          	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
          	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
          	'aff_form' => '', // faut il afficher le champ dans le formulaire 'ok'|''
          	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
          	'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
          	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
          	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
          	'size_champ' => '', // taille max du champ
          	'style' => '', // ajout du style sur le champ
          	'tableau_attribut' => '', // ajout d attributs sur le champ
          	'html_editable_td'=>'ok',
          	'fonction_javascript' => '', // ajout du javascript sur le champ
          	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
          	'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
          ));



        $this->ajout_champ(array(
            'type_champ' => 'textetape',
            'mapping_champ' => 'id_type_mobilier', // le nom du champ dans la table selectionnee
            'nom_variable' => 'id_prochaine_etape', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Prochaine étape', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => '', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td'=>'ok',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));




        /* --------- fin $this->Ajout_champ() --------- */


        $bnouveau=true;
        $bactionmodif=true;
        $aEmailing=array();
        $aTabMenuTacheAFaire=array();
        $aTabEvolution=array();
        $id_user="";
//*****************liste check tel**************
        $sRequete_nbrenfant=" select nom_item,id_item from  crm_item where supplogique_item='N' and filtre_item='checktel' order by position_item,nom_item";
        $aTabChecktel= $this->objBdd->renvoi_info_requete($sRequete_nbrenfant);
        $this->objSmarty->assign('aTabChecktel', $aTabChecktel);

//*****************liste mail**************
        $sRequete_nbrenfant=" select nom_item,id_item from  crm_item where supplogique_item='N' and filtre_item='checkmail' order by position_item,nom_item";
        $aTabCheckMail= $this->objBdd->renvoi_info_requete($sRequete_nbrenfant);
        $this->objSmarty->assign('aTabCheckMail', $aTabCheckMail);

//*****************liste check bv**************
        $sRequete_nbrenfant=" select nom_item,id_item from  crm_item where supplogique_item='N' and filtre_item='checkbv' order by position_item,nom_item";
        $aTabCheckBv= $this->objBdd->renvoi_info_requete($sRequete_nbrenfant);
        $this->objSmarty->assign('aTabCheckBv', $aTabCheckBv);

//*****************liste statut**************
        $sRequete_nbrenfant=" select nom_item,id_item from  crm_item where supplogique_item='N' and filtre_item='statut' order by position_item,nom_item";
        $aTabStatut= $this->objBdd->renvoi_info_requete($sRequete_nbrenfant);
        $this->objSmarty->assign('aTabStatut', $aTabStatut);

        //*****************liste Qualité**************
                $sRequete_nbrenfant=" select nom_item,id_item from  crm_item where supplogique_item='N' and filtre_item='qualite' order by position_item,nom_item";
                $aTabQualite= $this->objBdd->renvoi_info_requete($sRequete_nbrenfant);
                $this->objSmarty->assign('aTabQualite', $aTabQualite);
   /*
        //*****************liste cause du abandon**************
                $sRequete_nbrenfant=" select nom_item,id_item from  crm_item where supplogique_item='N' and filtre_item='causeadandon' order by position_item,nom_item";
                $aTabcauseabandon= $objClassGenerique->renvoi_info_requete($sRequete_nbrenfant);
                $this->objSmarty->assign('aTabcauseabandon', $aTabcauseabandon);
                $aEmailing=array();
                */
        $this->objSmarty->assign('bactionmodif', $bactionmodif);
//echo"<pre>";print_r($this->renvoi_formulaire());echo"</pre>";
        /*$ataleauliste = $this->renvoi_formulaire();
        $count=0;
        foreach ($ataleauliste as $valeur) {
            # code...
            echo $count."=>".
        }
        */
        $sTextImprimer="Télécharger une fiche à remplir";

        if($this->obtenir_id()){
            $sTextImprimer="Version imprimable";
        }


        $this->objSmarty->assign('sTextImprimer', $sTextImprimer);

        if($dateinfo=="")
            $dateinfo=date('Y-m-d');
//echo"<pre>";print_r($atableaudecalage);echo"</pre>";

        $idecalage = 0;

        if($dateprevision=="")
            $dateprevision=date('Y-m-d',mktime(0,0,0,date('m'),date('d')+$idecalage,date('Y')));

        $this->objSmarty->assign('dateinfo', $dateinfo);
        $this->objSmarty->assign('dateprevision', $dateprevision);

        $aTabMenuTacheAFaire=array();
        $aTabSuvitache=array();
        $aTabEvolution=array();

        $bMessageWarmingForm=false;
        $sMessageWarmingForm="";

        if($this->obtenir_id()){

            $aTabMenuTacheAFaire=$this->objBdd->liste_tache($this->obtenir_id(),"a faire",0,50,$bdebug);
            $aTabSuvitache=$this->objBdd->liste_tache($this->obtenir_id(),"fait",0,50,$bdebug);
            $aTabEvolution = $this->objBdd->renvoi_evolution_contact($this->obtenir_id());
            $aTabPropalaTabPropal = $this->objBdd->renvoi_propal_contact($this->obtenir_id());

            if($validation=="env"){
                $this->objBdd->envoi_emailing($this->obtenir_id(),$idemailing,date('Y-m-d'));
                $sMessageWarmingForm="Votre Emailing a bien été envoyé";
                $bMessageWarmingForm=true;

            }

            if($validation=="blq"){
                $this->objBdd->bloquer_envoi_emailing($this->obtenir_id(),$idemailing,date('Y-m-d'));
                $sMessageWarmingForm="L'envoi de votre Emailing a bien été bloqué";
                $bMessageWarmingForm=true;
            }

        }


       /* if($this->obtenir_id()){
            $aEmailing = $objTest->emailing_programmation($this->obtenir_id());
            //echo"<pre>";print_r($aEmailing);echo"</pre>";
        }
       */

        $this->objSmarty->assign('aEmailing', $aEmailing);
//recupération des action a faire sur ce commmercial

        $this->objSmarty->assign('datecrationfiche', $datecrationfiche);
        $this->objSmarty->assign('aTabEvolution', $aTabEvolution);

        $this->objSmarty->assign('aTabMenuTacheAFaire', $aTabMenuTacheAFaire);
        $this->objSmarty->assign('aTabSuvitache', $aTabSuvitache);
        $this->objSmarty->assign('id_user', $id_user);


        $this->objSmarty->assign('bconfig', false);
        $this->objSmarty->assign('idouvrepress', 22);
        $this->objSmarty->assign('idouvrecauseadandon', 59);



        $this->objSmarty->assign('bnouveau', $bnouveau);
        $this->objSmarty->assign('sMessageWarmingForm', $sMessageWarmingForm);
        $this->objSmarty->assign('bMessageWarmingForm', $bMessageWarmingForm);
        
        
        //echo"<pre>";print_r($this->renvoi_formulaire());echo"</pre>";

        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli
        
        return $aTabTpl;
    }
}