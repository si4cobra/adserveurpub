<?php

/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 08/12/2017
 * Time: 17:38
 */
class mod_crm extends mod_form_list
{

    /**
     * fonction renvvoi liste ds tache
     * @author Danon Gnakouri
     * @param email
     * @param motdepass mot de pass
     * @param $bdd mot de passe
     * @since 3.2
     * @return la connexion
     */

    public function liste_tache($idcontat,$filtre,$idebut,$idnombre,$bdebug=false){

        $aTableauRetour =array();

        $sRequete_liste_action="select type_action,
          commentaire_action,
          date_format(date_action,'%d/%m/%Y') as ladate,
          nom_contact,prenom_contact, nom_item 
          from crm_action 
	      left join  client_contact on client_contact.guid_contact = crm_action.guid_contact
	     left join crm_item on crm_item.id_item = crm_action.id_type_action
	    where supploqique_action='N' and client_contact.guid_contact='$idcontat' ";

        if($filtre!=""){
            $sRequete_liste_action.=" and programmation_action='$filtre'";
        }


        $sRequete_liste_action.=" order by date_action DESC limit $idebut,$idnombre";

        if($bdebug)
            echo $sRequete_liste_action."<br>";

        $aTableauListe  = $this->renvoi_info_requete($sRequete_liste_action);
        $i=0;

        if(!empty($aTableauListe)){
            foreach ($aTableauListe as $valeur) {
                $u=0;
                $tmp=array();
                $tmp[$u]['nom']=$valeur['ladate'];
                $u++;
                $tmp[$u]['nom']=$valeur['type_action'];
                $u++;
                $tmp[$u]['nom']=$valeur['nom_item'];
                $u++;
                $tmp[$u]['nom']=$valeur['commentaire_action'];
                $u++;

                $aTableauRetour[$i]['liste'] =$tmp;
                $i++;

            }
        }

        //echo"<pre>";print_r($aTableauRetour);echo"</pre>";

        return $aTableauRetour;

    }

    /**
     * fonction renvvoi liste evolution qualifaction contact
     * @author Danon Gnakouri
     * @param email
     * @param motdepass mot de pass
     * @param $bdd mot de passe
     * @since 3.2
     * @return la connexion
     */
    function renvoi_evolution_contact($idcontat){

        $aTableauRetour =array();

        $sRequete_liste ="select date_format(date_evolution_contact,'%d/%m/%Y %H:%i') as ladate,
		statut.nom_item as nomstatut,
		qualite.nom_item as qualitestatut,
		abandon.nom_item as abandonstatut
		 from crm_evolution_contact
		left join crm_item as statut on statut.id_item = crm_evolution_contact.id_type_statut
		left join crm_item as qualite on qualite.id_item = crm_evolution_contact.id_type_qualite
		left join crm_item as abandon on abandon.id_item = crm_evolution_contact.id_type_abandon
		where guid_contact='$idcontat' and supplogique_evolution_contact='N' order by date_evolution_contact DESC";

        $aTableauListe  = $this->renvoi_info_requete($sRequete_liste);
        $i=0;

        if(!empty($aTableauListe)){
            foreach ($aTableauListe as $valeur) {
                $u=0;
                $tmp=array();
                $tmp[$u]['nom']=$valeur['ladate'];
                $u++;
                $tmp[$u]['nom']=$valeur['nomstatut'];
                $u++;
                $tmp[$u]['nom']=$valeur['qualitestatut'];
                $u++;
                $tmp[$u]['nom']=$valeur['abandonstatut'];
                $u++;

                $aTableauRetour[$i]['liste'] =$tmp;
                $i++;

            }
        }

        return $aTableauRetour;

    }

    /**
     * Renvoi la dernier stratégie entregistré sur ce contact
     * @param $guidcontact
     * @return string
     */
    public function renvoi_dernier_stategie($guidcontact){

          $sStrategie="";
          $sRequete_liste_action="select type_action,
          commentaire_action,
          date_format(date_action,'%d/%m/%Y') as ladate,
          nom_contact,prenom_contact, nom_item 
          from crm_action 
	      left join  client_contact on client_contact.guid_contact = crm_action.guid_contact
	      left join crm_item on crm_item.id_item = crm_action.id_type_action
	      Where supploqique_action='N' and client_contact.guid_contact='$guidcontact' 
	      and programmation_action<>'a faire' order by date_action DESC limit 0,1";

          $aTableauListe  = $this->renvoi_info_requete($sRequete_liste_action);

          if(!empty($aTableauListe)){
              $sStrategie= $aTableauListe[0]['ladate']." ".$aTableauListe[0]['nom_item']." <br>".$aTableauListe[0]['commentaire_action'];
          }
          return $sStrategie;
    }

    /**
     * renvoi le parochaine etape programmée sur ce contact
     * @param $guidcontact
     * @return string
     */
    public function renvoi_prochiane_etape($guidcontact){
        $sStrategie="";
        $sRequete_liste_action="select type_action,
          commentaire_action,
          date_format(date_action,'%d/%m/%Y') as ladate,
          nom_contact,prenom_contact, nom_item 
          from crm_action 
	      left join  client_contact on client_contact.guid_contact = crm_action.guid_contact
	      left join crm_item on crm_item.id_item = crm_action.id_type_action
	      Where supploqique_action='N' and client_contact.guid_contact='$guidcontact' 
	      and programmation_action='a faire' order by date_action DESC limit 0,1";

        $aTableauListe  = $this->renvoi_info_requete($sRequete_liste_action);

        if(!empty($aTableauListe)){
            $sStrategie= $aTableauListe[0]['ladate']." ".$aTableauListe[0]['nom_item']." <br>".$aTableauListe[0]['commentaire_action'];
        }
        return $sStrategie;
    }


    /**
     * fonction enregistrement propositon commercial
     * @author Danon Gnakouri
     * @param email
     * @param motdepass mot de pass
     * @param $bdd mot de passe
     * @since 3.2
     * @return la connexion
     */
    public function enregistrement_propal_commercial($aTabParam){

        $sRequete_enreg_propal="INSERT  crm_propal_client set
        prix_proapl_client='".addslashes($aTabParam['prix_location_contact'])."',
        id_ville='".addslashes($aTabParam['idville'])."',
        type_mobilier='".addslashes($aTabParam['id_type_mobilier'])."',
        fraistechnique_propal_client='".addslashes($aTabParam['fraistechnique_contact'])."',
        num_mobillier_propal_client='".addslashes($aTabParam['mobilie_contact'])."',
        comment_propal_client='".addslashes($aTabParam['strategie_contact'])."',
        datepropostion_propal_client=now(),
        guid_contact='".$aTabParam['guid_contact']."',
        guid_propal_client='".class_helper::guid()."'";

        $this->execute_requete($sRequete_enreg_propal);

    }

    public function renvoi_propal_contact($guidcontact,$bdebug=false){

        $sRequete="SELECT   ville,
        fraistechnique_propal_client,
        prix_proapl_client,
        comment_propal_client,
        num_mobillier_propal_client,
        DATE_FORMAT(datepropostion_propal_client,'%d/%M/%Y') as datenereg,
        num_mobillier_propal_client,
        libelle_type_mobilier,
        guid_contact
        FROM  crm_propal_client
        left join ville_cedex on ville_cedex. = crm_propal_client.id_ville
        LEFT JOIN ric_mobilier_type on ric_mobilier_type.id_mobilier_type = crm_propal_client.type_mobilier
        WHERE guid_contact='".$guidcontact."' and supplogique_propal_client='N'";

        if($bdebug)
            echo $sRequete."<br>";

        $aTableauListe  = $this->renvoi_info_requete($sRequete);
        $i = 0;

        if(!empty($aTableauListe)) {
            foreach( $aTableauListe as $valeur ) {
                $u = 0;
                $tmp = array();

                    $tmp[$u]['nom'] = $valeur['datenereg'];
                    $u++;
                    $tmp[$u]['nom'] = $valeur['ville'] ;
                    $u++;
                    $tmp[$u]['nom'] = $valeur['libelle_type_mobilier'];
                    $u++;
                    $tmp[$u]['nom'] = $valeur['num_mobillier_propal_client'];
                    $u++;
                    $tmp[$u]['nom'] = $valeur['prix_proapl_client'];
                    $u++;
                    $tmp[$u]['nom'] = $valeur['fraistechnique_propal_client'];
                    $u++;
                    $tmp[$u]['nom'] = $valeur['comment_propal_client'];
                    $u++;
                    $aTableauRetour[$i]['liste'] =$tmp;
                    $aTableauRetour[$i]['id'] =$valeur['guid_contact'];
                    $i++;

            }
        }

        return $aTableauRetour;

    }

    /**
     * fonction renvvoi liste ds tache de l'utilisateur
     * @author Danon Gnakouri
     * @param email
     * @param motdepass mot de pass
     * @param $bdd mot de passe
     * @since 3.2
     * @return la connexion
     */

    public function liste_tache_user($filtre,$idebut,$idnombre,$guiduser,$filtreplus,$affichage,$triprogramme='',$tricommercial='',$tridatedebut='',$tridatefin='',$badmin=false,$bdebug=false){

        $aTableauRetour =array();

        $sRequete_liste_action="select type_action,
        commentaire_action,
        date_format(date_action,'%d/%m/%Y') as ladate,
        nom_contact,
        prenom_contact	, 
        typeaction.nom_item as nom_item
	   ,client_contact.guid_contact,
	   nom_client,
	   date_format(heure_action,'%H:%i') as lheure,
	   typestatut.nom_item as nomstatut,
	   nom_user,
	   advnaj_users.guid_user
        from crm_action 
        inner join  client_contact on  client_contact.guid_contact = crm_action.guid_contact
        left join advnaj_users on advnaj_users.guid_user = client_contact.guid_user
        left join client on client.id_client =  client_contact.id_client
        left join crm_item as typeaction on typeaction.id_item = crm_action.id_type_action
        left join crm_evolution_contact on crm_evolution_contact.guid_contact = client_contact.guid_contact and statut_evolution_contact='ouvert'
        left join  crm_item as typestatut on typestatut.id_item = crm_evolution_contact.id_type_statut
        where supploqique_action='N' ";

        $sordre="DESC";

        if($guiduser!=""){
            if($badmin){
                /* $sRequete_liste_action.="  AND infobysms_useralert.id_programme
                IN (SELECT infobysms_lienuserprogramme.id_programme FROM infobysms_lienuserprogramme
                    WHERE accessite_lienprogramme='Y' AND supplogique_lienuserprogrammme='N' AND infobysms_lienuserprogramme.id_user='".$this->id_user."'
                    and infobysms_lienuserprogramme.id_programme<>'0')";*/

              /*  $sRequete_liste_action.="  AND client_contact.id_client IN (select id_programme_pere from routeurmail_programme_pere_utilisateur
				where  routeurmail_programme_pere_utilisateur.id_user='".$this->id_user."' and supplogique_programme_pere_utilisateur='N')";
               */
            }else{
                $sRequete_liste_action.=" and crm_action.guid_user='".$guiduser."' ";
            }
        }

        if($filtre!=""){

            $sRequete_liste_action.=" and programmation_action='$filtre'";


            if($filtreplus=="1")
                $sRequete_liste_action.=" and type_action<>'rdv'";

            if($filtreplus=="2"){
                $sRequete_liste_action.=" and type_action='rdv'";
                $sordre="ASC";
            }
            if($filtreplus=="3"){
                $sRequete_liste_action.=" and date_action<'".date('Y-m-d')."'";
                $sordre="ASC";
            }
            if(($filtreplus==1 || $filtreplus==2) && (($tridatedebut==null && $tridatefin==null) || empty($tridatedebut) && empty($tridatefin) || $tridatedebut=='' && $tridatefin=='')){
                $sRequete_liste_action.= " and date_action='".date('Y-m-d')."'";
            }
        }

        if(!empty($triprogramme)){
            $sRequete_liste_action .= " AND client.id_client='".$triprogramme."'";
        }
        if(!empty($tridatedebut) && !empty($tridatefin) && $tridatedebut!=null && $tridatefin!=null){
            $sRequete_liste_action .= " AND date_action BETWEEN '".date("Y-m-d", strtotime($tridatedebut))."' AND '".date("Y-m-d", strtotime($tridatefin))."'";
        }
        /*if(!empty($tricommercial) and trim($tricommercial)!=""){
            $sRequete_liste_action .= " AND client_contact.guid_user='".$tricommercial."'";
        }*/

        /*if($filtreplus=="2"){
            echo $sRequete_liste_action;
        }*/


        $sRequete_liste_action.=" group by crm_action.id_action order by date_action,heure_action $sordre   limit $idebut,$idnombre";

        if($bdebug)
            echo $sRequete_liste_action."<br>";

        $aTableauListe  = $this->renvoi_info_requete($sRequete_liste_action);
        $i=0;
        /*if($bdebug){
            echo $sRequete_liste_action;
            echo"<pre>";print_r($aTableauListe);echo"</pre>";
        }
        */

        //echo $affichage."<br>";

        if(!empty($aTableauListe)){
            foreach ($aTableauListe as $valeur) {
                $u=0;
                $tmp=array();
                if($affichage=="Tabordjour"){
                    $tmp[$u]['nom']=$valeur['ladate'];$u++;
                    $tmp[$u]['nom']=$valeur['prenom_contact']." ".$valeur['nom_contact'];$u++;
                    $tmp[$u]['nom']=$valeur['nom_client'];$u++;
                    $tmp[$u]['nom']=$valeur['type_action'];$u++;
                    $tmp[$u]['nom']=$valeur['lheure'];$u++;
                    if($badmin=="admin"){
                        $tmp[$u]['nom']=$valeur['nom_user'];$u++;
                    }else{
                        $tmp[$u]['nom']=$valeur['commentaire_action'];$u++;
                    }
                    
                    //echo"<pre>";print_r($tmp);echo"</pre>";
                    //$tmp[$u]['nom']=$valeur['ladate'];$u++;
                }
                else if($affichage=="Tabordjourrdv"){
                    $tmp[$u]['nom']=$valeur['ladate'];$u++;
                    $tmp[$u]['nom']=$valeur['lheure'];$u++;
                    $tmp[$u]['nom']=$valeur['prenom_contact']." ".$valeur['nom_contact'];$u++;
                    $tmp[$u]['nom']=$valeur['nom_client'];$u++;
                    $tmp[$u]['nom']=$valeur['nomstatut'];$u++;
                    if($badmin=="admin"){
                        $tmp[$u]['nom']=$valeur['nom_user'];$u++;
                    }else{
                        $tmp[$u]['nom']=$valeur['commentaire_action'];$u++;
                    }
                }
                else if($affichage=="Tabordjouretard"){
                    $tmp[$u]['nom']=$valeur['type_action'];$u++;
                    $tmp[$u]['nom']=$valeur['prenom_contact']." ".$valeur['nom_contact'];$u++;
                    $tmp[$u]['nom']=$valeur['nom_client'];$u++;
                    $tmp[$u]['nom']=$valeur['ladate'];$u++;
                    if($badmin=="admin"){
                        $tmp[$u]['nom']=$valeur['nom_user'];$u++;
                    }
                }
                else{
                    $tmp[$u]['nom']=$valeur['ladate'];$u++;
                    $tmp[$u]['nom']=$valeur['prenom_contact']." ".$valeur['nom_contact'];$u++;
                    $tmp[$u]['nom']=$valeur['type_action'];$u++;
                    $tmp[$u]['nom']=$valeur['nom_item'];$u++;
                    $tmp[$u]['nom']=$valeur['commentaire_action'];$u++;
                }

                $aTableauRetour[$i]['liste'] =$tmp;
                $aTableauRetour[$i]['id'] =$valeur['guid_contact'];
                $aTableauRetour[$i]['iduser'] =$valeur['guid_user'];
                $i++;

            }
        }
        //echo"<pre>";print_r($aTableauRetour);echo"</pre>";

        return $aTableauRetour;


    }


    /**
     * fonction renvvoi liste des contactvt
     * @author Danon Gnakouri
     * @param email
     * @param motdepass mot de pass
     * @param $bdd mot de passe
     * @since 3.2
     * @return la connexion
     */

    public function liste_contact($idebut,$idnombre,$guid_user,$triprogramme='',$tricommercial='',$tridatedebut='',$tridatefin='',$badmin=false,$bdebug=false){

        $aTableauRetour =array();

        $sRquete_contact ="select nom_contact,
      prenom_contact,
      date_format(datenreg_contact,'%d/%m/%Y') as ladate,
      tel_contact,
      nom_user,prenom_user,
      nom_client,
      email_contact,
	 client_contact.guid_contact,
	 itemsource.nom_item as nomsource,
	 nom_client,
	 type_action,
	 client_contact.guid_user
	from client_contact 
	Left join advnaj_users on advnaj_users.guid_user = client_contact.guid_user
	left join client on client.id_client = client_contact.id_client
	left join crm_item as itemsource on itemsource.id_item = client_contact.id_nature_contact
	LEFT JOIN crm_action on crm_action.guid_contact = client_contact.guid_contact  and  supploqique_action='N'
	where supplogique_contact='N' and type_action IS NULL ";

        if($guid_user!=""){
            if($badmin){
                /* $sRquete_contact.="  AND infobysms_useralert.id_programme
                IN (SELECT infobysms_lienuserprogramme.id_programme FROM infobysms_lienuserprogramme
                    WHERE accessite_lienprogramme='Y' AND supplogique_lienuserprogrammme='N' AND infobysms_lienuserprogramme.id_user='".$this->id_user."'
                    and infobysms_lienuserprogramme.id_programme<>'0')";
                    */

               /* $sRquete_contact.="  AND infobysms_useralert.id_programme IN (select infobysms_annonceurprogramme.id_programme from infobysms_annonceurprogramme
			inner join infobysms_user on infobysms_user.id_annonceur = infobysms_annonceurprogramme.id_annonceur 
			where  infobysms_user.id_user='".$this->id_user."' and supplogique_annonceurprogramme='N')";
            */

            }else{
                $sRquete_contact.=" and client_contact.guid_user='".$guid_user."' ";
            }
        }

        if(!empty($triprogramme)){
            $sRquete_contact .= " AND client.id_client='".$triprogramme."'";
        }
        if(!empty($tridatedebut) && !empty($tridatefin) && $tridatedebut!=null && $tridatefin!=null){
            $sRquete_contact .= " AND datenreg_contact BETWEEN '".date("Y-m-d", strtotime($tridatedebut))." 00:00:00' AND '".date("Y-m-d", strtotime($tridatefin))." 23:59:59'";
        }
        if(!empty($tricommercial)){
            $sRquete_contact .= " AND client_contact.guid_user='".$tricommercial."'";
        }

        $sRquete_contact.=" order by datenreg_contact DESC limit $idebut,$idnombre";

        if($bdebug)
            echo $sRquete_contact." ";

        $aTableauListe  = $this->renvoi_info_requete($sRquete_contact);
        $i=0;


        //echo"<pre>";print_r($aTableauListe);echo"</pre>";
        if(!empty($aTableauListe)){
            foreach ($aTableauListe as $valeur) {

                $nomsource = $valeur['nomsource'];
                $u=0;
                $tmp=array();
                $tmp[$u]['nom']=$valeur['prenom_contact']." ".$valeur['nom_contact'];
                $u++;
                $tmp[$u]['nom']=$valeur['nom_client'];
                $u++;
                /*$tmp[$u]['nom']=$valeur['mail_useralert'];
                $u++;
                $tmp[$u]['nom']=$valeur['portable_useralert'];
                $u++;
                */
                $tmp[$u]['nom']=$valeur['ladate'];
                $u++;
                $tmp[$u]['nom']=$nomsource;
                $u++;

                if($badmin){
                    $tmp[$u]['nom']=$valeur['nom_user'];
                    $u++;
                }


                $aTableauRetour[$i]['liste'] =$tmp;
                $aTableauRetour[$i]['id'] =$valeur['guid_contact'];
                $aTableauRetour[$i]['iduser'] =$valeur['guid_user'];
                $i++;

            }
        }


        //echo"<pre>";print_r($aTableauRetour);echo"</pre>";
        return $aTableauRetour;

    }


}