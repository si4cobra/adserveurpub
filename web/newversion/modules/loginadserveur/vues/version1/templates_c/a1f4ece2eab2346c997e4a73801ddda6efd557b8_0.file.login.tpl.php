<?php
/* Smarty version 3.1.29, created on 2018-03-22 14:48:04
  from "/var/www/html/modules/fli_connexion/vues/version1/templates/login.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ab3b4143dcc80_84331069',
  'file_dependency' => 
  array (
    'a1f4ece2eab2346c997e4a73801ddda6efd557b8' => 
    array (
      0 => '/var/www/html/modules/fli_connexion/vues/version1/templates/login.tpl',
      1 => 1513156645,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ab3b4143dcc80_84331069 ($_smarty_tpl) {
?>
<div class="container">

    <form class="form-signin" method="post">
        <?php if (!empty($_smarty_tpl->tpl_vars['erreur']->value)) {?>
            <div class="popupAlert alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                Erreur de connexion !<br/>
                <?php echo $_smarty_tpl->tpl_vars['erreur']->value;?>

            </div>
        <?php }?>
        <h2 class="form-signin-heading">FRAMEWORK</h2>
        <input type="hidden" name="validation" value="ok">
        <label for="inputUsername" class="sr-only">Utilisateur</label>
        <input type="text" id="inputUsername" class="form-control" placeholder="Utilisateur" name="login" required
               autofocus>
        <label for="inputPassword" class="sr-only">Mot de passe</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" name="password"
               required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
    </form>

</div><?php }
}
