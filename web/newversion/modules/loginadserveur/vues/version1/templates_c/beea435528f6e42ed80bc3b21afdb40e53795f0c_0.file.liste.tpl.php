<?php /* Smarty version 3.1.27, created on 2015-12-16 17:38:07
         compiled from "modules/module_test/vues/version1/templates/liste.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:7868276705671936f50fdd1_81313711%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'beea435528f6e42ed80bc3b21afdb40e53795f0c' => 
    array (
      0 => 'modules/module_test/vues/version1/templates/liste.tpl',
      1 => 1450283809,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7868276705671936f50fdd1_81313711',
  'variables' => 
  array (
    'bDebugRequete' => 0,
    'sDebugRequeteSelect' => 0,
    'sDebugRequeteInsertUpdate' => 0,
    'bMessageSupprElem' => 0,
    'sMessageSupprElem' => 0,
    'bMessageSuccesForm' => 0,
    'sMessageSuccesForm' => 0,
    'bMessageErreurForm' => 0,
    'sMessageErreurForm' => 0,
    'sScriptJavascriptInsert' => 0,
    'sScriptJavascriptUpdate' => 0,
    'aListe' => 0,
    'bAffTitre' => 0,
    'sTitreListe' => 0,
    'sCategorieListe' => 0,
    'objCategorieListe' => 0,
    'sRetourListe' => 0,
    'sLabelRetourListe' => 0,
    'aRech' => 0,
    'sLabelRecherche' => 0,
    'bAffPrintBtn' => 0,
    'bTelechargementFichier' => 0,
    'sDirRech' => 0,
    'aParametreWizardListe' => 0,
    'CleParametreWizardListe' => 0,
    'ValeurParametreWizardListe' => 0,
    'aRetourListe' => 0,
    'sCleRetourListe' => 0,
    'sValeurRetourListe' => 0,
    'objRech' => 0,
    'cle' => 0,
    'valeur' => 0,
    'valeur_checkbox' => 0,
    'nom_checkbox' => 0,
    'id_valeur_possible' => 0,
    'valeur_possible_bdd' => 0,
    'valeur_radio' => 0,
    'nom_radio' => 0,
    'itemBoutons' => 0,
    'bCsv' => 0,
    'bLabelCreationElem' => 0,
    'bBtnRetour' => 0,
    'bFormPopup' => 0,
    'sLabelCreationElemUrl' => 0,
    'sLabelCreationElem' => 0,
    'bRetourSpecifique' => 0,
    'RetourElemUrl' => 0,
    'sLabelFileRetourElem' => 0,
    'sDirRetour' => 0,
    'bAffListe' => 0,
    'bAffNombreResult' => 0,
    'iTabListeCount' => 0,
    'sLabelNbrLigne' => 0,
    'bActiveFormSelect' => 0,
    'urlFormList' => 0,
    'bRadioSelect' => 0,
    'bCheckboxSelect' => 0,
    'bAffFiche' => 0,
    'bAffMod' => 0,
    'bAffSupp' => 0,
    'aEnteteListe' => 0,
    'objEnteteListe' => 0,
    'bLigneCliquable' => 0,
    'sLienLigne' => 0,
    'objListe' => 0,
    'listeElemSelect' => 0,
    'allElements' => 0,
    'iNombreColonneListe' => 0,
    'sTpl' => 0,
    'bPagination' => 0,
    'aPaginationListe' => 0,
    'page' => 0,
    'itemBoutonsListe' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_567193714b14f3_63373378',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_567193714b14f3_63373378')) {
function content_567193714b14f3_63373378 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '7868276705671936f50fdd1_81313711';
?>
       <div style="color:#313131;margin: 0 auto;padding: 20px;margin-top: 10px;margin: 0 auto;text-align: center;padding-top: 10px;padding-bottom:0;">
        <?php if ($_smarty_tpl->tpl_vars['bDebugRequete']->value) {?>
            <p>
                Requete Select : <br><br>
                <?php echo $_smarty_tpl->tpl_vars['sDebugRequeteSelect']->value;?>
<br><br>
                Requete Insert/Update : <br><br>
                <?php echo $_smarty_tpl->tpl_vars['sDebugRequeteInsertUpdate']->value;?>
<br><br>
            </p>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['bMessageSupprElem']->value) && $_smarty_tpl->tpl_vars['bMessageSupprElem']->value) {?>
            <div class="popupAlert alert alert-success alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <?php if (isset($_smarty_tpl->tpl_vars['sMessageSupprElem']->value)) {
echo $_smarty_tpl->tpl_vars['sMessageSupprElem']->value;
}?>
            </div>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['bMessageSuccesForm']->value) && $_smarty_tpl->tpl_vars['bMessageSuccesForm']->value) {?>
            <div class="popupAlert alert alert-success alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <?php if (isset($_smarty_tpl->tpl_vars['sMessageSuccesForm']->value)) {
echo $_smarty_tpl->tpl_vars['sMessageSuccesForm']->value;
}?>
            </div>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['bMessageErreurForm']->value) && $_smarty_tpl->tpl_vars['bMessageErreurForm']->value) {?>
            <div class="popupAlert alert alert-danger alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <?php if (isset($_smarty_tpl->tpl_vars['sMessageErreurForm']->value)) {
echo $_smarty_tpl->tpl_vars['sMessageErreurForm']->value;
}?>
            </div>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['sScriptJavascriptInsert']->value)) {?>
            <?php echo $_smarty_tpl->tpl_vars['sScriptJavascriptInsert']->value;?>

        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['sScriptJavascriptUpdate']->value)) {?>
            <?php echo $_smarty_tpl->tpl_vars['sScriptJavascriptUpdate']->value;?>

        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['aListe']->value)) {?>
        <?php if ($_smarty_tpl->tpl_vars['bAffTitre']->value) {?>
        <div id="page-heading" style="margin-bottom:20px;"><div style="display:inline-block;border:1px solid #D0D1D5;"><div class="hidden-xs title-head-search" style="display: inline-block;height: 73px;width: 73px;vertical-align: middle;padding: 20px;"><span class="glyphicon glyphicon-link" style="color:#fff;vertical-align: bottom;font-size: 20px;top: 6px;"></span></div><h1 style="font-size:30px;margin: 0px;display:inline-block;padding: 20px;vertical-align: bottom;" class="text-head-search"><?php echo $_smarty_tpl->tpl_vars['sTitreListe']->value;?>
</h1></div></div>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['sCategorieListe']->value) && $_smarty_tpl->tpl_vars['sCategorieListe']->value != '') {?>
            <?php if (is_array($_smarty_tpl->tpl_vars['sCategorieListe']->value)) {?>
                <?php
$_from = $_smarty_tpl->tpl_vars['sCategorieListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['objCategorieListe'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['objCategorieListe']->_loop = false;
$_smarty_tpl->tpl_vars['__foreach_scategorieliste'] = new Smarty_Variable(array('total' => $_smarty_tpl->_count($_from), 'iteration' => 0));
foreach ($_from as $_smarty_tpl->tpl_vars['objCategorieListe']->value) {
$_smarty_tpl->tpl_vars['objCategorieListe']->_loop = true;
$_smarty_tpl->tpl_vars['__foreach_scategorieliste']->value['iteration']++;
$_smarty_tpl->tpl_vars['__foreach_scategorieliste']->value['last'] = $_smarty_tpl->tpl_vars['__foreach_scategorieliste']->value['iteration'] == $_smarty_tpl->tpl_vars['__foreach_scategorieliste']->value['total'];
$foreach_objCategorieListe_Sav = $_smarty_tpl->tpl_vars['objCategorieListe'];
?><div style="background-color:#FFF;display:inline-block;vertical-align:top;height:20px;font-size:18px;padding:10px 0;font-family:Tahoma;"><?php echo $_smarty_tpl->tpl_vars['objCategorieListe']->value['index'];?>
</div><div style="background-color:<?php if ($_smarty_tpl->tpl_vars['objCategorieListe']->value['suivant']) {?>#e3e3e3<?php } else { ?>#777<?php }?>;height:40px;display:inline-block;vertical-align:middle;"><div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div><div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objCategorieListe']->value['texte'];?>
</div><?php if (!(isset($_smarty_tpl->tpl_vars['__foreach_scategorieliste']->value['last']) ? $_smarty_tpl->tpl_vars['__foreach_scategorieliste']->value['last'] : null)) {?><div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-right-radius: 0;border-bottom-right-radius: 0;background-color:#FFF;"></div><?php }?></div><?php
$_smarty_tpl->tpl_vars['objCategorieListe'] = $foreach_objCategorieListe_Sav;
}
?><div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div>
            <?php } else { ?>
                <div style="background-color:#777;height:40px;display:inline-block;vertical-align:middle;"><div style="display:inline-block;width:17px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div><div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['sCategorieListe']->value;?>
</div></div><div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div>
            <?php }?>
            <br><br><br>
        <?php }?>        
        <?php if (isset($_smarty_tpl->tpl_vars['sRetourListe']->value) && $_smarty_tpl->tpl_vars['sRetourListe']->value != '') {?>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td><a href="<?php echo $_smarty_tpl->tpl_vars['sRetourListe']->value;?>
"><img src="images/retour.png" height="40px" border="0" alt="<?php echo $_smarty_tpl->tpl_vars['sLabelRetourListe']->value;?>
"></a></td>
                    <td width="5px"></td>
                    <td><a href="<?php echo $_smarty_tpl->tpl_vars['sRetourListe']->value;?>
" style="text-decoration:none;color:#000;"><b><?php echo $_smarty_tpl->tpl_vars['sLabelRetourListe']->value;?>
</b></a></td>
                </tr>
            </table>
            <br>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['aRech']->value) && count($_smarty_tpl->tpl_vars['aRech']->value) > 0) {?>
          <!--<div style="margin-bottom:20px;"><div style="background-color:#777;height:40px;display:inline-block;vertical-align:middle;"><img src="images/loupe.png" height="20px" border="0" alt="<?php echo $_smarty_tpl->tpl_vars['sLabelRecherche']->value;?>
" style="padding:10px 0;background-color:#FFF;"><div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div><div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['sLabelRecherche']->value;?>
</div></div><div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div></div>-->
          <div style="background-color: #fff;border: 1px solid #D0D1D5;padding: 20px 20px;margin-bottom: 20px;margin-top:0px;display: inline-block;">
          <div style="margin-bottom: 20px;">
            <div style="display: inline-block;" class="title-search">
              <span class="glyphicon glyphicon-search title-search" style="display: inline-block;height: 35px;width: 35px;padding: 10px;vertical-align: top;top:0px;"></span>
              <h4 style="display: inline-block;margin: 0;padding: 10px;font-size: 14px;" class="text-search"><?php echo $_smarty_tpl->tpl_vars['sLabelRecherche']->value;?>
</h4>
            </div>
          </div>
          <?php if ($_smarty_tpl->tpl_vars['bAffPrintBtn']->value) {?>

          <?php }?>
          <form
                  class="form-inline"
                  action=""
                  id="formList"
                  method="GET"
                  <?php if (isset($_smarty_tpl->tpl_vars['bTelechargementFichier']->value) && $_smarty_tpl->tpl_vars['bTelechargementFichier']->value) {?>enctype="multipart/form-data"<?php }?>
                  >
                  <div>
          <input type="hidden" name="dir" value="<?php echo $_smarty_tpl->tpl_vars['sDirRech']->value;?>
">
          <?php if (isset($_smarty_tpl->tpl_vars['aParametreWizardListe']->value) && is_array($_smarty_tpl->tpl_vars['aParametreWizardListe']->value)) {?>
              <?php
$_from = $_smarty_tpl->tpl_vars['aParametreWizardListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['ValeurParametreWizardListe'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['ValeurParametreWizardListe']->_loop = false;
$_smarty_tpl->tpl_vars['CleParametreWizardListe'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['CleParametreWizardListe']->value => $_smarty_tpl->tpl_vars['ValeurParametreWizardListe']->value) {
$_smarty_tpl->tpl_vars['ValeurParametreWizardListe']->_loop = true;
$foreach_ValeurParametreWizardListe_Sav = $_smarty_tpl->tpl_vars['ValeurParametreWizardListe'];
?>
                  <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['CleParametreWizardListe']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['ValeurParametreWizardListe']->value;?>
">
              <?php
$_smarty_tpl->tpl_vars['ValeurParametreWizardListe'] = $foreach_ValeurParametreWizardListe_Sav;
}
?>
          <?php }?>
          <?php if (isset($_smarty_tpl->tpl_vars['aRetourListe']->value) && is_array($_smarty_tpl->tpl_vars['aRetourListe']->value)) {?>
              <?php
$_from = $_smarty_tpl->tpl_vars['aRetourListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['sValeurRetourListe'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['sValeurRetourListe']->_loop = false;
$_smarty_tpl->tpl_vars['sCleRetourListe'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['sCleRetourListe']->value => $_smarty_tpl->tpl_vars['sValeurRetourListe']->value) {
$_smarty_tpl->tpl_vars['sValeurRetourListe']->_loop = true;
$foreach_sValeurRetourListe_Sav = $_smarty_tpl->tpl_vars['sValeurRetourListe'];
?>
                  <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['sCleRetourListe']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['sValeurRetourListe']->value;?>
">
              <?php
$_smarty_tpl->tpl_vars['sValeurRetourListe'] = $foreach_sValeurRetourListe_Sav;
}
?>
          <?php }?>     

        <!-- NOUVELLE RECHERCHE BOOTSTRAP -->
        
        <?php
$_from = $_smarty_tpl->tpl_vars['aRech']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['objRech'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['objRech']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objRech']->value) {
$_smarty_tpl->tpl_vars['objRech']->_loop = true;
$foreach_objRech_Sav = $_smarty_tpl->tpl_vars['objRech'];
?>
            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['type_champ']) && ((isset($_smarty_tpl->tpl_vars['objRech']->value['aff_recherche']) && $_smarty_tpl->tpl_vars['objRech']->value['aff_recherche'] == 'ok') || ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'category' || $_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'bouton'))) {?>
                <?php if ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'text') {?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <input
                                    type="text"
                                    class="inp-form form-control"
                                    id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>                                
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            >
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'category') {?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'date' || $_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'datetime') {?>
                    <div class="form-group">
                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date']) && $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date'] == 'ok') {?>
                    
                        <label class="label-form-upper" style="width: 100%;"><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label']) && isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][0]) && $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][0] != '') {?>
                            <div class="input-group" style="display: inline-table;vertical-align: middle;border: 0;box-shadow: none;width: auto;"><span class="input-group-addon" id="basic-addon2" style="width:auto;min-width:52px;"><?php echo $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][0];?>
</span>
                        <?php }?>
                        <input
                            class="form-control"
                            type="date"
                            name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
[]"
                            aria-describedby="basic-addon2"
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][0]) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][0] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][0];?>
"<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="height: 28px;<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            >
                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label']) && isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][0]) && $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][0] != '') {?>
                            </div>
                        <?php }?>
                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label']) && isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][1]) && $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][1] != '') {?>
                            <div class="input-group" style="display: inline-table;vertical-align: middle;border: 0;box-shadow: none;width: auto;"><span class="input-group-addon" id="basic-addon2" style="width:auto;"><?php echo $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][1];?>
</span>
                        <?php }?>
                            <input
                                class="form-control"
                                type="date"
                                name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
[]"
                                aria-describedby="basic-addon2"
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][1]) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][1] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][1];?>
"<?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="height: 28px;<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                        <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                        <?php }?>
                                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                                <?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                        <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                        <?php }?>
                                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                                <?php }?>
                            >
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label']) && isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][1]) && $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][1] != '') {?>
                                </div>
                            <?php }?>
                    <?php } else { ?>
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <input
                            class="form-control"
                            type="date"
                            id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                            name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                        >
                    <?php }?>
                    </div> 

                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'textarea') {?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <textarea
                                    class="form-control"
                                    id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            ><?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
</textarea>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'checkbox') {?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <div style="display:inline-block;">
                            <table style="display:inline-block;">
                                <tbody>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['nom_checkbox'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = false;
$_smarty_tpl->tpl_vars['valeur_checkbox'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_checkbox']->value => $_smarty_tpl->tpl_vars['nom_checkbox']->value) {
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = true;
$foreach_nom_checkbox_Sav = $_smarty_tpl->tpl_vars['nom_checkbox'];
?>
                                    <tr>
                                        <td style="padding-right:10px;">
                                            <input
                                                    class="form-control"
                                                    type="checkbox"
                                                    name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
[]"
                                                    value="<?php echo $_smarty_tpl->tpl_vars['valeur_checkbox']->value;?>
"
                                                    <?php if (is_array($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && in_array($_smarty_tpl->tpl_vars['valeur_checkbox']->value,$_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'])) {?>checked<?php }?>
                                                    >
                                        </td>
                                        <td style="padding-right:20px;"><?php echo $_smarty_tpl->tpl_vars['nom_checkbox']->value;?>
</td>
                                    </tr>
                                <?php
$_smarty_tpl->tpl_vars['nom_checkbox'] = $foreach_nom_checkbox_Sav;
}
?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'select') {?>
                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['select_autocomplete']) && $_smarty_tpl->tpl_vars['objRech']->value['select_autocomplete'] == 'ok') {?>
                        <div class="form-group">
                            <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label_filtre'];?>
</label>
                            <input type='text'
                                       name='rech_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
'
                                       id='id_rech<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
'
                                       class="inp-form form-control"
                                       onKeyUp="affiche_liste_generique('<?php echo $_smarty_tpl->tpl_vars['objRech']->value['table_item'];?>
','<?php echo $_smarty_tpl->tpl_vars['objRech']->value['id_table_item'];?>
','<?php echo $_smarty_tpl->tpl_vars['objRech']->value['affichage_table_item'];?>
','<?php echo $_smarty_tpl->tpl_vars['objRech']->value['supplogique_table_item'];?>
', '<?php echo $_smarty_tpl->tpl_vars['objRech']->value['tabfiltre_autocomplete'];?>
',this.value,'id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
');">
                        </div>
                    <?php }?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <select
                                class="form-control"
                                style="padding:5px;width: 100px;"
                                id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];
if (isset($_smarty_tpl->tpl_vars['objRech']->value['multiple'])) {?>[]<?php }?>"
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['multiple'])) {?>multiple<?php }?>
                                <?php echo $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];?>


                                >
                            <option value="" <?php if ($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] == '') {?>selected<?php }?>></option>
                            <?php if (is_array($_smarty_tpl->tpl_vars['objRech']->value['lesitem'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = false;
$_smarty_tpl->tpl_vars['id_valeur_possible'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['id_valeur_possible']->value => $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value) {
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = true;
$foreach_valeur_possible_bdd_Sav = $_smarty_tpl->tpl_vars['valeur_possible_bdd'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['id_valeur_possible']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] == $_smarty_tpl->tpl_vars['id_valeur_possible']->value) {?>selected<?php }?>>
                                        <?php echo $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value;?>

                                    </option>
                                <?php
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = $foreach_valeur_possible_bdd_Sav;
}
?>
                            <?php }?>
                        </select>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'radio') {?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <div style="display:inline-block;">
                            <table>
                                <tbody>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['nom_radio'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['nom_radio']->_loop = false;
$_smarty_tpl->tpl_vars['valeur_radio'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_radio']->value => $_smarty_tpl->tpl_vars['nom_radio']->value) {
$_smarty_tpl->tpl_vars['nom_radio']->_loop = true;
$foreach_nom_radio_Sav = $_smarty_tpl->tpl_vars['nom_radio'];
?>
                                    <tr>
                                        <td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
                                            <input
                                                    class="form-control"
                                                    type="radio"
                                                    name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                                    value="<?php echo $_smarty_tpl->tpl_vars['valeur_radio']->value;?>
"
                                                    <?php if ($_smarty_tpl->tpl_vars['valeur_radio']->value == $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) {?>checked<?php }?>
                                                    >
                                        </td>
                                        <td style="padding-bottom:0;vertical-align:middle;height:32px;"><?php echo $_smarty_tpl->tpl_vars['nom_radio']->value;?>
</td>
                                    </tr>
                                <?php
$_smarty_tpl->tpl_vars['nom_radio'] = $foreach_nom_radio_Sav;
}
?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'file') {?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <input
                                class="form-control"
                                type="file"
                                id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                        <?php }?>
                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                        <?php }?>
                        >
                    </div>                
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'time') {?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <input
                                type="text"
                                class="timepicker inp-form form-control"
                                id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                        <?php }?>
                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                        <?php }?>
                        >
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'hour') {?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <input
                                type="text"
                                class="hourpicker inp-form form-control"
                                id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                        <?php }?>
                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                        <?php }?>
                        >
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'hidden') {?>
                    <div class="form-group">
                        <input
                            type="hidden"
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['nom_variable'])) {?>name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'])) {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                            >
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'password') {?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <input
                                    type="password"
                                    class="inp-form form-control"
                                    id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            >
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'email') {?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <input
                                    type="email"
                                    class="inp-form form-control"
                                    id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            >
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'telephone') {?>
                    <div class="form-group">
                        <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                        <input
                                    type="tel"
                                    class="inp-form form-control"
                                    id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                    <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                            <?php }?>
                            >
                    </div>
                <?php } else { ?>

                <?php }?>
            <?php }?>
        <?php
$_smarty_tpl->tpl_vars['objRech'] = $foreach_objRech_Sav;
}
?>

        <br/><br/>

        <div class="form-group">            
            <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['valider'])) {?>
                  <input
                  class="btn btn-primary btnbtnValForm"
                  type="submit"
                  id="id_<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['nom_variable'];?>
"
                  name="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['nom_variable'];?>
"
                  value="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['text_label'];?>
"
                  <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['style']) && $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['style'] != '') {?> style="margin-bottom: 5px;"<?php }?>
                  <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['tableau_attribut'])) {?>
                      <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                          <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                      <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                  <?php }?>
                  <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['fonction_javascript'])) {?>
                      <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                          <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                      <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                  <?php }?>
                  onclick="this.form.action='main.php';document.getElementById('formList').setAttribute('target','_top');"
                  >
              <?php }?>
              <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['reset'])) {?>
                      <input
                              class="btn btn-primary btnResForm"
                              type="button"
                              value="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['text_label'];?>
"
                              <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['style']) && $_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['style'] != '') {?>style="margin-bottom: 5px;"<?php }?>
                      <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['tableau_attribut'])) {?>
                          <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                              <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                          <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                      <?php }?>
                      <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['fonction_javascript'])) {?>
                          <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                              <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                          <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                      <?php }?>
                      onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['url'];?>
';"
                      >
              <?php }?>
              <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']) && $_smarty_tpl->tpl_vars['bCsv']->value) {?>
                  <input
                  class="btn btn-primary"
                  type="submit"
                  value="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['text_label'];?>
"
                  <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['style']) && $_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['style'];?>
font-size:13px;margin-bottom: 5px;"<?php }?>
                  <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['tableau_attribut'])) {?>
                      <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                          <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                      <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                  <?php }?>
                  <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['fonction_javascript'])) {?>
                      <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                          <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                      <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                  <?php }?>
                  onclick="this.form.action='main_csv.php';document.getElementById('formList').setAttribute('target','_BLANK');">
              <?php }?>
        </div>

        <!-- FIN NOUVELLE RECHERCHE BOOTSTRAP -->

          </div>
          </form>
      <?php }?>
      </div>

    <?php if ($_smarty_tpl->tpl_vars['bLabelCreationElem']->value || $_smarty_tpl->tpl_vars['bBtnRetour']->value || $_smarty_tpl->tpl_vars['bAffPrintBtn']->value) {?>
    <div style="margin-bottom:20px;text-align: center;">
        <?php if ($_smarty_tpl->tpl_vars['bLabelCreationElem']->value) {?>
            <?php if (!$_smarty_tpl->tpl_vars['bFormPopup']->value) {?>
                  <a href="<?php echo $_smarty_tpl->tpl_vars['sLabelCreationElemUrl']->value;?>
" class="btn btn-default" role="button"><span class="glyphicon glyphicon-plus"></span> <?php echo $_smarty_tpl->tpl_vars['sLabelCreationElem']->value;?>
</a>
                  <?php if ($_smarty_tpl->tpl_vars['bRetourSpecifique']->value) {?>
                  &nbsp;&nbsp; <a href="<?php echo $_smarty_tpl->tpl_vars['RetourElemUrl']->value;?>
" class="btn btn-default" role="button"><span class="glyphicon glyphicon-plus"></span> <?php echo $_smarty_tpl->tpl_vars['sLabelFileRetourElem']->value;?>
</a>
                  <?php }?>
            <?php } else { ?>
                <a href="#creerModal" class="openModal" id="test" style="text-decoration:none;color:#00e;"><?php echo $_smarty_tpl->tpl_vars['sLabelCreationElem']->value;?>
</a>
                <aside id="creerModal" class="modal">
                    <div style="overflow-y:auto;max-height:85%;background:#FFF;text-shadow:none;">
                        <?php echo $_smarty_tpl->getSubTemplate ('formulaire.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                        <a href="#close" title="Fermer" style="top:15px;right:15px;">Fermer</a>
                    </div>
                </aside>
                <?php if (!$_smarty_tpl->tpl_vars['bMessageSuccesForm']->value && $_smarty_tpl->tpl_vars['bMessageErreurForm']->value) {?>
                    <?php echo '<script'; ?>
>
                        creermodal = document.getElementById('test').click();
                    <?php echo '</script'; ?>
>
                <?php }?>
            <?php }?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['bBtnRetour']->value) {?>
            <a href="main.php?dir=<?php echo $_smarty_tpl->tpl_vars['sDirRetour']->value;?>
" class="btn btn-default" role="button"><span class="glyphicon glyphicon-share-alt" style="transform: scaleX(-1);"></span> Retour</a>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['bAffPrintBtn']->value) {?>
            <a href="Javascript:Void(0);" class="btn btn-default hidden-xs" role="button" onclick="window.print()" style="position: fixed;top: 8px;z-index: 10000;right: 20px;"><span class="glyphicon glyphicon-print" style="transform: scaleX(-1);"></span></a>
        <?php }?>
        
      
    </div>
    <?php }?>

        
      </div>
      <?php if ($_smarty_tpl->tpl_vars['bAffListe']->value) {?>
      <?php if ($_smarty_tpl->tpl_vars['bAffNombreResult']->value) {?>
      <div style="margin:0 auto;text-align: center;" id="blockAffInfo">
        <div style="display:inline-block;border: 1px solid #D0D1D5;margin-top:0px;margin-bottom:20px;background-color: #333;"><div style="display:inline-block;padding:20px;vertical-align: middle;" id="rechNbr" class="sub-title-head-search"><?php echo $_smarty_tpl->tpl_vars['iTabListeCount']->value;?>
</div><div id="rechLabel" style="display:inline-block;padding:20px;vertical-align: middle;" class="sub-text-head-search"><?php echo $_smarty_tpl->tpl_vars['sLabelNbrLigne']->value;?>
</div></div>
      </div>
      <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['bActiveFormSelect']->value) {?><form method="post" action="<?php echo $_smarty_tpl->tpl_vars['urlFormList']->value;?>
"><?php }?>
        <div style="margin: 0 15px;" class="row">
        <button class="btn btn-success btnScroll btnScrollLeft" style="position: fixed;margin-top: -34px;left: 16px;z-index: 1;"> <span class="glyphicon glyphicon-arrow-left"></span></button>
        <button class="btn btn-success btnScroll btnScrollRight" style="position: fixed;right: 15px;margin-top: -34px;z-index: 1;"> <span class="glyphicon glyphicon-arrow-right"></span></button>
        <div class="table-responsive">     
          <table style="background-color: #fafafa;" class="table table-hover table-condensed table-bordered table-striped">
            <thead>
              <tr>
                <?php if ($_smarty_tpl->tpl_vars['bActiveFormSelect']->value) {?>
                    <?php if ($_smarty_tpl->tpl_vars['bRadioSelect']->value) {?><th class="table-header-repeat line-left minwidth-1" style="color:#fff;font-size:16px;text-align: center;min-width: 20px;vertical-align: middle;">°</th><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['bCheckboxSelect']->value) {?><th class="table-header-repeat line-left minwidth-1" style="color:#fff;font-size:16px;text-align: center;min-width: 20px;vertical-align: middle;">#</th><?php }?>                    
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['bAffFiche']->value || $_smarty_tpl->tpl_vars['bAffMod']->value || $_smarty_tpl->tpl_vars['bAffSupp']->value) {?>
                    <th class="table-header-repeat line-left minwidth-1 title-table-head th-action" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                        <a href="javascript:Void(0);" style="color:#fff;">Actions</a>
                    </th>
                <?php }?>
                <?php
$_from = $_smarty_tpl->tpl_vars['aEnteteListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['objEnteteListe'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['objEnteteListe']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objEnteteListe']->value) {
$_smarty_tpl->tpl_vars['objEnteteListe']->_loop = true;
$foreach_objEnteteListe_Sav = $_smarty_tpl->tpl_vars['objEnteteListe'];
?>
                    <th class="table-header-repeat line-left minwidth-1 title-table-head <?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_perso_th'];?>
" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['sUrl'];?>
" style="color:#fff;"><?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['sLabel'];?>
</a>
                    </th>
                <?php
$_smarty_tpl->tpl_vars['objEnteteListe'] = $foreach_objEnteteListe_Sav;
}
?>                
              </tr>
            </thead>
            <tbody>
            <?php
$_from = $_smarty_tpl->tpl_vars['aListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['objListe'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['objListe']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objListe']->value) {
$_smarty_tpl->tpl_vars['objListe']->_loop = true;
$foreach_objListe_Sav = $_smarty_tpl->tpl_vars['objListe'];
?>
              <tr <?php if ($_smarty_tpl->tpl_vars['bLigneCliquable']->value) {?>onclick="document.location = '<?php echo $_smarty_tpl->tpl_vars['sLienLigne']->value;
echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
';" style="cursor:pointer;"<?php }?>>
                <?php if ($_smarty_tpl->tpl_vars['bAffFiche']->value || $_smarty_tpl->tpl_vars['bAffMod']->value || $_smarty_tpl->tpl_vars['bAffSupp']->value) {?>
                <td style="text-align:center;vertical-align: middle;" class="td-action">
                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      <span class="glyphicon glyphicon-option-vertical"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" style="left: 0 !important;">
                      <?php if ($_smarty_tpl->tpl_vars['bAffMod']->value) {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['objListe']->value['sUrlForm'];?>
"><span class="glyphicon glyphicon-pencil"></span> Modifier</a></li><?php }?>
                      <?php if ($_smarty_tpl->tpl_vars['bAffSupp']->value) {?><li><a href="#" onclick="bconf=confirm('Voulez-vous supprimer cette ligne ?');if(bconf)location.replace('<?php echo $_smarty_tpl->tpl_vars['objListe']->value['sUrlSupp'];?>
');"><span class="glyphicon glyphicon-trash"></span> Supprimer</a></li><?php }?>
                    </ul>
                  </div>
                </td>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['bActiveFormSelect']->value) {?>
                    <?php if ($_smarty_tpl->tpl_vars['bRadioSelect']->value) {?><td style="width: 28px;"><input type="radio" value="<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
" name="iIdSelected"></td><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['bCheckboxSelect']->value) {?><td style="width: 28px;"><input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
" name="iIds[]"></td><?php }?>                        
                <?php }?>
                <?php
$_from = $_smarty_tpl->tpl_vars['aEnteteListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['objEnteteListe'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['objEnteteListe']->_loop = false;
$_smarty_tpl->tpl_vars['__foreach_enteteliste'] = new Smarty_Variable(array('iteration' => 0));
foreach ($_from as $_smarty_tpl->tpl_vars['objEnteteListe']->value) {
$_smarty_tpl->tpl_vars['objEnteteListe']->_loop = true;
$_smarty_tpl->tpl_vars['__foreach_enteteliste']->value['iteration']++;
$_smarty_tpl->tpl_vars['__foreach_enteteliste']->value['first'] = $_smarty_tpl->tpl_vars['__foreach_enteteliste']->value['iteration'] == 1;
$foreach_objEnteteListe_Sav = $_smarty_tpl->tpl_vars['objEnteteListe'];
?>
                    <td style="vertical-align: middle;" <?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_perso_td'];?>
>
                        <?php if ((isset($_smarty_tpl->tpl_vars['__foreach_enteteliste']->value['first']) ? $_smarty_tpl->tpl_vars['__foreach_enteteliste']->value['first'] : null)) {?>
                            <?php if (isset($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) && is_array($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) && count($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) > 0) {?>
                                <input type="submit" value=" + " class="class_form_list_toggle_input">
                            <?php }?>
                        <?php }?>
                            
                        <?php if ($_smarty_tpl->tpl_vars['bActiveFormSelect']->value) {?>
                            <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['activ_form_select'] == 'ok') {?>   
                                <!-- ---------------- TEXT ---------------- --> 
                                <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'text') {?>
                                    <input   type="text"  name=" <?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']];?>
" >                             
                                <?php }?>
                                <!-- ---------------- SELECT ---------------- -->
                                <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'select') {?>
                                    <select
                                        style="padding:5px;"
                                        id="id_form_<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable'];?>
"
                                        name="<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable'];
if (isset($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['multiple'])) {?>[]<?php }?>"
                                        <?php if (isset($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['multiple'])) {?>multiple<?php }?>

                                        <?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['fonction_javascript'];?>

                                    >
                                        <option value=""></option>
                                        <?php if (is_array($_smarty_tpl->tpl_vars['listeElemSelect']->value)) {?>
                                            <?php
$_from = $_smarty_tpl->tpl_vars['listeElemSelect']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = false;
$_smarty_tpl->tpl_vars['id_valeur_possible'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['id_valeur_possible']->value => $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value) {
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = true;
$foreach_valeur_possible_bdd_Sav = $_smarty_tpl->tpl_vars['valeur_possible_bdd'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['id_valeur_possible']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']] == $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value) {?>selected<?php }?>>
                                                    <?php echo $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value;?>

                                                </option>
                                            <?php
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = $foreach_valeur_possible_bdd_Sav;
}
?>
                                        <?php }?>
                                    </select>
                                <?php }?>
                                <!-- ---------------- CHECKBOX ---------------- -->
                                <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'checkbox') {?>
                                    <table class="table table-hover table-condensed table-bordered">
                                        <tbody>
                                       <?php
$_from = $_smarty_tpl->tpl_vars['allElements']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['nom_checkbox'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = false;
$_smarty_tpl->tpl_vars['valeur_checkbox'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_checkbox']->value => $_smarty_tpl->tpl_vars['nom_checkbox']->value) {
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = true;
$foreach_nom_checkbox_Sav = $_smarty_tpl->tpl_vars['nom_checkbox'];
?> 
                                        <tr>
                                            <td style="padding-right:10px;">
                                                <input
                                                    type="checkbox"
                                                    name="<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable'];?>
[]"
                                                    value="<?php echo $_smarty_tpl->tpl_vars['valeur_checkbox']->value;?>
"
                                                    <?php if (is_array($_smarty_tpl->tpl_vars['objListe']->value['tableauChecked']) && in_array($_smarty_tpl->tpl_vars['valeur_checkbox']->value,$_smarty_tpl->tpl_vars['objListe']->value['tableauChecked'])) {?>checked<?php }?>
                                                >
                                                  
                                            </td>
                                            <td style="padding-right:20px;"><?php echo $_smarty_tpl->tpl_vars['nom_checkbox']->value;?>
</td>
                                        </tr>
                                        <?php
$_smarty_tpl->tpl_vars['nom_checkbox'] = $foreach_nom_checkbox_Sav;
}
?>
                                        </tbody>
                                    </table>
                                <?php }?>
                                <!-- ---------------- RADIO ---------------- -->
                                <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'radio') {?>
                                    <table class="table table-hover table-condensed table-bordered">
                                        <tbody>
                                       <?php
$_from = $_smarty_tpl->tpl_vars['allElements']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['nom_radio'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['nom_radio']->_loop = false;
$_smarty_tpl->tpl_vars['valeur_radio'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_radio']->value => $_smarty_tpl->tpl_vars['nom_radio']->value) {
$_smarty_tpl->tpl_vars['nom_radio']->_loop = true;
$foreach_nom_radio_Sav = $_smarty_tpl->tpl_vars['nom_radio'];
?>
                                        <tr>
                                            <td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
                                                <input
                                                    type="radio"
                                                    name="<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable'];?>
"
                                                    value="<?php echo $_smarty_tpl->tpl_vars['valeur_radio']->value;?>
"
                                                    <?php if ($_smarty_tpl->tpl_vars['valeur_radio']->value == $_smarty_tpl->tpl_vars['objListe']->value['checked']) {?>checked<?php }?>
                                                >
                                            </td>
                                            <td style="padding-bottom:0;vertical-align:middle;height:32px;"><?php echo $_smarty_tpl->tpl_vars['nom_radio']->value;?>
</td>
                                        </tr>
                                        <?php
$_smarty_tpl->tpl_vars['nom_radio'] = $foreach_nom_radio_Sav;
}
?> 
                                        </tbody>
                                    </table>
                                <?php }?>
                            <?php }?>
                        <?php } else { ?>
                            
                             <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['format_affiche_liste'] <> '') {?>
                                  <?php echo number_format($_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']],$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['format_affiche_liste'],".",".");?>

                            <?php } else { ?>
                                 <?php echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']];?>

                                                        
                            <?php }?>   
                        <?php }?>

                    </td>
                <?php
$_smarty_tpl->tpl_vars['objEnteteListe'] = $foreach_objEnteteListe_Sav;
}
?>                    
                </tr>
                <?php if (isset($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) && is_array($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) && count($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) > 0) {?>
                    <tr>
                        <td style="padding:0;vertical-align: middle;" colspan="<?php echo $_smarty_tpl->tpl_vars['iNombreColonneListe']->value;?>
">
                            <div class="class_form_list_toggle_div">
                                <div style="padding:20px 20px 0 20px;">
                                    <?php
$_from = $_smarty_tpl->tpl_vars['objListe']->value['liste_fils'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['sTpl'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['sTpl']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['sTpl']->value) {
$_smarty_tpl->tpl_vars['sTpl']->_loop = true;
$foreach_sTpl_Sav = $_smarty_tpl->tpl_vars['sTpl'];
?>
                                        <?php echo $_smarty_tpl->tpl_vars['sTpl']->value;?>

                                    <?php
$_smarty_tpl->tpl_vars['sTpl'] = $foreach_sTpl_Sav;
}
?>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php }?>
            <?php
$_smarty_tpl->tpl_vars['objListe'] = $foreach_objListe_Sav;
}
?>
            </tbody>            
          </table> 
        </div>
      <?php if ($_smarty_tpl->tpl_vars['bActiveFormSelect']->value) {?>
          <div style="clear:both;float:right;margin-top:20px;">
              <input type="submit" value="Valider" style="background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;">
          </div>
          </form>
      <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['bPagination']->value) && $_smarty_tpl->tpl_vars['bPagination']->value && isset($_smarty_tpl->tpl_vars['aPaginationListe']->value) && isset($_smarty_tpl->tpl_vars['aPaginationListe']->value['select']) && isset($_smarty_tpl->tpl_vars['aPaginationListe']->value['liste'])) {?>
        <div style="text-align: center;">
            <ul class="pagination">
              <li>
                <a href="<?php echo $_smarty_tpl->tpl_vars['aPaginationListe']->value['liste']['page_premiere'];?>
" aria-label="Previous">
                  <span aria-hidden="true" class="glyphicon glyphicon-fast-backward"></span>
                </a>
              </li>
              <li>
                <a href="<?php echo $_smarty_tpl->tpl_vars['aPaginationListe']->value['liste']['page_precedente'];?>
" aria-label="Previous">
                  <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
                </a>
              </li>
              <?php
$_from = $_smarty_tpl->tpl_vars['aPaginationListe']->value['liste']['page_liste'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['page'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['page']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['page']->value) {
$_smarty_tpl->tpl_vars['page']->_loop = true;
$foreach_page_Sav = $_smarty_tpl->tpl_vars['page'];
?>
                  <?php if (isset($_smarty_tpl->tpl_vars['page']->value['selected']) && $_smarty_tpl->tpl_vars['page']->value['selected']) {?>
                      <li class="active"><a href="#"><strong><?php echo $_smarty_tpl->tpl_vars['page']->value['numero'];?>
</strong><span class="sr-only">(current)</span></a></li>
                  <?php } else { ?>
                      <li><a href="<?php echo $_smarty_tpl->tpl_vars['page']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['page']->value['numero'];?>
</a></li>
                  <?php }?>
              <?php
$_smarty_tpl->tpl_vars['page'] = $foreach_page_Sav;
}
?>
              <li>
                <a href="<?php echo $_smarty_tpl->tpl_vars['aPaginationListe']->value['liste']['page_suivante'];?>
" aria-label="Next">
                  <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
                </a>
              </li>
              <li>
                <a href="<?php echo $_smarty_tpl->tpl_vars['aPaginationListe']->value['liste']['page_derniere'];?>
" aria-label="Previous">
                  <span aria-hidden="true" class="glyphicon glyphicon-fast-forward"></span>
                </a>
              </li>
            </ul>
            <select onchange="document.location.href=this.value" style="display: inline-block;margin: 20px 0;vertical-align: top;height: 34px;margin-left: 5px;">
                <option value="">Numéro de la page</option>
                <?php
$_from = $_smarty_tpl->tpl_vars['aPaginationListe']->value['select'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['page'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['page']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['page']->value) {
$_smarty_tpl->tpl_vars['page']->_loop = true;
$foreach_page_Sav = $_smarty_tpl->tpl_vars['page'];
?>
                    <?php if ($_smarty_tpl->tpl_vars['page']->value['active'] != '') {?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['page']->value['url'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['page']->value['selected']) && $_smarty_tpl->tpl_vars['page']->value['selected']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['page']->value['numero'];?>
</option>
                    <?php }?>
                <?php
$_smarty_tpl->tpl_vars['page'] = $foreach_page_Sav;
}
?>
            </select>
        </div>
    <?php }?>
  <?php }?>
  <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value) && count($_smarty_tpl->tpl_vars['itemBoutonsListe']->value) > 0) {?>
        <div style="clear:both;float:right;margin-top:20px;">
            <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent'])) {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['url'];?>
">
                    <input
                            type="button"
                            value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['text_label'];?>
"
                            <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['style'];?>
"<?php }?>
                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['tableau_attribut'])) {?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                    <?php }?>
                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['fonction_javascript'])) {?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                    <?php }?>
                    >
                </a>
            <?php }?>
            <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant'])) {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['url'];?>
">
                    <input
                            type="button"
                            id="id_<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['nom_variable'];?>
"
                            name="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['nom_variable'];?>
"
                            value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['text_label'];?>
"
                            <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['style'];?>
"<?php }?>
                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['tableau_attribut'])) {?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                    <?php }?>
                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['fonction_javascript'])) {?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                    <?php }?>
                    >
                </a>
            <?php }?>
            <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider'])) {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['url'];?>
">
                    <input
                            type="button"
                            id="id_<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['nom_variable'];?>
"
                            name="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['nom_variable'];?>
"
                            value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['text_label'];?>
"
                            <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['style'];?>
"<?php }?>
                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['tableau_attribut'])) {?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                    <?php }?>
                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['fonction_javascript'])) {?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                    <?php }?>
                    >
                </a>
            <?php }?>
            <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler'])) {?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['url'];?>
">
                    <input
                            type="button"
                            value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['text_label'];?>
"
                            <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['style'];?>
"<?php }?>
                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['tableau_attribut'])) {?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                    <?php }?>
                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['fonction_javascript'])) {?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$foreach_valeur_Sav = $_smarty_tpl->tpl_vars['valeur'];
?>
                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $foreach_valeur_Sav;
}
?>
                    <?php }?>
                    >
                </a>
            <?php }?>
        </div>
    <?php }?>
<?php }
}
}
?>