<?php
/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 16/12/2015
 * Time: 17:27
 */

class ctrl_fli_connexion extends class_form_list{

    /**
     * @return mixed
     * Fonction qui permet la connexion d'un utilisateur / mot de passe en SHA-256
     */
    public function fli_connexion(){

        $aVar = array( 'login', 'password','guid','auto' );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $erreur = "";


        if($auto == 'ok')
        {

            $objImport = new class_import();
            $objImport->import_modele('loginadserveur','mod_fli_connexion');

            $objModCon = new mod_fli_connexion();

            $sRequeteVerifConnection = "SELECT id_client as id_user,accesdirect_client as guid_user,'fr' as langue_user,
        'N' as activtraductionmodif_user FROM client WHERE 
         supplogique_client='N' AND login_client='".$login."' AND pass_client='".$password."'";




            $aResultRequete = $objModCon->renvoi_info_requete($sRequeteVerifConnection);

            //echo"<pre>";print_r($aResultRequete);echo"</pre>";
            //exit();
            if(!empty($aResultRequete))
            {
                class_fli::set_guid_user($aResultRequete[0]['guid_user']);

                setcookie(class_fli::get_nom_cookie(), $aResultRequete[0]['guid_user'], (time() + 604800));
                header('Location: '.class_fli::get_chemin_acces_absolu().'/'.class_fli::get_module_defaut().'-'.class_fli::get_controleur_defaut().'-'.class_fli::get_fonction_defaut());

            }

        }


        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $objImport = new class_import();
            $objImport->import_modele('loginadserveur','mod_fli_connexion');

            $objModCon = new mod_fli_connexion();

            $aSel = $objModCon->get_sel_user($login);
            if(isset($aSel[0]['sel_user']) && !empty($aSel[0]['sel_user'])){
                $sSel = $aSel[0]['sel_user'];
            }else{
                $sSel = '';
            }

            //$password = hash( 'sha512', $password.$sSel );

            $aTabUser = $objModCon->get_user($login,$password);
            


            if(!empty($aTabUser)){
                //Création du cookie
                class_fli::set_guid_user($aTabUser[0]['guid_user']);

                setcookie(class_fli::get_nom_cookie(), $aTabUser[0]['guid_user'], (time() + 604800));
                header('Location: '.class_fli::get_chemin_acces_absolu().'/'.class_fli::get_module_defaut().'-'.class_fli::get_controleur_defaut().'-'.class_fli::get_fonction_defaut());
                exit;
            }else{
                $erreur = "Le login et/ou le mot de passe ne correspond(ent) pas !";
            }
        }

        $this->sNomTable=$this->sPrefixeDb.'users';
        $this->sChampId='id_user';
        $this->sChampSupplogique='supplogique_user';
        $this->sDebugRequete=false;
        $this->sLabelCreationElem=true;
        $this->bTraiteConnexion=false;
        $this->sAffFiche=false;
        $this->sAffMod=true;
        $this->sAffSupp=true;
        $this->sPagination=true;
        $this->sNombrePage=50;
        $this->sFiltreliste='';
        $this->sTitreForm = 'Gestion de vos taches et suivi';
        $this->FiltrelisteOrderBy='';
        $this->sTitreCreationForm='Création d\'une nouvelle ';
        $this->sTitreModificationForm='Modification d\'une ';
        $this->sMessageCreationSuccesForm='La ligne  a été créee';
        $this->sMessageModificationSuccesForm='La  a bien été modifiée';
        $this->sMessageErreurForm = 'Veuillez compléter tous les champs obligatoires';
        $this->sMessageSupprElem = 'La ligne  a été supprimée';
        $this->sLabelCreationElem = 'Créer une tache ou un suvi ';
        $this->sTitreListe = 'Gestion de vos taches et suivi ';
        $this->sLabelNbrLigne = 'Tâche(s) correspond(ent) à votre recherche';
        $this->sLabelRecherche = 'Recherche d\'une ';
        $this->sLabelRetourListe = '';
        $this->sRetourValidationForm = 'liste';
        $this->sCategorieListe = '';
        $this->sListeTpl = 'login.tpl';
        $this->sTplSortie = 'principal_adserveur.tpl';

        $this->chargement('loginadserveur','version1');

        $this->objSmarty->assign('erreur',$erreur);
        $this->objSmarty->assign('titre_site',"AD-SERVEUR PUB");
        $this->objSmarty->assign('message',$erreur);

        //Ajout des css du module
        $aCss[0]['fichier'] = 'signin.css';
        class_fli::add_fli_css($aCss,'fli_connexion');

        $aTabTpl[] = $this->run();

        return $aTabTpl;
    }

}