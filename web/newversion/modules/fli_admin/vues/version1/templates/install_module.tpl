<div style="color:#313131;margin: 0 auto;padding: 15px;margin-top: 10px;margin: 0 auto;text-align: center;padding-top: 10px;">
    {if $bDebugRequete}
        <p>
            Requete Select : <br><br>
            {$sDebugRequeteSelect}<br><br>
            Requete Insert/Update : <br><br>
            {$sDebugRequeteInsertUpdate}<br><br>
        </p>
    {/if}
    {if isset($aForm)}
        <div id="page-heading"><div style="display:inline-block;border:1px solid #D0D1D5;margin-bottom: 20px;"><div class="hidden-xs title-head-search" style="display: inline-block;height: 73px;width: 73px;vertical-align: middle;padding: 20px;"><span class="glyphicon glyphicon-link" style="color:#fff;vertical-align: bottom;font-size: 20px;top: 6px;"></span></div><h1 style="font-size:30px;margin: 0px;display:inline-block;padding: 20px;vertical-align: bottom;" class="text-head-search">{$sTitreForm}</h1></div></div>
        <div style="margin-bottom:20px;">
            {if $sCategorieForm|is_array}
                {foreach name=scategorieform from=$sCategorieForm item=objCategorieForm}<div style="background-color:#FFF;display:inline-block;vertical-align:top;height:20px;font-size:18px;padding:10px 0;font-family:Tahoma;">{$objCategorieForm.index}</div><div style="background-color:{if $objCategorieForm.suivant}#e3e3e3{else}#777{/if};height:40px;display:inline-block;vertical-align:middle;"><div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div><div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;">{$objCategorieForm.texte}</div>{if ! $smarty.foreach.scategorieform.last}<div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-right-radius: 0;border-bottom-right-radius: 0;background-color:#FFF;"></div>{/if}</div>{/foreach}<div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div>
            {else}
                <div style="margin-bottom: 20px;">
                    <div style="display: inline-block;" class="sub-title-head-search">
                        <span class="glyphicon glyphicon-tags sub-title-head-search" style="display: inline-block;height: 35px;width: 35px;padding: 10px;vertical-align: top;top:0px;"></span>
                        <h4 class="sub-text-head-search" style="display: inline-block;margin: 0;padding: 10px;font-size: 14px;">Installation de modules</h4>
                    </div>
                </div>
            {/if}
        </div>
        {if isset($bMessageSuccesForm) and $bMessageSuccesForm}
            <div class="popupAlert alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {if isset($sMessageSuccesForm)}{$sMessageSuccesForm}{/if}
            </div>
        {/if}
        {if isset($bMessageErreurForm) and $bMessageErreurForm}
            <div class="popupAlert alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {if isset($sMessageErreurForm)}{$sMessageErreurForm}{/if}
            </div>
        {/if}
        <form
                method="POST"
                action="fli_admin-install_module"
                enctype="multipart/form-data"
        >
                <div style="display: inline-block;background-color: #fff;padding: 15px;border: 1px solid #D0D1D5;">
                    <table border="0" cellpadding="0" cellspacing="0" id="id-form" style="margin: 0 auto;padding:20px;background-color:#fff;" class="table table-condensed table-hover">
                        <tbody>
                            <tr>
                                <th style="border:0;">Module au format .zip uniquement</th>
                                <td style="border:0;text-align: left;">
                                    <div style="border-radius:6px;border:1px solid #ACACAC;padding:3px;display:inline-block;vertical-align:middle;">
                                        <input
                                                type="file"
                                                id="id_module"
                                                name="module"
                                        >
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div style="clear:both;margin-top:20px;text-align: center;">
                        <input
                                type="submit"
                                class="btn btn-primary btnValForm"
                                id="id_install"
                                name="install"
                                value="Installer le module"
                        >
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="fli_admin-modules">
                            <input
                                    type="button"
                                    class="btn btn-primary btnResForm"
                                    value="Annuler"
                            >
                        </a>
                    </div>
                </div>
        </form>
    {/if}
</div>