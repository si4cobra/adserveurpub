<?php

/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 18/04/2018
 * Time: 18:36
 */
class ctrl_programmation  extends class_form_list
{
    public function __construct($objSmartyImport,$sModule){

        parent::class_list($objSmartyImport,$sModule);
        $objImport = new class_import();
        $objImport->import_modele('adserveur', 'mod_adserver');
        $objMod = new mod_adserver();
        $this->objBdd = $objMod;



    }

    public function controle_form_fli_programmation()
    {

        $aVar = array( "mail","choix","date_fin","date_debut","mail","tel","validation", "heure_debut","heure_fin","id_lienclientrim",'id_planning','changeVisuel' , 'desactivation');
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $sTableauRetourControl['result'] = true;
        $sTableauRetourControl['message'][1]="Enregistrement réussit";

        //echo"<pre>";print_r($_POST);echo"</pre>";


        if($changeVisuel == 'Y' or empty($changeVisuel))
        {
            if(empty($_FILES['fichier_ecran']) ){
                $sTableauRetourControl['result'] = false;
                $sTableauRetourControl['message'][1]="Vous devez séelctionner un fichier";

            }

            if(!empty($_FILES['fichier_ecran']) and $_FILES['fichier_ecran']['name']==""){
                $sTableauRetourControl['result'] = false;
                $sTableauRetourControl['message'][1]="Vous devez séelctionner un fichier";
            }
            else{
                $fichier_ecran_info_image = getimagesize($_FILES['fichier_ecran']['tmp_name']);


                if ($fichier_ecran_info_image['mime'] == 'application/x-shockwave-flash') {
                    if ($fichier_ecran_info_image[0] / $fichier_ecran_info_image[1] != $this->objBdd->renvoi_variable_config('longueur_autorisee_image_pub') / $this->objBdd->renvoi_variable_config('hauteur_autorisee_image_pub')) {
                        $sTableauRetourControl['message'][1] = 'Les dimensions de l\'image ne sont pas autorisées';
                        $sTableauRetourControl['result'] = false;
                    }
                } else {
                    if ($fichier_ecran_info_image[0] !=  $this->objBdd->renvoi_variable_config('longueur_autorisee_image_pub')
                        || $fichier_ecran_info_image[1] !=  $this->objBdd->renvoi_variable_config('hauteur_autorisee_image_pub')) {
                        $sTableauRetourControl['message'][] = 'Les dimensions de l\'image ne sont pas autorisées.';
                        $sTableauRetourControl['result'] = false;
                    }
                }

                //echo"<pre>";print_r($_FILES['fichier_ecran']['size']);echo"</pre>";

                //echo"<pre>";print_r($this->objBdd->renvoi_variable_config('taille_maximum_image_pub'));echo"</pre>";

                if (($_FILES['fichier_ecran']['size'] >=  $this->objBdd->renvoi_variable_config('taille_maximum_image_pub'))) {
                    $sTableauRetourControl['message'][] = 'Le poids de l\'image est trop important. Veuillez le réduire.';
                    $sTableauRetourControl['result'] = false;
                }
            }
        }

        if( !empty($date_debut)   ){
        $formulaire['date_debut'] = implode('-', array_reverse(explode('/', $date_debut)));

        if ($strtotime = strtotime($formulaire['date_debut'])  &&  date('Y', strtotime($formulaire['date_debut']))>'2000'   ) {
            $formulaire['date_debut'] = date('Y-m-d', $strtotime);
        } else {
            $sTableauRetourControl['message'][1]  = 'La date de début est erronée. Veuillez retaper la date de début correctement';
            $sTableauRetourControl['result'] = false;
        }

        }
        else {
            $sTableauRetourControl['message'][1]  = 'La date de début est erronée. Veuillez retaper la date de début correctement';
            $sTableauRetourControl['result'] = false;
        }




        if( !empty($date_fin)   ){
            $formulaire['date_fin'] = implode('-', array_reverse(explode('/', $date_fin)));

            if ($strtotime = strtotime($formulaire['date_fin'])   &&  date('Y', strtotime($formulaire['date_fin']))>'2000'   ) {
                $formulaire['date_fin'] = date('Y-m-d', $strtotime);
            } else {
                $sTableauRetourControl['message'][1]  = 'La date de fin est erronée. Veuillez retaper la date de fin correctement';
                $sTableauRetourControl['result'] = false;
            }
        }
        else {
            $sTableauRetourControl['message'][1]  = 'La date de fin est erronée. Veuillez retaper la date de fin correctement';
            $sTableauRetourControl['result'] = false;
        }



        if(strtotime($date_fin) < strtotime($date_debut))
        {
            $sTableauRetourControl['message'][1]  = 'La date de début doit être inférieur date de fin . Veuillez corriger les dates';
            $sTableauRetourControl['result'] = false;
        }


        if( !empty($date_debut)  && !empty($date_fin)   ) {
            $datetime1 = date_create($date_debut);
            $datetime2 = date_create($date_fin);

            //  echo"<pre>";print_r('Date Debut');echo"</pre>";
            // echo"<pre>";print_r($datetime1);echo"</pre>";

            //  echo"<pre>";print_r('Date fin');echo"</pre>";
            //  echo"<pre>";print_r($datetime2);echo"</pre>";


            $interval = date_diff($datetime1, $datetime2);


            if( $interval->format('%y') > 2 ) {
                $sTableauRetourControl['message'][1] = "l'intervalle entre la date de début et la date de fin ne doit pas être supérieur à deux ans";
                $sTableauRetourControl['result'] = false;
            }
        }


      //  echo"<pre>";print_r($interval);echo"</pre>";

       // exit();


        //Controle que en mode insertion
        if(empty($id_planning)  && $desactivation =='N' )
        {
            $aTabVerifDatePlanning = $this->objBdd->ctrl_planification($date_debut,$date_fin , $heure_debut , $heure_fin , $id_lienclientrim,$id_planning);

            if(!empty($aTabVerifDatePlanning))
            {
                $sTableauRetourControl['message'][1] = 'La période a déjà un planning';
                $sTableauRetourControl['result'] = false;
            }
        }




        return $sTableauRetourControl;


    }

    public function renvoi_affiche_cellule_textjour($objElement,$objValeurBddListe){

        // echo"";print_r($objValeurBddListe);echo"";

         $atablistjour =str_split($objValeurBddListe[$objElement['mapping_champ']]['value']);
         
         //echo"<pre>";print_r($atablistjour);echo"</pre>";
         
         //echo"<pre>";print_r($atablistjour);echo"</pre>";
        $aTabJour[0]="Dim";
        $aTabJour[1]="Lun";
        $aTabJour[2]="Mar";
        $aTabJour[3]="Mer";
        $aTabJour[4]="Jeu";
        $aTabJour[5]="Ven";
        $aTabJour[6]="Sam";
        $sMessage_Post="";
        if(!empty($atablistjour[0])){
            foreach($atablistjour as $valeur){
                $sMessage_Post.=$aTabJour[$valeur]." ";
            }
        }
        
        //echo"<pre>";print_r($sMessage_Post);echo"</pre>";


        $aTableRetour['keys'] = $objElement['nom_variable'];
        $objValeurBddListe[$objElement['mapping_champ']]['value']=$sMessage_Post;
        $aTableRetour['value']=$objValeurBddListe[$objElement['mapping_champ']];
        return $aTableRetour;
    }

    public function renvoi_affiche_cellule_texthoraire($objElement,$objValeurBddListe){
        $sMessage_Post='Horaire programmé';
        //echo"<pre>";print_r($objValeurBddListe);echo"</pre>";

        if($objValeurBddListe['heuredebut_planning']['value']=="00:00:00" and $objValeurBddListe['heurefin_planning']['value']=="23:59:59"){
            $sMessage_Post='Toute la journée';
        }else{
            $sMessage_Post="de ".substr($objValeurBddListe['heuredebut_planning']['value'],0,5)." à ".substr($objValeurBddListe['heurefin_planning']['value'],0,5);
        }
        $aTableRetour['keys'] = $objElement['nom_variable'];
        $objValeurBddListe[$objElement['mapping_champ']]['value']=$sMessage_Post;
        $aTableRetour['value']=$objValeurBddListe[$objElement['mapping_champ']];
        //echo"<pre>";print_r($aTableRetour);echo"</pre>";
        return $aTableRetour;
    }

    public function renvoi_affiche_cellule_imageprogrammation($objElement,$objValeurBddListe)
    {
        //echo"<pre>";print_r($objValeurBddListe['id_ecran_rim_ecran']['value']);echo"</pre>";

        $fichier = $objValeurBddListe['id_ecran_rim_ecran']['value'];

        $extention_fichier = explode('.',$fichier);

        $aTabRetour['keys'] = 'extention';

        if (isset($extention_fichier[1])) {
        $aTabRetour['value']['value'] = $extention_fichier[1];
            }else
        { $aTabRetour['value']['value'] =''; }
        return $aTabRetour;
    }

    public function post_form_insert_fli_programmation()
    {

        $aVar = array( 'idrt', 'numface', 'bdebug', 'id_lienclientrim','heure_debut','heure_fin','desactivation');
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }




        $fichier = $_FILES;
        //echo"<pre>";print_r($fichier);echo"</pre>";

        $name = $fichier['fichier_ecran']['name'];

        $tmp = $fichier['fichier_ecran']['tmp_name'];
        
        //echo"<pre>";print_r($name);echo"</pre>";

        //echo"<pre>";print_r($tmp);echo"</pre>";

        $typeFichier = explode('.',$name);
        if(isset( $typeFichier[1]) )
        {
            $aTabTypeFichier = $this->objBdd->get_InfoTypeFichier($typeFichier[1]);
        }
        if(!empty($aTabTypeFichier[0]))
        {
            $typeFichier2 = $aTabTypeFichier[0]['id_typefichier'];

            $imageprincipal = "image".date('dmyHms')."_".".".$typeFichier[1];

            if(!move_uploaded_file($tmp,"../repimages/$imageprincipal"))
            {
                $aTabMessage['bResult_Post'] = 'error';
                $aTabMessage['sMessage_Post'] = "Image $name n'a pus être uploader ";
                if(!move_uploaded_file($tmp,"../images_upload_produits/$imageprincipal"))
                {
                    $aTabMessage['bResult_Post'] = 'error';
                    $aTabMessage['sMessage_Post'] = "Image $name n'a pus être uploader ";



                }


            }
            else
            {
                $aTabMessage['bResult_Post'] = 'valid';
                $aTabMessage['sMessage_Post'] = "";

                $aTabInfoEcran = array();


                $aTabInfoClientFromIdLienClientRim = $this->objBdd->get_InfoClientFromIdLienClientRim($id_lienclientrim);

                $id_client = $aTabInfoClientFromIdLienClientRim[0]['id_client'];

                $aTabInfoEcran['id_client'] = $id_client;
                $aTabInfoEcran['id_typefichier'] = $typeFichier2;
                $aTabInfoEcran['cheminfichier_ecran'] = $imageprincipal;



                $id_ecran = $this->objBdd->add_rimecran($aTabInfoEcran);

                $id_rimplanning = $this->aTabResultInsert['id'];

                $sJour = "";

                if(!empty($_POST['periodicite_jour']))
                {
                    foreach($_POST['periodicite_jour'] as $ejour)
                    {
                        $sJour .= $ejour;
                    }
                }



                if( empty($desactivation  ) )
                {
                    $this->objBdd->desactiver_planning($id_rimplanning, 'Y');

                }





                $this->objBdd->update_rimplanning($id_ecran,$id_rimplanning,$sJour,$heure_debut,$heure_fin);

                //$this->objBdd->add_rimecran($aTabInfoEcran);



            }


            //Envoie mail Alert
            $nom_client = $this->objBdd->renvoi_nom_client_par_id_client($id_client);
            $sSujet = "[ADSERVER][PUB][NOUVELLE AFFICHE] client ".$nom_client." numéro ".$id_client."";
            $sMessage=" Le client ".$nom_client." numéro ".$id_client." a créé/modifié une nouvelle affiche ADSERVER PUB<br>";



            if ($typeFichier2 != 'swf') {
                $sMessage .= '<img src="https://pub.adserveur-naja.fr/repimages/'.$imageprincipal.'" />';
            } else {
                $sMessage .= '<a href="https://pub.adserveur-naja.fr/repimages/'.$imageprincipal.'">Voir le fichier flash</a>';
            }



            $aParam['idalerte']="2952";
            $aParam['message']=$sMessage;
            $aParam['sujet']=$sSujet;
            $aParam['monmail']="Cobranaja Adserver Pub";

            $this->objBdd->envoi_mail_liste($aParam);

        }
        else
        {
            $aTabMessage['bResult_Post'] = 'error';
            $aTabMessage['sMessage_Post'] = "Le format du fichier $name n'est pas pris en compte";

        }

        return $aTabMessage;

    }

    public function post_form_update_fli_programmation()
    {

        $aVar = array( 'idrt', 'numface', 'bdebug', 'id_lienclientrim','id_planning','changeVisuel','heure_debut','heure_fin');
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }



        if($changeVisuel == 'Y')
        {
            $fichier = $_FILES;
            //echo"<pre>";print_r($fichier);echo"</pre>";

            $name = $fichier['fichier_ecran']['name'];

            $tmp = $fichier['fichier_ecran']['tmp_name'];

            //echo"<pre>";print_r($name);echo"</pre>";

            //echo"<pre>";print_r($tmp);echo"</pre>";

            $typeFichier = explode('.',$name);

            $aTabTypeFichier =  $this->objBdd->get_InfoTypeFichier($typeFichier[1]);

            //echo"<pre>";print_r($aTabTypeFichier);echo"</pre>";

            if(!empty($aTabTypeFichier[0]))
            {
                $typeFichier2 = $aTabTypeFichier[0]['id_typefichier'];

                $imageprincipal = "image".date('dmyHms')."_".".".$typeFichier[1];

                if(!move_uploaded_file($tmp,"../repimages/$imageprincipal"))
                {
                    $aTabMessage['bResult_Post'] = 'error';
                    $aTabMessage['sMessage_Post'] = "Image $name n'a pus être uploader ";

                    if(!move_uploaded_file($tmp,"../images_upload_produits/$imageprincipal"))
                    {
                        $aTabMessage['bResult_Post'] = 'error';
                        $aTabMessage['sMessage_Post'] = "Image $name n'a pus être uploader ";



                    }

                }
                /*if(!move_uploaded_file($tmp,"../images_upload_produits/$imageprincipal"))
                {
                    $aTabMessage['bResult_Post'] = 'error';
                    $aTabMessage['sMessage_Post'] = "Image $name n'a pus être uploader ";



                }*/
                else
                {
                    //echo"<pre>";print_r('test');echo"</pre>";
                    $aTabMessage['bResult_Post'] = 'valid';
                    $aTabMessage['sMessage_Post'] = "";

                    $aTabInfoEcran = array();


                    $aTabInfoClientFromIdLienClientRim = $this->objBdd->get_InfoClientFromIdLienClientRim($id_lienclientrim);

                    $id_client = $aTabInfoClientFromIdLienClientRim[0]['id_client'];

                    $aTabInfoEcran['id_client'] = $id_client;
                    $aTabInfoEcran['id_typefichier'] = $typeFichier2;
                    $aTabInfoEcran['cheminfichier_ecran'] = $imageprincipal;

                    /*echo"<pre>";print_r($aTabInfoEcran);echo"</pre>";

                    exit();*/

                    $id_ecran = $this->objBdd->add_rimecran($aTabInfoEcran);

                    $sJour = "";

                    if(!empty($_POST['periodicite_jour']))
                    {
                        foreach($_POST['periodicite_jour'] as $ejour)
                        {
                            $sJour .= $ejour;
                        }
                    }

                    $this->objBdd->update_rimplanning($id_ecran,$id_planning,$sJour,$heure_debut,$heure_fin);

                    //$this->objBdd->add_rimecran($aTabInfoEcran);

                }



                    //Envoie mail Alert
                    if( isset($id_lienclientrim)  && !empty($id_lienclientrim)  )
                    { $aTabInfoClientFromIdLienClientRim = $this->objBdd->get_InfoClientFromIdLienClientRim($id_lienclientrim);



                        $id_client = $aTabInfoClientFromIdLienClientRim[0]['id_client'];
                        if( isset($id_client) )
                        {
                            $nom_client = $this->objBdd->renvoi_nom_client_par_id_client($id_client);
                            $sSujet = "[ADSERVER][PUB][MODIFICATION AFFICHE] client ".$nom_client." numéro ".$id_client."";
                            $sMessage=" Le client ".$nom_client." numéro ".$id_client." a créé/modifié une nouvelle affiche ADSERVER PUB<br>";




                            if ($typeFichier2 != 'swf') {
                                $sMessage .= '<img src="https://pub.adserveur-naja.fr/repimages/'.$imageprincipal.'" />';
                            } else {
                                $sMessage .= '<a href="https://pub.adserveur-naja.fr/repimages/'.$imageprincipal.'">Voir le fichier flash</a>';
                            }



                            $aParam['idalerte']="2952";
                            $aParam['message']=$sMessage;
                            $aParam['sujet']=$sSujet;
                            $aParam['monmail']="Cobranaja Adserver Pub";

                            $this->objBdd->envoi_mail_liste($aParam);
                         }

                    }
            }
            else
            {
                $aTabMessage['bResult_Post'] = 'error';
                $aTabMessage['sMessage_Post'] = "Le format du fichier $name n'est pas pris en compte";

            }
        }
        else
        {

            $aTabMessage['bResult_Post'] = 'valid';
            $aTabMessage['sMessage_Post'] = "";

            $sJour = "";

            if(!empty($_POST['periodicite_jour']))
            {
                foreach($_POST['periodicite_jour'] as $ejour)
                {
                    $sJour .= $ejour;
                }
            }


            $aTabEcranByIdPLanning = $this->objBdd->get_EcranByIdPlanning($id_planning);

            //echo"<pre>";print_r($aTabEcranByIdPLanning);echo"</pre>";
            
            //exit();
            
            $this->objBdd->update_rimplanning($aTabEcranByIdPLanning[0]['id_ecran'],$id_planning,$sJour,$heure_debut,$heure_fin);


        }


        return $aTabMessage;

    }




    function fli_programmation()
    {


        //echo"<pre>";print_r($_POST);echo"</pre>";

        $aVar = array( 'idrt', 'numface', 'bdebug', 'id_lienclientrim', 'desct', 'idplanning', 'act', 'id_planning', 'id_face', 'numface' );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }


        if( $desct == 'ok' ) {
            $this->objBdd->desactiver_planning($idplanning, 'Y');
        }
        if( $act == 'ok' ) {
            $this->objBdd->desactiver_planning($idplanning, 'N');
        }

        $aTabLastDate = array();

        $aTabLastDate2 = $this->objBdd->get_LastDateProgrammation($id_lienclientrim, $idrt, $id_face);

        if( isset($aTabLastDate2) and !empty($aTabLastDate2[0]) ) {
            $aTabLastDate = $aTabLastDate2[0]['nextDate'];
        }

        //echo"<pre>";print_r($aTabLastDate);echo"</pre>";

        //echo"<pre>";print_r('toto');echo"</pre>";

        //$aTabVerifPlanning = $this->objBdd->ctrl_planification('2019-10-09 00:00:00','2019-10-31 00:00:00','1687');

        //echo"<pre>";print_r($aTabVerifPlanning);echo"</pre>";

        //BDD
        $this->sNomTable = 'rim_planning';                            //string Nom de la table liée
        $this->sChampId = 'id_planning';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid = '';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique = 'supplogique_planning';                    //string Nom du champ supplogique dans la table liée
        //Booléen
        $this->bTraiteConnexion = true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bKeyMultiple = false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bDebugRequete = $bdebug;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffDup = false;                            //bool bAffDup Permet de dupliquer une ligne
        $this->bAffTitre = true;                          //bool Permet d'afficher le titre de la liste
        $this->bAffFiche = false;                         //bool Permet d'afficher la liste (si aucun des autres droits vaut true)
        $this->bLabelCreationElem = true;                 //bool Permet d'afficher le bouton d'ajout
        $this->bAffMod = true;                           //bool Permet la modification
        $this->bAffRecapLigne = false;                     //bool  bAffRecapLigne Permet d'affiche dans unenouvelle fenetre le recap de la ligne
        $this->bAffSupp = true;                          //bool Permet la suppression
        $this->bBtnRetour = false;                        //bool Permet d'afficher le bouton 'Retour' de la liste
        $this->bPagination = true;                       //bool Permet l'affichage de la pagination
        $this->bAffAnnuler = true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->bAffListeQuandPasRecherche = true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->bRetourSpecifique = false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->bUseDelete = false;                       //bool Permet l'affichage de la pagination
        $this->bAffNombreResult = true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable = false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->bAffPrintBtn = false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug = false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bActiveFormSelect = false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup = false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->bRadioSelect = false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->bCheckboxSelect = false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire)
        $this->bCsv = false;                              //bool Permet d'afficher le bouton d'export en csv
        //Entier
        $this->iNombrePage = 50;                          //int Nombre de lignes désirées par page
        //String
        $this->sFiltreliste = '';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND')
        $this->sFiltrelisteOrderBy = 'order by  desactivation_planning DESC, datedebut_planning DESC';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->sTitreCreationForm = 'Enregistrement d\'un';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessage_PostCreationSuccesForm = 'Enregistement réussie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sMessage_PostDuplicationSucces = 'Duplication de la ligne reussie';                           // sMessage_PostDuplicationSucces Message affiché lorsquela duplication de la ligne s'est déroulé avec succès
        $this->sMessage_PostDuplicationError = 'Erreur pendant la duplication de la ligne';                          //string sMessage_PostDuplicationError Message affiché lorsque la duplication de la ligne s'est déroulé avec uen erreur
        $this->sTitreModificationForm = 'Modification d\'un thème';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessage_PostModificationSuccesForm = 'Modification reussie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->sMessage_PostSupprElem = 'Suppression réussie';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessage_PostErreurForm = 'Problème survenue pendant l\'enregistrement';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem = 'Aj';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche = 'Rechercher un';                      //string Titre de la section recherche
        $this->sTitreListe = '';                          //string Titre de la liste
        $this->sTitreForm = '';                           //string Titre du formulaire
        $this->sLabelNbrLigne = 'nombre(s) sur votre sélection';                       //string Texte à coté du nombre de lignes
        $this->sLabelRetourListe = '';                    //string Texte du bouton 'Retour' sur la liste
        $this->sLabelFileRetourElem = '';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->sUrlRetourConnexion = '';                  //string Module-controleur-fonction spécifié pour le module de connexion à utiliser
        $this->sDirRetour = 'adserveur-ctrl_face-fli_face?';                           //string Module-controleur-fonction spécifié pour le bouton 'Retour'
        $this->sRetourElemUrl = '';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sDirpagination = '';                       //string Le module-controleur-fonction pour la pagination quand l'url est spécifique
        $this->sUrlRetourSpecifique = '';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sTypeFichier = '';                         //string Le type de fichier attendu
        $this->sLienLigne = '';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert = '';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate = '';              //string Script executé après un UPDATE
        $this->sSuiteRequeteInsert = '';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate = '';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sListeTpl = 'liste_programation.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sFormulaireTpl = 'formulaire_adserver.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTplSortie = 'principal_adserveur.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)
        $this->sListeFilsTpl = 'liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->sListeFitreNopDuplicate = '';              //string sListeFitreNopDuplicate renvoi la liste des champs qu'on veut exclure de la duplication séparé par un point virgule
        //Array
        $this->aElement = array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->itemBoutonsForm = array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->aListeFils = array();                      //array Tableau contenant les données de la liste fils
        $this->aSelectSqlSuppl = array();                 //array Tableau Permettant de rajouter des champs dans le SELECT de la liste
        //Wizard
        $this->sParametreWizardListe = '';                //string Les paramètres de la liste du wizard
        $this->sCategorieListe = '';                      //string La catégorie de la liste lors du wizard
        $this->itemBoutonsListe = array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->aParametreWizardListe = array();           //array Tableau contenant les paramètres du wizard
        $this->aParamsNonSuivi = array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        //Peu utilisés
        $this->sDebugRequeteSelect = '';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate = '';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->sRetourValidationForm = 'liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne = '';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm = 'liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sRetourAnnulationFormExterne = '';         //string Permet de spécifier l'url de l'annulation
        $this->sRetourSuppressionFormExterne = '';        //string Permet de spécifier l'url de retour après la suppression
        $this->sUrlRetourSuppressionFormExterne = '';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sRetourListe = null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        //inforamtion sur le menu automatique
        $this->sMenuIntitule = 'Liste programmation';
        $this->sMenuVisibile = true;
        $this->sMenuTarge = false;
        $this->chargement();                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module


        $this->ajout_champ(array(
            'type_champ' => 'hidden',
            'mapping_champ' => 'id_lienclientrim', // le nom du champ dans la table selectionnee
            'nom_variable' => 'id_lienclientrim', // la valeur de l attribut "name" dans le formulaire
            'text_label' => '', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_form' => 'ok',
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|'
            'transfert_inter_module' => 'ok',
            'titre_inter_module' => '',//si a ok active la récriture du titre
            'bbd_titre_inter_module' => '',//le nom de la base de donné ou on va chercher l'info
            'champfiltre_titre_inter_module' => '',// le chmap sur lequel on fait le filtre
            'champaffiche_titre_inter_module' => '',//le champ ou la composition de champ a afficher
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'date',
            'mapping_champ' => 'datedebut_planning', // le nom du champ dans la table selectionnee
            'nom_variable' => 'date_debut', // la valeur de l attribut "name" dans le formulaire
            'alias_champ' => 'datedebut', //alias de la date dormat dans la table
            'text_label' => 'Date de début', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => $aTabLastDate, // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ

            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => '', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'date_format' => '%d/%m/%Y', // le format de la date a respecte
            'date_conversion_enreg' => 'frtoeng',
            'date_now_creation' => '', // si la date doit etre pre-rempli a la creation
            'date_now_modification' => '', // si la date doit etre mise a jour a chaque modification
            'recherche_intervalle_date' => '', //si le champ doit etre en recherche par intervalle
            'recherche_intervalle_date_label' => array( 'min', 'max' ) //tableau contenant 2 valeurs, les labels pour les dates a intervalle dans la recherche
        ));

        $this->ajout_champ(array(
            'type_champ' => 'date',
            'mapping_champ' => 'datefin_planning', // le nom du champ dans la table selectionnee
            'nom_variable' => 'date_fin', // la valeur de l attribut "name" dans le formulaire
            'alias_champ' => 'datefin', //alias de la date dormat dans la table
            'text_label' => 'Date de fin', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''

            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => '', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'date_format' => '%d/%m/%Y', // le format de la date a respecte
            'date_conversion_enreg' => 'frtoeng',
            'date_now_creation' => '', // si la date doit etre pre-rempli a la creation
            'date_now_modification' => '', // si la date doit etre mise a jour a chaque modification
            'recherche_intervalle_date' => '', //si le champ doit etre en recherche par intervalle
            'recherche_intervalle_date_label' => array( 'min', 'max' ) //tableau contenant 2 valeurs, les labels pour les dates a intervalle dans la recherche
        ));

        $this->ajout_champ(array(
            'type_champ' => 'textjour',
            'mapping_champ' => 'periodicitejoursemaine_planning', // le nom du champ dans la table selectionnee
            'nom_variable' => 'periodicitejoursemaine_planning', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Jours', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => '', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td' => 'ok',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'texthoraire',
            'mapping_champ' => 'typeperiodicite_planning', // le nom du champ dans la table selectionnee
            'nom_variable' => 'typeperiodicite_planning', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Jours', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => '', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td' => 'ok',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'periodiciteheurprecise_planning', // le nom du champ dans la table selectionnee
            'nom_variable' => 'periodiciteheurprecise_planning', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Horaire', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => '', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td' => 'ok',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $aTmpList = array();
        $aTmpList["Y"] = "Désactivé";
        $aTmpList["N"] = "Activé";

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'desactivation_planning', // le nom du champ dans la table selectionnee
            'nom_variable' => 'desactivation', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Saisir un nom',
            'text_label' => 'Status', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'lesitem' => $aTmpList,
            'bdebug' => ""

        ));

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'id_ecran', // le nom du champ dans la table selectionnee
            'nom_variable' => 'id_ecran', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => '',
            'text_label' => 'Nom ecran', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => '', // faut il afficher le champ dans le formulaire 'ok'|''
            'alias_champ' => '', //alias du champ dans la table selectionnee
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'actif_jointure' => '', // cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche  'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => 'rim_ecran', // la table liee pour ce champ
            'id_table_item' => 'id_ecran', // le champ de la table liee qui sert de cle primaire
            'alias_table_item' => 'alias_ecran', //Alias du de la table dans la jointure ;
            'affichage_table_item' => 'cheminfichier_ecran', // le champ de la table liee qui sert de label d affichage
            'alias_champ_item' => '',  //alias du champ item dans la base la requete générale pour champ select et ou selectdist
            'supplogique_table_item' => 'supplogique_ecran',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_pere_fils' => '', // si la liste deroulante a une hierachie le nom du champ père
            'select_value_pere_base' => '', // si la liste deroulante a une hierachie valeurpar defaut du pere
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'bdebug' => ""
        ));


        $this->ajout_champ(array(
            'type_champ' => 'imageprogrammation',
            'mapping_champ' => 'id_ecran', // le nom du champ dans la table selectionnee
            'nom_variable' => 'id_ecran_swf', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Nom ecran', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => '', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'heuredebut_planning', // le nom du champ dans la table selectionnee
            'nom_variable' => 'heure_debut', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Heure debut', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td' => 'ok',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'heurefin_planning', // le nom du champ dans la table selectionnee
            'nom_variable' => 'heure_fin', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Heure fin', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td' => 'ok',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        $aTabListeJourAffichage = $this->objBdd->renvoi_jour($id_planning);


        //echo"<pre>";print_r($aTabListeJourAffichage);echo"</pre>";

        $aTabJour2[0] = "Dimanche";
        $aTabJour2[1] = "Lundi";
        $aTabJour2[2] = "Mardi";
        $aTabJour2[3] = "Mercredi";
        $aTabJour2[4] = "Jeudi";
        $aTabJour2[5] = "Vendredi";
        $aTabJour2[6] = "Samedi";


        $aTabImage = array();

        if( !empty($id_planning) ) {
            $aTabImage = $this->objBdd->renvoi_image_planning($id_planning);
        }


        if( !empty($id_planning) ) {
            foreach( $aTabListeJourAffichage as $ejour ) {
                $aTabJour2[$ejour] = "ok";
            }
        }


        //echo"<pre>";print_r($aTabJour2);echo"</pre>";


        $idcleint = class_fli::get_session();
        /* --------- $this->Ajout_champ() ici --------- */
        class_fli::set_aData("nom_client", $this->objBdd->renvoi_nom_client($idcleint));
        //Réseau tournant - RESEAU TOURNANT BOURG-LA-REINE (RTBL)


        if( !empty($idrt) ) {
            $sTitreRt =  $this->objBdd->renvoi_nom_rt($idrt);
        } else {
            $sTitreRt = "Emplacement - " . $this->objBdd->renvoi_nom_rt($id_lienclientrim);

        }
        //echo"<pre>";print_r($id_planning);echo"</pre>";
        if( empty($id_face) )
        {
            $id_face = $this->objBdd->renvoi_num_face($id_lienclientrim);
         }

        $sRetour = "adserveur-ctrl_liste_rt-fli_liste_rt?id=$idrt";

        if(!empty($id_face))
        {
            $sRetour = "adserveur-ctrl_face-fli_face?id_face=".$id_face;
        }

        //echo"<pre>";print_r($sRetour);echo"</pre>";
        
        //echo"<pre>";print_r($numface);echo"</pre>";
        
        $this->objSmarty->assign("sTitre",$sTitreRt);
        $this->objSmarty->assign("id_face",$id_face);
        $this->objSmarty->assign("sRetour",$sRetour);
        $this->objSmarty->assign("aTabJour2",$aTabJour2);
        $this->objSmarty->assign("id_planning",$id_planning);
        $this->objSmarty->assign("numeroface",$numface);
        $this->objSmarty->assign("numface",$numface);
        /*$this->objSmarty->assign("numeroface",$numface);
        $this->objSmarty->assign("numface",$numface);-**/
        $this->objSmarty->assign("idrt",$idrt);
        $this->objSmarty->assign("id_lienclientrim",$id_lienclientrim);


        /* --------- fin $this->Ajout_champ() --------- */

        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli
        //echo"<pre>";print_r($this->aListe);echo"</pre>";

        
        return $aTabTpl;
    }

}