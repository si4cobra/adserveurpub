<?php
/* Smarty version 3.1.29, created on 2018-03-22 14:48:04
  from "/var/www/html/modules/fli_menu/vues/version1/templates/menu.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ab3b4143bc268_77902435',
  'file_dependency' => 
  array (
    '19db820fd3b9787dd2ab67e47ca98bb4d3956ebb' => 
    array (
      0 => '/var/www/html/modules/fli_menu/vues/version1/templates/menu.tpl',
      1 => 1453709593,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
  'tpl_function' => 
  array (
    'menu' => 
    array (
      'called_functions' => 
      array (
      ),
      'compiled_filepath' => '/var/www/html/modules/fli_menu/vues/version1/templates_c/19db820fd3b9787dd2ab67e47ca98bb4d3956ebb_0.file.menu.tpl.php',
      'uid' => '19db820fd3b9787dd2ab67e47ca98bb4d3956ebb',
      'call_name' => 'smarty_template_function_menu_9829467895ab3b41438bd47_30463325',
    ),
  ),
),false)) {
function content_5ab3b4143bc268_77902435 ($_smarty_tpl) {
if (!empty($_smarty_tpl->tpl_vars['aTabMenu']->value)) {?>
<div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="javascript:void(0);">
                    <span class="glyphicon glyphicon-th-list"></span> Menu
                </a>
            </li>

            

            <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'menu', array('data'=>$_smarty_tpl->tpl_vars['aTabMenu']->value), true);?>


        </ul>
    </div>
    <!-- /#sidebar-wrapper -->
</div>
<?php }
}
/* smarty_template_function_menu_9829467895ab3b41438bd47_30463325 */
if (!function_exists('smarty_template_function_menu_9829467895ab3b41438bd47_30463325')) {
function smarty_template_function_menu_9829467895ab3b41438bd47_30463325($_smarty_tpl,$params) {
$saved_tpl_vars = $_smarty_tpl->tpl_vars;
$params = array_merge(array('level'=>0), $params);
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value);
}?>
                <?php if ($_smarty_tpl->tpl_vars['level']->value == 0) {?>
                    <li>
                <?php }?>
                <?php
$_from = $_smarty_tpl->tpl_vars['data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_entry_0_saved_item = isset($_smarty_tpl->tpl_vars['entry']) ? $_smarty_tpl->tpl_vars['entry'] : false;
$_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['entry']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->value) {
$_smarty_tpl->tpl_vars['entry']->_loop = true;
$__foreach_entry_0_saved_local_item = $_smarty_tpl->tpl_vars['entry'];
?>
                    <?php if ($_smarty_tpl->tpl_vars['entry']->value['est_parent']) {?>
                        <a data-toggle="collapse" href="#collapseChamp<?php echo $_smarty_tpl->tpl_vars['entry']->value['id_route'];?>
" data-target="#collapseChamp<?php echo $_smarty_tpl->tpl_vars['entry']->value['id_route'];?>
" aria-expanded="false" aria-controls="collapseChamp<?php echo $_smarty_tpl->tpl_vars['entry']->value['id_route'];?>
" style="margin-left: <?php echo $_smarty_tpl->tpl_vars['level']->value*20;?>
px;"><?php echo $_smarty_tpl->tpl_vars['entry']->value['intitule_menu_route'];?>
</a>
                    <?php } else { ?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['entry']->value['route_route'];?>
" onclick="$('#wrapper').toggleClass('toggled');" style="margin-left: <?php echo $_smarty_tpl->tpl_vars['level']->value*20;?>
px;"><?php echo $_smarty_tpl->tpl_vars['entry']->value['intitule_menu_route'];?>
</a>
                    <?php }?>
                    <?php if (is_array($_smarty_tpl->tpl_vars['entry']->value['fils'])) {?>
                        <?php if (!empty($_smarty_tpl->tpl_vars['entry']->value['fils'])) {?>
                            <div class="collapse" id="collapseChamp<?php echo $_smarty_tpl->tpl_vars['entry']->value['id_route'];?>
">
                            <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'menu', array('data'=>$_smarty_tpl->tpl_vars['entry']->value['fils'],'level'=>$_smarty_tpl->tpl_vars['level']->value+1), true);?>

                            </div>
                        <?php }?>
                    <?php }?>
                <?php
$_smarty_tpl->tpl_vars['entry'] = $__foreach_entry_0_saved_local_item;
}
if ($__foreach_entry_0_saved_item) {
$_smarty_tpl->tpl_vars['entry'] = $__foreach_entry_0_saved_item;
}
?>
                <?php if ($_smarty_tpl->tpl_vars['level']->value == 0) {?>
                    </li>
                <?php }?>
            <?php foreach (Smarty::$global_tpl_vars as $key => $value){
if (!isset($_smarty_tpl->tpl_vars[$key]) || $_smarty_tpl->tpl_vars[$key] === $value) $saved_tpl_vars[$key] = $value;
}
$_smarty_tpl->tpl_vars = $saved_tpl_vars;
}
}
/*/ smarty_template_function_menu_9829467895ab3b41438bd47_30463325 */
}
