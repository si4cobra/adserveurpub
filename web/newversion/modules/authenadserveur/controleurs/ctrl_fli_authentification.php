<?php

/**
 * Nom: ctrl_fli_authentification
 * Description: Permet de déterminer l'authentification et les droits de l'utilisateur
 * Date: 10/12/2015
 */
class ctrl_fli_authentification
{

    public function verifier_authentification()
    {
        $sPrefixeDb = class_fli::get_prefixe();

        $aTabDroits = array('a'=>0,'m'=>0,'s'=>0,'v'=>0,'w'=>0);

        $aSession = class_fli::get_session();

        if( !empty($aSession) ) {
            $objImport = new class_import();
            $objImport->import_modele('authenadserveur', 'mod_fli_authentification');
            $objModAuth = new mod_fli_authentification();

            //Vérification si l'utilisateur existe grâce son guid ($aSession qui provient du module fli_session)
            $aTabUser = $objModAuth->check_user($aSession);
            if( !empty($aTabUser) ) {
                class_fli::set_fli_est_connecte(true);
                //Mise à jour des variables static (connexion, guid_user, guid_groupes)
                $aTabDroits = array('a'=>1,'m'=>1,'s'=>1,'v'=>1,'w'=>1);
            }else{
                //Si l'utilisateur n'existe pas
                class_fli::set_aData('guid_user',NULL);
                class_fli::set_aData('guid_groupes',NULL);
            }
        }else{
            //Si il n'y a pas de tentative de connexion
            class_fli::set_aData('guid_user',NULL);
            class_fli::set_aData('guid_groupes',NULL);
            //echo "oo";
        }
        //echo"<pre>";print_r($aTabDroits);echo"</pre>";
        class_fli::set_droits($aTabDroits);

        return $aTabDroits;
    }

}