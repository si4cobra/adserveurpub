<?php
/**
 * Created by PhpStorm.
 * User: Yann
 * Date: 26/07/2018
 * Time: 17:23
 */


header ("Content-type: image/jpeg");

$image=$_GET['image'];

// Sauvegarde l'image
//imagepng($im);


$principal = "https://pub.adserveur-naja.fr/repimages/".$image;
$destination = imagecreatefromjpeg($principal); // La photo est la destination
$largeur_destination = imagesx($destination);
$hauteur_destination = imagesy($destination);

$limite_hauteur = $hauteur_destination/2 ;

$im = imagecreatetruecolor(55, 30);
$black = imagecolorallocate($im, 0, 0, 0);

// Dessine un rectangle noir
imagefilledrectangle($destination,0,$limite_hauteur-25,$largeur_destination,$limite_hauteur+25,$black);



$image_trait = "trait_".$image;

$imageFinal = imagejpeg($destination);

return $imageFinal;

