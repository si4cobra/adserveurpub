<?php header("Content-type: text/css"); ?>
nav, #wrapper, #wrapper2,  #rowContainer, .btnScrollLeft, .btnScrollRight, .td-action, .th-action, .tdHidePrint, .thHidePrint{
	display: none !important;
}
a[href]:after {
    content: "" !important;
}

#idtitreprint{
	display: block !important;
}

td,th{
	font-size: 12px !important;
	padding: 2px !important;
}
body, h1, h2, h3, ol, ul, div {     width: auto !important;     border: 0 !important;     margin: 0 1% !important;     padding: 0 !important;     float: none !important;     position: static !important;     overflow: visible !important; page-break-after:avoid;}
.table{
	margin-bottom:0 !important;
}