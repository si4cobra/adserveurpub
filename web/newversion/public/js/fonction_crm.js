//fonction de transfert d'une liste à une autre
function transfert_listedroulante(id,idresult){
	//suppression de la ligne source
	valeur = document.getElementById(id).value;
	champ = document.getElementById(id).options[document.getElementById(id).selectedIndex].text
	//alert(valeur);
	//nombre=document.getElementById(id).selectedIndex
	//document.getElementById(id).options[nombre] = null;
	
	//ajout dans la ligne source
	nouvel_element = new Option(champ,valeur,false,false);
	var tailleselect = document.getElementById(idresult).length;
	document.getElementById(idresult).options[tailleselect] = nouvel_element;
}

//fonction de suppression d'une liste déroulante
function supprimer_listedroulante(id) {
	nombre=document.getElementById(id).selectedIndex
	document.getElementById(id).options[nombre] = null;
}

//fonction de selection de toute une liste déroulante
function selectiontout(id){
	for(var i=0;i<document.getElementById(id).length;i++){
		document.getElementById(id).options[i].selected=true;
	}
}

//fucntion vérifant et récupérant la bonne implémentation de l'objet ajax
function getXMLHTTP(){
  var xhr=null;
  if(window.XMLHttpRequest) // Firefox et autres
  xhr = new XMLHttpRequest();
  else if(window.ActiveXObject){ // Internet Explorer
    try {
      xhr = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e1) {
        xhr = null;
      }
    }
  }
  else { // XMLHttpRequest non supporté par le navigateur
    alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
  }
  return xhr;
}


function previsualiser(idchamp, idtemplate){

	xhr_object = getXMLHTTP(); 
	xhr_object.open("POST","page/preview.php", true);
	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	

	var data ="id_template="+idtemplate;
	xhr_object.send(data);
	xhr_object.onreadystatechange = function() {
		if((xhr_object.readyState == 4) && (xhr_object.status == 200)){
			document.getElementById(idchamp).innerHTML = xhr_object.responseText;
		}
	}
    
	
}

function objet_template(idtemplate,idinput){
	xhr_object_input = getXMLHTTP(); 
	xhr_object_input.open("POST","page/objet_programme.php", true);
	xhr_object_input.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	var data ="id_template="+idtemplate;
	xhr_object_input.send(data);
	xhr_object_input.onreadystatechange = function() {
		if((xhr_object_input.readyState == 4) && (xhr_object_input.status == 200)){
			document.getElementById(idinput).value= xhr_object_input.responseText;
		}
	}

}

