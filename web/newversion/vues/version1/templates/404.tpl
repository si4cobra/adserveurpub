<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="fr"> <!--<![endif]-->
<head>
    <title>{$sTitreDeLaPage}</title>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    {literal}
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
    {/literal}
    <!-- <link href="view/css/adserver.css" rel="stylesheet"> -->
    <link id="bs-css" href="view/css/bootstrap-cerulean.min.css" rel="stylesheet">
    <link href="view/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="view/css/charisma-app.min.css" rel="stylesheet">
    <link href='view/css/chosen.min.css' rel='stylesheet'>
    <link href='view/css/colorbox.min.css' rel='stylesheet'>
    <link href='view/css/elfinder.min.css' rel='stylesheet'>
    <link href='view/css/elfinder.theme.min.css' rel='stylesheet'>
    <link href='view/css/fullcalendar.min.css' rel='stylesheet'>
    <link href='view/css/fullcalendar.print.min.css' rel='stylesheet'  media='print'>
    <link href="view/css/jquery-ui-1.8.21.custom.min.css" rel="stylesheet">
    <link href='view/css/jquery.cleditor.min.css' rel='stylesheet'>
    <link href='view/css/jquery.iphone.toggle.min.css' rel='stylesheet'>
    <link href='view/css/jquery.noty.min.css' rel='stylesheet'>
    <link href='view/css/jquery.ui.timepicker.min.css' rel='stylesheet'>
    <link href='view/css/noty_theme_default.min.css' rel='stylesheet'>
    <link href='view/css/opa-icons.min.css' rel='stylesheet'>
    <link href='view/css/uniform.default.min.css' rel='stylesheet'>
    <link href='view/css/uploadify.min.css' rel='stylesheet'>
    <link href='view/css/experiment.css' rel='stylesheet'>
</head>
<body>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <div class="btn-group pull-right" >
                <a class="btn dropdown-toggle" href="deco.php">
                    <i class="icon-off">
                    </i>
                    <span class="hidden-phone">Déconnexion</span>
                </a>
            </div>
            <a class="brand" href="./" style="width:auto;float:none;">
                <span style="font-family:Verdana;float:none;">AD-SERVEUR PUB - {if isset($aData.nom_client)}{$aData.nom_client}{/if}</span>
            </a>
        </div>
    </div>
</div>



<!-- Page Content -->
<div id="page-content-wrapper" {if empty($tTplMenu)}style="margin-top: 40px;"{/if}>
    <div class="container-fluid">
        <div class="row">

            <div style="text-align: center;">
                <h1 style="font-size: 150px;color: rgb(162, 39, 38);">&#10008;</h1>
                <h1>ERREUR 404</h1>
                <h3>La page que vous recherchez n'existe pas !</h3>
            </div>

        </div>
    </div>
</div>
<!-- /#wrapper -->


<!-- <script src="view/js/adserver.js"></script> -->
<script src="view/js/jquery-1.7.2.min.js"></script>
<script src="view/js/jquery-ui-1.8.21.custom.min.js"></script>
<script src="view/js/bootstrap-transition.js"></script>
<script src="view/js/bootstrap-alert.js"></script>
<script src="view/js/bootstrap-modal.js"></script>
<script src="view/js/bootstrap-dropdown.js"></script>
<script src="view/js/bootstrap-scrollspy.js"></script>
<script src="view/js/bootstrap-tab.js"></script>
<script src="view/js/bootstrap-tooltip.js"></script>
<script src="view/js/bootstrap-popover.js"></script>
<script src="view/js/bootstrap-button.js"></script>
<script src="view/js/bootstrap-collapse.js"></script>
<script src="view/js/bootstrap-carousel.js"></script>
<script src="view/js/bootstrap-typeahead.js"></script>
<script src="view/js/bootstrap-tour.js"></script>
<script src="view/js/jquery.cookie.js"></script>
<script src='view/js/fullcalendar.min.js'></script>
<script src='view/js/jquery.dataTables.min.js'></script>
<script src="view/js/excanvas.js"></script>
<script src="view/js/jquery.flot.min.js"></script>
<script src="view/js/jquery.flot.pie.min.js"></script>
<script src="view/js/jquery.flot.stack.js"></script>
<script src="view/js/jquery.flot.resize.min.js"></script>
<script src="view/js/jquery.chosen.min.js"></script>
<script src="view/js/jquery.uniform.min.js"></script>
<script src="view/js/jquery.colorbox.min.js"></script>
<script src="view/js/jquery.cleditor.min.js"></script>
<script src="view/js/jquery.noty.js"></script>
<script src="view/js/jquery.elfinder.min.js"></script>
<script src="view/js/jquery.raty.min.js"></script>
<script src="view/js/jquery.iphone.toggle.js"></script>
<script src="view/js/jquery.autogrow-textarea.js"></script>
<script src="view/js/jquery.uploadify-3.1.min.js"></script>
<script src="view/js/jquery.history.js"></script>
<script src="view/js/jquery.ui.timepicker.js"></script>
<script src="view/js/script.js"></script>

</body>

</body>
</html>