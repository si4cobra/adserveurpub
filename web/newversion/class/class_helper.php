<?php

/**
 * Created by PhpStorm.
 * Date: 11/12/2015
 * Time: 16:36
 */
class class_helper
{
    /**
     * @return string Guid aléatoire
     */
    public static function guid()
    {
        if( function_exists('com_create_guid') === true ) {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    public static function  random($car) {
        $string = "";
        $chaine = "abcdefghijklmnpqrstuvwxy";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
        $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
        }

    /**
     * renvoi date
     * @param $Ladate
     * @param $langue
     * @param string $format
     * @return string
     */
    public static function renvoi_date($Ladate,$langue,$format="")
    {
        $Retour = "";

        if( $langue == "fr" ) {

            $recutableau = explode("-", $Ladate);
            $Retour = $recutableau[2] . "/" . $recutableau[1] . "/" . $recutableau[0];
        }

        if( $langue == "eng" ) {
            $tableaudate = explode("/", $Ladate);
            $Retour = $tableaudate[2] . "-" . $tableaudate[1] . "-" . $tableaudate[0];
        }

        if( $langue == "format" ) {
            $recutableau = explode("-", $Ladate);
            setlocale("LC_TIME", "fr");
            $Retour = strftime($format, mktime(0, 0, 0, $recutableau[1], $recutableau[2], $recutableau[0]));
        }

        return $Retour;
    }

    public static function renvoi_jour_literal($ladate){

        $aTableaudecoupe=explode("-",$ladate);
        $imktime=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2],$aTableaudecoupe[0]);
        $ijour=date('w',$imktime);
        $imois=date('n',$imktime);

        $jour = array( "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi" );

        $mois = array( "", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" );

        $datefr = $jour[$ijour] . " " . $aTableaudecoupe[2] . " " . $mois[$imois] . " " . $aTableaudecoupe[0];


        return $datefr;
    }

    public static function renvoi_jour_semaine($ladate){

        $aTableauRetour=array();
        $aTableaudecoupe=explode("-",$ladate);
        $imktime=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2],$aTableaudecoupe[0]);
        $ijour=date('w',$imktime);

        if($ijour==1){
            $aTableauRetour[]=  date('Y-m-d',$imktime);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+6,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==2){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-1,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+5,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==3){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-2,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+4,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==4){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-3,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+3,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==5){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-4,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+2,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==6){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-5,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+1,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==0){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-6,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2],$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else {
            $aTableauRetour=array();
        }


        return $aTableauRetour;
    }

    /**
     * methode  renvoi le dernier jour du mois
     * @author Danon Gnakouri
     * @since 3.2
     * @return la connexion
     */
public static function Getdernierjourmois ($mois,$annee) {

        $variable =$mois."-".$annee;
        list ($month, $year) = explode ('-', $variable);
        $year = ((int)$month === 12)?$year+1:$year;
        $month = ((int)$month + 1 === 13)?1:$month+1;
        $lastDay = mktime (0, 0, 0, $month, 0,  $year);
        return strftime ('%d', $lastDay);
    }

    public static  function get_lundi_vendredi_from_week($week,$year,$format="d/m/Y") {

        $firstDayInYear=date("N",mktime(0,0,0,1,1,$year));
        if ($firstDayInYear<5)
            $shift=-($firstDayInYear-1)*86400;
        else
            $shift=(8-$firstDayInYear)*86400;
        if ($week>1) $weekInSeconds=($week-1)*604800; else $weekInSeconds=0;
        $timestamp=mktime(0,0,0,1,1,$year)+$weekInSeconds+$shift;
        $timestamp_vendredi=mktime(0,0,0,1,7,$year)+$weekInSeconds+$shift;

        return array(date($format,$timestamp), date($format,$timestamp_vendredi));

    }

}