<?php
class Client {
	function renvoi_client($id_client) {
		return Query::fetchOne("
			SELECT
				c.id_client AS id,
				c.nom_client AS nom
			FROM client c
			WHERE c.supplogique_client = 'N'
			AND c.id_client = '".$id_client."'
		");
	}
	
	public function authentification_login_password($login, $password) {
		
		$sQl="
			SELECT c.id_client
			FROM client c
			WHERE c.supplogique_client = 'N'
			AND c.login_client = '".$login."'
			AND c.pass_client = '".$password."'
		";
		//echo $sQl;

		$bresult= Query::fetchOne($sQl);
		//exit();
		return $bresult;
	}
	
	public function authentification_autoconnect($autoconnect) {
		return Query::fetchOne("
			SELECT c.id_client
			FROM client c
			WHERE c.supplogique_client = 'N'
			AND c.identifiantautoconnect_connexion_client = '".$autoconnect."'
		");
	}
	
	public function authentification_cookie_connect($cookie_connect) {
		return Query::fetchOne("
			SELECT c.id_client
			FROM client c
			WHERE c.supplogique_client = 'N'
			AND c.accesdirect_client = '".$cookie_connect."'
		");
	}
	
	public function est_actif($id_client) {
		return Query::fetchOne("
			SELECT c.id_client
			FROM client c
			WHERE c.supplogique_client = 'N'
			AND c.id_client = '".$id_client."'
			AND c.actif_client = 'Y'
		");
	}
	
	public function set_cookie_connect($id_client, $cookie_connect) {
		return Query::fetchupdate("
			UPDATE client c
			SET c.accesdirect_client = '".$cookie_connect."'
			WHERE c.supplogique_client = 'N'
			AND c.id_client = '".$id_client."'
		");
	}
	
	public function get_cookie_connect($id_client) {
		return Query::fetchOne("
			SELECT c.accesdirect_client AS cookie_connect
			FROM client c
			WHERE c.supplogique_client = 'N'
			AND c.id_client = '".$id_client."'
		");
	}
}