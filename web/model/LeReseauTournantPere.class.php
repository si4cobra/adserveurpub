<?php
class LeReseauTournantPere {
	public function renvoi_liste_reseau_tournant($id_client) {


		$sRequete="SELECT 
				lrtp.id_reseau_tournant_pere AS id,
				lrtp.nom_reseau_tournant_pere AS nom,
				rlc.id_contratproduit,
				cp2.id_contrat
			FROM le_reseau_tournant_pere lrtp
			JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant_pere = lrtp.id_reseau_tournant_pere)
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_reseau_tournant = lrt.id_reseau_tournant)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = lre.id_emplacement)
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_emplacement = e.id_emplacement)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			WHERE lrtp.supplogique_le_reseau_tournant_pere = 'N'
			
			ORDER BY lrtp.id_reseau_tournant_pere";

			//echo $sRequete;

		return Query::fetch($sRequete);
	}
	
	public function renvoi_reseau_tournant($id_client, $id_reseau_tournant) {
		return Query::fetchOne("SELECT DISTINCT
				lrtp.id_reseau_tournant_pere AS id,
				lrtp.nom_reseau_tournant_pere AS nom,
				count(rlc.id_lienclientrim) AS nombre_emplacement
			FROM le_reseau_tournant_pere lrtp
			JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant_pere = lrtp.id_reseau_tournant_pere)
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_reseau_tournant = lrt.id_reseau_tournant)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = lre.id_emplacement)
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_emplacement = e.id_emplacement)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			WHERE lrtp.supplogique_le_reseau_tournant_pere = 'N'
			AND lrtp.id_reseau_tournant_pere = '".$id_reseau_tournant."'
			GROUP BY lrtp.id_reseau_tournant_pere");
	}
	
	public function renvoi_mobiliers_position_dans_reseau_tournant($id_reseau_tournant) {
		return Query::fetch("SELECT DISTINCT
				lre.position_lien_emplacement AS position_dans_reseau_tournant,
				v.ville AS ville,
				v.code_postal AS code_postal,
				rm.numero_mobilier AS numero_mobilier,
				e.numero_emplacement AS numero_emplacement,
				e.nom_emplacment AS nom_emplacement
			FROM le_reseau_tournant lrt
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_reseau_tournant = lrt.id_reseau_tournant)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = lre.id_emplacement)
			JOIN rim_gestionrim rg ON (rg.supplogique_rim = 'N' AND rg.id_rim = e.id_rim)
			JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_rim = rg.id_rim)
			JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_lerim = rlr.id_lerim)
			JOIN ric_mobilier rm ON (rm.supplogique_mobilier = 'N' AND rm.id_mobilier = rl.id_mobilier)
			JOIN ville_cedex v ON (v.supplogique_ville = 'N' AND v.id_code_postal = rm.id_ville)
			WHERE lrt.supplogique_reseau_tournant = 'N'
			AND lrt.id_reseau_tournant = '".$id_reseau_tournant."'
			ORDER BY lre.position_lien_emplacement ASC");
}

/**
 * methode renvoi nombre de jour entre deux date
 * @author Danon Gnakouri 
 * @param login login de connexion
 * @param motdepass mot de pass
 * @param $bdd mot de passe
 * @since 3.2
 * @return la connexion
 */
 public function posiion_client_rt($key,$datefiltre){
		
		$sRequete_eplacement_rt="SELECT client.id_client, 
		id_lienclientrim, 
		nom_client, 
		raisonsocial_client, 
		rim_emplacement.id_emplacement, 
		nom_emplacment,
		position_lien_emplacement,
		date_format(date_contratproduit,'%d/%m/%Y') as datecontratproduit ,
		nom_reseau_tournant,
		le_reseau_tournant.id_reseau_tournant,
		contratproduit.id_contratproduit,
		rim_gestionrim.nom_rim,
		nom_affiche_client_rim,
		nom_lerim
		from rim_lienclientrim
		inner join client on (client.id_client = rim_lienclientrim.id_client)
		inner join contratproduit on contratproduit.id_contratproduit = rim_lienclientrim.id_contratproduit and supplogique_contratpeoduit = 'N'
		inner join rim_emplacement on (rim_emplacement.id_emplacement = rim_lienclientrim.id_emplacement and rim_emplacement.suplogique_emplacement ='N')
		inner join lien_resau_emplacement on lien_resau_emplacement.id_emplacement = rim_emplacement.id_emplacement and supplogique_lien_emplacement='N'
		inner join rim_gestionrim on rim_gestionrim.id_rim = rim_emplacement.id_rim and supplogique_rim = 'N'
		inner join rim_lienrimface on rim_lienrimface.id_rim = rim_gestionrim.id_rim and supplogique_lienrimface='N'
		inner join rim_lerim on rim_lerim.id_lerim = rim_lienrimface.id_lerim and supplogique_lienrimface = 'N'
		inner join le_reseau_tournant on le_reseau_tournant.id_reseau_tournant = lien_resau_emplacement.id_reseau_tournant and supplogique_reseau_tournant = 'N'
		where  contratproduit.id_contratproduit='".$key."'";

		//echo"<pre>";print_r($sRequete_eplacement_rt);echo"</pre>";
		
		$aTableauListeEmplacment =  Query::fetch($sRequete_eplacement_rt);
		$sNomemplacment ="";
		
		//echo"<pre>";print_r($aTableauListeEmplacment);echo"</pre>";
		
		if(!empty($aTableauListeEmplacment)){
			foreach ($aTableauListeEmplacment as $valeuremplacement) {
				//$sNomemplacment.=$valeuremplacement['nom_emplacment']." (".$valeuremplacement['nom_reseau_tournant'].")<br>";
				$aTableauRetour = $this->positionement_rt($valeuremplacement['id_reseau_tournant'],$datefiltre);

				if(!empty($aTableauRetour)){
					foreach ($aTableauRetour as $atmp) {
						if($valeuremplacement['id_contratproduit']==$atmp['id_contratproduit']){

							if(trim($valeuremplacement['nom_affiche_client_rim'])=="")
									$valeuremplacement['nom_affiche_client_rim'] = $valeuremplacement['nom_lerim'];

							$sNomemplacment.="Actuellement diffusé sur <b>".$atmp['nom_emplacment_actuelle']."</b><br>";
					    }
					}
				}

				
			}
		}

	
	return $sNomemplacment;
}

/**
 * methode renvoi nombre de jour entre deux date
 * @author Danon Gnakouri 
 * @param login login de connexion
 * @param motdepass mot de pass
 * @param $bdd mot de passe
 * @since 3.2
 * @return la connexion
 */
 public function nombrejourdate($datedebut,$datefin){

	$aTableaudebut = explode("/",$datedebut);
	$aTableaufin =explode("/",$datefin);
	
	$mktimedebut = mktime(0,0,0,$aTableaudebut[1],$aTableaudebut[0],$aTableaudebut[2]);
	$mktimefin = mktime(0,0,0,$aTableaufin[1],$aTableaufin[0],$aTableaufin[2]);
	
	$nbrjour = ($mktimefin-$mktimedebut)/(24*60*60);
	
	return $nbrjour;
 }

 /**
 * methode renvoi position dans le tableau
 * @author Danon Gnakouri 
 * @param login login de connexion
 * @param motdepass mot de pass
 *@param $bdd mot de passe
 * @since 3.2
 * @return la connexion
 */
public function position_tableau($iteration,$itailletableau,$ipostionorigne){

	return ($iteration%$itailletableau + $ipostionorigne)%$itailletableau;

}

	/*
	*function positionnement reseau tournant
	*
	*
	*/
	public function positionement_rt($idreseau,$datefiltre){
		

		//récupération du positionnement défaut du reseau tournant
		$sRequete_eplacement_rt="SELECT client.id_client, 
					id_lienclientrim, 
					nom_client, 
					raisonsocial_client, 
					rim_emplacement.id_emplacement, 
					nom_emplacment,
					position_lien_emplacement,
					date_format(date_contratproduit,'%d/%m/%Y') as datecontratproduit ,
					nom_reseau_tournant,
					periodicite_reseau_tournant,
					date_format(datedebut_reseau_tournant,'%d/%m/%Y') as datedebutreseau,
					contratproduit.id_contratproduit,
					nom_affiche_client_rim,
					nom_lerim
					from le_reseau_tournant
					inner join lien_resau_emplacement on lien_resau_emplacement.id_reseau_tournant = le_reseau_tournant.id_reseau_tournant 
					inner join rim_emplacement on rim_emplacement.id_emplacement = lien_resau_emplacement.id_emplacement and supplogique_lien_emplacement='N'
					 and rim_emplacement.suplogique_emplacement ='N'
					inner join rim_gestionrim on rim_gestionrim.id_rim = rim_emplacement.id_rim
					inner join rim_lienrimface on rim_lienrimface.id_rim = rim_gestionrim.id_rim and supplogique_lienrimface='N'
					inner join rim_lerim on rim_lerim.id_lerim = rim_lienrimface.id_lerim
					left join  rim_lienclientrim on rim_lienclientrim.id_emplacement = rim_emplacement.id_emplacement and supplogique_lienclientrim='N'
					left join client on (client.id_client = rim_lienclientrim.id_client)
					left join contratproduit on contratproduit.id_contratproduit = rim_lienclientrim.id_contratproduit
					where le_reseau_tournant.id_reseau_tournant='".$idreseau."' order by position_lien_emplacement asc";

		$aTableau_liste =  Query::fetch($sRequete_eplacement_rt);

		if(!empty($aTableau_liste)){
			$nbrjour = $this->nombrejourdate($aTableau_liste[0]['datedebutreseau'],$datefiltre);

		 	//echo $nbrjour."<br>";
		 	if($aTableau_liste[0]['periodicite_reseau_tournant']==0)
		 			$aTableau_liste[0]['periodicite_reseau_tournant']=1;

		 	if($nbrjour>0)	
		 		$nbiteration = ceil($nbrjour/$aTableau_liste[0]['periodicite_reseau_tournant']-1);
		 	else
		 		$nbiteration=0;

		 	//echo "toto : ".$nbrjour." ".$aTableau_liste[0]['periodicite_reseau_tournant']." ".$nbiteration."<br>";

		 	$u=0;
		 	$itailletableau =count($aTableau_liste);
		 	if(!empty($aTableau_liste)){
		 		foreach ($aTableau_liste as $valeur) {
		 			
		 			
		 			$iposition = $this->position_tableau($nbiteration,$itailletableau,$valeur['position_lien_emplacement']-1);

		 			if(trim($aTableau_liste[$iposition]['nom_affiche_client_rim'])=="")
									$aTableau_liste[$iposition]['nom_affiche_client_rim'] = $aTableau_liste[$iposition]['nom_lerim'];


		 			$aTableau_liste[$u]['position_actuelle'] = $aTableau_liste[$iposition]['position_lien_emplacement'];
		 			$aTableau_liste[$u]['nom_emplacment_actuelle'] = $aTableau_liste[$iposition]['nom_affiche_client_rim'];
		 			$u++;
		 		}
		 	}
	 }


	 	return $aTableau_liste;

	}
}