<?php
/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 20/04/2018
 * Time: 09:28
 */
header('Content-type: image/jpeg');
$image=$_GET['image'];

$aTableauInfo = getimagesize($image);

//echo"<pre>";print_r($aTableauInfo);echo"</pre>";

// On charge d'abord les images
$destination = imagecreatefromjpeg($image); // Le logo est la source
$source = imagecreatefromjpeg("GABARIT-RIM.jpg"); // La photo est la destination

// Les fonctions imagesx et imagesy renvoient la largeur et la hauteur d'une image
$largeur_source = imagesx($source);
$hauteur_source = imagesy($source);
$largeur_destination = imagesx($destination);
$hauteur_destination = imagesy($destination);

// On veut placer le logo en bas à droite, on calcule les coordonnées où on doit placer le logo sur la photo
$destination_x = $largeur_destination;
$destination_y =  $hauteur_destination /2;

// On met le logo (source) dans l'image de destination (la photo)
imagecopymerge($destination, $source, 0, 1280, 0, 0, $largeur_source, $hauteur_source, 60);

// On affiche l'image de destination qui a été fusionnée avec le logo
imagejpeg($destination);