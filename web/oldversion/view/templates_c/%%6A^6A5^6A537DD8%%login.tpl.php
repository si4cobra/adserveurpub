<?php /* Smarty version 2.6.9, created on 2018-03-21 18:36:54
         compiled from login.tpl */ ?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span12 center login-header" style="height:auto;margin:30px auto;padding-top:0;">
				<h2 style="font-family:Verdana;"><?php echo $this->_tpl_vars['titre_site']; ?>
</h2>
			</div>
		</div>
		<div class="row-fluid">
			<div class="well span5 center login-box" style="padding-bottom:0;">
				<div class="alert alert-info">
					Merci de vous identifier avec votre nom d'utilisateur et mot de passe.
				</div>
				<form class="form-horizontal" action="./" method="post" style="margin:0 0 20px;">
					<fieldset>
						<div class="input-prepend" title="Nom d'utilisateur" data-rel="tooltip">
							<span class="add-on"><i class="icon-user"></i></span><input autofocus class="input-large span10" name="log" id="username" type="text" value="" />
						</div>
						<div class="clearfix"></div>
						<div class="input-prepend" title="Mot de passe" data-rel="tooltip">
							<span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="password" id="password" type="password" value="" />
						</div>
						<div class="clearfix"></div>
						<p class="center span5">
							<button type="submit" class="btn btn-primary">Se connecter</button>
						</p>
					</fieldset>
				</form>
				<?php if ($this->_tpl_vars['message']): ?>
				<div class="alert alert-block" style="margin-bottom:20px;">
					<?php echo $this->_tpl_vars['message']; ?>

				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>