<?php /* Smarty version 2.6.9, created on 2019-11-18 17:53:23
         compiled from liste2.tpl */ ?>
<div class="span12 well" style="padding:10px;margin:0 0 10px 0;width:auto;display:block;float:none;">
	<h3>FACE <?php echo $this->_tpl_vars['numero_de_face']; ?>
</h3>
	<table>
		<tr>
			<td style="vertical-align:top;padding-bottom:20px;">
				<i class="icon-stop"></i>
			</td>
			<td style="vertical-align:top;padding-bottom:20px;">
				<p style="margin:0;padding:0;font-size:16px;">Affiche tampon*</p>
			</td>
			<aside id="id_affiche_tampon_<?php echo $this->_tpl_vars['affiche_tampon']['id']; ?>
" class="modal_seb" style="text-align:center;">
				<div>
					<?php if ($this->_tpl_vars['affiche_tampon']['extension'] != 'swf'): ?>
					<img alt="" src="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" style="height:100%;">
					<?php else: ?>
					<object type="application/x-shockwave-flash" data="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" height="100%">
						<param name="movie" value="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" />
						<param name="wmode" value="transparent" />
						<p>swf non affichable</p>
					</object>
					<?php endif; ?>
					<a href="#close" title="Close"></a>
				</div>
			</aside>
			<td style="width:40px;min-width:40px;padding-bottom:20px;">
				<?php if ($this->_tpl_vars['affiche_tampon']['extension'] != 'swf'): ?>
				<img src="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" style="width:40px;"/>
				<?php else: ?>
				<object type="application/x-shockwave-flash" data="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" width="40" height="143">
					<param name="movie" value="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" />
					<param name="wmode" value="transparent" />
					<p>swf non affichable</p>
				</object>
				<?php endif; ?>
			</td>
			<td style="padding-bottom:20px;">
				<a href="#id_affiche_tampon_<?php echo $this->_tpl_vars['affiche_tampon']['id']; ?>
">
					<span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
				</a>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top;padding-bottom:20px;">
				<i class="icon-stop"></i>
			</td>
			<td style="vertical-align:top;padding-bottom:20px;">
				<p style="margin:0 0 10px;padding:0;font-size:16px;">Visuel actuellement diffusé</p>
			</td>
			<td style="width:40px;min-width:40px;padding-bottom:20px;">
				<?php if (isset ( $this->_tpl_vars['planning_actuel'] )): ?>
				<aside id="id_affiche_actuel_<?php echo $this->_tpl_vars['planning_actuel']['id']; ?>
" class="modal_seb" style="text-align:center;">
					<div>
						<?php if ($this->_tpl_vars['planning_actuel']['extension'] != 'swf'): ?>
						<img src="<?php echo $this->_tpl_vars['planning_actuel']['image']; ?>
" style="height:100%;"/>
						<?php else: ?>
						<object type="application/x-shockwave-flash" data="<?php echo $this->_tpl_vars['planning_actuel']['image']; ?>
" height="100%">
							<param name="movie" value="<?php echo $this->_tpl_vars['planning_actuel']['image']; ?>
" />
							<param name="wmode" value="transparent" />
							<p>swf non affichable</p>
						</object>
						<?php endif; ?>
						<a href="#close" title="Close"></a>
					</div>
				</aside>
				<?php if ($this->_tpl_vars['planning_actuel']['extension'] != 'swf'): ?>
				<img src="<?php echo $this->_tpl_vars['planning_actuel']['image']; ?>
" style="width:40px;"/>
				<?php else: ?>
				<object type="application/x-shockwave-flash" data="<?php echo $this->_tpl_vars['planning_actuel']['image']; ?>
" width="40" height="143">
					<param name="movie" value="<?php echo $this->_tpl_vars['planning_actuel']['image']; ?>
" />
					<param name="wmode" value="transparent" />
					<p>swf non affichable</p>
				</object>
				<?php endif; ?>
				<?php else: ?>
				<aside id="id_affiche_tampon_actuel_<?php echo $this->_tpl_vars['affiche_tampon']['id']; ?>
" class="modal_seb" style="text-align:center;">
					<div>
						<?php if ($this->_tpl_vars['affiche_tampon']['extension'] != 'swf'): ?>
						<img alt="" src="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" style="height:100%;">
						<?php else: ?>
						<object type="application/x-shockwave-flash" data="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" height="100%">
							<param name="movie" value="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" />
							<param name="wmode" value="transparent" />
							<p>swf non affichable</p>
						</object>
						<?php endif; ?>
						<a href="#close" title="Close"></a>
					</div>
				</aside>
				<?php if ($this->_tpl_vars['affiche_tampon']['extension'] != 'swf'): ?>
				<img src="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" style="width:40px;"/>
				<?php else: ?>
				<object type="application/x-shockwave-flash" data="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" width="40" height="143">
					<param name="movie" value="<?php echo $this->_tpl_vars['affiche_tampon']['image']; ?>
" />
					<param name="wmode" value="transparent" />
					<p>swf non affichable</p>
				</object>
				<?php endif; ?>
				<?php endif; ?>
			</td>
			<td style="padding-bottom:20px;">
				<?php if (isset ( $this->_tpl_vars['planning_actuel'] )): ?>
				<a href="#id_affiche_actuel_<?php echo $this->_tpl_vars['planning_actuel']['id']; ?>
">
					<span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
				</a>
				<?php else: ?>
				<a href="#id_affiche_tampon_actuel_<?php echo $this->_tpl_vars['affiche_tampon']['id']; ?>
">
					<span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
				</a>
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" style="vertical-align:top;">
				<div>
					<a href="index.php?page=emplacement&emplacement=<?php echo $this->_tpl_vars['id']; ?>
&id_contratproduit=<?php echo $this->_tpl_vars['id_contratproduit']; ?>
">
						<button class="btn btn-large btn-info" style="padding:2px;">
							<span title=".icon32  .icon-white  .icon-copy " class="icon32 icon-white icon-copy" style="vertical-align:middle;"></span>
							Voir toutes vos programmations de visuels
						</button>
					</a>
				</div>
				<p style="margin:0 0 10px;padding:0;font-size:12px;">(campagne promotionnelle ponctuelle ou déclinaisons de visuels)</p>
				<div>
					<a href="index.php?page=planning&action=creer&emplacement=<?php echo $this->_tpl_vars['id']; ?>
&<?php echo $this->_tpl_vars['selection']; ?>
&id_contratproduit=<?php echo $this->_tpl_vars['id_contratproduit']; ?>
">
						<button class="btn btn-large btn-success" style="padding:2px;">
							<span title=".icon32  .icon-white  .icon-square-plus " class="icon32 icon-white icon-square-plus" style="vertical-align:middle;"></span>
							Programmer un autre visuel
						</button>
					</a>
				</div>
			</td>
		</tr>
	</table>
</div>