<div class="container-fluid">
	<div class="row-fluid">
		{include file='menu1.tpl'}
		<div id="content" class="span10">
			{if $tableau_message|@count gt 0 or $plannings_chevauches|@count gt 0}
			<div class="alert alert-error" style="display:inline-block;margin-bottom:8px;">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Erreur!</strong>
				{if $tableau_message|@count gt 0}
				{foreach from=$tableau_message item=message}
				<br>{$message}
				{/foreach}
				{/if}
				{if $plannings_chevauches|@count gt 0}
				{foreach from=$plannings_chevauches item=planning_chevauche}
				<br>- Conflit avec le planning ({$planning_chevauche.date_debut})-({$planning_chevauche.date_fin}) {$planning_chevauche.periodicite} ({$planning_chevauche.heure_debut}-{$planning_chevauche.heure_fin})
				{/foreach}
				{/if}
			</div>
			{/if}
			{if $message_ajout_planning_reussi neq ''}
			<div class="alert alert-success" style="display:inline-block;margin-bottom:8px;">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Bravo!</strong> {$message_ajout_planning_reussi}
			</div>
			{/if}
			<div class="row-fluid sortable ui-sortable">
				<div class="box span12" style="width:auto;margin-top:0px;margin-bottom:8px;">
					<div class="box-header well" data-original-title>
						<i class="icon-edit"></i>
						Formulaire {if $id_planning neq ''} d'édition {else} d'ajout {/if} d'un planning
						de l'emplacement "{$emplacement.nom}"
						de la face venant de "{$face.nom}"
						du mobilier "{$mobilier.numero}",
						{$mobilier.adresse}
						{$mobilier.codepostal}
						{$mobilier.ville}
						<div class="btn-group pull-right">
							<a class="btn dropdown-toggle" href="main.php{$ajout_lien_retour}">
								<i class="icon-arrow-left"></i>
								<span class="hidden-phone">Retour aux plannings</span>
							</a>
						</div>
					</div>
					<div class="box-content">
						<form action="main.php?dir=editer{$ajout_lien_formulaire}" method="POST" class="form-horizontal" style="margin-bottom:0;" enctype="multipart/form-data">
							<fieldset>
								<div style="display:inline-block;vertical-align:top;">
									<legend style="margin-bottom:10px;">Image</legend>
									<div style="margin:10px 20px 10px 0">
										{if $id_planning eq ''}
										<label class="control-label" for="fichier_ecran" style="width:auto;margin-left:10px;">Fichier:</label>
										<div class="controls" style="margin-left:10px;">
											<input class="input-file uniform_on" id="fichier_ecran" name="fichier_ecran" type="file" size="25" style="opacity:0;">
											<p>format fichier autorisé ({', '|implode:$tableau_planning_type_mime_autorise}), dimensions autorisées ({$longueur_autorisee_image_pub}*{$hauteur_autorisee_image_pub}), poids fichier autorisé (800 Ko)</p>
										</div>
										{else}
										{if $planning.extension_image neq 'swf'}
											<img src="{$planning.image}" style="width:55px;"/>
										{else}
											<object type="application/x-shockwave-flash" data="{$planning.image}" width="55" height="196">
												<param name="movie" value="{$planning.image}" />
												<param name="wmode" value="transparent" />
												<p>swf non affichable</p>
											</object>
										{/if}
										{/if}
									</div>
								</div>
								<div
									{if $id_planning neq ''}
									style="display:inline-block;"
									{/if}
								>
								<legend style="margin-bottom:10px;">Période d'affichage</legend>
								<table>
									<tbody>
										<tr>
											<td>
												<label class="control-label" for="date_debut" style="width:auto;margin-left:10px;">Date de début:</label>
												<div class="controls" style="margin-left:100px;">
													<input type="text" class="input-xlarge datepicker_date" id="date_debut" name="date_debut" style="margin-left:5px;width:80px;"
													{if $planning_valeur_utilisateur}
														value="{$planning_valeur_utilisateur.date_debut}"
													{elseif $id_planning neq ''}
														value="{$planning.date_debut}"
													{/if}
													>
												</div>
											</td>
											<td>
												<label class="control-label" for="date_fin" style="width:auto;margin-left:10px;">Date de fin:</label>
												<div class="controls" style="margin-left:80px;">
													<input type="text" class="input-xlarge datepicker_date" id="date_fin" name="date_fin" style="margin-left:5px;width:80px;"
													{if $planning_valeur_utilisateur}
														value="{$planning_valeur_utilisateur.date_fin}"
													{elseif $id_planning neq ''}
														value="{$planning.date_fin}"
													{/if}
													>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<legend style="margin-bottom:10px;">Périodicité d'affichage</legend>
								<div style="margin:10px 0">
									<label style="width:auto;margin-left:10px;display:inline;">Jours:</label>
									<div style="margin-left:60px;">
										<label style="display:inline;">
											<input type="radio" name="periodicite"
											{if $planning_valeur_utilisateur}
												{if $planning_valeur_utilisateur.periodicite|count_characters eq 7}
												checked
												{/if}
											{elseif $id_planning neq ''}
												{if $planning.periodicite|count_characters eq 7}
												checked
												{/if}
											{/if}
											>
											Tous les jours
										</label>
										{foreach from=$tableau_correspondance_indice_jour key=indice item=intitule}
										<label style="display:inline;">
											<input type="checkbox" name="periodicite_jour[]" value="{$indice}"
											{if $planning_valeur_utilisateur}
												{if $planning_valeur_utilisateur.periodicite|match:$indice}
												checked
												{/if}
											{elseif $id_planning neq ''}
												{if $planning.periodicite|match:$indice}
												checked
												{/if}
											{/if}
											>
											{$intitule}
										</label>
										{/foreach}
									</div>
								</div>
								<legend style="margin-bottom:10px;">Horaire d'affichage</legend>
								<table>
									<tbody>
										<tr>
											<td>
												Toute la journée: <input type="checkbox" id="toute_la_journee" value="1">
											</td>
											<td>
												<label class="control-label" for="heure_debut" style="width:auto;margin-left:10px;">Heure de début:</label>
												<div class="controls" style="margin-left:105px;">
													<input type="text" class="input-xlarge datepicker_hour" id="heure_debut" name="heure_debut" style="margin-left:5px;width:80px;"
													{if $planning_valeur_utilisateur}
														value="{$planning_valeur_utilisateur.heure_debut}"
													{elseif $id_planning neq ''}
														value="{$planning.heure_debut}"
													{/if}
													>
												</div>
											</td>
											<td>
												<label class="control-label" for="heure_fin" style="width:auto;margin-left:10px;">Heure de fin:</label>
												<div class="controls" style="margin-left:85px;">
													<input type="text" class="input-xlarge datepicker_hour" id="heure_fin" name="heure_fin" style="margin-left:5px;width:80px;"
													{if $planning_valeur_utilisateur}
														value="{$planning_valeur_utilisateur.heure_fin}"
													{elseif $id_planning neq ''}
														value="{$planning.heure_fin}"
													{/if}
													>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<legend style="margin-bottom:10px;">Activation du planning</legend>
								<div style="margin:10px 0">
									<label class="control-label" for="fichier_ecran" style="width:auto;margin-left:10px;">Activer:</label>
									<div class="controls" style="margin-left:70px;">
										<input type="checkbox" name="activer" value="1"
										{if $id_planning neq ''}
											{if $planning.desactivation eq 'N'}
												checked
											{/if}
										{else}
											checked
										{/if}
										>
									</div>
								</div>
								<div class="form-actions" style="display:inline-block;padding:10px">
									<button type="submit" class="btn btn-primary" name="valider_edition_planning" value="1">Valider l'édition du planning</button>
									<a href="main.php{$ajout_lien_retour}">
										<button class="btn">Annuler</button>
									</a>
								</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>