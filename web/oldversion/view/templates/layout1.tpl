<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="fr"> <!--<![endif]-->
{$head}
	<body style="padding:0 0 20px;">
		{$content}
		{$script}
	</body>
</html>