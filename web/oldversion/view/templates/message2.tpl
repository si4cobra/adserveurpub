{if $message_info}
<div class="alert {$message_info.type_info}" style="display:inline-block;margin-bottom:8px;">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{$message_info.titre}</strong><br>
	{$message_info.description}
</div>
{/if}