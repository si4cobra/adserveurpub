{if $liste_face|@count gt 0}
<table class="table table-bordered table-striped" style="width:auto;margin-bottom:8px;">
	<thead>
		<tr>
			<th>Ville</th>
			<th>Mobilier</th>
			<th>Adresse</th>
			<th>Visibilité</th>
			<th>Action</th>
		</tr>
	</thead>
	{foreach from=$liste_face item=face}
	<tr>
		<td>{$face.ville}</td>
		<td>
			{if $face.numero_mobilier neq ''}
			RIM No {$face.numero_mobilier}
			{/if}
		</td>
		<td>{$face.adresse_mobilier}
		<td>{$face.nom_face}</td>
		<td style="padding-bottom:0;">
			<a class="btn btn-info" href="index.php?page=selection&face={$face.id}&id_contratproduit={$face.id_cp}" style="display:block;margin-bottom:8px;">
				<i class="icon-list icon-white"></i>  
				Visualiser
			</a>
		</td>
	</tr>
	{/foreach}
</table>
{/if}