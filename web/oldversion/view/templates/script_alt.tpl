{literal}
<script src="view/js/jquery-1.7.2.min.js"></script>
<script src="view/js/jquery-ui-1.8.21.custom.min.js"></script>
<script src="view/js/bootstrap-transition.js"></script>
<script src="view/js/bootstrap-alert.js"></script>
<script src="view/js/bootstrap-modal.js"></script>
<script src="view/js/bootstrap-dropdown.js"></script>
<script src="view/js/bootstrap-scrollspy.js"></script>
<script src="view/js/bootstrap-tab.js"></script>
<script src="view/js/bootstrap-tooltip.js"></script>
<script src="view/js/bootstrap-popover.js"></script>
<script src="view/js/bootstrap-button.js"></script>
<script src="view/js/bootstrap-collapse.js"></script>
<script src="view/js/bootstrap-carousel.js"></script>
<script src="view/js/bootstrap-typeahead.js"></script>
<script src="view/js/bootstrap-tour.js"></script>
<script src="view/js/jquery.cookie.js"></script>
<script src='view/js/fullcalendar.min.js'></script>
<script src='view/js/jquery.dataTables.min.js'></script>
<script src="view/js/excanvas.js"></script>
<script src="view/js/jquery.flot.min.js"></script>
<script src="view/js/jquery.flot.pie.min.js"></script>
<script src="view/js/jquery.flot.stack.js"></script>
<script src="view/js/jquery.flot.resize.min.js"></script>
<script src="view/js/jquery.chosen.min.js"></script>
<script src="view/js/jquery.uniform.min.js"></script>
<script src="view/js/jquery.colorbox.min.js"></script>
<script src="view/js/jquery.cleditor.min.js"></script>
<script src="view/js/jquery.noty.js"></script>
<script src="view/js/jquery.elfinder.min.js"></script>
<script src="view/js/jquery.raty.min.js"></script>
<script src="view/js/jquery.iphone.toggle.js"></script>
<script src="view/js/jquery.autogrow-textarea.js"></script>
<script src="view/js/jquery.uploadify-3.1.min.js"></script>
<script src="view/js/jquery.history.js"></script>
<script src="view/js/jquery.ui.timepicker.js"></script>
<script src="view/js/script.js"></script>
{/literal}