{if $reseaux_tournants|@count gt 0 or $mobiliers|@count gt 0}
<div class="span2 main-menu-span">
	<div class="well nav-collapse sidebar-nav" style="padding:10px;">
		<ul class="nav nav-tabs nav-stacked main-menu" style="margin-bottom:0;">
			{if $reseaux_tournants|@count gt 0}
			<li class="nav-header hidden-tablet" style="margin-left:0px;padding-left:0px;">
				Réseaux tournants
			</li>
			{foreach from=$reseaux_tournants item=reseau_tournant}
			<li style="margin-left:0px;">
				<a class="ajax-link" href="?page=principal&action=selection&reseau_tournant={$reseau_tournant.id}" style="display:inline-block;">
					<i class="icon-refresh"></i>
					<span class="hidden-tablet">
						{$reseau_tournant.nom}<br>
						{if $reseau_tournant.code_postal neq NULL}
						{$reseau_tournant.code_postal}
						{/if}
						{if $reseau_tournant.ville neq NULL}
						{$reseau_tournant.ville}
						{/if}
					</span>
				</a>
				<ul class="nav nav-tabs nav-stacked main-menu">
					<li class="nav-header hidden-tablet" style="margin-left:15px;padding-left:0px;">
						Emplacements
					</li>
					{foreach from=$reseau_tournant.emplacements item=emplacement}
					<li style="margin-left:15px;">
						<a class="ajax-link" href="?page=principal&action=selection&emplacement_reseau_tournant={$emplacement.id}" style="display:inline-block;">
							<i class="icon-file"></i>
							<span class="hidden-tablet">{$emplacement.nom}</span>
						</a>
					</li>
					{/foreach}
				</ul>
			</li>
			{/foreach}
			{/if}
			{if $mobiliers|@count gt 0}
			<li class="nav-header hidden-tablet" style="margin-left:0px;padding-left:0px;">
				Mobiliers
			</li>
			{foreach from=$mobiliers item=mobilier}
			<li style="margin-left:0px;">
				<a class="ajax-link" href="?page=principal&action=selection&mobilier={$mobilier.id}" style="display:inline-block;">
					<i class="icon-calendar"></i>
					<span class="hidden-tablet">
						{$mobilier.numero}<br>
						{if $mobilier.adresse neq ''}
						{$mobilier.adresse}<br>
						{/if}
						{$mobilier.codepostal} {$mobilier.ville}
					</span>
				</a>
				<ul class="nav nav-tabs nav-stacked main-menu" style="margin-bottom:0;">
					<li class="nav-header hidden-tablet" style="margin-left:15px;padding-left:0px;">
						Faces
					</li>
					{foreach from=$mobilier.faces item=face}
					<li style="margin-left:15px;">
						<a class="ajax-link" href="?page=principal&action=selection&face={$face.id}" style="display:inline-block;">
							<i class="icon-list-alt"></i>
							<span class="hidden-tablet">{$face.nom}</span>
						</a>
						<ul class="nav nav-tabs nav-stacked main-menu">
							<li class="nav-header hidden-tablet" style="margin-left:15px;padding-left:0px;">
								Emplacements
							</li>
							{foreach from=$face.emplacements item=emplacement}
							<li style="margin-left:15px;">
								<a class="ajax-link" href="?page=principal&action=selection&emplacement={$emplacement.id}" style="display:inline-block;">
									<i class="icon-file"></i>
									<span class="hidden-tablet">{$emplacement.nom}</span>
								</a>
							</li>
							{/foreach}
						</ul>
					</li>
					{/foreach}
				</ul>
			</li>
			{/foreach}
			{/if}
		</ul>
	</div>
</div>
{/if}