<?php
/**
 * inclusion de l'initialisation du framework
 */
require 'framework/init.php';

/**
 * lancement du dispatcher de requêtes http
 */
Dispatcher::launch();