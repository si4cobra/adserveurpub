<?php
/**
 * @author Sébastien Robert
 * @version 1
 * @method errorhandler de gestion d'erreurs
 * @param errno numéro d'erreur
 * @param errstr message d'erreur
 * @param errfile fichier dans lequel l'erreur est apparue
 * @param errline ligne dans le fichier dans lequel l'erreur est apparue
 */
function errorhandler($errno, $errstr, $errfile, $errline) {
	throw new Exception(
		'Erreur:<br>'.
		'&nbsp;&nbsp;Numero: '.$errno.'<br>'.
		'&nbsp;&nbsp;Message: '.$errstr.'<br>'.
		'&nbsp;&nbsp;Fichier: '.$errfile.'<br>'.
		'&nbsp;&nbsp;Ligne: '.$errline.'<br>'
	);
}

/**
 * enregistrement de la fonction errorhandler en tant que fonction de gestion des erreurs PHP
 */
set_error_handler('errorhandler');