<?php
/**
 * @author Sébastien Robert
 * @version 1
 * @name Template classe de gestion des templates se basant sur Smarty
 */
class Template extends smartyDM_fleet {
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @var template_name fichier du template utilisé
	 */
	protected $template_name;
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @var variables elements utilisés dans le template
	 */
	protected $variables;
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method __construct() initialisation de la classe avec spécification du dossier où se trouve les templates
	 */
	public function __construct() {
		parent::__construct('view');
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method set spécification du fichier template ainsi que les variables qui y seront affichées
	 * @param template_name nom du fichier du template
	 * @param variables elements qui seront affichés dans le template
	 * @return le template
	 */
	public function set($template_name, $variables = array()) {
		$this->template_name = $template_name;
		$this->variables = $variables;
		return $this;
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method add ajout d'une variable dans le template
	 * @param tree endroit dans l'arborescence ou se trouve la nouvelle variable
	 * @param variable_name nom de la variable
	 * @param value valeur de la variable
	 * @return le template
	 */
	public function add($tree = array(), $variable_name, $value) {
		if (count($tree) > 0) {
			$node = array_shift($tree);
			
			if ($node != NULL) {
				if (isset($this->variables[$node])) {
					if ($this->variables[$node] instanceof Template) {
						$this->variables[$node]->add($tree, $variable_name, $value);
					}
				}
			}
		} else {
			$this->variables[$variable_name] = $value;
		}
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method display construction de l'affichage grâce aux templates et aux variables, de façon récursive
	 * @return la page html générée
	 */
	public function draw() {
		foreach ($this->variables as $cle => $valeur) {
			if ($valeur instanceof Template) {
				$valeur = $valeur->draw();
			} else if (is_array($valeur) && current($valeur) instanceof Template) {
				$template = '';
				
				foreach ($valeur as $valeur2) {
					if ($valeur2 instanceof Template) {
						$template .= $valeur2->draw();
					} else {
						$template .= $valeur2;
					}
				}
				
				$valeur = $template;
			}
			
			$this->assign($cle, $valeur);
		}
		
		$fetch = $this->fetch($this->template_name);
		
		$this->clear_all_assign();
		
		return $fetch;
	}
}