<?php
/**
 * @author Sébastien Robert
 * @version 1
 * @name TemplateList classe de gestion de template sur un tableau d'élément
 */
class TemplateList extends Template {
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method __construct() initialisation de la classe
	 */
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method display construction de l'affichage grâce aux templates et aux variables, de façon récursive
	 * @return la page html générée
	 */
	public function draw() {
		$liste = $this->variables;
		$draw = '';
		
		foreach ($liste as $variables) {
			$this->variables = $variables;
			$draw .= parent::draw();
		}
		
		$this->variables = $liste;
		
		return $draw;
	}
}