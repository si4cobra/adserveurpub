<?php
/**
 * @author Sébastien Robert
 * @version 1
 * @name Dispatcher classe de redirection des requêtes http vers le bon contrôleur et la bonne action
 */
class Dispatcher {
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method launch lance la redirection des requêtes http vers le bon contrôleur et la bonne action en fonction des paramètres utilisateurs
	 * @uses Request
	 */
	public static function launch() {
		try {
			$call_connexion = self::call('Connexion');
			
			if ($call_connexion === true) {
				if (Request::hasGetParameter('page')) {
					if (Request::hasGetParameter('action')) {
						$page = str_replace(' ', '', ucwords(str_replace('_', ' ', Request::getGetParameter('page'))));
						$action = str_replace(' ', '', ucwords(str_replace('_', ' ', Request::getGetParameter('action'))));
						
						$call_class_method = self::call($page, $action);
						
						if ($call_class_method === false) {
							$call_principal = self::call('Principal');
							
							if ($call_principal === false) {
								self::call('Login');
							}
						}
					} else {
						$page = str_replace(' ', '', ucwords(str_replace('_', ' ', Request::getGetParameter('page'))));
						
						$call_class = self::call($page);
						
						if ($call_class === false) {
							$call_principal = self::call('Principal');
						
							if ($call_principal === false) {
								self::call('Login');
							}
						}
					}
				} else {
					$call_principal = self::call('Principal');
					
					if ($call_principal === false) {
						self::call('Login');
					}
				}
			} else {
				self::call('Login');
			}
		} catch(Exception $e) {
			if (defined('DEV')) {
				echo $e->getMessage();
			} else {
				$liste_email = Loader::get('UtilisateurListeDiffussion')->envoi_liste_diffusion('777');
				
				if ($liste_email) {
					$id_client = StackValue::getValue('id_client');
					$client = Loader::get('Client')->renvoi_client($id_client);
					
					$liste_email = current(MethodArray::getColumn(array($liste_email), 'liste_email'));
					Loader::get('Mailer')->envoi_mail_erreur_code($liste_email, $client, $e->getMessage());
				}
				
				header('Location:./');
			}
		}
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method call appelle la méthode de la classe, toutes les deux spécifiées en paramètres
	 * @param class la classe a appelée
	 * @param method la méthode a appelée, par défaut appel de la méthode Index
	 * @return si la méthode de la classe spécifiée a été appelé avec succès
	 */
	private static function call($class, $method = 'Index') {
		if (method_exists($class, $method)) {
			return call_user_func(array($class, $method));
		}
	
		return false;
	}
}