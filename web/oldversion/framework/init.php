<?php
define("DEV", true);
/**
 * inclusion de la définiton du timezone
 */
require 'date_timezone.php';

/**
 * inclusion de la fonction de gestion d'erreurs PHP
 */
require 'errorhandler.php';

/**
 * inclusion de la fonction d'autochargement de classes PHP
 */
require 'autoload.php';

/**
 * inclusion de la configuration de la base de données
 */
require 'config/config.php';

