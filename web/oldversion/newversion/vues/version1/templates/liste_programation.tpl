
<div class="row-fluid" style="padding:0;">
    <div id="content" class="span10" style="width:auto;">
        <div class="row-fluid sortable ui-sortable" style="display:inline-block;width:auto;">
            <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                <div class="box-header well" data-original-title="" style="padding:5px;height:auto;cursor:auto;">
                    <div class="btn-group pull-right" style="margin-left:10px;">
                        <a class="btn dropdown-toggle" href="{$sRetour}">
                            <i class="icon-arrow-left"></i>
                            <span class="hidden-phone">Retour</span>
                        </a>
                    </div>
                    <span style="vertical-align:middle;">{$sTitre}</span>
                    <div style="clear:both;"></div>
                </div>
                <div class="box-content" style="padding-bottom:0;display:inline-block;">
                    <div style="width:auto;margin-bottom:0;display:inline-block;vertical-align:top;">
                        <div class="span12 well" style="padding:10px;margin-bottom:10px;width:auto;display:inline-block;float:none;">
                            Toutes les programmations de visuels pour la FACE {$numeroface}
                        </div>
                        {foreach from=$aListe item=objplanning}
                        <table class="table table-bordered table-striped" style="margin-bottom:10px;width:auto;">
                            <tbody>
                            <tr>
                                <td rowspan="3" style="min-width:40px;">
                                    <aside id="id_affiche_{$objplanning.id}" class="modal_seb" style="text-align:center;">
                                        <div>
                                            <img src="../repimages/{$objplanning.id_ecran.value}" style="height:100%;">
                                            <a href="#close" title="Close"></a>
                                        </div>
                                    </aside>
                                    <img src="../repimages/{$objplanning.id_ecran.value}" style="width:40px;">
                                </td>
                                <th colspan="6" style="vertical-align:middle;text-align:center;">
                                    <strong>Planning de programmation</strong>
                                </th>
                            </tr>
                            <tr>
                                <th style="vertical-align:middle;text-align:center;">Date de début</th>
                                <th style="vertical-align:middle;text-align:center;">Date de fin</th>
                                <th style="vertical-align:middle;text-align:center;">Jours</th>
                                <th style="vertical-align:middle;text-align:center;">Horaires</th>
                                <th style="vertical-align:middle;text-align:center;">Statut</th>
                                <th style="vertical-align:middle;text-align:center;">Actions</th>
                            </tr>
                            <tr>
                                <td rowspan="2">{$objplanning.datedebut.value}</td>
                                <td rowspan="2">{$objplanning.datefin.value}</td>
                                <td rowspan="2">
                                    
                                    {$objplanning.periodicitejoursemaine_planning.value}</td>
                                <td rowspan="2">{$objplanning.typeperiodicite_planning.value}</td>
                                <td rowspan="2">{$objplanning.desactivation.value}</td>
                                <td rowspan="2" class="center" style="padding-right:0;">
                                    <a class="btn btn-info" href="{$objplanning.sUrlForm}&idrt={$idrt}&numface={$numeroface}&action=form" style="display:inline-block;margin-bottom:5px;margin-right:5px;">
                                        <i class="icon-edit icon-white"></i>
                                        Modifier
                                    </a>



                                    {if $objplanning.desactivation.value == 'Activé'}

                                    <a class="btn btn-warning" href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim={$id_lienclientrim}&idplanning={$objplanning.id}&idrt={$idrt}&numface={$numeroface}&desct=ok" style="display:inline-block;margin-bottom:5px;margin-right:5px;" onclick="return confirm('Voulez-vous vraiment désactiver ce planning ?');">
                                        <i class="icon-ban-circle icon-white"></i>
                                        Désactiver
                                    </a>
                                    {/if}
                                    {if $objplanning.desactivation.value == 'Désactivé'}
                                        <a class="btn btn-warning" href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim={$id_lienclientrim}&idplanning={$objplanning.id}&idrt={$idrt}&numface={$numeroface}&act=ok" style="display:inline-block;margin-bottom:5px;margin-right:5px;" onclick="return confirm('Voulez-vous vraiment activer ce planning ?');">
                                            <i class="icon-ban-circle icon-white"></i>
                                            Activer
                                        </a>
                                    {/if}
                                    <a class="btn btn-danger"  style="display:inline-block;margin-bottom:5px;margin-right:5px;" onclick="bconf=confirm('Voulez-vous supprimer cette ligne ?');if(bconf)location.replace('{$objplanning.sUrlSupp}');">
                                        <i class="icon-remove icon-white"></i>
                                        Supprimer
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="script_image_trais_noir.php?image={$objplanning.id_ecran.value}">
                                        <span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        {/foreach}

                        <div class="span12 well" style="padding:10px;margin:0 0 10px;width:auto;display:inline-block;float:none;">
                            <a href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim={$id_lienclientrim}&idrt={$idrt}&numface={$numeroface}&action=form">
                                <button class="btn btn-large btn-success" style="padding:2px;">
                                    <span title=".icon32  .icon-white  .icon-square-plus " class="icon32 icon-white icon-square-plus" style="vertical-align:middle;"></span>
                                    Programmer un autre visuel
                                </button>
                            </a>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
        </div>
    </div>
</div>