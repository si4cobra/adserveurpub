<div class="container-fluid" style="padding:0 20px;">
    <div class="row-fluid" style="padding:0;">
        <div id="content" class="span10" style="width:auto;">
            <div class="row-fluid sortable ui-sortable" style="display:inline-block;width:auto;">
                <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                    <div class="box-header well" data-original-title="" style="padding:5px;height:auto;cursor:auto;">
                        <div class="btn-group pull-right" style="margin-left:10px;">
                            <a class="btn dropdown-toggle" href="adserveur-ctrl_adserveur-fli_home">
                                <i class="icon-arrow-left"></i>
                                <span class="hidden-phone">Retour </span>
                            </a>
                        </div>
                        <span style="vertical-align:middle;">{$sTitre}</span>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="span12 well" style="text-align:center;padding:10px;margin:0 0 10px 0;width:auto;display:block;float:none;">
                        <p style="margin:0;font-size:16px;">Vous disposez de {$nbrFace} faces sur le réseau tournant</p>
                    </div>


                {foreach from=$aTabFace item=objface}
                <div class="span12 well" style="padding:10px;margin:0 0 10px 0;width:auto;display:block;float:none;">
                    <h3>FACE {$objface.numero_de_face}</h3>
                    <table>
                        <tr>
                            <td style="vertical-align:top;padding-bottom:20px;">
                                <i class="icon-stop"></i>
                            </td>
                            <td style="vertical-align:top;padding-bottom:20px;">
                                <p style="margin:0;padding:0;font-size:16px;">Affiche tampon*</p>
                            </td>
                            <aside id="id_affiche_tampon_{$objface.affiche_tampon.id}" class="modal_seb" style="text-align:center;">
                                <div>
                                    {if $objface.affiche_tampon.extension neq 'swf'}
                                        <img alt="" src="../{$objface.affiche_tampon.image}" style="height:100%;">
                                    {else}
                                        <object type="application/x-shockwave-flash" data="{$objface.affiche_tampon.image}" height="100%">
                                            <param name="movie" value="../{$objface.affiche_tampon.image}" />
                                            <param name="wmode" value="transparent" />
                                            <p>swf non affichable</p>
                                        </object>
                                    {/if}
                                    <a href="#close" title="Close"></a>
                                </div>
                            </aside>
                            <td style="width:40px;min-width:40px;padding-bottom:20px;">
                                {if $objface.affiche_tampon.extension neq 'swf'}
                                    <img src="../{$objface.affiche_tampon.image}" style="width:40px;"/>
                                {else}
                                    <object type="application/x-shockwave-flash" data="{$objface.affiche_tampon.image}" width="40" height="143">
                                        <param name="movie" value="../{$objface.affiche_tampon.image}" />
                                        <param name="wmode" value="transparent" />
                                        <p>swf non affichable</p>
                                    </object>
                                {/if}
                            </td>
                            <td style="padding-bottom:20px;">
                                <a href="#id_affiche_tampon_{$objface.affiche_tampon.id}">
                                    <span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top;padding-bottom:20px;">
                                <i class="icon-stop"></i>
                            </td>
                            <td style="vertical-align:top;padding-bottom:20px;">
                                <p style="margin:0 0 10px;padding:0;font-size:16px;">Visuel actuellement diffusé</p>
                            </td>
                            <td style="width:40px;min-width:40px;padding-bottom:20px;">
                                {if isset($objface.planning_actuel)}
                                    <aside id="id_affiche_actuel_{$objface.planning_actuel.id}" class="modal_seb" style="text-align:center;">
                                        <div>
                                            {if $objface.planning_actuel.extension neq 'swf'}
                                                <img src="../{$objface.planning_actuel.image}" style="height:100%;"/>
                                            {else}
                                                <object type="application/x-shockwave-flash" data="{$objface.planning_actuel.image}" height="100%">
                                                    <param name="movie" value="../{$objface.planning_actuel.image}" />
                                                    <param name="wmode" value="transparent" />
                                                    <p>swf non affichable</p>
                                                </object>
                                            {/if}
                                            <a href="#close" title="Close"></a>
                                        </div>
                                    </aside>
                                    {if $objface.planning_actuel.extension neq 'swf'}
                                        <img src="../{$objface.planning_actuel.image}" style="width:40px;"/>
                                    {else}
                                        <object type="application/x-shockwave-flash" data="{$objface.planning_actuel.image}" width="40" height="143">
                                            <param name="movie" value="../{$objface.planning_actuel.image}" />
                                            <param name="wmode" value="transparent" />
                                            <p>swf non affichable</p>
                                        </object>
                                    {/if}
                                {else}
                                    <aside id="id_affiche_tampon_actuel_{$objface.affiche_tampon.id}" class="modal_seb" style="text-align:center;">
                                        <div>
                                            {if $objface.affiche_tampon.extension neq 'swf'}
                                                <img alt="" src="../{$objface.affiche_tampon.image}" style="height:100%;">
                                            {else}
                                                <object type="application/x-shockwave-flash" data="{$objface.affiche_tampon.image}" height="100%">
                                                    <param name="movie" value="../{$objface.affiche_tampon.image}" />
                                                    <param name="wmode" value="transparent" />
                                                    <p>swf non affichable</p>
                                                </object>
                                            {/if}
                                            <a href="#close" title="Close"></a>
                                        </div>
                                    </aside>
                                    {if $objface.affiche_tampon.extension neq 'swf'}
                                        <img src="../{$objface.affiche_tampon.image}" style="width:40px;"/>
                                    {else}
                                        <object type="application/x-shockwave-flash" data="{$objface.affiche_tampon.image}" width="40" height="143">
                                            <param name="movie" value="../{$objface.affiche_tampon.image}" />
                                            <param name="wmode" value="transparent" />
                                            <p>swf non affichable</p>
                                        </object>
                                    {/if}
                                {/if}
                            </td>
                            <td style="padding-bottom:20px;">
                                {if isset($objface.planning_actuel)}
                                    <a href="#id_affiche_actuel_{$objface.planning_actuel.id}">
                                        <span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
                                    </a>
                                {else}
                                    <a href="#id_affiche_tampon_actuel_{$objface.affiche_tampon.id}">
                                        <span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
                                    </a>
                                {/if}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="vertical-align:top;">
                                <div>
                                    <a href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim={$objface.id}&idrt={$objface.id_reseau_tournant}&numface={$objface.numero_de_face}">
                                        <button class="btn btn-large btn-info" style="padding:2px;">
                                            <span title=".icon32  .icon-white  .icon-copy " class="icon32 icon-white icon-copy" style="vertical-align:middle;"></span>
                                            Voir toutes vos programmations de visuels
                                        </button>
                                    </a>
                                </div>
                                <p style="margin:0 0 10px;padding:0;font-size:12px;">(campagne promotionnelle ponctuelle ou déclinaisons de visuels)</p>
                                <div>
                                    <a href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim={$objface.id}&idrt={$objface.id_reseau_tournant}&numface={$objface.numero_de_face}&action=form">
                                        <button class="btn btn-large btn-success" style="padding:2px;">
                                            <span title=".icon32  .icon-white  .icon-square-plus " class="icon32 icon-white icon-square-plus" style="vertical-align:middle;"></span>
                                            Programmer un autre visuel
                                        </button>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>
</div>