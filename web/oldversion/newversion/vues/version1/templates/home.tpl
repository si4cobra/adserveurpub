
<div class="container-fluid" style="padding:0 20px;">
    <div class="row-fluid" style="padding:0;">
        <div id="content" class="span10" style="width:auto;">
            <div class="row-fluid sortable ui-sortable" style="display:inline-block;width:auto;">

                {if !empty($aTabRim)}
                <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                    <div class="box-header well" data-original-title style="padding:5px;height:auto;cursor:auto;">
                        <span style="vertical-align:middle;">Actuellement vous êtes présent sur les Relais Informations Multimedia (RIM) suivants</span>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="box-content" style="padding-bottom:0;display:inline-block;">
                        <table class="table table-bordered table-striped" style="width:auto;margin-bottom:8px;">
                            <thead>
                            <tr>
                                <th>Ville</th>
                                <th>Mobilier</th>
                                <th>Adresse</th>
                                <th>Visibilité</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            {foreach from=$aTabRim item=objRIN}
                            <tr>
                                <td>{$objRIN.ville_lerim}</td>
                                <td>
                                    RIM No {$objRIN.numero_mobilier}
                                </td>
                                <td>{$objRIN.adresse_mobilier}</td>
                                <td>{$objRIN.nom_emplacement}</td>
                                <td style="padding-bottom:0;">
                                    <a class="btn btn-info" href="adserveur-ctrl_face-fli_face?id_face={$objRIN.id_face}" style="display:block;margin-bottom:8px;">
                                        <i class="icon-list icon-white"></i>
                                        Visualiser
                                    </a>
                                </td>
                            </tr>
                            {/foreach}
                        </table>

                    </div>
                </div>
                {/if}


                {if !empty($aTabRimRt)}
                <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                    <div class="box-header well" data-original-title style="padding:5px;height:auto;cursor:auto;">
                        <span style="vertical-align:middle;">Actuellement vous êtes présent sur les réseaux tournants suivants</span>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="box-content" style="padding-bottom:0;display:inline-block;">
                        <table class="table table-bordered table-striped" style="width:auto;margin-bottom:8px;">
                            <thead>
                            <th>Réseau tournant</th>

                            </thead>
                            {foreach from=$aTabRimRt item=objRt}
                            <tr>
                                <td>{$objRt.nom_reseau_tournant_pere}<br>Actuellement diffusé sur <b>{$objRt.nom_affiche_client_rim}</b></td>
                                <!--<td style="padding-bottom:0;">
                                    <a class="btn btn-info" href="adserveur-ctrl_liste_rt-fli_liste_rt?id={$objRt.id}" style="display:block;margin-bottom:8px;">
                                        <i class="icon-list icon-white"></i>
                                        Visualiser
                                    </a>
                                </td>-->
                            </tr>
                            {/foreach}
                            <tr>
                                <td style="padding-bottom:0;">
                                    <a class="btn btn-info" href="adserveur-ctrl_liste_rt-fli_liste_rt?id={$aTabRimRt[0]['id']}" style="display:block;margin-bottom:8px;">
                                        <i class="icon-list icon-white"></i>
                                        Visualiser la liste des faces sur les réseaux tournants
                                    </a>
                                </td>

                            </tr>
                        </table>

                    </div>
                </div>
                {/if}






            </div>
        </div>
    </div>
</div>
