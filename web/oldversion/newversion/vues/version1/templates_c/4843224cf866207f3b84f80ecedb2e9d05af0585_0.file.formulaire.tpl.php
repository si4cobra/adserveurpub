<?php
/* Smarty version 3.1.29, created on 2018-04-19 17:38:51
  from "/var/www/html/vues/version1/templates/formulaire.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ad8b80b3e81c0_95920152',
  'file_dependency' => 
  array (
    '4843224cf866207f3b84f80ecedb2e9d05af0585' => 
    array (
      0 => '/var/www/html/vues/version1/templates/formulaire.tpl',
      1 => 1504794665,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ad8b80b3e81c0_95920152 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/var/www/html/libs/plugins/modifier.replace.php';
if (!is_callable('smarty_modifier_date_format')) require_once '/var/www/html/libs/plugins/modifier.date_format.php';
?>
<div style="color:#313131;margin: 0 auto;padding: 15px;margin-top: 10px;margin: 0 auto;text-align: center;padding-top: 10px;">
<?php if ($_smarty_tpl->tpl_vars['bDebugRequete']->value) {?>
<p>
	Requete Select : <br><br>
	<?php echo $_smarty_tpl->tpl_vars['sDebugRequeteSelect']->value;?>
<br><br>
	Requete Insert/Update : <br><br>
	<?php echo $_smarty_tpl->tpl_vars['sDebugRequeteInsertUpdate']->value;?>
<br><br>
</p>
<?php }
if (isset($_smarty_tpl->tpl_vars['sScriptJavascriptInsert']->value)) {?>
	<?php echo $_smarty_tpl->tpl_vars['sScriptJavascriptInsert']->value;?>

<?php }
if (isset($_smarty_tpl->tpl_vars['sScriptJavascriptUpdate']->value)) {?>
	<?php echo $_smarty_tpl->tpl_vars['sScriptJavascriptUpdate']->value;?>

<?php }
if (isset($_smarty_tpl->tpl_vars['aForm']->value)) {?>
<div id="page-heading"><div style="display:inline-block;border:1px solid #D0D1D5;margin-bottom: 20px;"><div class="hidden-xs title-head-search" style="display: inline-block;height: 73px;width: 73px;vertical-align: middle;padding: 20px;"><span class="glyphicon glyphicon-link" style="color:#fff;vertical-align: bottom;font-size: 20px;top: 6px;"></span></div><h1 style="font-size:30px;margin: 0px;display:inline-block;padding: 20px;vertical-align: bottom;" class="text-head-search"><?php echo $_smarty_tpl->tpl_vars['sTitreForm']->value;
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('fli_titrefrom','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['sTitreForm']->value,'\'','\\\'');?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','fli_titrefrom');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></h1></div></div>
	<div style="margin-bottom:20px;">
		<?php if (is_array($_smarty_tpl->tpl_vars['sCategorieForm']->value)) {?>
			<?php
$_from = $_smarty_tpl->tpl_vars['sCategorieForm']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_scategorieform_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_foreach_scategorieform']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_scategorieform'] : false;
$__foreach_scategorieform_0_saved_item = isset($_smarty_tpl->tpl_vars['objCategorieForm']) ? $_smarty_tpl->tpl_vars['objCategorieForm'] : false;
$__foreach_scategorieform_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
$_smarty_tpl->tpl_vars['objCategorieForm'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['__smarty_foreach_scategorieform'] = new Smarty_Variable(array());
$__foreach_scategorieform_0_iteration=0;
$_smarty_tpl->tpl_vars['objCategorieForm']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objCategorieForm']->value) {
$_smarty_tpl->tpl_vars['objCategorieForm']->_loop = true;
$__foreach_scategorieform_0_iteration++;
$_smarty_tpl->tpl_vars['__smarty_foreach_scategorieform']->value['last'] = $__foreach_scategorieform_0_iteration == $__foreach_scategorieform_0_total;
$__foreach_scategorieform_0_saved_local_item = $_smarty_tpl->tpl_vars['objCategorieForm'];
?><div style="background-color:#FFF;display:inline-block;vertical-align:top;height:20px;font-size:18px;padding:10px 0;font-family:Tahoma;"><?php echo $_smarty_tpl->tpl_vars['objCategorieForm']->value['index'];?>
</div><div style="background-color:<?php if ($_smarty_tpl->tpl_vars['objCategorieForm']->value['suivant']) {?>#e3e3e3<?php } else { ?>#777<?php }?>;height:40px;display:inline-block;vertical-align:middle;"><div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div><div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objCategorieForm']->value['texte'];?>
</div><?php if (!(isset($_smarty_tpl->tpl_vars['__smarty_foreach_scategorieform']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_scategorieform']->value['last'] : null)) {?><div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-right-radius: 0;border-bottom-right-radius: 0;background-color:#FFF;"></div><?php }?></div><?php
$_smarty_tpl->tpl_vars['objCategorieForm'] = $__foreach_scategorieform_0_saved_local_item;
}
if ($__foreach_scategorieform_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_foreach_scategorieform'] = $__foreach_scategorieform_0_saved;
}
if ($__foreach_scategorieform_0_saved_item) {
$_smarty_tpl->tpl_vars['objCategorieForm'] = $__foreach_scategorieform_0_saved_item;
}
?><div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div>
		<?php } else { ?>
		<div style="margin-bottom: 20px;">
          <div style="display: inline-block;" class="sub-title-head-search">
            <span class="glyphicon glyphicon-tags sub-title-head-search" style="display: inline-block;height: 35px;width: 35px;padding: 10px;vertical-align: top;top:0px;"></span>
            <h4 class="sub-text-head-search" style="display: inline-block;margin: 0;padding: 10px;font-size: 14px;"><?php echo $_smarty_tpl->tpl_vars['sCategorieForm']->value;
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('fli_catfrom','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['sCategorieForm']->value,'\'','\\\'');?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','fli_catfrom');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></h4>
          </div>
        </div>										
		<?php }?>
	</div>
	<?php if (isset($_smarty_tpl->tpl_vars['bMessageSuccesForm']->value) && $_smarty_tpl->tpl_vars['bMessageSuccesForm']->value) {?>
	<div class="popupAlert alert alert-success alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <?php if (isset($_smarty_tpl->tpl_vars['sMessageSuccesForm']->value)) {
echo $_smarty_tpl->tpl_vars['sMessageSuccesForm']->value;
}?>
    </div>
	<?php }?>
	<?php if (isset($_smarty_tpl->tpl_vars['bMessageErreurForm']->value) && $_smarty_tpl->tpl_vars['bMessageErreurForm']->value) {?>
	<div class="popupAlert alert alert-danger alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <?php if (isset($_smarty_tpl->tpl_vars['sMessageErreurForm']->value)) {
echo $_smarty_tpl->tpl_vars['sMessageErreurForm']->value;
}?>
    </div>
	<?php }?>
	<form
		method="POST"
		action="<?php echo $_smarty_tpl->tpl_vars['url_form']->value;
if ($_smarty_tpl->tpl_vars['bDuplicate']->value) {?>&duplicate=ok<?php }?>"
		<?php if (isset($_smarty_tpl->tpl_vars['bTelechargementFichier']->value) && $_smarty_tpl->tpl_vars['bTelechargementFichier']->value) {?>enctype="multipart/form-data"<?php }?>
	>
		<input type="hidden" name="fi_keymutiple" value="<?php echo $_smarty_tpl->tpl_vars['fi_keymutiple']->value;?>
">

		<?php if (isset($_smarty_tpl->tpl_vars['aForm']->value)) {?>
		<div style="display: inline-block;background-color: #fff;padding: 15px;border: 1px solid #D0D1D5;">
		<table border="0" cellpadding="0" cellspacing="0" id="id-form" style="margin: 0 auto;padding:20px;background-color:#fff;" class="table table-condensed table-hover">
			<tbody>
			
			<?php
$_from = $_smarty_tpl->tpl_vars['aForm']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_objForm_1_saved_item = isset($_smarty_tpl->tpl_vars['objForm']) ? $_smarty_tpl->tpl_vars['objForm'] : false;
$_smarty_tpl->tpl_vars['objForm'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['objForm']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objForm']->value) {
$_smarty_tpl->tpl_vars['objForm']->_loop = true;
$__foreach_objForm_1_saved_local_item = $_smarty_tpl->tpl_vars['objForm'];
?>

				<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['type_champ']) && ((isset($_smarty_tpl->tpl_vars['objForm']->value['aff_form']) && $_smarty_tpl->tpl_vars['objForm']->value['aff_form'] == 'ok') || ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'category' || $_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'bouton'))) {?>
					<?php if ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'text') {?>
						<tr>
							<th valign="top" style="border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
							<td style="border:0;text-align: left;">
								<input 
									type="text"
									class="inp-form"
									id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
									name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
										<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_2_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_2_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_2_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
											<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
										<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_2_saved_local_item;
}
if ($__foreach_valeur_2_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_2_saved_item;
}
if ($__foreach_valeur_2_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_2_saved_key;
}
?>
									<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
										<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_3_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_3_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_3_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
											<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
										<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_3_saved_local_item;
}
if ($__foreach_valeur_3_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_3_saved_item;
}
if ($__foreach_valeur_3_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_3_saved_key;
}
?>
									<?php }?>
								>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
								<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
								<?php }?>
							</td>
						</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'color') {?>
						<tr>
							<th valign="top" style="border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
							<td style="border:0;text-align: left;">
								<input
										type="color"
										class="inp-form"
										id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
										name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
										<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
										<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_4_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_4_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_4_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_4_saved_local_item;
}
if ($__foreach_valeur_4_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_4_saved_item;
}
if ($__foreach_valeur_4_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_4_saved_key;
}
?>
								<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_5_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_5_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_5_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_5_saved_local_item;
}
if ($__foreach_valeur_5_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_5_saved_item;
}
if ($__foreach_valeur_5_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_5_saved_key;
}
?>
								<?php }?>
								>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
									<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
								<?php }?>
							</td>
						</tr>

					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'category') {?>
					<tr>
						<th colspan="2" style="border:0;">
							<h1><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
</h1>
						</th>
					</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'date') {?>
					<tr>
						<th valign="top" style="border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
						<td style="border:0;text-align: left;">
							<input
								type="date"
								id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '') {?>value='<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'],"%Y-%m-%d");?>
'<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_6_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_6_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_6_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_6_saved_local_item;
}
if ($__foreach_valeur_6_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_6_saved_item;
}
if ($__foreach_valeur_6_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_6_saved_key;
}
?>
								<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_7_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_7_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_7_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_7_saved_local_item;
}
if ($__foreach_valeur_7_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_7_saved_item;
}
if ($__foreach_valeur_7_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_7_saved_key;
}
?>
								<?php }?>
							>
							<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
							<?php }?>
						</td>
					</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'textarea') {?>
					<tr>
						<th valign="top" style="border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
						<td style="border:0;text-align: left;">
							<textarea
								id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['wysiwyg']) && $_smarty_tpl->tpl_vars['objForm']->value['wysiwyg'] == 'ok') {?>class="ckeditor"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_8_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_8_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_8_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_8_saved_local_item;
}
if ($__foreach_valeur_8_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_8_saved_item;
}
if ($__foreach_valeur_8_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_8_saved_key;
}
?>
								<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_9_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_9_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_9_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_9_saved_local_item;
}
if ($__foreach_valeur_9_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_9_saved_item;
}
if ($__foreach_valeur_9_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_9_saved_key;
}
?>
								<?php }?>
							><?php echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
</textarea>
							<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
							<?php }?>
						</td>
					</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'checkbox') {?>
					<tr>
						<th style="vertical-align:top;border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
						<td style="padding-top:5px;border:0;text-align: left;">
							<?php if ($_smarty_tpl->tpl_vars['objForm']->value['tags'] == 'ok') {?>
								<select name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
[]" class="js-example-responsive" multiple="multiple" style="width: 100%;">
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_nom_checkbox_10_saved_item = isset($_smarty_tpl->tpl_vars['nom_checkbox']) ? $_smarty_tpl->tpl_vars['nom_checkbox'] : false;
$__foreach_nom_checkbox_10_saved_key = isset($_smarty_tpl->tpl_vars['valeur_checkbox']) ? $_smarty_tpl->tpl_vars['valeur_checkbox'] : false;
$_smarty_tpl->tpl_vars['nom_checkbox'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_checkbox'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_checkbox']->value => $_smarty_tpl->tpl_vars['nom_checkbox']->value) {
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = true;
$__foreach_nom_checkbox_10_saved_local_item = $_smarty_tpl->tpl_vars['nom_checkbox'];
?>
								  		<option value="<?php echo $_smarty_tpl->tpl_vars['valeur_checkbox']->value;?>
" <?php if (is_array($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && in_array($_smarty_tpl->tpl_vars['valeur_checkbox']->value,$_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'])) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['nom_checkbox']->value;?>
</option>
								  	<?php
$_smarty_tpl->tpl_vars['nom_checkbox'] = $__foreach_nom_checkbox_10_saved_local_item;
}
if ($__foreach_nom_checkbox_10_saved_item) {
$_smarty_tpl->tpl_vars['nom_checkbox'] = $__foreach_nom_checkbox_10_saved_item;
}
if ($__foreach_nom_checkbox_10_saved_key) {
$_smarty_tpl->tpl_vars['valeur_checkbox'] = $__foreach_nom_checkbox_10_saved_key;
}
?>						  
								</select>								
							<?php } else { ?>
							<table style="display:inline-block;">
								<tbody>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_nom_checkbox_11_saved_item = isset($_smarty_tpl->tpl_vars['nom_checkbox']) ? $_smarty_tpl->tpl_vars['nom_checkbox'] : false;
$__foreach_nom_checkbox_11_saved_key = isset($_smarty_tpl->tpl_vars['valeur_checkbox']) ? $_smarty_tpl->tpl_vars['valeur_checkbox'] : false;
$_smarty_tpl->tpl_vars['nom_checkbox'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_checkbox'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_checkbox']->value => $_smarty_tpl->tpl_vars['nom_checkbox']->value) {
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = true;
$__foreach_nom_checkbox_11_saved_local_item = $_smarty_tpl->tpl_vars['nom_checkbox'];
?>
									<tr>
										<td style="padding-right:10px;">
											<input
												type="checkbox"
												name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
[]"
												value="<?php echo $_smarty_tpl->tpl_vars['valeur_checkbox']->value;?>
"
												<?php if (is_array($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && in_array($_smarty_tpl->tpl_vars['valeur_checkbox']->value,$_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'])) {?>checked<?php }?>
											>
										</td>
										<td style="padding-right:20px;"><?php echo $_smarty_tpl->tpl_vars['nom_checkbox']->value;?>
</td>
									</tr>
									<?php
$_smarty_tpl->tpl_vars['nom_checkbox'] = $__foreach_nom_checkbox_11_saved_local_item;
}
if ($__foreach_nom_checkbox_11_saved_item) {
$_smarty_tpl->tpl_vars['nom_checkbox'] = $__foreach_nom_checkbox_11_saved_item;
}
if ($__foreach_nom_checkbox_11_saved_key) {
$_smarty_tpl->tpl_vars['valeur_checkbox'] = $__foreach_nom_checkbox_11_saved_key;
}
?>
								</tbody>
							</table>
							<?php }?>
							<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
							<?php }?>
						</td>
					</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'select') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['select_autocomplete']) && $_smarty_tpl->tpl_vars['objForm']->value['select_autocomplete'] == 'ok') {?>
						<tr>
							<th valign="top" style="border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label_filtre'];?>
</th>
							<td style="border:0;text-align: left;">
								<input type='text' 
								name='rech_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
' 	
								id='id_rech<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
'
								class="inp-form"
                                <?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_12_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_12_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_12_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                        <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
                                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_12_saved_local_item;
}
if ($__foreach_valeur_12_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_12_saved_item;
}
if ($__foreach_valeur_12_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_12_saved_key;
}
?>
                                <?php }?>
								onKeyUp="affiche_liste_generique('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['table_item'];?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['id_table_item'];?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['affichage_table_item'];?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['supplogique_table_item'];?>
', '<?php echo $_smarty_tpl->tpl_vars['objForm']->value['tabfiltre_autocomplete'];?>
',this.value,'id_form_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
');">
							</td>
						</tr>		
						<?php }?>
					<tr>
						<th style="border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
						<td style="padding-top:8px;border:0;text-align: left;">

 						<?php if ($_smarty_tpl->tpl_vars['objForm']->value['is_keymultiple'] == "ok" && $_smarty_tpl->tpl_vars['fi_keymutiple']->value == "modif") {?>

                            <?php if (is_array($_smarty_tpl->tpl_vars['objForm']->value['lesitem'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_possible_bdd_13_saved_item = isset($_smarty_tpl->tpl_vars['valeur_possible_bdd']) ? $_smarty_tpl->tpl_vars['valeur_possible_bdd'] : false;
$__foreach_valeur_possible_bdd_13_saved_key = isset($_smarty_tpl->tpl_vars['id_valeur_possible']) ? $_smarty_tpl->tpl_vars['id_valeur_possible'] : false;
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['id_valeur_possible'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['id_valeur_possible']->value => $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value) {
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = true;
$__foreach_valeur_possible_bdd_13_saved_local_item = $_smarty_tpl->tpl_vars['valeur_possible_bdd'];
?>
									<?php if ($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] == $_smarty_tpl->tpl_vars['id_valeur_possible']->value) {
echo $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value;?>

										<input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['id_valeur_possible']->value;?>
">
                                    <?php }?>
							    <?php
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = $__foreach_valeur_possible_bdd_13_saved_local_item;
}
if ($__foreach_valeur_possible_bdd_13_saved_item) {
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = $__foreach_valeur_possible_bdd_13_saved_item;
}
if ($__foreach_valeur_possible_bdd_13_saved_key) {
$_smarty_tpl->tpl_vars['id_valeur_possible'] = $__foreach_valeur_possible_bdd_13_saved_key;
}
?>
                            <?php }?>
						<?php } else { ?>

							<select
								style="padding:5px;"
								id="id_form_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];
if (isset($_smarty_tpl->tpl_vars['objForm']->value['multiple'])) {?>[]<?php }?>"
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['multiple'])) {?>multiple<?php }?>

    							<?php echo $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];?>

									>
								<option value="" <?php if ($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] == '') {?>selected<?php }?>></option>
								<?php if (is_array($_smarty_tpl->tpl_vars['objForm']->value['lesitem'])) {?>
								<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_possible_bdd_14_saved_item = isset($_smarty_tpl->tpl_vars['valeur_possible_bdd']) ? $_smarty_tpl->tpl_vars['valeur_possible_bdd'] : false;
$__foreach_valeur_possible_bdd_14_saved_key = isset($_smarty_tpl->tpl_vars['id_valeur_possible']) ? $_smarty_tpl->tpl_vars['id_valeur_possible'] : false;
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['id_valeur_possible'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['id_valeur_possible']->value => $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value) {
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = true;
$__foreach_valeur_possible_bdd_14_saved_local_item = $_smarty_tpl->tpl_vars['valeur_possible_bdd'];
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['id_valeur_possible']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] == $_smarty_tpl->tpl_vars['id_valeur_possible']->value) {?>selected<?php }?>>
										<?php echo $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value;?>

									</option>
								<?php
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = $__foreach_valeur_possible_bdd_14_saved_local_item;
}
if ($__foreach_valeur_possible_bdd_14_saved_item) {
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = $__foreach_valeur_possible_bdd_14_saved_item;
}
if ($__foreach_valeur_possible_bdd_14_saved_key) {
$_smarty_tpl->tpl_vars['id_valeur_possible'] = $__foreach_valeur_possible_bdd_14_saved_key;
}
?>
								<?php }?>
							</select>
							<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
							<?php }?>
                        <?php }?>
						</td>
					</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'radio') {?>
					<tr>
						<th valign="top" style="border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
						<td style="padding-top:8px;border:0;text-align: left;">
							<div style="display:inline-block;">
								<table>
									<tbody>
										<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_nom_radio_15_saved_item = isset($_smarty_tpl->tpl_vars['nom_radio']) ? $_smarty_tpl->tpl_vars['nom_radio'] : false;
$__foreach_nom_radio_15_saved_key = isset($_smarty_tpl->tpl_vars['valeur_radio']) ? $_smarty_tpl->tpl_vars['valeur_radio'] : false;
$_smarty_tpl->tpl_vars['nom_radio'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_radio'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['nom_radio']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_radio']->value => $_smarty_tpl->tpl_vars['nom_radio']->value) {
$_smarty_tpl->tpl_vars['nom_radio']->_loop = true;
$__foreach_nom_radio_15_saved_local_item = $_smarty_tpl->tpl_vars['nom_radio'];
?>
										<tr>
											<td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
												<input
													type="radio"
													name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
													value="<?php echo $_smarty_tpl->tpl_vars['valeur_radio']->value;?>
"
													<?php if ($_smarty_tpl->tpl_vars['valeur_radio']->value == $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) {?>checked<?php }?>
												>
											</td>
											<td style="padding-bottom:0;vertical-align:middle;height:32px;"><?php echo $_smarty_tpl->tpl_vars['nom_radio']->value;?>
</td>
										</tr>
										<?php
$_smarty_tpl->tpl_vars['nom_radio'] = $__foreach_nom_radio_15_saved_local_item;
}
if ($__foreach_nom_radio_15_saved_item) {
$_smarty_tpl->tpl_vars['nom_radio'] = $__foreach_nom_radio_15_saved_item;
}
if ($__foreach_nom_radio_15_saved_key) {
$_smarty_tpl->tpl_vars['valeur_radio'] = $__foreach_nom_radio_15_saved_key;
}
?> 
									</tbody>
								</table>
							</div>
							<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
							<?php }?>
						</td>
					</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'file') {?>
					<tr>
						<th style="border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
						<td style="border:0;text-align: left;">
							<div style="border-radius:6px;border:1px solid #ACACAC;padding:3px;display:inline-block;vertical-align:middle;">
								<input
									type="file"
									id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
									name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
										<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_16_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_16_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_16_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
											<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
										<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_16_saved_local_item;
}
if ($__foreach_valeur_16_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_16_saved_item;
}
if ($__foreach_valeur_16_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_16_saved_key;
}
?>
									<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
										<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_17_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_17_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_17_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
											<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
										<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_17_saved_local_item;
}
if ($__foreach_valeur_17_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_17_saved_item;
}
if ($__foreach_valeur_17_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_17_saved_key;
}
?>
									<?php }?>
								>
							</div>
							<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['file_aff_modif_form']) && $_smarty_tpl->tpl_vars['objForm']->value['file_aff_modif_form'] == 'ok') {?>
								<?php if ($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '') {?>
									<?php if (preg_match("/\.png"."$"."/",$_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) || preg_match("/\.jpeg"."$"."/",$_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) || preg_match("/\.gif"."$"."/",$_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'])) {?>
									<div style="
										border-radius:6px;
										border:1px solid #ACACAC;
										padding:3px;
										display:inline-block;
										vertical-align:middle;
										<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['file_aff_modif_form_couleur_fond'])) {?>background-color:<?php echo $_smarty_tpl->tpl_vars['objForm']->value['file_aff_modif_form_couleur_fond'];
}?>;"
									>
										<img
											src="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['file_visu'];
echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"
											style="display:inline-block;vertical-align:middle;"
											<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['file_aff_modif_form_taille'])) {?>width="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['file_aff_modif_form_taille'];?>
"<?php }?>
										>
									</div>
									<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '' && preg_match("/\.swf"."$"."/",$_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'])) {?>
									<div style="
										border-radius:6px;
										border:1px solid #ACACAC;
										padding:3px;
										display:inline-block;
										vertical-align:middle;
										<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['file_aff_modif_form_couleur_fond'])) {?>background-color:<?php echo $_smarty_tpl->tpl_vars['objForm']->value['file_aff_modif_form_couleur_fond'];
}?>;"
									>
										<object
											type="application/x-shockwave-flash"
											data="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['file_visu'];
echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"
											<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['file_aff_modif_form_taille'])) {?>width="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['file_aff_modif_form_taille'];?>
"<?php }?>
										>
											<param name="movie" value="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['file_visu'];
echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
" />
											<param name="wmode" value="transparent" />
											<p>swf non affichable</p>
										</object>
									</div>
									<?php } else { ?>
									<a target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['file_visu'];
echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
">Voir le fichier</a>
									<?php }?>
								<?php }?>
							<?php }?>
							<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
							<?php }?>
						</td>
					</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'time') {?>
					<tr>
						<th style="border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
						<td style="border:0;text-align: left;">
							<input
								type="time"
								class="timepicker inp-form"
								id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_18_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_18_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_18_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_18_saved_local_item;
}
if ($__foreach_valeur_18_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_18_saved_item;
}
if ($__foreach_valeur_18_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_18_saved_key;
}
?>
								<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_19_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_19_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_19_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_19_saved_local_item;
}
if ($__foreach_valeur_19_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_19_saved_item;
}
if ($__foreach_valeur_19_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_19_saved_key;
}
?>
								<?php }?>
							>
							<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
							<?php }?>
						</td>
					</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'hour') {?>
					<tr>
						<th style="border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
						<td style="border:0;text-align: left;">
							<input
								type="text"
								class="hourpicker inp-form"
								id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_20_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_20_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_20_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_20_saved_local_item;
}
if ($__foreach_valeur_20_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_20_saved_item;
}
if ($__foreach_valeur_20_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_20_saved_key;
}
?>
								<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_21_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_21_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_21_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_21_saved_local_item;
}
if ($__foreach_valeur_21_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_21_saved_item;
}
if ($__foreach_valeur_21_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_21_saved_key;
}
?>
								<?php }?>
							>
							<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
							<?php }?>
						</td>
					</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'hidden') {?>
					<tr>
						<td style="padding:0;border:0;"></td>
						<td style="padding:0;border:0;">
							<input
								type="hidden"
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['nom_variable'])) {?>name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"  id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'])) {?>value="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"<?php }?>
							>
						</td>
					</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'password') {?>
						<?php if ($_smarty_tpl->tpl_vars['objForm']->value['double_password'] == 'ok') {?>
							<tr>
								<th valign="top"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
								<td>
									<input
											type="password"
											class="inp-form"
											id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
											name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
											<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"<?php }?>
										<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
											<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
										<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_22_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_22_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_22_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
											<?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
										<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_22_saved_local_item;
}
if ($__foreach_valeur_22_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_22_saved_item;
}
if ($__foreach_valeur_22_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_22_saved_key;
}
?>
									<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
										<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_23_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_23_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_23_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
											<?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
										<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_23_saved_local_item;
}
if ($__foreach_valeur_23_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_23_saved_item;
}
if ($__foreach_valeur_23_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_23_saved_key;
}
?>
									<?php }?>
									>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
										<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
									<?php }?>
								</td>
							</tr>
							<tr>
								<th valign="top"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
 2</th>
								<td>
									<input
											type="password"
											class="inp-form"
											id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
_2"
											name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
_2"
											<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"<?php }?>
										<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
											<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
										<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_24_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_24_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_24_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
											<?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
										<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_24_saved_local_item;
}
if ($__foreach_valeur_24_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_24_saved_item;
}
if ($__foreach_valeur_24_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_24_saved_key;
}
?>
									<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
										<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_25_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_25_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_25_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
											<?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
										<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_25_saved_local_item;
}
if ($__foreach_valeur_25_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_25_saved_item;
}
if ($__foreach_valeur_25_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_25_saved_key;
}
?>
									<?php }?>
									>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
										<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
									<?php }?>
								</td>
							</tr>
						<?php } else { ?>
							<tr>
								<th valign="top"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
								<td>
									<input
											type="password"
											class="inp-form"
											id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
											name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
											<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"<?php }?>
										<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
											<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
										<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_26_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_26_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_26_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
											<?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
										<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_26_saved_local_item;
}
if ($__foreach_valeur_26_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_26_saved_item;
}
if ($__foreach_valeur_26_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_26_saved_key;
}
?>
									<?php }?>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
										<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_27_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_27_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_27_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
											<?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
										<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_27_saved_local_item;
}
if ($__foreach_valeur_27_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_27_saved_item;
}
if ($__foreach_valeur_27_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_27_saved_key;
}
?>
									<?php }?>
									>
									<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
										<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
									<?php }?>
								</td>
							</tr>
						<?php }?>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'email') {?>
					<tr>
						<th valign="top" style="border:0;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
						<td style="border:0;text-align: left;">
							<input
								type="email"
								class="inp-form"
								id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_28_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_28_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_28_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_28_saved_local_item;
}
if ($__foreach_valeur_28_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_28_saved_item;
}
if ($__foreach_valeur_28_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_28_saved_key;
}
?>
								<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_29_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_29_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_29_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_29_saved_local_item;
}
if ($__foreach_valeur_29_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_29_saved_item;
}
if ($__foreach_valeur_29_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_29_saved_key;
}
?>
								<?php }?>
							>
							<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
							<?php }?>
						</td>
					</tr>
					<?php } elseif ($_smarty_tpl->tpl_vars['objForm']->value['type_champ'] == 'telephone') {?>
					<tr>
						<th style="border:0;" valign="top"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['text_label'];?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objForm']->value['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></th>
						<td style="border:0;text-align: left;">
							<input
								type="tel"
								class="inp-form"
								id="id_<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								name="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['nom_variable'];?>
"
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['valeur_variable'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['size_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['size_champ'] != '') {?>size="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['size_champ'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['style']) && $_smarty_tpl->tpl_vars['objForm']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objForm']->value['style'];?>
"<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_30_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_30_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_30_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_30_saved_local_item;
}
if ($__foreach_valeur_30_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_30_saved_item;
}
if ($__foreach_valeur_30_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_30_saved_key;
}
?>
								<?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'])) {?>
									<?php
$_from = $_smarty_tpl->tpl_vars['objForm']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_31_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_31_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_31_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
										<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
									<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_31_saved_local_item;
}
if ($__foreach_valeur_31_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_31_saved_item;
}
if ($__foreach_valeur_31_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_31_saved_key;
}
?>
								<?php }?>
							>
							<?php if (isset($_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ']) && $_smarty_tpl->tpl_vars['objForm']->value['ctrl_champ'] == 'ok') {?>
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objForm']->value['mess_erreur'];?>
</div>
							<?php }?>
						</td>
					</tr>
					<?php }?>
				<?php }?>
			<?php
$_smarty_tpl->tpl_vars['objForm'] = $__foreach_objForm_1_saved_local_item;
}
if ($__foreach_objForm_1_saved_item) {
$_smarty_tpl->tpl_vars['objForm'] = $__foreach_objForm_1_saved_item;
}
?>
			</tbody>
		</table>
			<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value) && is_array($_smarty_tpl->tpl_vars['itemBoutonsForm']->value) && count($_smarty_tpl->tpl_vars['itemBoutonsForm']->value) > 0) {?>
			<div style="clear:both;margin-top:20px;text-align: center;">
				<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['precedent'])) {?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['precedent']['url'];?>
">
					<input
						type="button"
						class="btn btn-primary btnResForm"
						value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['precedent']['text_label'];?>
"
						<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['precedent']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['precedent']['style'] != '') {
}?>
						<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['precedent']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['precedent']['tableau_attribut'])) {?>
							<?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['precedent']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_32_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_32_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_32_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
								<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
							<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_32_saved_local_item;
}
if ($__foreach_valeur_32_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_32_saved_item;
}
if ($__foreach_valeur_32_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_32_saved_key;
}
?>
						<?php }?>
						<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['precedent']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['precedent']['fonction_javascript'])) {?>
							<?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['precedent']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_33_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_33_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_33_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
								<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
							<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_33_saved_local_item;
}
if ($__foreach_valeur_33_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_33_saved_item;
}
if ($__foreach_valeur_33_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_33_saved_key;
}
?>
						<?php }?>
					>
				</a>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant'])) {?>
				<input
					type="submit"
					id="id_<?php echo $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant']['nom_variable'];?>
"
					class="btn btn-primary btnValForm"
					name="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant']['nom_variable'];?>
"
					value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant']['text_label'];?>
"
					<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant']['style'] != '') {
}?>
					<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant']['tableau_attribut'])) {?>
						<?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_34_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_34_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_34_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
							<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
						<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_34_saved_local_item;
}
if ($__foreach_valeur_34_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_34_saved_item;
}
if ($__foreach_valeur_34_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_34_saved_key;
}
?>
					<?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant']['fonction_javascript'])) {?>
						<?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['suivant']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_35_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_35_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_35_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
							<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
						<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_35_saved_local_item;
}
if ($__foreach_valeur_35_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_35_saved_item;
}
if ($__foreach_valeur_35_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_35_saved_key;
}
?>
					<?php }?>
				>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider'])) {?>
				<input
					type="submit"
					class="btn btn-primary btnValForm"
					id="id_<?php echo $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider']['nom_variable'];?>
"
					name="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider']['nom_variable'];?>
"
					value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider']['text_label'];?>
"
					<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider']['style'] != '') {
}?>
					<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider']['tableau_attribut'])) {?>
						<?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_36_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_36_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_36_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
							<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
						<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_36_saved_local_item;
}
if ($__foreach_valeur_36_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_36_saved_item;
}
if ($__foreach_valeur_36_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_36_saved_key;
}
?>
					<?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider']['fonction_javascript'])) {?>
						<?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['valider']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_37_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_37_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_37_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
							<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
						<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_37_saved_local_item;
}
if ($__foreach_valeur_37_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_37_saved_item;
}
if ($__foreach_valeur_37_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_37_saved_key;
}
?>
					<?php }?>
				>
				<?php }?>												
				<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['annuler']) && $_smarty_tpl->tpl_vars['bAffAnnuler']->value) {?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['annuler']['url'];?>
">
					<input
						type="button"
						class="btn btn-primary btnResForm"
						value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['annuler']['text_label'];?>
"
						<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['annuler']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['annuler']['style'] != '') {
}?>
						<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['annuler']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['annuler']['tableau_attribut'])) {?>
							<?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['annuler']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_38_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_38_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_38_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
								<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
							<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_38_saved_local_item;
}
if ($__foreach_valeur_38_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_38_saved_item;
}
if ($__foreach_valeur_38_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_38_saved_key;
}
?>
						<?php }?>
						<?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['annuler']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsForm']->value['annuler']['fonction_javascript'])) {?>
							<?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsForm']->value['annuler']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_39_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_39_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_39_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
								<?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {
echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"<?php }?>
							<?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_39_saved_local_item;
}
if ($__foreach_valeur_39_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_39_saved_item;
}
if ($__foreach_valeur_39_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_39_saved_key;
}
?>
						<?php }?>
					>
				</a>
				<?php }?>
			</div>
			<?php }?>
			</div>
		<?php }?>
	</form>
<?php }?>
</div><?php }
}
