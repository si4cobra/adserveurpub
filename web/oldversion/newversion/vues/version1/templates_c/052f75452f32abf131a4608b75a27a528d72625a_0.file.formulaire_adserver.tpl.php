<?php
/* Smarty version 3.1.29, created on 2019-10-22 16:44:31
  from "/var/www/html/newversion/vues/version1/templates/formulaire_adserver.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5daf15cff329c4_43597214',
  'file_dependency' => 
  array (
    '052f75452f32abf131a4608b75a27a528d72625a' => 
    array (
      0 => '/var/www/html/newversion/vues/version1/templates/formulaire_adserver.tpl',
      1 => 1571755445,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5daf15cff329c4_43597214 ($_smarty_tpl) {
?>
<link id="bs-css" href="view/css/bootstrap-cerulean.min.css" rel="stylesheet">
<link href="view/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="view/css/charisma-app.min.css" rel="stylesheet">
<link href='../view/css/chosen.min.css' rel='stylesheet'>
<link href='../view/css/colorbox.min.css' rel='stylesheet'>
<link href='../view/css/elfinder.min.css' rel='stylesheet'>
<link href='../view/css/elfinder.theme.min.css' rel='stylesheet'>
<link href='../view/css/fullcalendar.min.css' rel='stylesheet'>
<link href='../view/css/fullcalendar.print.min.css' rel='stylesheet'  media='print'>
<link href="view/css/jquery-ui-1.8.21.custom.min.css" rel="stylesheet">
<link href='../view/css/jquery.cleditor.min.css' rel='stylesheet'>
<link href='../view/css/jquery.iphone.toggle.min.css' rel='stylesheet'>
<link href='../view/css/jquery.noty.min.css' rel='stylesheet'>
<link href='../view/css/jquery.ui.timepicker.min.css' rel='stylesheet'>
<link href='../view/css/noty_theme_default.min.css' rel='stylesheet'>
<link href='../view/css/opa-icons.min.css' rel='stylesheet'>
<link href='../view/css/uniform.default.min.css' rel='stylesheet'>
<link href='../view/css/uploadify.min.css' rel='stylesheet'>
<link href='../view/css/experiment.css' rel='stylesheet'>
<div class="container-fluid" style="padding:0 20px;">
    <div class="row-fluid" style="padding:0;">
        <div id="content" class="span10" style="width:auto;">
            <div class="row-fluid sortable ui-sortable" style="display:inline-block;width:auto;">
                <?php if (isset($_smarty_tpl->tpl_vars['bMessageErreurForm']->value) && $_smarty_tpl->tpl_vars['bMessageErreurForm']->value) {?>
                <div class="alert alert-error" style="display:inline-block;margin-bottom:8px;">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>Echec!</strong><br>
                    <?php if (isset($_smarty_tpl->tpl_vars['sMessageErreurForm']->value)) {
echo $_smarty_tpl->tpl_vars['sMessageErreurForm']->value;
}?>
                </div>
                <?php }?>


                <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                    <div class="box-header well" data-original-title="" style="padding:5px;height:auto;cursor:auto;">
                        <div class="btn-group pull-right" style="margin-left:10px;">
                            <a class="btn dropdown-toggle" href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim=<?php echo $_smarty_tpl->tpl_vars['id_lienclientrim']->value;?>
&idrt=<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
&numface=<?php echo $_smarty_tpl->tpl_vars['numface']->value;?>
">
                                <i class="icon-arrow-left"></i>
                                <span class="hidden-phone">Retour</span>
                            </a>
                        </div>
                        <span style="vertical-align:middle;"><?php echo $_smarty_tpl->tpl_vars['sTitre']->value;?>
</span>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="box-content" style="padding-bottom:0;display:inline-block;">
                        <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">

                            <div class="box-header well" data-original-title="" style="padding:5px;height:auto;cursor:auto;">
                                <span style="vertical-align:middle;">Nouvelle programmation de visuels pour la FACE 1</span>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="box-content" style="padding-bottom:0;display:inline-block;">
                                <form action="<?php echo $_smarty_tpl->tpl_vars['url_form']->value;?>
" method="POST" class="form-horizontal" style="margin-bottom:0;" enctype="multipart/form-data">
                                    <input type="hidden" name="id_lienclientrim" value="<?php echo $_smarty_tpl->tpl_vars['id_lienclientrim']->value;?>
">
                                    <input type="hidden" name="idrt" value="<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
">
                                    <input type="hidden" name="numface" value="<?php echo $_smarty_tpl->tpl_vars['numface']->value;?>
">
                                    <input type="hidden" id="id_planning" value="<?php echo $_smarty_tpl->tpl_vars['id_planning']->value;?>
" >
                                    <fieldset style="text-align:center;">
                                        <?php if (!empty($_smarty_tpl->tpl_vars['id_planning']->value)) {?>
                                            <div style="vertical-align:top;text-align:left;">


                                                <legend style="margin-bottom:10px;">Changer le visuel :</legend>

                                                <div>
                                                    
                                                    <input type="radio" onclick="displayDiv('visuel','on')" name="changeVisuel" checked value="Y">Oui
                                                    <input type="radio"  onclick="displayDiv('visuel','off')" name="changeVisuel" value="N">Non
                                                </div>
                                            </div>
                                        <?php }?>
                                        <div style="vertical-align:top;text-align:left;" id="visuel" >
                                            <legend style="margin-bottom:10px;">Sélectionner un visuel:</legend>
                                            <div style="margin:10px 20px 30px 0">
                                                <label class="control-label" for="fichier_ecran" style="width:auto;margin-left:10px;">Visuel:</label>
                                                <div class="controls" style="margin-left:10px;">
                                                    <div class="uploader" id="uniform-fichier_ecran"><input class="input-file uniform_on" id="fichier_ecran" name="fichier_ecran" type="file" size="25" style="opacity:0;"></div>
                                                    <p>format fichier autorisé (png, jpg, gif), dimensions autorisées (720*2560), poids fichier autorisé (878.90625 Ko)</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="width:auto;text-align:left;">
                                            <legend style="margin-bottom:10px;line-height:initial;">Planifier sa diffusion:</legend>
                                            <div style="width:auto;text-align:left;margin-left:30px;">
                                                <legend style="margin-bottom:10px;line-height:initial;">Période d'affichage</legend>
                                                <div style="margin:10px 0;">
                                                    <div style="display:inline-block;">
                                                        <label class="control-label" for="date_debut" style="width:auto;margin-left:10px;">Date de début:</label>
                                                        <div class="controls" style="margin-left:105px;">
                                                            <input type="date"  id="date_debut" name="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[1]['nom_variable'];?>
" style="margin-left:5px;width:125px;" value="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[1]['valeur_variable'];?>
">
                                                        </div>
                                                    </div>
                                                    <div style="display:inline-block;">
                                                        <label class="control-label" for="date_fin" style="width:auto;margin-left:10px;">Date de fin:</label>
                                                        <div class="controls" style="margin-left:85px;">
                                                            <input type="date"  id="date_fin" name="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[2]['nom_variable'];?>
" style="margin-left:5px;width:125px;" value="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[2]['valeur_variable'];?>
">
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="btn btn-warning" id="bouton_mode_expert_planning">
                                                    <i class="icon-edit icon-white"></i>
                                                    <span id="label_bouton_mode_expert_planning">
												Affiner la programmation (mode expert)
											</span>
                                                </a>
                                                <div id="mode_expert_planning" style="display:none;">
                                                    <legend style="margin-bottom:10px;line-height:initial;">Jours d'affichage</legend>
                                                    <div style="margin:10px 0">
                                                        <div style="margin:10px;display:inline;">
                                                            <div style="display:inline-block;margin-right:20px;">
                                                                <input type="checkbox" name="periodicite" <?php if ($_smarty_tpl->tpl_vars['aTabJour2']->value[0] == 'ok' && $_smarty_tpl->tpl_vars['aTabJour2']->value[1] == 'ok' && $_smarty_tpl->tpl_vars['aTabJour2']->value[2] == 'ok' && $_smarty_tpl->tpl_vars['aTabJour2']->value[3] == 'ok' && $_smarty_tpl->tpl_vars['aTabJour2']->value[4] == 'ok' && $_smarty_tpl->tpl_vars['aTabJour2']->value[5] == 'ok' && $_smarty_tpl->tpl_vars['aTabJour2']->value[6] == 'ok' || empty($_smarty_tpl->tpl_vars['id_planning']->value)) {?>checked=""<?php }?>>
                                                                <label style="display:inline;">
                                                                    Tous les jours
                                                                </label>
                                                            </div>
                                                            <div style="display:inline-block;">
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" id="1" name="periodicite_jour[]" value="1" <?php if ($_smarty_tpl->tpl_vars['aTabJour2']->value[1] == 'ok' || empty($_smarty_tpl->tpl_vars['id_planning']->value)) {?>checked="" <?php }?>>
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Lundi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" id="2" name="periodicite_jour[]" value="2" <?php if ($_smarty_tpl->tpl_vars['aTabJour2']->value[2] == 'ok' || empty($_smarty_tpl->tpl_vars['id_planning']->value)) {?>checked=""<?php }?>>
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Mardi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" id="3" name="periodicite_jour[]" value="3" <?php if ($_smarty_tpl->tpl_vars['aTabJour2']->value[3] == 'ok' || empty($_smarty_tpl->tpl_vars['id_planning']->value)) {?>checked=""<?php }?>>
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Mercredi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" id="4" name="periodicite_jour[]" value="4" <?php if ($_smarty_tpl->tpl_vars['aTabJour2']->value[4] == 'ok' || empty($_smarty_tpl->tpl_vars['id_planning']->value)) {?>checked=""<?php }?>>
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Jeudi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" id="5" name="periodicite_jour[]" value="5" <?php if ($_smarty_tpl->tpl_vars['aTabJour2']->value[5] == 'ok' || empty($_smarty_tpl->tpl_vars['id_planning']->value)) {?>checked=""<?php }?>>
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Vendredi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" id="6" name="periodicite_jour[]" value="6" <?php if ($_smarty_tpl->tpl_vars['aTabJour2']->value[6] == 'ok' || empty($_smarty_tpl->tpl_vars['id_planning']->value)) {?>checked=""<?php }?>>
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Samedi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" id="7" name="periodicite_jour[]" value="0" <?php if ($_smarty_tpl->tpl_vars['aTabJour2']->value[0] == 'ok' || empty($_smarty_tpl->tpl_vars['id_planning']->value)) {?>checked=""<?php }?>>
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Dimanche
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <legend style="margin-bottom:10px;line-height:initial;">Horaire d'affichage</legend>
                                                    <div style="margin:10px 0;">
                                                        <div style="display:inline-block;vertical-align:top;margin-bottom:10px;margin-right:20px;">
                                                            <label style="display:inline;">Toute la journée:</label>
                                                            <input type="checkbox" id="toute_la_journee" value="1" <?php if (empty($_smarty_tpl->tpl_vars['id_planning']->value) || ($_smarty_tpl->tpl_vars['aForm']->value[4]['valeur_variable'] == '00:00:00' && $_smarty_tpl->tpl_vars['aForm']->value[5]['valeur_variable'] == '23:59:59')) {?>checked=""<?php }?>>
                                                        </div>
                                                        <div style="display:inline-block;">
                                                            <div style="display:inline-block;margin-right:10px;">
                                                                <label class="control-label " for="heure_debut" style="width:auto;">Heure de début:</label>
                                                                <div class="controls" style="margin-left:105px;">
                                                                    <input type="text" class="input-xlarge datepicker_hour " id="heure_debut" name="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[4]['nom_variable'];?>
" style="margin-left:5px;width:80px;" value="<?php if ($_smarty_tpl->tpl_vars['aForm']->value[4]['valeur_variable'] == '') {?>00:00:00<?php } else {
echo $_smarty_tpl->tpl_vars['aForm']->value[4]['valeur_variable'];
}?>">
                                                                </div>
                                                            </div>
                                                            <div style="display:inline-block;">
                                                                <label class="control-label" for="heure_fin" style="width:auto;">Heure de fin:</label>
                                                                <div class="controls" style="margin-left:85px;">
                                                                    <input type="text" class="input-xlarge datepicker_hour" id="heure_fin" name="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[5]['nom_variable'];?>
" style="margin-left:5px;width:80px;" value="<?php if ($_smarty_tpl->tpl_vars['aForm']->value[5]['valeur_variable'] == '') {?>23:59:59<?php } else {
echo $_smarty_tpl->tpl_vars['aForm']->value[5]['valeur_variable'];
}?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <legend style="margin-bottom:10px;line-height:initial;">Activation du planning</legend>
                                                    <div style="margin:10px 0">
                                                        <label class="control-label" for="desactivation" style="width:auto;margin-left:10px;">Activer:</label>
                                                        <div class="controls" style="margin-left:70px;">
                                                            <input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[3]['nom_variable'];?>
" value="N"  <?php if ($_smarty_tpl->tpl_vars['aForm']->value[3]['valeur_variable'] == 'Activé') {
} else { ?>checked="checked"<?php }?>>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions" style="display:inline-block;padding:10px">
                                            <button type="submit" class="btn btn-primary" name="valider_edition_planning" value="1">Valider</button>
                                            <a class="btn dropdown-toggle" href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim=<?php echo $_smarty_tpl->tpl_vars['id_lienclientrim']->value;?>
&idrt=<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
&numface=<?php echo $_smarty_tpl->tpl_vars['numface']->value;?>
">
                                                Annuler
                                            </a>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo '<script'; ?>
>
    

    function displayDiv(div,hide) {
        if (hide == 'on')
            {
                $("#"+div).fadeIn();
            }
        else
            {
                $("#"+div).fadeOut();
            }
    }

    $(function() {
        $('#datepicker').datepicker({
            dateFormat: 'yy-dd-mm',
            onSelect: function(datetext) {
                var d = new Date(); // for now

                var h = d.getHours();
                h = (h < 10) ? ("0" + h) : h ;

                var m = d.getMinutes();
                m = (m < 10) ? ("0" + m) : m ;

                var s = d.getSeconds();
                s = (s < 10) ? ("0" + s) : s ;

                datetext = datetext + " " + h + ":" + m + ":" + s;

                $('#datepicker').val(datetext);
            }
        });
    });

    
<?php echo '</script'; ?>
>

<?php }
}
