<?php
/* Smarty version 3.1.29, created on 2018-04-23 10:25:57
  from "/var/www/html/vues/version1/templates/liste_face.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5add9895e5c361_53224521',
  'file_dependency' => 
  array (
    'eee1ac3bc537c7ac3bc97f8a0a23f13f050b75f1' => 
    array (
      0 => '/var/www/html/vues/version1/templates/liste_face.tpl',
      1 => 1524471944,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5add9895e5c361_53224521 ($_smarty_tpl) {
?>
<div class="container-fluid" style="padding:0 20px;">
    <div class="row-fluid" style="padding:0;">
        <div id="content" class="span10" style="width:auto;">
            <div class="row-fluid sortable ui-sortable" style="display:inline-block;width:auto;">
                <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                    <div class="box-header well" data-original-title="" style="padding:5px;height:auto;cursor:auto;">
                        <div class="btn-group pull-right" style="margin-left:10px;">
                            <a class="btn dropdown-toggle" href="">
                                <i class="icon-arrow-left"></i>
                                <span class="hidden-phone">Retour </span>
                            </a>
                        </div>
                        <span style="vertical-align:middle;">Réseau tournant - RESEAU TOURNANT CLAMART (RTCL)</span>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="span12 well" style="text-align:center;padding:10px;margin:0 0 10px 0;width:auto;display:block;float:none;">
                        <p style="margin:0;font-size:16px;">Vous disposez de <?php echo $_smarty_tpl->tpl_vars['nbrFace']->value;?>
 faces sur le réseau tournant</p>
                    </div>


                <?php
$_from = $_smarty_tpl->tpl_vars['aTabFace']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_objface_0_saved_item = isset($_smarty_tpl->tpl_vars['objface']) ? $_smarty_tpl->tpl_vars['objface'] : false;
$_smarty_tpl->tpl_vars['objface'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['objface']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objface']->value) {
$_smarty_tpl->tpl_vars['objface']->_loop = true;
$__foreach_objface_0_saved_local_item = $_smarty_tpl->tpl_vars['objface'];
?>
                <div class="span12 well" style="padding:10px;margin:0 0 10px 0;width:auto;display:block;float:none;">
                    <h3>FACE <?php echo $_smarty_tpl->tpl_vars['objface']->value['numero_de_face'];?>
</h3>
                    <table>
                        <tr>
                            <td style="vertical-align:top;padding-bottom:20px;">
                                <i class="icon-stop"></i>
                            </td>
                            <td style="vertical-align:top;padding-bottom:20px;">
                                <p style="margin:0;padding:0;font-size:16px;">Affiche tampon*</p>
                            </td>
                            <aside id="id_affiche_tampon_<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['id'];?>
" class="modal_seb" style="text-align:center;">
                                <div>
                                    <?php if ($_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['extension'] != 'swf') {?>
                                        <img alt="" src="<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['image'];?>
" style="height:100%;">
                                    <?php } else { ?>
                                        <object type="application/x-shockwave-flash" data="<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['image'];?>
" height="100%">
                                            <param name="movie" value="<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['image'];?>
" />
                                            <param name="wmode" value="transparent" />
                                            <p>swf non affichable</p>
                                        </object>
                                    <?php }?>
                                    <a href="#close" title="Close"></a>
                                </div>
                            </aside>
                            <td style="width:40px;min-width:40px;padding-bottom:20px;">
                                <?php if ($_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['extension'] != 'swf') {?>
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['image'];?>
" style="width:40px;"/>
                                <?php } else { ?>
                                    <object type="application/x-shockwave-flash" data="<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['image'];?>
" width="40" height="143">
                                        <param name="movie" value="<?php echo $_smarty_tpl->tpl_vars['']->value['objface']['affiche_tampon']['image'];?>
" />
                                        <param name="wmode" value="transparent" />
                                        <p>swf non affichable</p>
                                    </object>
                                <?php }?>
                            </td>
                            <td style="padding-bottom:20px;">
                                <a href="#id_affiche_tampon_<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['id'];?>
">
                                    <span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top;padding-bottom:20px;">
                                <i class="icon-stop"></i>
                            </td>
                            <td style="vertical-align:top;padding-bottom:20px;">
                                <p style="margin:0 0 10px;padding:0;font-size:16px;">Visuel actuellement diffusé</p>
                            </td>
                            <td style="width:40px;min-width:40px;padding-bottom:20px;">
                                <?php if (isset($_smarty_tpl->tpl_vars['objface']->value['planning_actuel'])) {?>
                                    <aside id="id_affiche_actuel_<?php echo $_smarty_tpl->tpl_vars['objface']->value['planning_actuel']['id'];?>
" class="modal_seb" style="text-align:center;">
                                        <div>
                                            <?php if ($_smarty_tpl->tpl_vars['objface']->value['planning_actuel']['extension'] != 'swf') {?>
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['objface']->value['planning_actuel']['image'];?>
" style="height:100%;"/>
                                            <?php } else { ?>
                                                <object type="application/x-shockwave-flash" data="<?php echo $_smarty_tpl->tpl_vars['objface']->value['planning_actuel']['image'];?>
" height="100%">
                                                    <param name="movie" value="<?php echo $_smarty_tpl->tpl_vars['objface']->value['planning_actuel']['image'];?>
" />
                                                    <param name="wmode" value="transparent" />
                                                    <p>swf non affichable</p>
                                                </object>
                                            <?php }?>
                                            <a href="#close" title="Close"></a>
                                        </div>
                                    </aside>
                                    <?php if ($_smarty_tpl->tpl_vars['objface']->value['planning_actuel']['extension'] != 'swf') {?>
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['objface']->value['planning_actuel']['image'];?>
" style="width:40px;"/>
                                    <?php } else { ?>
                                        <object type="application/x-shockwave-flash" data="<?php echo $_smarty_tpl->tpl_vars['objface']->value['planning_actuel']['image'];?>
" width="40" height="143">
                                            <param name="movie" value="<?php echo $_smarty_tpl->tpl_vars['objface']->value['planning_actuel']['image'];?>
" />
                                            <param name="wmode" value="transparent" />
                                            <p>swf non affichable</p>
                                        </object>
                                    <?php }?>
                                <?php } else { ?>
                                    <aside id="id_affiche_tampon_actuel_<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['id'];?>
" class="modal_seb" style="text-align:center;">
                                        <div>
                                            <?php if ($_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['extension'] != 'swf') {?>
                                                <img alt="" src="<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['image'];?>
" style="height:100%;">
                                            <?php } else { ?>
                                                <object type="application/x-shockwave-flash" data="<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['image'];?>
" height="100%">
                                                    <param name="movie" value="<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['image'];?>
" />
                                                    <param name="wmode" value="transparent" />
                                                    <p>swf non affichable</p>
                                                </object>
                                            <?php }?>
                                            <a href="#close" title="Close"></a>
                                        </div>
                                    </aside>
                                    <?php if ($_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['extension'] != 'swf') {?>
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['image'];?>
" style="width:40px;"/>
                                    <?php } else { ?>
                                        <object type="application/x-shockwave-flash" data="<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['image'];?>
" width="40" height="143">
                                            <param name="movie" value="<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['image'];?>
" />
                                            <param name="wmode" value="transparent" />
                                            <p>swf non affichable</p>
                                        </object>
                                    <?php }?>
                                <?php }?>
                            </td>
                            <td style="padding-bottom:20px;">
                                <?php if (isset($_smarty_tpl->tpl_vars['objface']->value['planning_actuel'])) {?>
                                    <a href="#id_affiche_actuel_<?php echo $_smarty_tpl->tpl_vars['objface']->value['planning_actuel']['id'];?>
">
                                        <span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
                                    </a>
                                <?php } else { ?>
                                    <a href="#id_affiche_tampon_actuel_<?php echo $_smarty_tpl->tpl_vars['objface']->value['affiche_tampon']['id'];?>
">
                                        <span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
                                    </a>
                                <?php }?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="vertical-align:top;">
                                <div>
                                    <a href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim=<?php echo $_smarty_tpl->tpl_vars['objface']->value['id'];?>
&idrt=<?php echo $_smarty_tpl->tpl_vars['objface']->value['id_reseau_tournant'];?>
&numface=<?php echo $_smarty_tpl->tpl_vars['objface']->value['numero_de_face'];?>
">
                                        <button class="btn btn-large btn-info" style="padding:2px;">
                                            <span title=".icon32  .icon-white  .icon-copy " class="icon32 icon-white icon-copy" style="vertical-align:middle;"></span>
                                            Voir toutes vos programmations de visuels
                                        </button>
                                    </a>
                                </div>
                                <p style="margin:0 0 10px;padding:0;font-size:12px;">(campagne promotionnelle ponctuelle ou déclinaisons de visuels)</p>
                                <div>
                                    <a href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim=<?php echo $_smarty_tpl->tpl_vars['objface']->value['id'];?>
&idrt=<?php echo $_smarty_tpl->tpl_vars['objface']->value['id_reseau_tournant'];?>
&numface=<?php echo $_smarty_tpl->tpl_vars['objface']->value['numero_de_face'];?>
&action=form">
                                        <button class="btn btn-large btn-success" style="padding:2px;">
                                            <span title=".icon32  .icon-white  .icon-square-plus " class="icon32 icon-white icon-square-plus" style="vertical-align:middle;"></span>
                                            Programmer un autre visuel
                                        </button>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <?php
$_smarty_tpl->tpl_vars['objface'] = $__foreach_objface_0_saved_local_item;
}
if ($__foreach_objface_0_saved_item) {
$_smarty_tpl->tpl_vars['objface'] = $__foreach_objface_0_saved_item;
}
?>
            </div>
        </div>
    </div>
</div>
</div><?php }
}
