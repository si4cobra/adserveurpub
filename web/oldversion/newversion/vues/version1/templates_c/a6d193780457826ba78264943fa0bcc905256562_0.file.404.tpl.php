<?php
/* Smarty version 3.1.29, created on 2019-10-07 09:51:23
  from "/var/www/html/newversion/vues/version1/templates/404.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5d9aee7b8d2ce3_52795479',
  'file_dependency' => 
  array (
    'a6d193780457826ba78264943fa0bcc905256562' => 
    array (
      0 => '/var/www/html/newversion/vues/version1/templates/404.tpl',
      1 => 1523609685,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d9aee7b8d2ce3_52795479 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="fr"> <!--<![endif]-->
<head>
    <title><?php echo $_smarty_tpl->tpl_vars['sTitreDeLaPage']->value;?>
</title>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
    
    <!-- <link href="view/css/adserver.css" rel="stylesheet"> -->
    <link id="bs-css" href="view/css/bootstrap-cerulean.min.css" rel="stylesheet">
    <link href="view/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="view/css/charisma-app.min.css" rel="stylesheet">
    <link href='view/css/chosen.min.css' rel='stylesheet'>
    <link href='view/css/colorbox.min.css' rel='stylesheet'>
    <link href='view/css/elfinder.min.css' rel='stylesheet'>
    <link href='view/css/elfinder.theme.min.css' rel='stylesheet'>
    <link href='view/css/fullcalendar.min.css' rel='stylesheet'>
    <link href='view/css/fullcalendar.print.min.css' rel='stylesheet'  media='print'>
    <link href="view/css/jquery-ui-1.8.21.custom.min.css" rel="stylesheet">
    <link href='view/css/jquery.cleditor.min.css' rel='stylesheet'>
    <link href='view/css/jquery.iphone.toggle.min.css' rel='stylesheet'>
    <link href='view/css/jquery.noty.min.css' rel='stylesheet'>
    <link href='view/css/jquery.ui.timepicker.min.css' rel='stylesheet'>
    <link href='view/css/noty_theme_default.min.css' rel='stylesheet'>
    <link href='view/css/opa-icons.min.css' rel='stylesheet'>
    <link href='view/css/uniform.default.min.css' rel='stylesheet'>
    <link href='view/css/uploadify.min.css' rel='stylesheet'>
    <link href='view/css/experiment.css' rel='stylesheet'>
</head>
<body>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <div class="btn-group pull-right" >
                <a class="btn dropdown-toggle" href="deco.php">
                    <i class="icon-off">
                    </i>
                    <span class="hidden-phone">Déconnexion</span>
                </a>
            </div>
            <a class="brand" href="./" style="width:auto;float:none;">
                <span style="font-family:Verdana;float:none;">AD-SERVEUR PUB - <?php if (isset($_smarty_tpl->tpl_vars['aData']->value['nom_client'])) {
echo $_smarty_tpl->tpl_vars['aData']->value['nom_client'];
}?></span>
            </a>
        </div>
    </div>
</div>



<!-- Page Content -->
<div id="page-content-wrapper" <?php if (empty($_smarty_tpl->tpl_vars['tTplMenu']->value)) {?>style="margin-top: 40px;"<?php }?>>
    <div class="container-fluid">
        <div class="row">

            <div style="text-align: center;">
                <h1 style="font-size: 150px;color: rgb(162, 39, 38);">&#10008;</h1>
                <h1>ERREUR 404</h1>
                <h3>La page que vous recherchez n'existe pas !</h3>
            </div>

        </div>
    </div>
</div>
<!-- /#wrapper -->


<!-- <?php echo '<script'; ?>
 src="view/js/adserver.js"><?php echo '</script'; ?>
> -->
<?php echo '<script'; ?>
 src="view/js/jquery-1.7.2.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery-ui-1.8.21.custom.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-transition.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-alert.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-modal.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-dropdown.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-scrollspy.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-tab.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-tooltip.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-popover.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-button.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-collapse.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-carousel.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-typeahead.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/bootstrap-tour.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.cookie.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src='view/js/fullcalendar.min.js'><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src='view/js/jquery.dataTables.min.js'><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/excanvas.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.flot.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.flot.pie.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.flot.stack.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.flot.resize.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.chosen.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.uniform.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.colorbox.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.cleditor.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.noty.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.elfinder.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.raty.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.iphone.toggle.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.autogrow-textarea.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.uploadify-3.1.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.history.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/jquery.ui.timepicker.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="view/js/script.js"><?php echo '</script'; ?>
>

</body>

</body>
</html><?php }
}
