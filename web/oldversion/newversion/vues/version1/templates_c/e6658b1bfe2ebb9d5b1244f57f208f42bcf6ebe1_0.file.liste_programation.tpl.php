<?php
/* Smarty version 3.1.29, created on 2018-08-02 08:55:22
  from "/var/www/html/vues/version1/templates/liste_programation.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b62aada22d392_00562034',
  'file_dependency' => 
  array (
    'e6658b1bfe2ebb9d5b1244f57f208f42bcf6ebe1' => 
    array (
      0 => '/var/www/html/vues/version1/templates/liste_programation.tpl',
      1 => 1533192651,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b62aada22d392_00562034 ($_smarty_tpl) {
?>

<div class="row-fluid" style="padding:0;">
    <div id="content" class="span10" style="width:auto;">
        <div class="row-fluid sortable ui-sortable" style="display:inline-block;width:auto;">
            <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                <div class="box-header well" data-original-title="" style="padding:5px;height:auto;cursor:auto;">
                    <div class="btn-group pull-right" style="margin-left:10px;">
                        <a class="btn dropdown-toggle" href="adserveur-ctrl_liste_rt-fli_liste_rt?id=<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
">
                            <i class="icon-arrow-left"></i>
                            <span class="hidden-phone">Retour</span>
                        </a>
                    </div>
                    <span style="vertical-align:middle;"><?php echo $_smarty_tpl->tpl_vars['sTitre']->value;?>
</span>
                    <div style="clear:both;"></div>
                </div>
                <div class="box-content" style="padding-bottom:0;display:inline-block;">
                    <div style="width:auto;margin-bottom:0;display:inline-block;vertical-align:top;">
                        <div class="span12 well" style="padding:10px;margin-bottom:10px;width:auto;display:inline-block;float:none;">
                            Toutes les programmations de visuels pour la FACE <?php echo $_smarty_tpl->tpl_vars['numeroface']->value;?>

                        </div>
                        <?php
$_from = $_smarty_tpl->tpl_vars['aListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_objplanning_0_saved_item = isset($_smarty_tpl->tpl_vars['objplanning']) ? $_smarty_tpl->tpl_vars['objplanning'] : false;
$_smarty_tpl->tpl_vars['objplanning'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['objplanning']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objplanning']->value) {
$_smarty_tpl->tpl_vars['objplanning']->_loop = true;
$__foreach_objplanning_0_saved_local_item = $_smarty_tpl->tpl_vars['objplanning'];
?>
                        <table class="table table-bordered table-striped" style="margin-bottom:10px;width:auto;">
                            <tbody>
                            <tr>
                                <td rowspan="3" style="min-width:40px;">
                                    <aside id="id_affiche_<?php echo $_smarty_tpl->tpl_vars['objplanning']->value['id'];?>
" class="modal_seb" style="text-align:center;">
                                        <div>
                                            <img src="repimages/<?php echo $_smarty_tpl->tpl_vars['objplanning']->value['id_ecran']['value'];?>
" style="height:100%;">
                                            <a href="#close" title="Close"></a>
                                        </div>
                                    </aside>
                                    <img src="repimages/<?php echo $_smarty_tpl->tpl_vars['objplanning']->value['id_ecran']['value'];?>
" style="width:40px;">
                                </td>
                                <th colspan="6" style="vertical-align:middle;text-align:center;">
                                    <strong>Planning de programmation</strong>
                                </th>
                            </tr>
                            <tr>
                                <th style="vertical-align:middle;text-align:center;">Date de début</th>
                                <th style="vertical-align:middle;text-align:center;">Date de fin</th>
                                <th style="vertical-align:middle;text-align:center;">Jours</th>
                                <th style="vertical-align:middle;text-align:center;">Horaires</th>
                                <th style="vertical-align:middle;text-align:center;">Statut</th>
                                <th style="vertical-align:middle;text-align:center;">Actions</th>
                            </tr>
                            <tr>
                                <td rowspan="2"><?php echo $_smarty_tpl->tpl_vars['objplanning']->value['datedebut']['value'];?>
</td>
                                <td rowspan="2"><?php echo $_smarty_tpl->tpl_vars['objplanning']->value['datefin']['value'];?>
</td>
                                <td rowspan="2"><?php echo $_smarty_tpl->tpl_vars['objplanning']->value['periodicitejoursemaine_planning']['value'];?>
</td>
                                <td rowspan="2"><?php echo $_smarty_tpl->tpl_vars['objplanning']->value['typeperiodicite_planning']['value'];?>
</td>
                                <td rowspan="2"><?php echo $_smarty_tpl->tpl_vars['objplanning']->value['desactivation']['value'];?>
</td>
                                <td rowspan="2" class="center" style="padding-right:0;">
                                    <a class="btn btn-info" href="<?php echo $_smarty_tpl->tpl_vars['objplanning']->value['sUrlForm'];?>
&idrt=<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
&numface=<?php echo $_smarty_tpl->tpl_vars['numeroface']->value;?>
&action=form" style="display:inline-block;margin-bottom:5px;margin-right:5px;">
                                        <i class="icon-edit icon-white"></i>
                                        Modifier
                                    </a>
                                    <a class="btn btn-warning" href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim=<?php echo $_smarty_tpl->tpl_vars['id_lienclientrim']->value;?>
&idplanning=<?php echo $_smarty_tpl->tpl_vars['objplanning']->value['id'];?>
&idrt=<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
&numface=<?php echo $_smarty_tpl->tpl_vars['numeroface']->value;?>
&desct=ok" style="display:inline-block;margin-bottom:5px;margin-right:5px;" onclick="return confirm('Voulez-vous vraiment désactiver ce planning ?');">
                                        <i class="icon-ban-circle icon-white"></i>
                                        Désactiver
                                    </a>
                                    <a class="btn btn-danger" href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim=<?php echo $_smarty_tpl->tpl_vars['id_lienclientrim']->value;?>
&idplanning=<?php echo $_smarty_tpl->tpl_vars['objplanning']->value['id'];?>
&idrt=<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
&numface=<?php echo $_smarty_tpl->tpl_vars['numeroface']->value;?>
&bupp=ok" style="display:inline-block;margin-bottom:5px;margin-right:5px;" onclick="return confirm('Voulez-vous vraiment supprimer ce planning ?');">
                                        <i class="icon-remove icon-white"></i>
                                        Supprimer
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="script_image_trais_noir?php?image=<?php echo $_smarty_tpl->tpl_vars['objplanning']->value['id_ecran']['value'];?>
">
                                        <span title=".icon32  .icon-zoomin " class="icon32 icon-zoomin"></span>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <?php
$_smarty_tpl->tpl_vars['objplanning'] = $__foreach_objplanning_0_saved_local_item;
}
if ($__foreach_objplanning_0_saved_item) {
$_smarty_tpl->tpl_vars['objplanning'] = $__foreach_objplanning_0_saved_item;
}
?>

                        <div class="span12 well" style="padding:10px;margin:0 0 10px;width:auto;display:inline-block;float:none;">
                            <a href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim=<?php echo $_smarty_tpl->tpl_vars['id_lienclientrim']->value;?>
&idplanning=<?php echo $_smarty_tpl->tpl_vars['objplanning']->value['id'];?>
&idrt=<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
&numface=<?php echo $_smarty_tpl->tpl_vars['numeroface']->value;?>
&action=form">
                                <button class="btn btn-large btn-success" style="padding:2px;">
                                    <span title=".icon32  .icon-white  .icon-square-plus " class="icon32 icon-white icon-square-plus" style="vertical-align:middle;"></span>
                                    Programmer un autre visuel
                                </button>
                            </a>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
