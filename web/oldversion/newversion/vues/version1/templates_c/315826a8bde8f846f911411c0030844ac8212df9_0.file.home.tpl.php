<?php
/* Smarty version 3.1.29, created on 2018-07-26 10:14:32
  from "/var/www/html/vues/version1/templates/home.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b5982e8a75741_13065843',
  'file_dependency' => 
  array (
    '315826a8bde8f846f911411c0030844ac8212df9' => 
    array (
      0 => '/var/www/html/vues/version1/templates/home.tpl',
      1 => 1532592862,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5982e8a75741_13065843 ($_smarty_tpl) {
?>

<div class="container-fluid" style="padding:0 20px;">
    <div class="row-fluid" style="padding:0;">
        <div id="content" class="span10" style="width:auto;">
            <div class="row-fluid sortable ui-sortable" style="display:inline-block;width:auto;">

                <?php if (!empty($_smarty_tpl->tpl_vars['aTabRim']->value)) {?>
                <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                    <div class="box-header well" data-original-title style="padding:5px;height:auto;cursor:auto;">
                        <span style="vertical-align:middle;">Actuellement vous êtes présent sur les Relais Informations Multimedia (RIM) suivants</span>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="box-content" style="padding-bottom:0;display:inline-block;">
                        <table class="table table-bordered table-striped" style="width:auto;margin-bottom:8px;">
                            <thead>
                            <tr>
                                <th>Ville</th>
                                <th>Mobilier</th>
                                <th>Adresse</th>
                                <th>Visibilité</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <?php
$_from = $_smarty_tpl->tpl_vars['aTabRim']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_objRIN_0_saved_item = isset($_smarty_tpl->tpl_vars['objRIN']) ? $_smarty_tpl->tpl_vars['objRIN'] : false;
$_smarty_tpl->tpl_vars['objRIN'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['objRIN']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objRIN']->value) {
$_smarty_tpl->tpl_vars['objRIN']->_loop = true;
$__foreach_objRIN_0_saved_local_item = $_smarty_tpl->tpl_vars['objRIN'];
?>
                            <tr>
                                <td><?php echo $_smarty_tpl->tpl_vars['objRIN']->value['ville_lerim'];?>
</td>
                                <td>
                                    RIM No <?php echo $_smarty_tpl->tpl_vars['objRIN']->value['numero_mobilier'];?>

                                </td>
                                <td><?php echo $_smarty_tpl->tpl_vars['objRIN']->value['adresse_mobilier'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['objRIN']->value['nom_emplacement'];?>
</td>
                                <td style="padding-bottom:0;">
                                    <a class="btn btn-info" href="" style="display:block;margin-bottom:8px;">
                                        <i class="icon-list icon-white"></i>
                                        Visualiser
                                    </a>
                                </td>
                            </tr>
                            <?php
$_smarty_tpl->tpl_vars['objRIN'] = $__foreach_objRIN_0_saved_local_item;
}
if ($__foreach_objRIN_0_saved_item) {
$_smarty_tpl->tpl_vars['objRIN'] = $__foreach_objRIN_0_saved_item;
}
?>
                        </table>

                    </div>
                </div>
                <?php }?>


                <?php if (!empty($_smarty_tpl->tpl_vars['aTabRimRt']->value)) {?>
                <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                    <div class="box-header well" data-original-title style="padding:5px;height:auto;cursor:auto;">
                        <span style="vertical-align:middle;">Actuellement vous êtes présent sur les réseaux tournants suivants</span>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="box-content" style="padding-bottom:0;display:inline-block;">
                        <table class="table table-bordered table-striped" style="width:auto;margin-bottom:8px;">
                            <thead>
                            <th>Réseau tournant</th>
                            <th>Action</th>
                            </thead>
                            <?php
$_from = $_smarty_tpl->tpl_vars['aTabRimRt']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_objRt_1_saved_item = isset($_smarty_tpl->tpl_vars['objRt']) ? $_smarty_tpl->tpl_vars['objRt'] : false;
$_smarty_tpl->tpl_vars['objRt'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['objRt']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objRt']->value) {
$_smarty_tpl->tpl_vars['objRt']->_loop = true;
$__foreach_objRt_1_saved_local_item = $_smarty_tpl->tpl_vars['objRt'];
?>
                            <tr>
                                <td><?php echo $_smarty_tpl->tpl_vars['objRt']->value['nom_reseau_tournant_pere'];?>
</td>
                                <td style="padding-bottom:0;">
                                    <a class="btn btn-info" href="adserveur-ctrl_liste_rt-fli_liste_rt?id=<?php echo $_smarty_tpl->tpl_vars['objRt']->value['id'];?>
" style="display:block;margin-bottom:8px;">
                                        <i class="icon-list icon-white"></i>
                                        Visualiser
                                    </a>
                                </td>
                            </tr>
                            <?php
$_smarty_tpl->tpl_vars['objRt'] = $__foreach_objRt_1_saved_local_item;
}
if ($__foreach_objRt_1_saved_item) {
$_smarty_tpl->tpl_vars['objRt'] = $__foreach_objRt_1_saved_item;
}
?>
                        </table>

                    </div>
                </div>
                <?php }?>






            </div>
        </div>
    </div>
</div>
<?php }
}
