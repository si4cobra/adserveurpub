<?php
/* Smarty version 3.1.29, created on 2019-10-07 11:50:12
  from "/var/www/html/newversion/vues/version1/templates/home.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5d9b0a54ab0f64_22509293',
  'file_dependency' => 
  array (
    '590b269265addb8642b2d6622ff11d6883abe8d0' => 
    array (
      0 => '/var/www/html/newversion/vues/version1/templates/home.tpl',
      1 => 1570441806,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d9b0a54ab0f64_22509293 ($_smarty_tpl) {
?>

<div class="container-fluid" style="padding:0 20px;">
    <div class="row-fluid" style="padding:0;">
        <div id="content" class="span10" style="width:auto;">
            <div class="row-fluid sortable ui-sortable" style="display:inline-block;width:auto;">

                <?php if (!empty($_smarty_tpl->tpl_vars['aTabRim']->value)) {?>
                <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                    <div class="box-header well" data-original-title style="padding:5px;height:auto;cursor:auto;">
                        <span style="vertical-align:middle;">Actuellement vous êtes présent sur les Relais Informations Multimedia (RIM) suivants</span>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="box-content" style="padding-bottom:0;display:inline-block;">
                        <table class="table table-bordered table-striped" style="width:auto;margin-bottom:8px;">
                            <thead>
                            <tr>
                                <th>Ville</th>
                                <th>Mobilier</th>
                                <th>Adresse</th>
                                <th>Visibilité</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <?php
$_from = $_smarty_tpl->tpl_vars['aTabRim']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_objRIN_0_saved_item = isset($_smarty_tpl->tpl_vars['objRIN']) ? $_smarty_tpl->tpl_vars['objRIN'] : false;
$_smarty_tpl->tpl_vars['objRIN'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['objRIN']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objRIN']->value) {
$_smarty_tpl->tpl_vars['objRIN']->_loop = true;
$__foreach_objRIN_0_saved_local_item = $_smarty_tpl->tpl_vars['objRIN'];
?>
                            <tr>
                                <td><?php echo $_smarty_tpl->tpl_vars['objRIN']->value['ville_lerim'];?>
</td>
                                <td>
                                    RIM No <?php echo $_smarty_tpl->tpl_vars['objRIN']->value['numero_mobilier'];?>

                                </td>
                                <td><?php echo $_smarty_tpl->tpl_vars['objRIN']->value['adresse_mobilier'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['objRIN']->value['nom_emplacement'];?>
</td>
                                <td style="padding-bottom:0;">
                                    <a class="btn btn-info" href="adserveur-ctrl_face-fli_face?id_face=<?php echo $_smarty_tpl->tpl_vars['objRIN']->value['id_face'];?>
" style="display:block;margin-bottom:8px;">
                                        <i class="icon-list icon-white"></i>
                                        Visualiser
                                    </a>
                                </td>
                            </tr>
                            <?php
$_smarty_tpl->tpl_vars['objRIN'] = $__foreach_objRIN_0_saved_local_item;
}
if ($__foreach_objRIN_0_saved_item) {
$_smarty_tpl->tpl_vars['objRIN'] = $__foreach_objRIN_0_saved_item;
}
?>
                        </table>

                    </div>
                </div>
                <?php }?>


                <?php if (!empty($_smarty_tpl->tpl_vars['aTabRimRt']->value)) {?>
                <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                    <div class="box-header well" data-original-title style="padding:5px;height:auto;cursor:auto;">
                        <span style="vertical-align:middle;">Actuellement vous êtes présent sur les réseaux tournants suivants</span>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="box-content" style="padding-bottom:0;display:inline-block;">
                        <table class="table table-bordered table-striped" style="width:auto;margin-bottom:8px;">
                            <thead>
                            <th>Réseau tournant</th>

                            </thead>
                            <?php
$_from = $_smarty_tpl->tpl_vars['aTabRimRt']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_objRt_1_saved_item = isset($_smarty_tpl->tpl_vars['objRt']) ? $_smarty_tpl->tpl_vars['objRt'] : false;
$_smarty_tpl->tpl_vars['objRt'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['objRt']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objRt']->value) {
$_smarty_tpl->tpl_vars['objRt']->_loop = true;
$__foreach_objRt_1_saved_local_item = $_smarty_tpl->tpl_vars['objRt'];
?>
                            <tr>
                                <td><?php echo $_smarty_tpl->tpl_vars['objRt']->value['nom_reseau_tournant_pere'];?>
<br>Actuellement diffusé sur <b><?php echo $_smarty_tpl->tpl_vars['objRt']->value['nom_affiche_client_rim'];?>
</b></td>
                                <!--<td style="padding-bottom:0;">
                                    <a class="btn btn-info" href="adserveur-ctrl_liste_rt-fli_liste_rt?id=<?php echo $_smarty_tpl->tpl_vars['objRt']->value['id'];?>
" style="display:block;margin-bottom:8px;">
                                        <i class="icon-list icon-white"></i>
                                        Visualiser
                                    </a>
                                </td>-->
                            </tr>
                            <?php
$_smarty_tpl->tpl_vars['objRt'] = $__foreach_objRt_1_saved_local_item;
}
if ($__foreach_objRt_1_saved_item) {
$_smarty_tpl->tpl_vars['objRt'] = $__foreach_objRt_1_saved_item;
}
?>
                            <tr>
                                <td style="padding-bottom:0;">
                                    <a class="btn btn-info" href="adserveur-ctrl_liste_rt-fli_liste_rt?id=<?php echo $_smarty_tpl->tpl_vars['aTabRimRt']->value[0]['id'];?>
" style="display:block;margin-bottom:8px;">
                                        <i class="icon-list icon-white"></i>
                                        Visualiser la liste des faces sur les réseaux tournants
                                    </a>
                                </td>

                            </tr>
                        </table>

                    </div>
                </div>
                <?php }?>






            </div>
        </div>
    </div>
</div>
<?php }
}
