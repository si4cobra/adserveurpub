<?php
/* Smarty version 3.1.29, created on 2018-07-26 14:07:24
  from "/var/www/html/vues/version1/templates/formulaire_adserver.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b59b97c691da3_59087350',
  'file_dependency' => 
  array (
    '2a1db2bd0a4e21330c81e1544ad454af11a6f781' => 
    array (
      0 => '/var/www/html/vues/version1/templates/formulaire_adserver.tpl',
      1 => 1532599732,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b59b97c691da3_59087350 ($_smarty_tpl) {
?>

<div class="container-fluid" style="padding:0 20px;">
    <div class="row-fluid" style="padding:0;">
        <div id="content" class="span10" style="width:auto;">
            <div class="row-fluid sortable ui-sortable" style="display:inline-block;width:auto;">
                <?php if (isset($_smarty_tpl->tpl_vars['bMessageErreurForm']->value) && $_smarty_tpl->tpl_vars['bMessageErreurForm']->value) {?>
                <div class="alert alert-error" style="display:inline-block;margin-bottom:8px;">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>Echec!</strong><br>
                    <?php if (isset($_smarty_tpl->tpl_vars['sMessageErreurForm']->value)) {
echo $_smarty_tpl->tpl_vars['sMessageErreurForm']->value;
}?>
                </div>
                <?php }?>


                <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                    <div class="box-header well" data-original-title="" style="padding:5px;height:auto;cursor:auto;">
                        <div class="btn-group pull-right" style="margin-left:10px;">
                            <a class="btn dropdown-toggle" href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim=<?php echo $_smarty_tpl->tpl_vars['id_lienclientrim']->value;?>
&idrt=<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
&numface=<?php echo $_smarty_tpl->tpl_vars['numface']->value;?>
">
                                <i class="icon-arrow-left"></i>
                                <span class="hidden-phone">Retour</span>
                            </a>
                        </div>
                        <span style="vertical-align:middle;"><?php echo $_smarty_tpl->tpl_vars['sTitre']->value;?>
</span>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="box-content" style="padding-bottom:0;display:inline-block;">
                        <div class="box" style="width:auto;margin-top:0;margin-bottom:8px;">
                            <div class="box-header well" data-original-title="" style="padding:5px;height:auto;cursor:auto;">
                                <span style="vertical-align:middle;">Nouvelle programmation de visuels pour la FACE 1</span>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="box-content" style="padding-bottom:0;display:inline-block;">
                                <form action="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim=<?php echo $_smarty_tpl->tpl_vars['id_lienclientrim']->value;?>
&idrt=<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
&numface=<?php echo $_smarty_tpl->tpl_vars['numface']->value;?>
&action=form" method="POST" class="form-horizontal" style="margin-bottom:0;" enctype="multipart/form-data">
                                    <input type="hidden" name="id_lienclientrim" value="<?php echo $_smarty_tpl->tpl_vars['id_lienclientrim']->value;?>
">
                                    <input type="hidden" name="idrt" value="<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
">
                                    <input type="hidden" name="numface" value="<?php echo $_smarty_tpl->tpl_vars['numface']->value;?>
">
                                    <fieldset style="text-align:center;">
                                        <div style="vertical-align:top;text-align:left;">
                                            <legend style="margin-bottom:10px;">Sélectionner un visuel:</legend>
                                            <div style="margin:10px 20px 30px 0">
                                                <label class="control-label" for="fichier_ecran" style="width:auto;margin-left:10px;">Visuel:</label>
                                                <div class="controls" style="margin-left:10px;">
                                                    <div class="uploader" id="uniform-fichier_ecran"><input class="input-file uniform_on" id="fichier_ecran" name="fichier_ecran" type="file" size="25" style="opacity:0;"></div>
                                                    <p>format fichier autorisé (png, jpg, swf, gif), dimensions autorisées (720*2560), poids fichier autorisé (878.90625 Ko)</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="width:auto;text-align:left;">
                                            <legend style="margin-bottom:10px;line-height:initial;">Plannifier sa diffusion:</legend>
                                            <div style="width:auto;text-align:left;margin-left:30px;">
                                                <legend style="margin-bottom:10px;line-height:initial;">Période d'affichage</legend>
                                                <div style="margin:10px 0;">
                                                    <div style="display:inline-block;">
                                                        <label class="control-label" for="date_debut" style="width:auto;margin-left:10px;">Date de début:</label>
                                                        <div class="controls" style="margin-left:105px;">
                                                            <input type="date"  id="date_debut" name="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[1]['nom_variable'];?>
" style="margin-left:5px;width:125px;" value="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[1]['valeur_variable'];?>
">
                                                        </div>
                                                    </div>
                                                    <div style="display:inline-block;">
                                                        <label class="control-label" for="date_fin" style="width:auto;margin-left:10px;">Date de fin:</label>
                                                        <div class="controls" style="margin-left:85px;">
                                                            <input type="date"  id="date_fin" name="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[2]['nom_variable'];?>
" style="margin-left:5px;width:125px;" value="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[2]['valeur_variable'];?>
">
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="btn btn-warning" id="bouton_mode_expert_planning">
                                                    <i class="icon-edit icon-white"></i>
                                                    <span id="label_bouton_mode_expert_planning">
												Affiner la programmation (mode expert)
											</span>
                                                </a>
                                                <div id="mode_expert_planning" style="display:none;">
                                                    <legend style="margin-bottom:10px;line-height:initial;">Jours d'affichage</legend>
                                                    <div style="margin:10px 0">
                                                        <div style="margin:10px;display:inline;">
                                                            <div style="display:inline-block;margin-right:20px;">
                                                                <input type="checkbox" name="periodicite" checked="">
                                                                <label style="display:inline;">
                                                                    Tous les jours
                                                                </label>
                                                            </div>
                                                            <div style="display:inline-block;">
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" name="periodicite_jour[]" value="1" checked="">
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Lundi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" name="periodicite_jour[]" value="2" checked="">
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Mardi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" name="periodicite_jour[]" value="3" checked="">
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Mercredi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" name="periodicite_jour[]" value="4" checked="">
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Jeudi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" name="periodicite_jour[]" value="5" checked="">
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Vendredi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" name="periodicite_jour[]" value="6" checked="">
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Samedi
                                                                    </label>
                                                                </div>
                                                                <div style="display:inline-block;margin-left:10px;">
                                                                    <input type="checkbox" name="periodicite_jour[]" value="0" checked="">
                                                                    <label style="display:inline;margin-left:3px;">
                                                                        Dimanche
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <legend style="margin-bottom:10px;line-height:initial;">Horaire d'affichage</legend>
                                                    <div style="margin:10px 0;">
                                                        <div style="display:inline-block;vertical-align:top;margin-bottom:10px;margin-right:20px;">
                                                            <label style="display:inline;">Toute la journée:</label>
                                                            <input type="checkbox" id="toute_la_journee" value="1" checked="">
                                                        </div>
                                                        <div style="display:inline-block;">
                                                            <div style="display:inline-block;margin-right:10px;">
                                                                <label class="control-label" for="heure_debut" style="width:auto;">Heure de début:</label>
                                                                <div class="controls" style="margin-left:105px;">
                                                                    <input type="text" class="input-xlarge datepicker_hour hasTimepicker" id="heure_debut" name="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[4]['nom_variable'];?>
" style="margin-left:5px;width:80px;" value="<?php if ($_smarty_tpl->tpl_vars['aForm']->value[4]['valeur_variable'] == '') {?>00:00:00<?php } else {
echo $_smarty_tpl->tpl_vars['aForm']->value[4]['valeur_variable'];
}?>">
                                                                </div>
                                                            </div>
                                                            <div style="display:inline-block;">
                                                                <label class="control-label" for="heure_fin" style="width:auto;">Heure de fin:</label>
                                                                <div class="controls" style="margin-left:85px;">
                                                                    <input type="text" class="input-xlarge datepicker_hour hasTimepicker" id="heure_fin" name="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[5]['nom_variable'];?>
" style="margin-left:5px;width:80px;" value="<?php if ($_smarty_tpl->tpl_vars['aForm']->value[5]['valeur_variable'] == '') {?>23:59:59<?php } else {
echo $_smarty_tpl->tpl_vars['aForm']->value[5]['valeur_variable'];
}?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <legend style="margin-bottom:10px;line-height:initial;">Activation du planning</legend>
                                                    <div style="margin:10px 0">
                                                        <label class="control-label" for="desactivation" style="width:auto;margin-left:10px;">Activer:</label>
                                                        <div class="controls" style="margin-left:70px;">
                                                            <input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['aForm']->value[3]['nom_variable'];?>
" value="N"  <?php if ($_smarty_tpl->tpl_vars['aForm']->value[3]['valeur_variable'] == 'N') {
} else { ?>checked="checked"<?php }?>>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions" style="display:inline-block;padding:10px">
                                            <button type="submit" class="btn btn-primary" name="valider_edition_planning" value="1">Valider</button>
                                            <a class="btn dropdown-toggle" href="adserveur-ctrl_programmation-fli_programmation?id_lienclientrim=<?php echo $_smarty_tpl->tpl_vars['id_lienclientrim']->value;?>
&idrt=<?php echo $_smarty_tpl->tpl_vars['idrt']->value;?>
&numface=<?php echo $_smarty_tpl->tpl_vars['numface']->value;?>
">
                                                Annuler
                                            </a>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
