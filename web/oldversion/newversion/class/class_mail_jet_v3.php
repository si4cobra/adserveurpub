<?php

/**
 * @name class_form_list classe commnique avec le service mailjet
 * @author Gnakouri Danon
 * @version 1.0
 */
class class_mail_jet_v3
{

    /**
     * @var Objgenerique objet pour les requete sql
     */
    private $Objgenerique;

    /**
     * @var ObjetMAiljet l'instance de mail jet
     */
    private $ObjetMAiljet;

    /**
     * @method class_mail_jet constructeur de la classe permet d'initialiser des constantes
     * @author Gnakouri Danon
     * @version 1
     */
    public function class_mail_jet_v3()
    {

       /* $this->Objgenerique = new class_generique('', '', '', '');
        $this->Objgenerique->verificationConnexion(0, '');
       */

        include_once 'Mailjet_v3.php';
        $this->ObjetMAiljet = new Mailjet_v3();
    }


    /**
     * @method Test valider connexion au service mailjet
     * @author Gnakouri Danon
     * @version 1
     */
    public function test_connexion()
    {

        $objtest = $this->ObjetMAiljet->userInfos();
    }

    /**
     * @method fonction creation d'un campagne mailjet
     * @author Gnakouri Danon
     * @version 1
     */
    public function creation_campagne_mailjet($idusertemplate)
    {
        //recupération des inforamtions sur le  template
        $sResquete = "SELECT objet_email_template,concat(nom_user,' ',prenom_user) AS nom,email_user,routeurmail_user_template.idcampmailjet_user_tempalte FROM crm_emailing_template
		INNER JOIN routeurmail_user_template ON routeurmail_user_template.id_template = crm_emailing_template.id_template
		AND supplogique_user_template='N'
		INNER JOIN infobysms_lienuserprogramme ON infobysms_lienuserprogramme.id_lienuserprogramme = routeurmail_user_template.id_lienuserprogramme
		AND supplogique_lienuserprogrammme='N'
		INNER JOIN  infobysms_user ON  infobysms_user.id_user = infobysms_lienuserprogramme.id_user
		WHERE routeurmail_user_template.id_user_template='" . $idusertemplate . "'";

        //echo $sResquete;

        $aTableauResultat = $this->Objgenerique->renvoi_info_requete($sResquete);

        //echo $sResquete."\n";

        if( !empty($aTableauResultat) ) {
            if( $aTableauResultat[0]['idcampmailjet_user_tempalte'] == "" ) {
                $params = array(
                    'method' => 'POST',
                    'subject' => $aTableauResultat[0]['objet_email_template'] . ' ' . $aTableauResultat[0]['nom'],
                    'lang' => 'fr',
                    'from' => addslashes(str_replace(" ", "", $aTableauResultat[0]['email_user'])),
                    'from_name' => $aTableauResultat[0]['nom'],
                    'footer' => 'default'
                );
                $response = $this->ObjetMAiljet->messageCreateCampaign($params);
                $id = $response->campaign->id;
                $sRequeteUpdateIdCampaign = "UPDATE routeurmail_user_template SET idcampmailjet_user_tempalte='" . $id . "' WHERE id_user_template='" . $idusertemplate . "'";
                $this->Objgenerique->executionRequeteId($sRequeteUpdateIdCampaign);
            }
        }
    }

    public function set_template_mailjet($idusertemplate)
    {
        $sRequete = "SELECT crm_emailing_module.contenu_module,crm_emailing_template_module.position_template_module,routeurmail_user_template.idcampmailjet_user_tempalte,crm_emailing_template.format_text_template FROM crm_emailing_module
		INNER JOIN crm_emailing_template_module ON crm_emailing_template_module.id_module=crm_emailing_module.id_module AND crm_emailing_template_module.supplogique_template_module='N'
		INNER JOIN crm_emailing_template ON crm_emailing_template.id_template=crm_emailing_template_module.id_template AND crm_emailing_template.supplogique_template='N'
		INNER JOIN routeurmail_user_template ON routeurmail_user_template.id_template=crm_emailing_template.id_template AND supplogique_user_template='N'
		WHERE routeurmail_user_template.id_user_template='" . $idusertemplate . "' AND crm_emailing_module.supplogique_module='N' ORDER BY crm_emailing_template_module.position_template_module ASC";

        $aTableauResultat = $this->Objgenerique->renvoi_info_requete($sRequete);
        $htmlMailjet = "";

        if( !empty($aTableauResultat) ) {
            $idCampaign = $aTableauResultat[0]['idcampmailjet_user_tempalte'];
            $formatText = $aTableauResultat[0]['format_text_template'];
            foreach( $aTableauResultat as $aTableauResultatParc ) {
                $htmlMailjet .= $aTableauResultatParc['contenu_module'];
            }
        }

        if( $htmlMailjet != "" ) {
            $params = array(
                'method' => 'POST',
                'id' => $idCampaign,
                'html' => $htmlMailjet . '<br><br><a href="[[UNSUB_LINK_FR]]">Se désabonner</a>',
                'text' => $formatText
            );
            $this->ObjetMAiljet->messageSetHtmlCampaign($params);
        }
    }

    public function verif_sender($sEmail)
    {
        $params = array(
            'method' => 'GET',
            'email' => addslashes(str_replace(" ", "", $sEmail))
        );
        $reponse = $this->ObjetMAiljet->sender($params);
        return $reponse;
    }

    public function add_sender($sEmail)
    {
        $params = array(
            'method' => 'POST',
            'email' => addslashes(str_replace(" ", "", $sEmail))
        );
        $reponse = $this->ObjetMAiljet->sender($params);
        return $reponse;
    }

    public function send_mail($sMailComm, $sMailTo, $sSubject, $sMsg, $sAttachments = '', $sUrl = '',$name='')
    {

        $aNewTab = array();

        if( !empty($sAttachments) ) {

            if( empty($sUrl) ) {
                //$sUrl = "http://devcrm.immo-immo.info/dossier_emailing/";
                $sUrl = "http://crm.immo-immo.info/dossier_emailing/";
            }

            $aTabAttachments = explode(";", $sAttachments);

            foreach( $aTabAttachments as $aTabAttachmentsParc ) {
                $aNewTab[] = '@' . $sUrl . $aTabAttachmentsParc;
            }

        }
        //echo"<pre>";print_r($aNewTab);echo"</pre>";
        //exit;
        $aTab =array();
        $aTab[]=array('Email' => "si2@cobranaja.com");
        $params = array(
            "method" => "POST",
            "from" => $sMailComm,
            "to" => $sMailTo,
            'FromName' =>$name,
            "subject" => $sSubject,
            "html" => $sMsg,
            "attachment" => $aNewTab
        );

        //echo"<pre>";print_r($params);echo"</pre>";

       return $this->ObjetMAiljet->sendEmail($params);
    }

}

?>