<?php

/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 05/03/2018
 * Time: 16:50
 */
require_once('class/html2pdf/html2pdf.class.php');
class class_pdf
{

    public static function  generate_pdf($atab,$atabentete,$titre){

        $sContenu="";
        $sContenu = "<page backbottom='30px' footer='page;date;'>
	    <page_footer></page_footer>
	     <style>
                  .tabFac
                    {
                    height: 2%;
                    text-align: center;
                    font-size: 10px;
                    }
                    
                    .tabFac2
                    {
                    height: 2%;
                    text-align: center;
                    }       
                    
                </style>
	    ";
        //echo"<pre>";print_r($atabentete);echo"</pre>";
        //echo"<pre>";print_r($atab);echo"</pre>";
        //exit();
        $sContenu .= "<h2 align='center'>".$titre." </h2>";
        if(!empty($atabentete)) {
            $sContenu.="<table  style='font-size: 10px;' border='1' style='width:100%;' cellpadding='0' cellspacing='0'>";
            $sContenu.="<tr>";
            foreach( $atabentete as $valeurEntete ) {

                if($valeurEntete['objElement']['aff_pdf']=="ok")
                    $sContenu.="<td style='".$valeurEntete['objElement']['style_pdf']."'>".$valeurEntete['objElement']['text_label']."</td>";

            }
            $sContenu.="</tr>";
        }

        if(!empty($atab)){
            foreach($atab as $valeur){
                $sContenu.="<tr>";
                foreach( $atabentete as $valeurEntete ) {
                    //$sgros = "";

                    if( $valeurEntete['objElement']['aff_pdf'] == "ok" ) {
                        if( $valeurEntete['objElement']['alias_champ'] != "" )
                            $valeurEntete['objElement']['nom_variable'] = $valeurEntete['objElement']['alias_champ'];
                          $sContenu .= "<td style='" . $valeurEntete['objElement']['style_pdf'] . "' >" . str_replace("<br>", "<br/>", $valeur[$valeurEntete['objElement']['nom_variable']]['value']) . "</td>";
                    }
                }
                $sContenu.="</tr>";
            }
        }

        $sContenu.="</table></page>";
        $html2pdf = new HTML2PDF('L', 'A4', 'fr');
        $html2pdf->pdf->setTitle( 'Etat pdf' );
        $html2pdf->WriteHTML($sContenu);
        $html2pdf->Output('etat_pdf.pdf' );
        //echo $sContenu;
        exit();
        
    }



    public static function  generate_pdf_caro($atab,$atabentete){


        //echo"<pre>";print_r($_GET);echo"</pre>";
        //exit();

        $sContenu="";
        $aTableauListeRanger=array();

        foreach($atab as $Valeur){
            if($Valeur['guid_user_affect_filtre']['value']==""){
                $Valeur['guid_user_affect_filtre']['value']="personne";
                $Valeur['guid_user_affecte']['value']="Aucune Affectation";
            }

            $aTableauListeRanger[$Valeur['guid_user_affect_filtre']['value']][]=$Valeur;
        }
        //echo"<pre>";print_r($aTableauListeRanger);echo"</pre>";
        $sTitrePeriode =" ";
        //exit();
        if(isset($_GET['rech_dateprevisionnelle_action'])){
            $aTabdate = $_GET['rech_dateprevisionnelle_action'];

            if($aTabdate[0]!="" and $aTabdate[1]!=""){
                $sTitrePeriode= " Dossier à Traiter du ".class_helper::renvoi_date($aTabdate[0],"fr")." au ".class_helper::renvoi_date($aTabdate[1],"fr");
            }
        }

        if(!empty($aTableauListeRanger)) {
            foreach($aTableauListeRanger as  $itemlist) {
                $sContenu.= "<page backbottom='30px' footer='page;date;'>
                <page_footer></page_footer>
                 <style>
                          .tabFac
                            {
                            height: 2%;
                            text-align: center;
                            font-size: 10px;
                            }
                            
                            .tabFac2
                            {
                            height: 2%;
                            text-align: center;
                            }       
                            
                        </style>
                ";
                //echo"<pre>";print_r($atabentete);echo"</pre>";
                //echo"<pre>";print_r($atab);echo"</pre>";
                //exit();


                $sContenu .= "<h2 align='center'>".$itemlist[0]['guid_user_affecte']['value']." ".$sTitrePeriode." </h2>";
                if( !empty($atabentete) ) {
                    $sContenu .= "<table  style='font-size: 10px;' border='1' style='width:100%;' cellpadding='0' cellspacing='0'>";
                    $sContenu .= "<tr>";
                    foreach( $atabentete as $valeurEntete ) {

                        if( $valeurEntete['objElement']['aff_pdf'] == "ok" )
                            $sContenu .= "<td style='" . $valeurEntete['objElement']['style_pdf'] . "'>" . $valeurEntete['objElement']['text_label'] . "</td>";

                    }
                    $sContenu .= "</tr>";
                }

                if( !empty($itemlist) ) {
                    foreach( $itemlist as $valeur ) {
                        $sContenu .= "<tr>";
                        foreach( $atabentete as $valeurEntete ) {
                            //$sgros = "";

                            if( $valeurEntete['objElement']['aff_pdf'] == "ok" ) {
                                if( $valeurEntete['objElement']['alias_champ'] != "" )
                                    $valeurEntete['objElement']['nom_variable'] = $valeurEntete['objElement']['alias_champ'];
                                $sContenu .= "<td style='" . $valeurEntete['objElement']['style_pdf'] . "' >" . str_replace("<br>", "<br/>", $valeur[$valeurEntete['objElement']['nom_variable']]['value']) . "</td>";
                            }
                        }
                        $sContenu .= "</tr>";
                    }
                }

                $sContenu .= "</table>";
                $sContenu .= "</page>";
            }
        }else{
            $sContenu.= "<page backbottom='30px' footer='page;date;'>
                <page_footer></page_footer>
                Aucun element disponible
                </page>";
        }

        $html2pdf = new HTML2PDF('L', 'A4', 'fr');
        $html2pdf->pdf->setTitle( 'test' );
        $html2pdf->WriteHTML($sContenu);
        $html2pdf->Output('Dossier_planning.pdf' );
        //echo $sContenu;
        exit();

    }


}