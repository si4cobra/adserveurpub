<?php

/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 04/04/2018
 * Time: 18:11
 */
class mod_adserver  extends mod_form_list
{

    public function renvoi_emplacement_par_face($id_client) {
       $sRequete="SELECT 
				rlc.id_lienclientrim AS id,
				e.nom_emplacment AS nom_emplacement,
				rg.id_rim AS id_face,
				nom_lerim,
				codepostal_lerim,
				ville_lerim,
				numero_mobilier,
				adresse_mobilier
			FROM rim_lienclientrim rlc
			JOIN rim_gestionrim rg ON (rg.supplogique_rim = 'N' AND rg.id_rim = rlc.id_rim)
			JOIN  rim_lienrimface on  rim_lienrimface.id_rim = rlc.id_rim and supplogique_lienrimface='N'
			JOIN  rim_lerim on  rim_lerim.id_lerim = rim_lienrimface.id_lerim
			JOIN  ric_mobilier on  ric_mobilier.id_mobilier=rim_lerim.id_mobilier
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.accesdirect_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			LEFT JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			LEFT JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement)
			LEFT JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			WHERE rlc.supplogique_lienclientrim = 'N'
			AND lrt.id_reseau_tournant IS NULL
			ORDER BY rlc.id_lienclientrim";



         //echo $sRequete."<br>";
        $aTableauretour=$this->renvoi_info_requete($sRequete);
        return $aTableauretour;
    }




    function renvoi_emplacement_par_reseau_tournant($id_client) {

        $aTableauRetourListe=array();
        $sRequete="SELECT  id_lienclientrim,
				lrt.id_reseau_tournant_pere AS id,
				lrt.nom_reseau_tournant AS nom,
				e.nom_emplacment AS nom_emplacement,
				e.id_emplacement AS id_emplacement,
				lrt.id_reseau_tournant AS id_reseau_tournant,
				nom_reseau_tournant_pere,
				lrt.id_reseau_tournant_pere,
				cp.id_contratproduit,
				lre.id_reseau_tournant
			FROM rim_lienclientrim rlc
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.accesdirect_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement)
			JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			JOIN le_reseau_tournant_pere lrtpere ON lrtpere.id_reseau_tournant_pere = lrt.id_reseau_tournant_pere
			WHERE rlc.supplogique_lienclientrim = 'N'
			ORDER BY rlc.id_lienclientrim";


        //echo $sRequete."<br>";
        $aTableauretour=$this->renvoi_info_requete($sRequete);
        
        //echo"<pre>";print_r($aTableauretour);echo"</pre>";


        if(!empty($aTableauretour)){
            $i=0;
            foreach($aTableauretour as $valeur){

                $aTableauEmplacement = $this->renvoi_position_rt_client($valeur['id_reseau_tournant'],date('d/m/Y'),$valeur['id_lienclientrim']);
                
                //echo"<pre>";print_r($aTableauEmplacement);echo"</pre>";

                if(!isset($aTableauRetourListe[$valeur['id_reseau_tournant_pere']]['nom_reseau_tournant_pere'])){
                    $aTableauRetourListe[$valeur['id_reseau_tournant_pere']]['nom_reseau_tournant_pere']=$valeur['nom_reseau_tournant_pere'];
                }

                /*$aTableauRetourListe[$valeur['id_reseau_tournant_pere']]['id']= $valeur['id_reseau_tournant'];
                $aTableauRetourListe[$valeur['id_reseau_tournant_pere']]['id_lienclientrim']= $valeur['id_lienclientrim'];
                $aTableauRetourListe[$valeur['id_reseau_tournant_pere']]['nom_reseau_tournant_pere']= $aTableauRetourListe[$valeur['id_reseau_tournant_pere']]['nom_reseau_tournant_pere']."<br>Actuellement diffusé sur <b>".$aTableauEmplacement['nom_emplacment_actuelle']."</b>";
                $aTableauRetourListe[$valeur['id_reseau_tournant_pere']]['id_contratproduit']= $valeur['id_contratproduit'];
                $aTableauRetourListe[$valeur['id_reseau_tournant_pere']]['id_reseau_tournant']= $valeur['id_reseau_tournant'];*/

                $aTableauretour[$i]['nom_affiche_client_rim'] = $aTableauEmplacement['nom_affiche_client_rim'];
                $i++;

            }
        }

        //echo"<pre>";print_r($aTableauRetourListe);echo"</pre>";

        return $aTableauretour;
    }

    public function renvoi_mobilier_face($id_face)
    {

    }

    public function renvoi_nom_client($id_client){
        $sRquete_nom="Select nom_client FROM client where accesdirect_client = '".$id_client."'";
        $aTableauInfo =  $this->renvoi_info_requete($sRquete_nom);

        //echo $sRquete_nom."<br>";

        if(!empty($aTableauInfo)){
            return $aTableauInfo[0]['nom_client'];
        }else{
            return "Aucun client trouvé";
        }
    }

    function renvoi_nom_rt($idrt){
        $sRequete_info_rim="SELECT nom_reseau_tournant_pere as nom FROM   le_reseau_tournant_pere WHERE 	id_reseau_tournant_pere='".$idrt."'";
        $aTableauInfoRim =$this->renvoi_info_requete($sRequete_info_rim);

        //echo"<pre>";print_r($sRequete_info_rim);echo"</pre>";
        
        if($aTableauInfoRim){
            //echo"<pre>";print_r($aTableauInfoRim[0]['nom']);echo"</pre>";
            return $aTableauInfoRim[0]['nom'];
        }else{
            $sRequete_Face = "Select nom_rim as nom 
                              from rim_gestionrim 
                              inner join rim_lienclientrim on rim_lienclientrim.id_rim = rim_gestionrim.id_rim 
                              where id_lienclientrim = $idrt ";

            //echo"<pre>";print_r($sRequete_Face);echo"</pre>";
            $aTableauInfoRim = $this->renvoi_info_requete($sRequete_Face);
            if($aTableauInfoRim)
            {
                //echo"<pre>";print_r($aTableauInfoRim);echo"</pre>";
              return $aTableauInfoRim[0]['nom'];
            }
           


        }
    }


    function get_InfoTypeFichier($typeFichier)
    {

        $sRequeteInfoTypeFichier = "Select id_typefichier from typefichier where supplogique_typefichier = 'N' and extension_typefichier = '$typeFichier' ";
        
        //echo"<pre>";print_r($sRequeteInfoTypeFichier);echo"</pre>";

        $aTabInfoTypeFichier = $this->renvoi_info_requete($sRequeteInfoTypeFichier);

        return $aTabInfoTypeFichier;

    }

    function get_EcranByIdPlanning($id_planning)
    {
        $sRequeteEcranByIdPlanning = "Select id_ecran from rim_planning where id_planning = $id_planning";

        $aTabEcranByIdPLanning = $this->renvoi_info_requete($sRequeteEcranByIdPlanning);

        return $aTabEcranByIdPLanning;

    }

    function renvoi_image_planning($id_planning)
    {
        $sRequeteImage = "Select cheminfichier from rim_ecran inner join  ";
    }

    function get_InfoClientFromIdLienClientRim($id_lienclientrim)
    {
        $sRequeteInfoClientFromIdLienClientRim = "Select id_client from rim_lienclientrim where id_lienclientrim = $id_lienclientrim ";

        $aTabClientFromIdLienClientRim = $this->renvoi_info_requete($sRequeteInfoClientFromIdLienClientRim);

        return $aTabClientFromIdLienClientRim;

    }

    public function desactiver_planning($id_planning,$value)
    {
        $sRequeteDesactiverPlanning = "update rim_planning set desactivation_planning = '$value' where id_planning = $id_planning ";

        //echo"<pre>";print_r($sRequeteDesactiverPlanning);echo"</pre>";

        $this->execute_requete($sRequeteDesactiverPlanning);

    }

    function update_rimplanning($id_ecran,$id_planning,$ejour,$heuredebut,$heurefin)
    {
        // rim_planning
        $sRequeteUpdateEcranOnRimPlanning = "Update rim_planning set id_ecran = '$id_ecran', periodicitejoursemaine_planning = '$ejour',heuredebut_planning = '$heuredebut:00:00',heurefin_planning='$heurefin:00:00' where id_planning = '$id_planning' ";

        //echo"<pre>";print_r($sRequeteUpdateEcranOnRimPlanning);echo"</pre>";

        $this->execute_requete($sRequeteUpdateEcranOnRimPlanning);


    }

    function add_rimecran($aTabInfoEcran)
    {

        //echo"<pre>";print_r($aTabInfoEcran);echo"</pre>";

        $sRequeteAddRimEcran = "Insert into rim_ecran (id_typefichier,id_client,cheminfichier_ecran) 
                                VALUE ('".$aTabInfoEcran['id_typefichier']."','".$aTabInfoEcran['id_client']."','".$aTabInfoEcran['cheminfichier_ecran']."') ";


        
        $id_ecran = $this->executionRequeteId($sRequeteAddRimEcran);

        return $id_ecran;

    }

    /**
     * Renvoi reseau tournant
     * @param $id_client
     * @param $id_reseau_tournant
     * @return array
     */
    function renvoi_emplacements_param_reseau_tournant($id_client, $id_reseau_tournant) {

       $aTableauRetourListe=array();
        $sRequete="	SELECT 
				rlc.id_lienclientrim AS id,
				lrtp.id_reseau_tournant_pere AS id_reseau_tournant,
				lrt.id_reseau_tournant AS id_reseau_tournant_fils,
				cp.id_contratproduit,
				e.nom_emplacment AS nom_emplacement,
				 co.id_contrat,
				 nom_reseau_tournant_pere,
				 nom_reseau_tournant,
				 cp.id_contratproduit,
				 supplogique_lienclientrim,
				 cp.image_contratproduit
			FROM rim_lienclientrim rlc
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.accesdirect_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement )
			JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			JOIN le_reseau_tournant_pere lrtp ON (lrtp.supplogique_le_reseau_tournant_pere = 'N' 
			AND lrtp.id_reseau_tournant_pere = '".$id_reseau_tournant."' AND lrtp.id_reseau_tournant_pere = lrt.id_reseau_tournant_pere)
			WHERE rlc.supplogique_lienclientrim = 'N' group by rlc.id_lienclientrim
			ORDER BY rlc.id_lienclientrim ";

        //echo $sRequete."<br>";
        
        $aTableauretour=$this->renvoi_info_requete($sRequete);
        
        //echo"<pre>";print_r($aTableauretour);echo"</pre>";
        $u=0;
        $i=1;
        if(!empty($aTableauretour)){
            foreach($aTableauretour as $valeur){
                $aTableauRetourListe[$u]['id']= $valeur['id'];
                $aTableauRetourListe[$u]['numero_de_face']= $i;
                $aTableauRetourListe[$u]['nom_reseau_tournant_pere']= $valeur['nom_reseau_tournant_pere'];
                $aTableauRetourListe[$u]['nom_reseau_tournant_pere']= $valeur['nom_reseau_tournant_pere'];
                $aTableauRetourListe[$u]['id_reseau_tournant']= $valeur['id_reseau_tournant'];
                $aTableauRetourListe[$u]['nom_emplacement']= $valeur['nom_emplacement'];
                $aTableauRetourListe[$u]['id_reseau_tournant_fils']= $valeur['id_reseau_tournant_fils'];
                $aTableauRetourListe[$u]['id_reseau_tournant_fils']= $valeur['id_reseau_tournant_fils'];
                $aTableauRetourListe[$u]['nom_reseau_tournant']= $valeur['nom_reseau_tournant'];
                $aTableauRetourListe[$u]['id_contratproduit']= $valeur['id_contratproduit'];
                $aTableauRetourListe[$u]['supplogique_lienclientrim']= $valeur['supplogique_lienclientrim'];
                $aTableauRetourListe[$u]['image_contratproduit']= $valeur['image_contratproduit'];
                $aTableauRetourListe[$u]['affiche_tampon']['extension']= "";
                $aTableauRetourListe[$u]['affiche_tampon']['image']=  "repimages/".$valeur['image_contratproduit'];
                $aTableauRetourListe[$u]['affiche_tampon']['id']=  $valeur['id'];
                $aTableauRetourListe[$u]['planning_actuel']['extension']= "";
                $aTableauRetourListe[$u]['planning_actuel']['image']=  "repimages/".$valeur['image_contratproduit'];
                $aTableauRetourListe[$u]['planning_actuel']['id']=  $valeur['id'];
                $u++;
                $i++;
            }
        }

        //echo"<pre>";print_r($aTableauRetourListe);echo"</pre>";
        return $aTableauRetourListe;
    }


    /**
     * methode renvoi nombre de jour entre deux date
     * @author Danon Gnakouri
     * @param login login de connexion
     * @param motdepass mot de pass
     * @param $bdd mot de passe
     * @since 3.2
     * @return la connexion
     */
    public function nombrejourdate($datedebut,$datefin){

        $aTableaudebut = explode("/",$datedebut);
        $aTableaufin =explode("/",$datefin);

        $mktimedebut = mktime(0,0,0,$aTableaudebut[1],$aTableaudebut[0],$aTableaudebut[2]);
        $mktimefin = mktime(0,0,0,$aTableaufin[1],$aTableaufin[0],$aTableaufin[2]);

        $nbrjour = ($mktimefin-$mktimedebut)/(24*60*60);

        return $nbrjour;
    }

    /**
     * methode renvoi position dans le tableau
     * @author Danon Gnakouri
     * @param login login de connexion
     * @param motdepass mot de pass
     *@param $bdd mot de passe
     * @since 3.2
     * @return la connexion
     */
    public function position_tableau($iteration,$itailletableau,$ipostionorigne){

        return ($iteration%$itailletableau + $ipostionorigne)%$itailletableau;

    }

    public function renvoi_face($id_client, $id_face) {

        $sql ="
			SELECT DISTINCT
				rlc.id_lienclientrim AS id,
				rlc.id_contratproduit AS nom,
				e.nom_emplacment AS nom_emplacement,
				e.numero_emplacement AS numero_emplacement,
				rg.id_rim AS id_face
			FROM rim_lienclientrim rlc
			JOIN rim_gestionrim rg ON (rg.supplogique_rim = 'N' AND rg.id_rim = '".$id_face."' AND rg.id_rim = rlc.id_rim)
			JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_rim = rg.id_rim)
			JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_lerim = rlr.id_lerim)
			JOIN ric_mobilier rm ON (rm.supplogique_mobilier = 'N' AND rm.id_mobilier = rl.id_mobilier)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.accesdirect_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			LEFT JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			WHERE rlc.supplogique_lienclientrim = 'N'";






        return $this->renvoi_info_requete($sql);
    }


    function renvoi_plannings_param_tableau_id_emplacement_date($tableau_id_emplacement, $date, $periodicite, $heure) {
        return $this->renvoi_info_requete("
			SELECT
				rp.id_planning AS id,
				rp.datedebut_planning AS date_debut,
				rp.datefin_planning AS date_fin,
				rp.periodicitejoursemaine_planning AS periodicite,
				rp.desactivation_planning AS desactivation,
				rp.heuredebut_planning AS heure_debut,
				rp.heurefin_planning AS heure_fin,
				re.cheminfichier_ecran AS image,
				tf.extension_typefichier AS extension,
				rlc.id_lienclientrim AS id_emplacement
			FROM rim_planning rp
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_lienclientrim IN ( $tableau_id_emplacement) AND rlc.id_lienclientrim = rp.id_lienclientrim)
			JOIN rim_ecran re ON (re.supplogique_ecran = 'N' AND re.id_ecran = rp.id_ecran)
			JOIN typefichier tf ON (tf.supplogique_typefichier = 'N' AND tf.id_typefichier = re.id_typefichier)
			WHERE rp.supplogique_planning = 'N'
			AND rp.desactivation_planning = 'N'
			AND '".$date."' >= rp.datedebut_planning AND '".$date."' <= rp.datefin_planning
			AND rp.periodicitejoursemaine_planning regexp '[".$periodicite."]'
			AND '".$heure."' >= rp.heuredebut_planning AND '".$heure."' < rp.heurefin_planning
			GROUP BY rlc.id_lienclientrim
		");
    }

    public function renvoi_affiches_tampons_param_tableau_id_emplacement($tableau_id_emplacement) {
        $sRequete = "
			SELECT
				rlc.id_lienclientrim AS id,
				cp.image_contratproduit AS image,
				rlc.id_lienclientrim AS id_emplacement
			FROM contratproduit cp
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_lienclientrim IN ( $tableau_id_emplacement) AND rlc.id_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat)
			WHERE cp.supplogique_contratpeoduit = 'N'
			AND cp.actif_contratproduit = 'Y'
		";



        $aTabRetour = $this->renvoi_info_requete($sRequete);

        $u=0;
        $i=1;
        if(!empty($aTabRetour)){
            foreach($aTabRetour as $valeur){
                $aTableauRetourListe[$u]['id']= $valeur['id'];
                $aTableauRetourListe[$u]['numero_de_face']= $i;
                //$aTableauRetourListe[$u]['nom_reseau_tournant_pere']= $valeur['nom_reseau_tournant_pere'];
                //$aTableauRetourListe[$u]['nom_reseau_tournant_pere']= $valeur['nom_reseau_tournant_pere'];
                $aTableauRetourListe[$u]['id_emplacement']= $valeur['id_emplacement'];
                //$aTableauRetourListe[$u]['nom_emplacement']= $valeur['nom_emplacement'];
                //$aTableauRetourListe[$u]['id_reseau_tournant_fils']= $valeur['id_reseau_tournant_fils'];
                //$aTableauRetourListe[$u]['id_reseau_tournant_fils']= $valeur['id_reseau_tournant_fils'];
                //$aTableauRetourListe[$u]['nom_reseau_tournant']= $valeur['nom_reseau_tournant'];
                //$aTableauRetourListe[$u]['id_contratproduit']= $valeur['id_contratproduit'];
                //$aTableauRetourListe[$u]['supplogique_lienclientrim']= $valeur['supplogique_lienclientrim'];
                $aTableauRetourListe[$u]['image_contratproduit']= $valeur['image'];
                $aTableauRetourListe[$u]['affiche_tampon']['extension']= explode('.',$valeur['image'])[1];
                $aTableauRetourListe[$u]['affiche_tampon']['image']=  "repimages/".$valeur['image'];
                $aTableauRetourListe[$u]['affiche_tampon']['id']=  $valeur['id'];
                $aTableauRetourListe[$u]['planning_actuel']['extension']= explode('.',$valeur['image'])[1];
                $aTableauRetourListe[$u]['planning_actuel']['image']=  "repimages/".$valeur['image'];
                $aTableauRetourListe[$u]['planning_actuel']['id']=  $valeur['id'];
                $u++;
                $i++;
            }
        }
        
        //echo"<pre>";print_r($aTableauRetourListe);echo"</pre>";

        return $aTableauRetourListe;

    }


    public function renvoi_jour($id_planning)
    {
        $sRequetejour = "Select periodicitejoursemaine_planning from rim_planning where id_planning = '$id_planning'";

        $aTabListeJour = $this->renvoi_info_requete($sRequetejour);

        if(!empty($aTabListeJour))
        {
            $aTabListeJour = str_split($aTabListeJour[0]['periodicitejoursemaine_planning']);
        }

        
        //echo"<pre>";print_r($aTabListeJour);echo"</pre>";


        return $aTabListeJour;
    }

    /*
    *function positionnement reseau tournant
    *
    *
    */
    public function positionement_rt($idreseau,$datefiltre){


        //récupération du positionnement défaut du reseau tournant
        $sRequete_eplacement_rt="SELECT client.id_client, 
					id_lienclientrim, 
					nom_client, 
					raisonsocial_client, 
					rim_emplacement.id_emplacement, 
					nom_emplacment,
					position_lien_emplacement,
					date_format(date_contratproduit,'%d/%m/%Y') as datecontratproduit ,
					nom_reseau_tournant,
					periodicite_reseau_tournant,
					date_format(datedebut_reseau_tournant,'%d/%m/%Y') as datedebutreseau,
					contratproduit.id_contratproduit,
					nom_affiche_client_rim,
					nom_lerim
					from le_reseau_tournant
					inner join lien_resau_emplacement on lien_resau_emplacement.id_reseau_tournant = le_reseau_tournant.id_reseau_tournant 
					inner join rim_emplacement on rim_emplacement.id_emplacement = lien_resau_emplacement.id_emplacement and supplogique_lien_emplacement='N'
					 and rim_emplacement.suplogique_emplacement ='N'
					inner join rim_gestionrim on rim_gestionrim.id_rim = rim_emplacement.id_rim
					inner join rim_lienrimface on rim_lienrimface.id_rim = rim_gestionrim.id_rim and supplogique_lienrimface='N'
					inner join rim_lerim on rim_lerim.id_lerim = rim_lienrimface.id_lerim
					left join  rim_lienclientrim on rim_lienclientrim.id_emplacement = rim_emplacement.id_emplacement and supplogique_lienclientrim='N'
					left join client on (client.id_client = rim_lienclientrim.id_client)
					left join contratproduit on contratproduit.id_contratproduit = rim_lienclientrim.id_contratproduit
					where le_reseau_tournant.id_reseau_tournant='".$idreseau."' order by position_lien_emplacement asc";

        $aTableau_liste = $this->renvoi_info_requete($sRequete_eplacement_rt);

        if(!empty($aTableau_liste)) {
            $nbrjour = $this->nombrejourdate($aTableau_liste[0]['datedebutreseau'], $datefiltre);

            //echo $nbrjour."<br>";
            if( $aTableau_liste[0]['periodicite_reseau_tournant'] == 0 )
                $aTableau_liste[0]['periodicite_reseau_tournant'] = 1;

            if( $nbrjour > 0 )
                $nbiteration = ceil($nbrjour / $aTableau_liste[0]['periodicite_reseau_tournant'] - 1);
            else
                $nbiteration = 0;

            //echo "toto : ".$nbrjour." ".$aTableau_liste[0]['periodicite_reseau_tournant']." ".$nbiteration."<br>";

            $u = 0;
            $itailletableau = count($aTableau_liste);
            if( !empty($aTableau_liste) ) {
                foreach( $aTableau_liste as $valeur ) {


                    $iposition = $this->position_tableau($nbiteration, $itailletableau, $valeur['position_lien_emplacement'] - 1);


                    if( trim($aTableau_liste[$iposition]['nom_affiche_client_rim']) == "" )
                        $aTableau_liste[$iposition]['nom_affiche_client_rim'] = $aTableau_liste[$iposition]['nom_lerim'];


                    $aTableau_liste[$u]['position_actuelle'] = $aTableau_liste[$iposition]['position_lien_emplacement'];
                    $aTableau_liste[$u]['nom_emplacment_actuelle'] = $aTableau_liste[$iposition]['nom_affiche_client_rim'];
                    $u++;


                }
            }
        }


        return $aTableau_liste;

    }


    /**
     * @param $idreseau
     * @param $datefiltre
     * @param $idlienclientrim
     * @return array
     */
    function renvoi_position_rt_client($idreseau, $datefiltre, $idlienclientrim){
        $aTableau = $this->positionement_rt($idreseau,$datefiltre);

        if(!empty($aTableau)){
            foreach($aTableau as $valeur){
                if($valeur['id_lienclientrim']==$idlienclientrim)
                    return $valeur;
            }
        }else{
            return array();
        }
    }


    public function ctrl_planification($datedebut,$datefin,$id,$id_planning)
    {
        $sRequeteElement = "Select id_planning 
                        from rim_planning 
                        where supplogique_planning = 'N' 
                        and datedebut_planning between '$datedebut' 
                        and '$datefin' and datefin_planning between '$datedebut' 
                        and '$datefin' and id_lienclientrim = '".$id."'";


        if(!empty($id_planning))
        {
            $sRequeteElement .= "and id_planning != '$id_planning'";
        }


        $aTabPlanning = $this->renvoi_info_requete($sRequeteElement);


        /*foreach($aTabAllElement as $eElement)
        {
            $sRequeteVerifPlanning = "Select id_planning from rim_planning where supplogique_planning = 'N' and datedebut_planning between '$datedebut' and '$datefin' and datefin_planning between '$datedebut' and '$datefin' and id_lienclientrim = '".$eElement['id_lienclientrim']."' ";

            $aTabVerifPlanning = $this->renvoi_info_requete($sRequeteVerifPlanning);

            if(!empty($aTabVerifPlanning[0]))
            {
                $aTabPlanning[0] = "planning utilisé";
            }

        }*/

        return $aTabPlanning;

    }

    /**
     * @param $libelle_variable
     * @return string
     */
    public function renvoi_variable_config($libelle_variable){
        $sRequete_variable="SELECT v.valeur_variables AS valeur
			FROM variables v
			WHERE v.supplogique_variables = 'N'
			AND v.libelle_variables = '".$libelle_variable."'";

            $aTableauInfovaleur = $this->renvoi_info_requete($sRequete_variable);

            if(!empty($aTableauInfovaleur)){
                return $aTableauInfovaleur[0]['valeur'];
            }else{
                return "";
            }
    }


}