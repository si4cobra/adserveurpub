<?php

/**
 * Nom: ctrl_fli_session
 * Description: controleur permettant de retrouner le cookie de session
 * Date: 10/12/2015
 */
class ctrl_fli_session
{

    //Ajoute la valeur du cookie en variable static
    public function session()
    {
        if( isset($_COOKIE[class_fli::get_nom_cookie()]) ) {
            class_fli::set_session($_COOKIE[class_fli::get_nom_cookie()]);
        }
    }
}