<?php

/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 07/12/2017
 * Time: 14:49
 */
class ctrl_tableaubord extends class_form_list
{

    public function __construct($objSmartyImport, $sModule)
    {

        parent::class_list($objSmartyImport, $sModule);
        $objImport = new class_import();
        $objImport->import_modele('crm', 'mod_crm');
        $objModoutils = new mod_crm();
        $this->objBdd = $objModoutils;
    }

    public function fli_tableaubord(){


        $aVar = array( 'triprogramme', 'tricommercial', 'tridatedebut', 'tridatedebut','tridatefin','bdebug' );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $guid_user= class_fli::get_aData('guid_user');

        //BDD
        $this->sNomTable='client_contact';                            //string Nom de la table liée
        $this->sChampId='id_contact';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid='guid_contact';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique='supplogique_contact';                    //string Nom du champ supplogique dans la table liée
        //Booléen
        $this->bTraiteConnexion=true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bKeyMultiple=false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bDebugRequete=false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffDup=false;                            //bool bAffDup Permet de dupliquer une ligne
        $this->bAffTitre=true;                          //bool Permet d'afficher le titre de la liste
        $this->bAffFiche=false;                         //bool Permet d'afficher la liste (si aucun des autres droits vaut true)
        $this->bLabelCreationElem=true;                 //bool Permet d'afficher le bouton d'ajout
        $this->bAffMod=false;                           //bool Permet la modification
        $this->bAffRecapLigne=false;                     //bool  bAffRecapLigne Permet d'affiche dans unenouvelle fenetre le recap de la ligne
        $this->bAffSupp=false;                          //bool Permet la suppression
        $this->bBtnRetour=false;                        //bool Permet d'afficher le bouton 'Retour' de la liste
        $this->bPagination=true;                       //bool Permet l'affichage de la pagination
        $this->bAffAnnuler=true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->bAffListeQuandPasRecherche=true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->bRetourSpecifique=false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->bUseDelete=false;                       //bool Permet l'affichage de la pagination
        $this->bAffNombreResult=true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable=false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->bAffPrintBtn=false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug=false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bActiveFormSelect=false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup=false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->bRadioSelect=false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->bCheckboxSelect=false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire)
        $this->bCsv=false;                              //bool Permet d'afficher le bouton d'export en csv
        //Entier
        $this->iNombrePage=10;                          //int Nombre de lignes désirées par page
        //String
        $this->sFiltreliste='';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND')
        $this->sFiltrelisteOrderBy='';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->sTitreCreationForm='Enregistrement d\'un';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm='Enregistement réussie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sMessageDuplicationSucces='Duplication de la ligne reussie';                           // sMessageDuplicationSucces Message affiché lorsquela duplication de la ligne s'est déroulé avec succès
        $this->sMessageDuplicationError='Erreur pendant la duplication de la ligne';                          //string sMessageDuplicationError Message affiché lorsque la duplication de la ligne s'est déroulé avec uen erreur
        $this->sTitreModificationForm='Modification d\'un thème';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm='Modification reussie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->sMessageSupprElem='Suppression réussie';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm='Problème survenue pendant l\'enregistrement';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem='Aj';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche='Rechercher un';                      //string Titre de la section recherche
        $this->sTitreListe='';                          //string Titre de la liste
        $this->sTitreForm='';                           //string Titre du formulaire
        $this->sLabelNbrLigne='nombre(s) sur votre sélection';                       //string Texte à coté du nombre de lignes
        $this->sLabelRetourListe='';                    //string Texte du bouton 'Retour' sur la liste
        $this->sLabelFileRetourElem='';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->sUrlRetourConnexion='';                  //string Module-controleur-fonction spécifié pour le module de connexion à utiliser
        $this->sDirRetour='';                           //string Module-controleur-fonction spécifié pour le bouton 'Retour'
        $this->sRetourElemUrl='';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sDirpagination='';                       //string Le module-controleur-fonction pour la pagination quand l'url est spécifique
        $this->sUrlRetourSpecifique='';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sTypeFichier='';                         //string Le type de fichier attendu
        $this->sLienLigne='';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert='';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate='';              //string Script executé après un UPDATE
        $this->sSuiteRequeteInsert='';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate='';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sListeTpl='dashboad.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sFormulaireTpl='formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTplSortie='principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)
        $this->sListeFilsTpl='liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->sListeFitreNopDuplicate='';              //string sListeFitreNopDuplicate renvoi la liste des champs qu'on veut exclure de la duplication séparé par un point virgule
        //Array
        $this->aElement=array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->itemBoutonsForm=array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->aListeFils=array();                      //array Tableau contenant les données de la liste fils
        $this->aSelectSqlSuppl=array();                 //array Tableau Permettant de rajouter des champs dans le SELECT de la liste
        //Wizard
        $this->sParametreWizardListe='';                //string Les paramètres de la liste du wizard
        $this->sCategorieListe='';                      //string La catégorie de la liste lors du wizard
        $this->itemBoutonsListe=array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->aParametreWizardListe=array();           //array Tableau contenant les paramètres du wizard
        $this->aParamsNonSuivi=array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        //Peu utilisés
        $this->sDebugRequeteSelect='';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate='';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->sRetourValidationForm='liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne='';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm='liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sRetourAnnulationFormExterne='';         //string Permet de spécifier l'url de l'annulation
        $this->sRetourSuppressionFormExterne='';        //string Permet de spécifier l'url de retour après la suppression
        $this->sUrlRetourSuppressionFormExterne='';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sRetourListe=null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        //inforamtion sur le menu automatique
        $this->sMenuIntitule='Tableau bord';
        $this->sMenuVisibile=true;
        $this->sMenuTarge=false;                   //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module

        /* --------- $this->Ajout_champ() ici --------- */
        $bAdmin=true;
        $id_user=0;
        $this->chargement("crm","version1");

        /* --------- fin $this->Ajout_champ() --------- */

        $aTabMenuTacheAFaireEntete=array();
        $aTabMenuTacheAFaireEntete[]=array('nom'=>"Date");
        $aTabMenuTacheAFaireEntete[]=array('nom'=>"Nom");
        $aTabMenuTacheAFaireEntete[]=array('nom'=>"Société");
        $aTabMenuTacheAFaireEntete[]=array('nom'=>"Action");
        $aTabMenuTacheAFaireEntete[]=array('nom'=>"Heure");
        if(strtolower($bAdmin)=="admin"){
            $aTabMenuTacheAFaireEntete[]=array('nom'=>"Commercial");
        }
        else{
            $aTabMenuTacheAFaireEntete[]=array('nom'=>"Commentaire");
        }

        $aTabMenuTacheAFaireRDVEntete=array();
        $aTabMenuTacheAFaireRDVEntete[]=array('nom'=>"Date");
        $aTabMenuTacheAFaireRDVEntete[]=array('nom'=>"Heure");
        $aTabMenuTacheAFaireRDVEntete[]=array('nom'=>"Nom");
        $aTabMenuTacheAFaireRDVEntete[]=array('nom'=>"Société");
        $aTabMenuTacheAFaireRDVEntete[]=array('nom'=>"Statut");
        if(strtolower($bAdmin)=="admin"){
            $aTabMenuTacheAFaireRDVEntete[]=array('nom'=>"Commercial");
        }
        else{
            $aTabMenuTacheAFaireRDVEntete[]=array('nom'=>"Commentaire");
        }


        $aTabMailsAutomatiquesEntetes=array();
        $aTabMailsAutomatiquesEntetes[]=array('nom'=>"Nom");
        $aTabMailsAutomatiquesEntetes[]=array('nom'=>"Société");
        $aTabMailsAutomatiquesEntetes[]=array('nom'=>"Mail n°");
        $aTabMailsAutomatiquesEntetes[]=array('nom'=>"");


        $aTabMenuNouveauxContactsEntete=array();
        $aTabMenuNouveauxContactsEntete[]=array('nom'=>"Nom prospect");
//$aTabMenuNouveauxContactsEntete[]=array('nom'=>"Email");
//$aTabMenuNouveauxContactsEntete[]=array('nom'=>"Telephone");
        $aTabMenuNouveauxContactsEntete[]=array('nom'=>"Société");
        $aTabMenuNouveauxContactsEntete[]=array('nom'=>"Date");
        $aTabMenuNouveauxContactsEntete[]=array('nom'=>"Source");
        if(strtolower($bAdmin)=="admin"){
            $aTabMenuNouveauxContactsEntete[]=array('nom'=>"Commercial");
        }

        $aTabMenuTacheEnRetardEntete=array();
        $aTabMenuTacheEnRetardEntete[]=array('nom'=>"Action");
        $aTabMenuTacheEnRetardEntete[]=array('nom'=>"Nom");
        $aTabMenuTacheEnRetardEntete[]=array('nom'=>"Société");
        $aTabMenuTacheEnRetardEntete[]=array('nom'=>"Date");
        if(strtolower($bAdmin)=="admin"){
            $aTabMenuTacheEnRetardEntete[]=array('nom'=>"Commercial");
        }
        $triprogramme=array();
        $aTabTriCom=array();
        $aTabTriProg=array();
        $aTabMenuTacheAFaire=array();
        $aTabMenuTacheAFaireRDV=array();
        $aTabMenuNouveauxContacts=array();
        $aTabMenuTacheEnRetard=array();

        $this->objSmarty->assign('triprogramme',$triprogramme);
        if(empty($tridatedebut)){
            $tridatedebut=date('Y-m-d');
        }
        if(empty($tridatefin)){
            $tridatefin=date('Y-m-d');
        }


        $aTabMenuTacheAFaire=$this->objBdd->liste_tache_user("a faire",0,10,$guid_user,"","Tabordjour",$triprogramme,$tricommercial,$tridatedebut,$tridatefin,false,$bdebug);
        //echo"<pre>";print_r($aTabMenuTacheAFaire);echo"</pre>";

        $aTabMenuTacheAFaireRDV=$this->objBdd->liste_tache_user("a faire",0,10,$guid_user,2,"Tabordjourrdv",$triprogramme,$tricommercial,$tridatedebut,$tridatefin,false,$bdebug);

        $aTabMenuTacheEnRetard=$this->objBdd->liste_tache_user("a faire",0,10,$guid_user,3,'Tabordjouretard',$triprogramme,$tricommercial,null,null,false);
       //echo"<pre>";print_r($aTabMenuTacheEnRetard);echo"</pre>";

        $aTabMenuNouveauxContacts=$this->objBdd->liste_contact(0,10,$guid_user,$triprogramme,$tricommercial,null,null,false,false);


        $this->objSmarty->assign('iduser',$id_user);

        $this->objSmarty->assign('aTabMenuTacheAFaireEntete',$aTabMenuTacheAFaireEntete);
        $this->objSmarty->assign('aTabMenuTacheAFaire',$aTabMenuTacheAFaire);


        $this->objSmarty->assign('aTabMenuTacheAFaireRDVEntete',$aTabMenuTacheAFaireRDVEntete);
        $this->objSmarty->assign('aTabMenuTacheAFaireRDV',$aTabMenuTacheAFaireRDV);

        $this->objSmarty->assign('aTabMailsAutomatiquesEntetes',$aTabMailsAutomatiquesEntetes);
        $this->objSmarty->assign('aTabMailsAutomatiques',$aTabMenuTacheAFaireRDV);

        $this->objSmarty->assign('aTabMenuNouveauxContactsEntete',$aTabMenuNouveauxContactsEntete);
        $this->objSmarty->assign('aTabMenuNouveauxContacts',$aTabMenuNouveauxContacts);

        $this->objSmarty->assign('aTabMenuTacheEnRetardEntete',$aTabMenuTacheEnRetardEntete);
        $this->objSmarty->assign('aTabMenuTacheEnRetard',$aTabMenuTacheEnRetard);





        $this->objSmarty->assign('tridatedebut',$tridatedebut);
        $this->objSmarty->assign('tridatefin',$tridatefin);
        $this->objSmarty->assign('datejour',date('d/m/Y'));
        $this->objSmarty->assign('bAdmin',true);
        $this->objSmarty->assign('aTabTriProg',$aTabTriProg);
        $this->objSmarty->assign('aTabTriCom',$aTabTriCom);



        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli

        return $aTabTpl;
    }
}