<?php

/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 26/02/2016
 * Time: 12:03
 */
class mod_fli_admin extends mod_form_list
{
    public function get_sel_user($guid_user)
    {
        $aParam[] = $guid_user;

        $aResultRequete = $this->renvoi_info_requete("SELECT sel_user FROM " . $this->sPrefixeDb . "users WHERE supplogique_user='N' AND guid_user=?", $aParam);

        return $aResultRequete;
    }

    public function set_sel_user($sSel, $guid_user)
    {
        $aTab['sel_user'] = $sSel;
        $aTab['guid_user'] = $guid_user;

        $sql = "UPDATE " . $this->sPrefixeDb . "users SET sel_user=:sel_user WHERE guid_user=:guid_user";

        $this->insert_update_requete($sql, $aTab);
    }

    public function set_password_user($password, $guid)
    {

        $aTab['password_user'] = $password;
        $aTab['guid_user'] = $guid;

        $sql = "UPDATE " . $this->sPrefixeDb . "users SET password_user=:password_user WHERE guid_user=:guid_user";

        $this->insert_update_requete($sql, $aTab);
    }

    public function get_user($guid_user)
    {
        $aParam[] = $guid_user;

        $aResultRequete = $this->renvoi_info_requete("SELECT login_user FROM " . $this->sPrefixeDb . "users WHERE supplogique_user='N' AND guid_user=?", $aParam);

        return $aResultRequete;
    }

    public function get_info_menu($sroute)
    {
        $aParam[] = $sroute;
        $sRequeteInfoRoute = "SELECT intitule_menu_route,description_route FROM " . $this->sPrefixeDb . "routes WHERE  route_route='" . $sroute . "'";
        //echo $sRequeteInfoRoute."<br>";
        $aResultRequete = $this->renvoi_info_requete($sRequeteInfoRoute);
        return $aResultRequete;
    }


    public function set_doc_menu($sroute, $sdecription, $guid_user)
    {
        $sRequeteInfoRoute = "SELECT intitule_menu_route,description_route FROM " . $this->sPrefixeDb . "routes WHERE  route_route='" . $sroute . "'";
        $aResultRequete = $this->renvoi_info_requete($sRequeteInfoRoute);
        /*
         * Enregistrement de l'historique
         */
        if( !empty($aResultRequete) ) {
            $sRequete_histo = "Insert " . $this->sPrefixeDb . "historiquedoc SET description_historiquedoc='" . addslashes($aResultRequete[0]['description_route']) . "',
            guid_historiquedoc='" . class_helper::guid() . "',guid_user='" . class_fli::get_guid_user() . "'";
            $this->execute_requete($sRequete_histo);
        }


    }


    /**
     * Affectation d'un menu à un groupe
     * @param $tabmenu
     * @param $guidgroupe
     * @return array
     */
    public function affectation_menu_groupe($tabmenu,$guidgroupe,$sprefixe){
        $aTableauRetour=array();
        $aTableauRetour['message']="";

        //echo"test<pre>";print_r($tabmenu);echo"</pre>";

        if(!empty($tabmenu)){
            foreach($tabmenu as $valeur){

                //récupération des information sur le menu
                $sRequete_info_menu=" SELECT id_route,
                guid_route,
                intitule_menu_route
                  from  ".$sprefixe."routes where id_route='".$valeur."'";
                $aTableauInfoMneu= $this->renvoi_info_requete($sRequete_info_menu);

                $sRequete_affectation ="Insert  ".$sprefixe."groupes_routes set guid_groupe='".$guidgroupe."',
                guid_route='".$aTableauInfoMneu[0]['guid_route']."',
                ajout_groupe_route='1',
                modif_groupe_route='1',
                suppr_groupe_route='1',
                visu_groupe_route='1',
                switch_wizard_groupes_routes='0'
                ON DUPLICATE KEY UPDATE  supplogique_groupe_route='N',
                ajout_groupe_route='1',
                modif_groupe_route='1',
                suppr_groupe_route='1',
                visu_groupe_route='1',
                switch_wizard_groupes_routes='0'";
                //echo $sRequete_affectation."<br>";
                $bRresult= $this->execute_requete($sRequete_affectation);
                if($bRresult){
                    $aTableauRetour['message'].="Affectation du menu ".$aTableauInfoMneu[0]['intitule_menu_route']." réussie<br>";
                }else{
                    $aTableauRetour['message'].=" Problème survenu pendant l'affectation du menu ".$aTableauInfoMneu[0]['intitule_menu_route']." <br>";
                }
            }
        }

        return $aTableauRetour;

    }


    /**
     * Désaffectation d'un menu à un groupe
     * @param $tabmenu
     * @param $guidgroupe
     * @return array
     */
    public function desaffectation_menu_groupe($tabmenu, $guidgroupe,$sprefixe){
        $aTableauRetour=array();
        $aTableauRetour['message']="";

        //echo"test<pre>";print_r($tabmenu);echo"</pre>";

        if(!empty($tabmenu)){
            foreach($tabmenu as $valeur){

                //récupération des information sur le menu
                $sRequete_info_menu=" SELECT id_route,
                guid_route,
                intitule_menu_route
                  from  ".$sprefixe."routes where id_route='".$valeur."'";

                //echo $sRequete_info_menu."<br>";

                $aTableauInfoMneu= $this->renvoi_info_requete($sRequete_info_menu);

                $sRequete_affectation ="update  ".$sprefixe."groupes_routes 
                set supplogique_groupe_route='Y' where 
                guid_groupe='".$guidgroupe."' and guid_route='".$aTableauInfoMneu[0]['guid_route']."'";
                //echo $sRequete_affectation."<br>";

                $bRresult= $this->execute_requete($sRequete_affectation);

                if($bRresult){
                    $aTableauRetour['message'].=" Désaffectation du menu ".$aTableauInfoMneu[0]['intitule_menu_route']." réussie<br>";
                }else{
                    $aTableauRetour['message'].=" Problème survenu pendant la désaffectation du menu ".$aTableauInfoMneu[0]['intitule_menu_route']." <br>";
                }
            }
        }

        return $aTableauRetour;

    }

    public function renvoi_menu_liste($identifiantpere,$prefixe=""){

        $sRequete_menu="select id_route,
        route_route,
        intitule_menu_route,
        pere_menu_route,
        afficher_menu_route,
        ordre_menu_route,
        target_menu_route,
        lien_menu_route,
        guid_route,
        supplogique_route
        from ".$prefixe."routes 
        where supplogique_route='N'
       and pere_menu_route='".$identifiantpere."' order by intitule_menu_route ";
        $aTabMenuFiltre=array();
        $k=0;



        //echo $sRequete_menu."<br>";

        $aTableauMenu = $this->renvoi_info_requete($sRequete_menu);

        if(!empty($aTableauMenu)){
            foreach($aTableauMenu as $valeur){
                $aTabMenuFiltre[$k]['id_route']=$valeur['id_route'];
                $aTabMenuFiltre[$k]['intitule_menu_route']=$valeur['intitule_menu_route'];
                $aTabMenuFiltre[$k]['route_route']=$valeur['route_route'];

                $aTtmp = $this->renvoi_menu_liste($valeur['route_route'],$prefixe);

                $aTabMenuFiltre[$k]['fils']=$aTtmp;
                if(empty($aTtmp)){
                    $aTabMenuFiltre[$k]['est_parent'] = false;
                }else {
                    $aTabMenuFiltre[$k]['est_parent'] = true;
                }

                $k++;
            }
        }



        return  $aTabMenuFiltre;

    }


    public function renvoi_menu_liste_groupe($identifiantpere,$guidgroupe,$prefixe=""){
        $sRequete_menu="select  id_route,
        route_route,
        intitule_menu_route,
        pere_menu_route,
        afficher_menu_route,
        ordre_menu_route,
        target_menu_route,
        lien_menu_route,
        ".$prefixe."groupes_routes.guid_route,
        supplogique_route,
        ajout_groupe_route,
        modif_groupe_route,
        suppr_groupe_route,
        switch_wizard_groupes_routes,
        visu_groupe_route,
        guid_groupe_route
        from ".$prefixe."routes 
        INNER JOIN  ".$prefixe."groupes_routes on ".$prefixe."groupes_routes.guid_route = ".$prefixe."routes .guid_route
        and supplogique_groupe_route='N'
        where supplogique_route='N' 
        and pere_menu_route='".$identifiantpere."' and ".$prefixe."groupes_routes.guid_groupe='".$guidgroupe."' 
        group by route_route
        order by intitule_menu_route ";
        $aTabMenuFiltre=array();
        $k=0;

        //echo $sRequete_menu."<br>";

        $aTableauMenu = $this->renvoi_info_requete($sRequete_menu);

        //echo $sRequete_menu."<br>";

        if(!empty($aTableauMenu)){
            foreach($aTableauMenu as $valeur){
                $aTabMenuFiltre[$k]['id_route']=$valeur['id_route'];
                $aTabMenuFiltre[$k]['guid_route']=$valeur['guid_route'];
                $aTabMenuFiltre[$k]['guid_groupe_route']=$valeur['guid_groupe_route'];
                $aTabMenuFiltre[$k]['intitule_menu_route']=$valeur['intitule_menu_route'];
                $aTabMenuFiltre[$k]['route_route']=$valeur['route_route'];
                $aTabMenuFiltre[$k]['ajout_groupe_route']=$valeur['ajout_groupe_route'];
                $aTabMenuFiltre[$k]['modif_groupe_route']=$valeur['modif_groupe_route'];
                $aTabMenuFiltre[$k]['suppr_groupe_route']=$valeur['suppr_groupe_route'];
                $aTabMenuFiltre[$k]['visu_groupe_route']=$valeur['visu_groupe_route'];
                $aTabMenuFiltre[$k]['switch_wizard_groupes_routes']=$valeur['switch_wizard_groupes_routes'];

                $aTtmp = $this->renvoi_menu_liste_groupe($valeur['route_route'],$guidgroupe,$prefixe);

                $aTabMenuFiltre[$k]['fils']=$aTtmp;
                if(empty($aTtmp)){
                    $aTabMenuFiltre[$k]['est_parent'] = false;
                }else {
                    $aTabMenuFiltre[$k]['est_parent'] = true;
                }

                $k++;
            }
        }



        return  $aTabMenuFiltre;

    }
}
