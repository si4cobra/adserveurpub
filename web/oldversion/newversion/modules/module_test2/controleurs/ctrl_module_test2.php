<?php
/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 16/12/2015
 * Time: 17:27
 */

class ctrl_module_test2 extends class_form_list{

    /**
     * @return mixed
     * Fonction qui renvoie le menu en fonction des différents modules présents dans le framework et selon le fichier install.php
     */
    public function test(){
        $this->sNomTable='utilisateur_administration';
        $this->sChampId='id_administration';
        $this->sChampSupplogique='supplogique_administration';
        $this->sDebugRequete=false;
        $this->sLabelCreationElem=true;
        $this->sAffFiche=false;
        $this->sAffMod=true;
        $this->sAffSupp=true;
        $this->sPagination=true;
        $this->sNombrePage=50;
        $this->sFiltreliste='';
        $this->sTitreForm = 'Gestion de vos taches et suivi';
        $this->FiltrelisteOrderBy='';
        $this->sTitreCreationForm='Création d\'une nouvelle ';
        $this->sTitreModificationForm='Modification d\'une ';
        $this->sMessageCreationSuccesForm='La ligne  a été créee';
        $this->sMessageModificationSuccesForm='La  a bien été modifiée';
        $this->sMessageErreurForm = 'Veuillez compléter tous les champs obligatoires';
        $this->sMessageSupprElem = 'La ligne  a été supprimée';
        $this->sLabelCreationElem = 'Créer une tache ou un suvi ';
        $this->sTitreListe = 'Gestion de vos taches et suivi ';
        $this->sLabelNbrLigne = 'Tâche(s) correspond(ent) à votre recherche';
        $this->sLabelRecherche = 'Recherche d\'une ';
        $this->sLabelRetourListe = '';
        $this->sRetourValidationForm = 'liste';
        $this->sCategorieListe = '';
        $this->sListeTpl = 'liste.tpl';

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'nom_administration', // le nom du champ dans la table selectionnee
            'nom_variable' => 'nom_administration', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Nom', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array('size'=>50), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $aTabTpl[] = $this->run();



        return $aTabTpl;
    }

}