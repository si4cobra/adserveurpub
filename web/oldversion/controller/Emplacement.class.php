<?php
class Emplacement {
	public static function Index() {
		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'content', self::ListePlanning());
		echo $template_layout->draw();
	}
	
	public static function ActiverPlanning() {
		if (!Request::hasGetParameter('planning')) {
			header('Location:./');
		}
	
		$id_client = StackValue::getValue('id_client');
	
		$planning = Loader::get('RimPlanning')->renvoi_planning($id_client, Request::getGetParameter('planning'));
	
		if (!$planning) {
			header('Location:./');
		}
	
		$planning_heure_debut_en_seconde = (strtotime('t'.$planning['heure_debut']) - strtotime(date('Y-m-d'))) % 86400;
/*
		if ((strtotime($planning['date_debut']) + $planning_heure_debut_en_seconde) < (strtotime(date('Y-m-d H:i:s')) + StaticDbValue::get('nombre_seconde_avant_debut_planning autorise'))) {
			$planning['date_debut'] = date('Y-m-d');
			
			if ((strtotime($planning['date_debut']) + $planning_heure_debut_en_seconde) < (strtotime(date('Y-m-d H:i:s')) + StaticDbValue::get('nombre_seconde_avant_debut_planning autorise'))) {
				$planning['date_debut'] = date('Y-m-d', strtotime('+1 day', strtotime($planning['date_debut']) + 86400 * (StaticDbValue::get('nombre_seconde_avant_debut_planning autorise') / 86400)));
			}
			
			if ($planning['date_debut'] > $planning['date_fin']) {
				$planning['date_fin'] = StaticDbValue::get('max_date');
			}
		}
*/
		$plannings_chevauches = Loader::get('RimPlanning')->renvoi_plannings_chevauches($planning);
			
		if (count($plannings_chevauches) == 0) {
			$activation_planning = Loader::get('RimPlanning')->activer_planning($planning);
			
			$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_debut');
			$planning = array_shift($planning);
			$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_fin');
			$planning = array_shift($planning);
			$planning = MethodArray::affectColumnValueWithFrenchDay(array($planning), 'periodicite', ', ');
			$planning = array_shift($planning);
			$planning = MethodArray::affectColumnValueWithFrenchSchedule(array($planning), 'heure_debut', 'heure_fin', 'horaire');
			$planning = array_shift($planning);

			if ($activation_planning) {
				$message_info = array(
					'type_info' => 'alert-success',
					'titre' => 'Activation!',
					'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', a été activée.'
				);
			} else {
				$message_info = array(
					'type_info' => 'alert-error',
					'titre' => 'Erreur!',
					'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', n\'a pas pu être activée. Veuillez réessayer plus tard.'
				);
			}
		} else {
			$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDate($plannings_chevauches, 'date_debut');
			$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDate($plannings_chevauches, 'date_fin');
			$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDay($plannings_chevauches, 'periodicite', ', ');
			$plannings_chevauches = MethodArray::affectColumnValueWithFrenchSchedule($plannings_chevauches, 'heure_debut', 'heure_fin', 'horaire');

			$description = array();

			foreach ($plannings_chevauches as $planning_chevauche) {
				$description[] = '- Conflit avec l\'affiche planifiée '.$planning_chevauche['date_debut'].' au '.$planning_chevauche['date_fin'].', '.$planning_chevauche['periodicite'].', '.$planning_chevauche['horaire'];
			}

			$description = implode('<br>', $description);

			$message_info = array(
				'type_info' => 'alert-error',
				'titre' => 'Erreur!',
				'description' => $description
			);
		}
	
		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'content', array(
			Loader::get('Template')->set('message2.tpl', array(
				'message_info' => $message_info
			)),
			self::ListePlanning()
		));
		echo $template_layout->draw();
	}
	
	public static function DesactiverPlanning() {
		if (!Request::hasGetParameter('planning')) {
			header('Location:./');
		}
	
		$id_client = StackValue::getValue('id_client');
	
		$planning = Loader::get('RimPlanning')->renvoi_planning($id_client, Request::getGetParameter('planning'));
	
		if (!$planning) {
			header('Location:./');
		}
	
		$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_debut');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_fin');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchDay(array($planning), 'periodicite', ', ');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchSchedule(array($planning), 'heure_debut', 'heure_fin', 'horaire');
		$planning = array_shift($planning);
	
		$desactivation_planning = Loader::get('RimPlanning')->desactiver_planning(Request::getGetParameter('planning'));
	
		if ($desactivation_planning) {
			$message_info = array(
				'type_info' => 'alert-info',
				'titre' => 'Désactivation!',
				'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', a été désactivée.'
			);
		} else {
			$message_info = array(
				'type_info' => 'alert-error',
				'titre' => 'Erreur!',
				'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', n\'a pas pu être désactivée. Veuillez réessayer plus tard.'
			);
		}
	
		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'content', array(
			Loader::get('Template')->set('message2.tpl', array(
				'message_info' => $message_info
			)),
			self::ListePlanning()
		));
		echo $template_layout->draw();
	}
	
	public static function SupprimerPlanning() {
		if (!Request::hasGetParameter('planning')) {
			header('Location:./');
		}
	
		$id_client = StackValue::getValue('id_client');
	
		$planning = Loader::get('RimPlanning')->renvoi_planning($id_client, Request::getGetParameter('planning'));
	
		if (!$planning) {
			header('Location:./');
		}
	
		$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_debut');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_fin');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchDay(array($planning), 'periodicite', ', ');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchSchedule(array($planning), 'heure_debut', 'heure_fin', 'horaire');
		$planning = array_shift($planning);
	
		$suppression_planning = Loader::get('RimPlanning')->supprimer_planning(Request::getGetParameter('planning'));
	
		if ($suppression_planning) {
			$message_info = array(
				'type_info' => 'alert-info',
				'titre' => 'Suppression!',
				'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', a été supprimée.'
			);
		} else {
			$message_info = array(
				'type_info' => 'alert-error',
				'titre' => 'Erreur!',
				'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', n\'a pas pu être supprimée. Veuillez réessayer plus tard.'
			);
		}
	
		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'content', array(
			Loader::get('Template')->set('message2.tpl', array(
				'message_info' => $message_info
			)),
			self::ListePlanning()
		));
		echo $template_layout->draw();
	}

	public static function ListePlanning() {
		if (!Request::hasGetParameter('emplacement')) {
			header('Location:./');
		}
		
		$id_client = StackValue::getValue('id_client');
		
		$emplacement = Loader::get('RimLienClientRim')->renvoi_emplacement_param_id_emplacement($id_client, Request::getGetParameter('emplacement'));
		
		if (!$emplacement) {
			header('Location:./');
		}
		
		if ($emplacement['id_reseau_tournant'] != NULL) {
			$reseau_tournant = Loader::get('LeReseauTournantPere')->renvoi_reseau_tournant($id_client, $emplacement['id_reseau_tournant']);
			
			$emplacements = Loader::get('RimLienClientRim')->renvoi_emplacements_param_reseau_tournant($id_client, $emplacement['id_reseau_tournant']);
			
			if (!$reseau_tournant) {
				header('Location:./');
			}
		} else {
			$emplacements = Loader::get('RimLienClientRim')->renvoi_emplacements_param_id_face($id_client, $emplacement['id_face']);
		}
		
		$numero_de_face = 0;
		
		foreach($emplacements as $e) {
			$numero_de_face++;
			if ($e['id'] == $emplacement['id']) {
				$emplacement['numero_de_face'] = $numero_de_face;
				break;
			}
		}
		
		$emplacement = MethodArray::affectColumnValueWithOtherColumn(array($emplacement), 'nom_emplacement', 'numero_emplacement');
		$emplacement = array_shift($emplacement);
		$emplacement = MethodArray::affectColumnValueWithOtherColumn(array($emplacement), 'numero_emplacement', 'nom');
		$emplacement = array_shift($emplacement);
		$emplacement = MethodArray::affectColumnValueWithOtherColumn(array($emplacement), 'nom_vision_face', 'nom_face');
		$emplacement = array_shift($emplacement);
		$emplacement['selection'] = 'emplacement='.$emplacement['id'];
		
		if ($emplacement['id_reseau_tournant'] != NULL) {
			$titre = 'Réseau tournant - '.$emplacement['nom_reseau_tournant'];
		} else {
			$emplacement['numero_mobilier'] = 0 + $emplacement['numero_mobilier'];
			
			$titre = $emplacement['ville_mobilier'].' - RIM No '.$emplacement['numero_mobilier'].' - '.$emplacement['adresse_mobilier'];
		}
		
		$affiche_tampon = Loader::get('ContratProduit')->renvoi_affiche_tampon_param_id_emplacement(Request::getGetParameter('emplacement'));
		
		if ($affiche_tampon) {
			$affiche_tampon = MethodArray::prefixColumnValue(array($affiche_tampon), 'image', StaticValue::$adresse_image_site);
			$affiche_tampon = array_shift($affiche_tampon);
			
			$fichier_ecran_info_image = getimagesize($affiche_tampon['image']);
				
			if ($fichier_ecran_info_image['mime'] == 'application/x-shockwave-flash') {
				$affiche_tampon['extension'] = 'swf';
			} else {
				$affiche_tampon['extension'] = 'no_swf';
			}
		}
		
		$emplacement['affiche_tampon'] = $affiche_tampon;
		
		$plannings = Loader::get('RimPlanning')->renvoi_plannings_param_id_emplacement($id_client, Request::getGetParameter('emplacement'));
		
		if ($plannings) {
			$plannings = MethodArray::prefixColumnValue($plannings, 'image', StaticValue::$adresse_image_site);
			$plannings = MethodArray::affectColumnValueWithFrenchDate($plannings, 'date_debut');
			$plannings = MethodArray::affectColumnValueWithFrenchDate($plannings, 'date_fin');
			$plannings = MethodArray::affectColumnValueWithFrenchDay($plannings, 'periodicite');
			$plannings = MethodArray::affectColumnValueWithFrenchSchedule($plannings, 'heure_debut', 'heure_fin', 'horaire');
		}
		
		//echo"<pre>";print_r($plannings);echo"</pre>";

        $id_contratproduit = $_GET['id_contratproduit'];

		$emplacement['plannings'] = $plannings;
		

		
		if ($emplacement['id_reseau_tournant'] != NULL) {
			$bouton_retour_url = 'index.php?page=selection&reseau_tournant='.$emplacement['id_reseau_tournant'].'&id_contratproduit='.$id_contratproduit;
		} else {
			$bouton_retour_url = 'index.php?page=selection&face='.$emplacement['id_face'];
		}
		
		return Loader::get('Template')->set('liste1.tpl', array(
			'titre' => $titre,
			'bouton_retour_libelle' => 'Retour',
			'bouton_retour_url' => $bouton_retour_url,
			'content' => Loader::get('Template')->set('liste3.tpl', $emplacement)
		));
	}
	
	private static function Layout() {
		$id_client = StackValue::getValue('id_client');
		$client = Loader::get('Client')->renvoi_client($id_client);
	
		return Loader::get('Template')->set('layout1.tpl', array(
			'head' => Loader::get('Template')->set('head.tpl', array(
				'titre_site' => StaticValue::$titre_site
			)),
			'content' => Loader::get('Template')->set('principal.tpl', array(
				'header' => Loader::get('Template')->set('header.tpl', array(
					'titre_site' => StaticValue::$titre_site,
					'client' => $client
				))
			)),
			'script' => Loader::get('Template')->set('script.tpl')
		));
	}
}