<?php
class Planning {
	public static function Creer() {
		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'content', self::CreationPlanning());
		echo $template_layout->draw();
	}
	
	public static function Modifier() {
		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'content', self::ModificationPlanning());
		echo $template_layout->draw();
	}
	
	private static function CreationPlanning() {
		if (!Request::hasGetParameter('emplacement')) {
			header('Location:./');
		}
		
		$id_client = StackValue::getValue('id_client');
		$client = Loader::get('Client')->renvoi_client($id_client);
		
		$emplacement = Loader::get('RimLienClientRim')->renvoi_emplacement_param_id_emplacement($id_client, Request::getGetParameter('emplacement'));
		
		if (!$emplacement) {
			header('Location:./');
		}
		
		if ($emplacement['id_reseau_tournant'] != NULL) {
			$reseau_tournant = Loader::get('LeReseauTournantPere')->renvoi_reseau_tournant($id_client, $emplacement['id_reseau_tournant']);
		
			if (!$reseau_tournant) {
				header('Location:./');
			}
		}
		
		$emplacement = MethodArray::affectColumnValueWithOtherColumn(array($emplacement), 'nom_emplacement', 'numero_emplacement');
		$emplacement = array_shift($emplacement);
		$emplacement = MethodArray::affectColumnValueWithOtherColumn(array($emplacement), 'numero_emplacement', 'nom');
		$emplacement = array_shift($emplacement);
		$emplacement = MethodArray::affectColumnValueWithOtherColumn(array($emplacement), 'nom_vision_face', 'nom_face');
		$emplacement = array_shift($emplacement);
		$emplacement['selection'] = 'emplacement='.$emplacement['id'];
		
		if ($emplacement['id_reseau_tournant'] != NULL) {
			$emplacements = Loader::get('RimLienClientRim')->renvoi_emplacements_param_reseau_tournant($id_client, $emplacement['id_reseau_tournant']);
			
			$numero_de_face = 0;
			
			foreach($emplacements as $e) {
				$numero_de_face++;
				if ($e['id'] == $emplacement['id']) {
					$emplacement['numero_de_face'] = $numero_de_face;
					break;
				}
			}
			
			$titre = 'Réseau tournant - '.$emplacement['nom_reseau_tournant'];
			$sous_titre = 'Nouvelle programmation de visuels pour la FACE '.$numero_de_face;
		} else {			
			$emplacements = Loader::get('RimLienClientRim')->renvoi_emplacements_param_id_face($id_client, $emplacement['id_face']);
			

			
			$numero_de_face = 0;
				
			foreach($emplacements as $e) {
				$numero_de_face++;
				if ($e['id'] == $emplacement['id']) {
					$emplacement['numero_de_face'] = $numero_de_face;
					break;
				}
			}
			
			$emplacement['numero_mobilier'] = 0 + $emplacement['numero_mobilier'];
			
			$titre = $emplacement['ville_mobilier'].' - RIM No '.$emplacement['numero_mobilier'].' - '.$emplacement['adresse_mobilier'];
			$sous_titre = 'Nouvelle programmation de visuels pour la FACE '.$numero_de_face;
		}
		
		if (Request::hasGetParameter('face')) {
			$bouton_retour_url = 'index.php?page=selection&face='.Request::getGetParameter('face');
			$formulaire_url = 'index.php?page=planning&action=creer&emplacement='.Request::getGetParameter('emplacement').'&face='.Request::getGetParameter('face');
		} else if (Request::hasGetParameter('reseau_tournant')) {
			$bouton_retour_url = 'index.php?page=selection&reseau_tournant='.Request::getGetParameter('reseau_tournant').'&id_contratproduit='.Request::getGetParameter('id_contratproduit');
			$formulaire_url = 'index.php?page=planning&action=creer&emplacement='.Request::getGetParameter('emplacement').'&reseau_tournant='.Request::getGetParameter('reseau_tournant').'&id_contratproduit='.Request::getGetParameter('id_contratproduit');
		} else if (Request::hasGetParameter('emplacement')) {
			$bouton_retour_url = 'index.php?page=emplacement&emplacement='.Request::getGetParameter('emplacement').'&id_contratproduit='.Request::getGetParameter('id_contratproduit');
			$formulaire_url = 'index.php?page=planning&action=creer&emplacement='.Request::getGetParameter('emplacement').'&id_contratproduit='.Request::getGetParameter('id_contratproduit');
		} else {
			header('Location:./');
		}
		
		//gestion du formulaire
		$tableau_cle_message = array();
		$tableau_message = array();
		$formulaire = array();
		$message_ajout_planning_reussi = '';
		$plannings_chevauches = array();
		$message_info = false;
		
		if (Request::hasPostParameter('valider_edition_planning')) {
			//vérification des valeurs utilisateurs
			if (!Request::hasFileParameter('fichier_ecran') || ($fichier_ecran = Request::getFileParameter('fichier_ecran')) == false || $fichier_ecran['tmp_name'] == '') {
				$tableau_cle_message[] = 'formulaire_image_non_renseignee';
			} else {
				$fichier_ecran_info_image = getimagesize($fichier_ecran['tmp_name']);
					
				if (!(in_array($fichier_ecran_info_image['mime'], StaticValue::$type_mime_autorise_image_planning))) {
					$tableau_cle_message[] = 'formulaire_image_type_non_autorise';
				}
					
				if ($fichier_ecran_info_image['mime'] == 'application/x-shockwave-flash') {
					if ($fichier_ecran_info_image[0] / $fichier_ecran_info_image[1] != StaticDbValue::get('longueur_autorisee_image_pub') / StaticDbValue::get('hauteur_autorisee_image_pub')) {
						$tableau_cle_message[] = 'formulaire_dimension_image_pub_non_autorisee';
					}
				} else {
					if ($fichier_ecran_info_image[0] != StaticDbValue::get('longueur_autorisee_image_pub')
					|| $fichier_ecran_info_image[1] != StaticDbValue::get('hauteur_autorisee_image_pub')) {
						$tableau_cle_message[] = 'formulaire_dimension_image_pub_non_autorisee';
					}
				}
					
				if (!($fichier_ecran['size'] <= StaticDbValue::get('taille_maximum_image_pub'))) {
					$tableau_cle_message[] = 'formulaire_taille_image_pub_non_autorisee';
				}
			}
			
			if (Request::hasPostParameter('date_debut') && ($formulaire['date_debut'] = Request::getPostParameter('date_debut')) != '') {
				$formulaire['date_debut'] = implode('-', array_reverse(explode('/', $formulaire['date_debut'])));
		
				if ($strtotime = strtotime($formulaire['date_debut'])) {
					$formulaire['date_debut'] = date('Y-m-d', $strtotime);
				} else {
					$tableau_cle_message[] = 'formulaire_date_debut_erronee';
				}
			} else {
				$tableau_cle_message[] = 'formulaire_date_debut_non_rempli';
			}
		
			if (Request::hasPostParameter('date_fin') && ($formulaire['date_fin'] = Request::getPostParameter('date_fin')) != '') {
				$formulaire['date_fin'] = implode('-', array_reverse(explode('/', $formulaire['date_fin'])));
		
				if ($strtotime = strtotime($formulaire['date_fin'])) {
					$formulaire['date_fin'] = date('Y-m-d', $strtotime);
				} else {
					$tableau_cle_message[] = 'formulaire_date_fin_erronee';
				}
			} else {
				$formulaire['date_fin'] = StaticDbValue::get('max_date');
			}
		
			if (!in_array('formulaire_date_fin_erronee', $tableau_cle_message) && $formulaire['date_debut'] > $formulaire['date_fin']) {
				$tableau_cle_message[] = 'formulaire_date_conflit_debut_fin';
			}
		
			if (Request::hasPostParameter('periodicite') && ($formulaire['periodicite'] = Request::getPostParameter('periodicite')) != '') {
				$formulaire['periodicite'] = '1234560';
			} else if (Request::hasPostParameter('periodicite_jour') && is_array(Request::getPostParameter('periodicite_jour'))) {
				$periodicite_jour = Request::getPostParameter('periodicite_jour');
				sort($periodicite_jour);
				$formulaire['periodicite'] = '';
		
				foreach ($periodicite_jour as $jour) {
					if ($jour >= 1 && $jour <= 6) {
						$formulaire['periodicite'] .= $jour;
					} else if ($jour == 0) {
						continue;
					} else {
						$tableau_cle_message[] = 'formulaire_periodicite_jour_corrompu';
						break;
					}
				}
		
				if (in_array('0', $periodicite_jour)) {
					$formulaire['periodicite'] .= '0';
				}
			} else {
				$formulaire['periodicite'] = '';
				$tableau_cle_message[] = 'formulaire_periodicite_jour_non_rempli';
			}
		
			if (Request::hasPostParameter('heure_debut') && ($formulaire['heure_debut'] = Request::getPostParameter('heure_debut')) != '') {
				if (intval($formulaire['heure_debut']) >= 0 && intval($formulaire['heure_debut']) <= 23) {
					if (intval($formulaire['heure_debut']) < 10) {
						$formulaire['heure_debut'] = '0'.intval($formulaire['heure_debut']).':00:00';
					} else {
						$formulaire['heure_debut'] = intval($formulaire['heure_debut']).':00:00';
					}
				} else {
					$tableau_cle_message[] = 'formulaire_heure_debut_erronee';
				}
			} else {
				$formulaire['heure_debut'] = '';
				$tableau_cle_message[] = 'formulaire_heure_debut_non_rempli';
			}
		
			if (Request::hasPostParameter('heure_fin') && ($formulaire['heure_fin'] = Request::getPostParameter('heure_fin')) != '') {
				if ($formulaire['heure_fin'] == '23:59:59') {
					
				} else if (intval($formulaire['heure_fin']) >= 0 && intval($formulaire['heure_fin']) <= 23) {
					if (intval($formulaire['heure_fin']) < 10) {
						$formulaire['heure_fin'] = '0'.intval($formulaire['heure_fin']).':00:00';
					} else {
						$formulaire['heure_fin'] = intval($formulaire['heure_fin']).':00:00';
					}
				} else {
					$tableau_cle_message[] = 'formulaire_heure_fin_erronee';
				}
			} else {
				$formulaire['heure_fin'] = '';
				$tableau_cle_message[] = 'formulaire_heure_fin_non_rempli';
			}
		
			if (!in_array('formulaire_heure_fin_erronee', $tableau_cle_message)
			&& !in_array('formulaire_heure_fin_non_rempli', $tableau_cle_message)
			&& $formulaire['heure_debut'] >= $formulaire['heure_fin']) {
				$tableau_cle_message[] = 'formulaire_heure_conflit_debut_fin';
			}
		
			if (!in_array('formulaire_heure_debut_erronee', $tableau_cle_message)
			&& !in_array('formulaire_heure_debut_non_rempli', $tableau_cle_message)) {
				$planning_heure_debut_en_seconde = (strtotime('t'.$formulaire['heure_debut']) - strtotime(date('Y-m-d'))) % 86400;
			}
		
			if (Request::hasPostParameter('desactivation') && ($formulaire['desactivation'] = Request::getPostParameter('desactivation')) != '') {
				$formulaire['desactivation'] = 'N';
			} else {
				$formulaire['desactivation'] = 'Y';
			}
			
			$formulaire['id_emplacement'] = $emplacement['id'];
			$formulaire['id'] = '0';
		
			if (count($tableau_cle_message) == 0 && $formulaire['desactivation'] == 'N') {
				$plannings_chevauches = Loader::get('RimPlanning')->renvoi_plannings_chevauches($formulaire);
				$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDate($plannings_chevauches, 'date_debut');
				$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDate($plannings_chevauches, 'date_fin');
				$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDay($plannings_chevauches, 'periodicite', ', ');
				$plannings_chevauches = MethodArray::affectColumnValueWithFrenchSchedule($plannings_chevauches, 'heure_debut', 'heure_fin', 'horaire');
			}
		
			if (count($tableau_cle_message) == 0 && count($plannings_chevauches) == 0) {
				//ajout du planning en bdd
				$tableau_planning_type_mime_autorise_inverse = array_flip(StaticValue::$type_mime_autorise_image_planning);
					
				$planning_type_fichier = $tableau_planning_type_mime_autorise_inverse[$fichier_ecran_info_image['mime']];
					
				$nom_fichier = date('dmYHis').'_'.$id_client;
				
				$chemin_fichier = StaticValue::$chemin_local_image_site.$nom_fichier.'.'.$planning_type_fichier;
				$chemin_fichier2 = StaticValue::$chemin_local_image_site2.$nom_fichier.'.'.$planning_type_fichier;
					
				copy($fichier_ecran['tmp_name'], $chemin_fichier);
				copy($fichier_ecran['tmp_name'], $chemin_fichier2);
					
				$id_ecran = Loader::get('RimEcran')->creer_rim_ecran(array(
					'type_fichier' => $planning_type_fichier,
					'chemin_fichier' => $nom_fichier
				), $id_client);
					
				Loader::get('RimPlanning')->creer_planning($formulaire, $id_ecran);
				
				$message_info = array(
					'type_info' => 'alert-success',
					'titre' => 'Votre programmation a été intégrée.',
					'description' => 'Elle sera effective dans un delai minimum de 2 heures (contrainte technique incompressible)'
				);
				
				foreach ($formulaire as &$element) {
					$element = '';
				}
				
				//envoi mails modification image
				$liste_email = Loader::get('UtilisateurListeDiffussion')->envoi_liste_diffusion('2952');
				
				if ($liste_email) {
					$liste_email = MethodArray::getColumn(array($liste_email), 'liste_email');
					$liste_email = array_shift($liste_email);
					
					if ($planning_type_fichier != 'swf') {
						$affiche = '<img src="'.StaticValue::$adresse_image_site.$nom_fichier.'.'.$planning_type_fichier.'" />';
					} else {
						$affiche = '<a href="'.StaticValue::$adresse_image_site.$nom_fichier.'.'.$planning_type_fichier.'">Voir le fichier flash</a>';
					}
					
					Loader::get('Mailer')->envoi_mail_modification_pub_param_liste_email($liste_email, $client, $affiche);
				}
			} else {
				if (count($tableau_cle_message) > 0) {
					foreach ($tableau_cle_message as $cle_message) {
						$tableau_message[] = StaticValue::$message_cle_valeur[$cle_message];
					}
					
					$message_info = array(
						'type_info' => 'alert-error',
						'titre' => 'Echec!',
						'description' => implode('<br>', $tableau_message)
					);
				} else {
					$tableau_planning_chevauche = array();
					
					foreach ($plannings_chevauches as $planning_chevauche) {
						$tableau_planning_chevauche[] = '- Conflit avec l\'affiche plannifiée du '.$planning_chevauche['date_debut'].' au '.$planning_chevauche['date_fin'].', '.$planning_chevauche['periodicite'].', '.$planning_chevauche['horaire'];
					}
					
					$message_info = array(
						'type_info' => 'alert-error',
						'titre' => 'Echec!',
						'description' => implode('<br>', $tableau_planning_chevauche)
					);
				}
				
				$formulaire = MethodArray::affectColumnValueWithFrenchDate(array($formulaire), 'date_debut', '', '/');
				$formulaire = array_shift($formulaire);
				$formulaire = MethodArray::affectColumnValueWithFrenchDate(array($formulaire), 'date_fin', '', '/');
				$formulaire = array_shift($formulaire);
				
				if ($formulaire['heure_debut'] != '') {
					$formulaire = MethodArray::affectColumnValueWithFrenchHourFormulaire(array($formulaire), 'heure_debut');
					$formulaire = array_shift($formulaire);
				}
				
				if ($formulaire['heure_fin'] != '') {
					$formulaire = MethodArray::affectColumnValueWithFrenchHourFormulaire(array($formulaire), 'heure_fin');
					$formulaire = array_shift($formulaire);
				}
			}
		} else {
			$formulaire['date_debut'] = '';
			$formulaire['date_fin'] = '';
			$formulaire['periodicite'] = '';
			$formulaire['periodicite_jour'] = '';
			$formulaire['heure_debut'] = '';
			$formulaire['heure_fin'] = '';
			$formulaire['desactivation'] = 'N';
		}
		
		return array(
			Loader::get('Template')->set('message2.tpl', array(
				'message_info' => $message_info
			)),
			Loader::get('Template')->set('liste1.tpl', array(
				'titre' => $titre,
				'bouton_retour_libelle' => 'Retour',
				'bouton_retour_url' => $bouton_retour_url,
				'content' => Loader::get('Template')->set('liste1.tpl', array(
					'titre' => $sous_titre,
					'content' => Loader::get('Template')->set('creer_planning.tpl', array(
						'formulaire' => $formulaire,
						'formulaire_url' => $formulaire_url,
						'bouton_annuler_url' => $bouton_retour_url,
						'tableau_planning_type_mime_autorise' => array_keys(StaticValue::$type_mime_autorise_image_planning),
						'longueur_autorisee_image_pub' => StaticDbValue::get('longueur_autorisee_image_pub'),
						'hauteur_autorisee_image_pub' =>  StaticDbValue::get('hauteur_autorisee_image_pub'),
						'taille_maximum_image_pub' => (StaticDbValue::get('taille_maximum_image_pub') / 1024),
						'tableau_correspondance_indice_jour' => StaticValue::$tableau_correspondance_indice_jour
					))
				))
			))
		);
	}
	
	private static function ModificationPlanning() {
		$id_client = StackValue::getValue('id_client');
		$client = Loader::get('Client')->renvoi_client($id_client);
		
		if (!Request::hasGetParameter('planning')) {
			header('Location:./');
		}
		
		$formulaire = Loader::get('RimPlanning')->renvoi_planning($id_client, Request::getGetParameter('planning'));
	
		$formulaire = MethodArray::affectColumnValueWithFrenchDate(array($formulaire), 'date_debut', '', '/');
		$formulaire = array_shift($formulaire);
		$formulaire = MethodArray::affectColumnValueWithFrenchDate(array($formulaire), 'date_fin', '', '/');
		$formulaire = array_shift($formulaire);
		$formulaire = MethodArray::affectColumnValueWithFrenchHourFormulaire(array($formulaire), 'heure_debut');
		$formulaire = array_shift($formulaire);
		$formulaire = MethodArray::affectColumnValueWithFrenchHourFormulaire(array($formulaire), 'heure_fin');
		$formulaire = array_shift($formulaire);
		$formulaire = MethodArray::prefixColumnValue(array($formulaire), 'image', StaticValue::$adresse_image_site);
		$formulaire = array_shift($formulaire);
	
		if ($formulaire) {
			$emplacement = Loader::get('RimLienClientRim')->renvoi_emplacement_param_id_planning($id_client, Request::getGetParameter('planning'));
		} else {
			header('Location:./');
		}
		
		if (!$emplacement) {
			header('Location:./');
		}
		
		if ($emplacement['id_reseau_tournant'] != NULL) {
			$reseau_tournant = Loader::get('LeReseauTournantPere')->renvoi_reseau_tournant($id_client, $emplacement['id_reseau_tournant']);
				
			if (!$reseau_tournant) {
				header('Location:./');
			}
		}
		
		$emplacement = MethodArray::affectColumnValueWithOtherColumn(array($emplacement), 'nom_emplacement', 'numero_emplacement');
		$emplacement = array_shift($emplacement);
		$emplacement = MethodArray::affectColumnValueWithOtherColumn(array($emplacement), 'numero_emplacement', 'nom');
		$emplacement = array_shift($emplacement);
		$emplacement = MethodArray::affectColumnValueWithOtherColumn(array($emplacement), 'nom_vision_face', 'nom_face');
		$emplacement = array_shift($emplacement);
		$emplacement['selection'] = 'emplacement='.$emplacement['id'];
		
		if ($emplacement['id_reseau_tournant'] != NULL) {
			$emplacements = Loader::get('RimLienClientRim')->renvoi_emplacements_param_reseau_tournant($id_client, $emplacement['id_reseau_tournant']);
			
			$numero_de_face = 0;
			
			foreach($emplacements as $e) {
				$numero_de_face++;
				if ($e['id'] == $emplacement['id']) {
					$emplacement['numero_de_face'] = $numero_de_face;
					break;
				}
			}
			
			$titre = 'Réseau tournant - '.$emplacement['nom_reseau_tournant'];
			$sous_titre = 'Modification d\'une programmation de visuels pour la FACE '.$numero_de_face;
		} else {
			$emplacements = Loader::get('RimLienClientRim')->renvoi_emplacements_param_id_face($id_client, $emplacement['id_face']);
			
			$numero_de_face = 0;
			
			foreach($emplacements as $e) {
				$numero_de_face++;
				if ($e['id'] == $emplacement['id']) {
					$emplacement['numero_de_face'] = $numero_de_face;
					break;
				}
			}
			
			$emplacement['numero_mobilier'] = 0 + $emplacement['numero_mobilier'];
			
			$titre = $emplacement['ville_mobilier'].' - RIM No '.$emplacement['numero_mobilier'].' - '.$emplacement['adresse_mobilier'];
			$sous_titre = 'Modification d\'une programmation de visuels pour la FACE '.$numero_de_face;
		}
		
		if (Request::hasGetParameter('face')) {
			$bouton_retour_url = 'index.php?page=selection&face='.Request::getGetParameter('face');
			$formulaire_url = 'index.php?page=planning&action=modifier&planning='.$formulaire['id'].'&face='.Request::getGetParameter('face');
		} else if (Request::hasGetParameter('reseau_tournant')) {
			$bouton_retour_url = 'index.php?page=selection&reseau_tournant='.Request::getGetParameter('reseau_tournant');
			$formulaire_url = 'index.php?page=planning&action=modifier&planning='.$formulaire['id'].'&reseau_tournant='.Request::getGetParameter('reseau_tournant');
		} else if (Request::hasGetParameter('emplacement')) {
			$bouton_retour_url = 'index.php?page=emplacement&emplacement='.Request::getGetParameter('emplacement');
			$formulaire_url = 'index.php?page=planning&action=modifier&planning='.$formulaire['id'].'&emplacement='.Request::getGetParameter('emplacement');
		} else {
			header('Location:./');
		}
		
		//gestion du formulaire
		$tableau_cle_message = array();
		$tableau_message = array();
		$message_ajout_planning_reussi = '';
		$plannings_chevauches = array();
		$message_info = false;
		
		if (Request::hasPostParameter('valider_edition_planning')) {
			if (Request::hasFileParameter('fichier_ecran') && ($fichier_ecran = Request::getFileParameter('fichier_ecran')) != false && $fichier_ecran['tmp_name'] != '') {
				$fichier_ecran_info_image = getimagesize($fichier_ecran['tmp_name']);
					
				if (!(in_array($fichier_ecran_info_image['mime'], StaticValue::$type_mime_autorise_image_planning))) {
					$tableau_cle_message[] = 'formulaire_image_type_non_autorise';
				}
					
				if ($fichier_ecran_info_image['mime'] == 'application/x-shockwave-flash') {
					if ($fichier_ecran_info_image[0] / $fichier_ecran_info_image[1] != StaticDbValue::get('longueur_autorisee_image_pub') / StaticDbValue::get('hauteur_autorisee_image_pub')) {
						$tableau_cle_message[] = 'formulaire_dimension_image_pub_non_autorisee';
					}
				} else {
					if ($fichier_ecran_info_image[0] != StaticDbValue::get('longueur_autorisee_image_pub')
					|| $fichier_ecran_info_image[1] != StaticDbValue::get('hauteur_autorisee_image_pub')) {
						$tableau_cle_message[] = 'formulaire_dimension_image_pub_non_autorisee';
					}
				}
					
				if (!($fichier_ecran['size'] <= StaticDbValue::get('taille_maximum_image_pub'))) {
					$tableau_cle_message[] = 'formulaire_taille_image_pub_non_autorisee';
				}
			} else if (Request::hasPostParameter('oui_modification_affiche') && Request::getPostParameter('oui_modification_affiche') == '1') {
				$tableau_cle_message[] = 'formulaire_pas_de_nouveau_visuel';
				$formulaire['oui_modification_affiche'] = '1';
			}
			
			if (Request::hasPostParameter('date_debut') && ($formulaire['date_debut'] = Request::getPostParameter('date_debut')) != '') {
				$formulaire['date_debut'] = implode('-', array_reverse(explode('/', $formulaire['date_debut'])));
		
				if ($strtotime = strtotime($formulaire['date_debut'])) {
					$formulaire['date_debut'] = date('Y-m-d', $strtotime);
				} else {
					$tableau_cle_message[] = 'formulaire_date_debut_erronee';
				}
			} else {
				$tableau_cle_message[] = 'formulaire_date_debut_non_rempli';
			}
		
			if (Request::hasPostParameter('date_fin') && ($formulaire['date_fin'] = Request::getPostParameter('date_fin')) != '') {
				$formulaire['date_fin'] = implode('-', array_reverse(explode('/', $formulaire['date_fin'])));
		
				if ($strtotime = strtotime($formulaire['date_fin'])) {
					$formulaire['date_fin'] = date('Y-m-d', $strtotime);
				} else {
					$tableau_cle_message[] = 'formulaire_date_fin_erronee';
				}
			} else {
				$formulaire['date_fin'] = StaticDbValue::get('max_date');
			}
			
			if (!in_array('formulaire_date_fin_erronee', $tableau_cle_message) && $formulaire['date_debut'] > $formulaire['date_fin']) {
				$tableau_cle_message[] = 'formulaire_date_conflit_debut_fin';
			}
		
			if (Request::hasPostParameter('periodicite') && ($formulaire['periodicite'] = Request::getPostParameter('periodicite')) != '') {
				$formulaire['periodicite'] = '1234560';
			} else if (Request::hasPostParameter('periodicite_jour') && is_array(Request::getPostParameter('periodicite_jour'))) {
				$periodicite_jour = Request::getPostParameter('periodicite_jour');
				sort($periodicite_jour);
				$formulaire['periodicite'] = '';
		
				foreach ($periodicite_jour as $jour) {
					if ($jour >= 1 && $jour <= 6) {
						$formulaire['periodicite'] .= $jour;
					} else if ($jour == 0) {
						continue;
					} else {
						$tableau_cle_message[] = 'formulaire_periodicite_jour_corrompu';
						break;
					}
				}
		
				if (in_array('0', $periodicite_jour)) {
					$formulaire['periodicite'] .= '0';
				}
			} else {
				$tableau_cle_message[] = 'formulaire_periodicite_jour_non_rempli';
			}
		
			if (Request::hasPostParameter('heure_debut') && ($formulaire['heure_debut'] = Request::getPostParameter('heure_debut')) != '') {
				if (intval($formulaire['heure_debut']) >= 0 && intval($formulaire['heure_debut']) <= 23) {
					if (intval($formulaire['heure_debut']) < 10) {
						$formulaire['heure_debut'] = '0'.intval($formulaire['heure_debut']).':00:00';
					} else {
						$formulaire['heure_debut'] = intval($formulaire['heure_debut']).':00:00';
					}
				} else {
					$tableau_cle_message[] = 'formulaire_heure_debut_erronee';
				}
			} else {
				$tableau_cle_message[] = 'formulaire_heure_debut_non_rempli';
			}
		
			if (Request::hasPostParameter('heure_fin') && ($formulaire['heure_fin'] = Request::getPostParameter('heure_fin')) != '') {
				if ($formulaire['heure_fin'] == '23:59:59') {
						
				} else if (intval($formulaire['heure_fin']) >= 0 && intval($formulaire['heure_fin']) <= 23) {
					if (intval($formulaire['heure_fin']) < 10) {
						$formulaire['heure_fin'] = '0'.intval($formulaire['heure_fin']).':00:00';
					} else {
						$formulaire['heure_fin'] = intval($formulaire['heure_fin']).':00:00';
					}
				} else {
					$tableau_cle_message[] = 'formulaire_heure_fin_erronee';
				}
			} else {
				$tableau_cle_message[] = 'formulaire_heure_fin_non_rempli';
			}
		
			if (!in_array('formulaire_heure_fin_erronee', $tableau_cle_message)
			&& !in_array('formulaire_heure_fin_non_rempli', $tableau_cle_message)
			&& $formulaire['heure_debut'] >= $formulaire['heure_fin']) {
				$tableau_cle_message[] = 'formulaire_heure_conflit_debut_fin';
			}
		
			if (!in_array('formulaire_heure_debut_erronee', $tableau_cle_message)
			&& !in_array('formulaire_heure_debut_non_rempli', $tableau_cle_message)) {
				$planning_heure_debut_en_seconde = (strtotime('t'.$formulaire['heure_debut']) - strtotime(date('Y-m-d'))) % 86400;
			}
		
			if (Request::hasPostParameter('desactivation') && ($formulaire['desactivation'] = Request::getPostParameter('desactivation')) != '') {
				$formulaire['desactivation'] = 'N';
			} else {
				$formulaire['desactivation'] = 'Y';
			}
				
			$formulaire['id_emplacement'] = $emplacement['id'];
		
			if (count($tableau_cle_message) == 0 && $formulaire['desactivation'] == 'N') {
				$plannings_chevauches = Loader::get('RimPlanning')->renvoi_plannings_chevauches($formulaire);
				$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDate($plannings_chevauches, 'date_debut');
				$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDate($plannings_chevauches, 'date_fin');
				$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDay($plannings_chevauches, 'periodicite', ', ');
				$plannings_chevauches = MethodArray::affectColumnValueWithFrenchSchedule($plannings_chevauches, 'heure_debut', 'heure_fin', 'horaire');
			}
		
			if (count($tableau_cle_message) == 0 && count($plannings_chevauches) == 0) {
				if (Request::hasFileParameter('fichier_ecran') && ($fichier_ecran = Request::getFileParameter('fichier_ecran')) != false && $fichier_ecran['tmp_name'] != '') {
					$tableau_planning_type_mime_autorise_inverse = array_flip(StaticValue::$type_mime_autorise_image_planning);
						
					$planning_type_fichier = $tableau_planning_type_mime_autorise_inverse[$fichier_ecran_info_image['mime']];
						
					$nom_fichier = date('dmYHis').'_'.$id_client;
					
					$chemin_fichier = StaticValue::$chemin_local_image_site.$nom_fichier.'.'.$planning_type_fichier;
					$chemin_fichier2 = StaticValue::$chemin_local_image_site2.$nom_fichier.'.'.$planning_type_fichier;
						
					copy($fichier_ecran['tmp_name'], $chemin_fichier);
					copy($fichier_ecran['tmp_name'], $chemin_fichier2);
						
					$id_ecran = Loader::get('RimEcran')->creer_rim_ecran(array(
							'type_fichier' => $planning_type_fichier,
							'chemin_fichier' => $nom_fichier
					), $id_client);
					
					$formulaire['id_ecran'] = $id_ecran;
					
					//envoi mails modification image
					$liste_email = Loader::get('UtilisateurListeDiffussion')->envoi_liste_diffusion('2952');
					
					if ($liste_email) {
						$liste_email = MethodArray::getColumn(array($liste_email), 'liste_email');
						$liste_email = array_shift($liste_email);
							
						if ($planning_type_fichier != 'swf') {
							$affiche = '<img src="'.StaticValue::$adresse_image_site.$nom_fichier.'.'.$planning_type_fichier.'" />';
						} else {
							$affiche = '<a href="'.StaticValue::$adresse_image_site.$nom_fichier.'.'.$planning_type_fichier.'">Voir le fichier flash</a>';
						}
							
						Loader::get('Mailer')->envoi_mail_modification_pub_param_liste_email($liste_email, $client, $affiche);
					}
				}
				
				$modification_reussie = Loader::get('RimPlanning')->modifier_planning($formulaire);
				
				if ($modification_reussie) {
					$formulaire = Loader::get('RimPlanning')->renvoi_planning($id_client, Request::getGetParameter('planning'));
	
					$formulaire = MethodArray::affectColumnValueWithFrenchDate(array($formulaire), 'date_debut', '', '/');
					$formulaire = array_shift($formulaire);
					$formulaire = MethodArray::affectColumnValueWithFrenchDate(array($formulaire), 'date_fin', '', '/');
					$formulaire = array_shift($formulaire);
					$formulaire = MethodArray::affectColumnValueWithFrenchHourFormulaire(array($formulaire), 'heure_debut');
					$formulaire = array_shift($formulaire);
					$formulaire = MethodArray::affectColumnValueWithFrenchHourFormulaire(array($formulaire), 'heure_fin');
					$formulaire = array_shift($formulaire);
					$formulaire = MethodArray::prefixColumnValue(array($formulaire), 'image', StaticValue::$adresse_image_site);
					$formulaire = array_shift($formulaire);
					
					$message_info = array(
						'type_info' => 'alert-success',
						'titre' => 'Votre programmation a été modifiée.',
						'description' => 'Elle sera effective dans un delai minimum de 2 heures (contrainte technique incompressible)'
					);
				}
			} else {
				if (count($tableau_cle_message) > 0) {
					foreach ($tableau_cle_message as $cle_message) {
						$tableau_message[] = StaticValue::$message_cle_valeur[$cle_message];
					}
						
					$message_info = array(
						'type_info' => 'alert-error',
						'titre' => 'Echec!',
						'description' => implode('<br>', $tableau_message)
					);
				} else {
					$tableau_planning_chevauche = array();
						
					foreach ($plannings_chevauches as $planning_chevauche) {
						$tableau_planning_chevauche[] = '- Conflit avec l\'affiche plannifiée du '.$planning_chevauche['date_debut'].' au '.$planning_chevauche['date_fin'].', '.$planning_chevauche['periodicite'].', '.$planning_chevauche['horaire'];
					}
						
					$message_info = array(
						'type_info' => 'alert-error',
						'titre' => 'Echec!',
						'description' => implode('<br>', $tableau_planning_chevauche)
					);
				}
		
				$formulaire = MethodArray::affectColumnValueWithFrenchDate(array($formulaire), 'date_debut', '', '/');
				$formulaire = array_shift($formulaire);
				$formulaire = MethodArray::affectColumnValueWithFrenchDate(array($formulaire), 'date_fin', '', '/');
				$formulaire = array_shift($formulaire);
				$formulaire = MethodArray::affectColumnValueWithFrenchHourFormulaire(array($formulaire), 'heure_debut');
				$formulaire = array_shift($formulaire);
				$formulaire = MethodArray::affectColumnValueWithFrenchHourFormulaire(array($formulaire), 'heure_fin');
				$formulaire = array_shift($formulaire);
			}
		}
		
		return array(
			Loader::get('Template')->set('message2.tpl', array(
				'message_info' => $message_info
			)),
			Loader::get('Template')->set('liste1.tpl', array(
				'titre' => $titre,
				'bouton_retour_libelle' => 'Retour',
				'bouton_retour_url' => $bouton_retour_url,
				'content' => Loader::get('Template')->set('liste1.tpl', array(
					'titre' => $sous_titre,
					'content' => Loader::get('Template')->set('modifier_planning.tpl', array(
						'formulaire' => $formulaire,
						'formulaire_url' => $formulaire_url,
						'bouton_annuler_url' => $bouton_retour_url,
						'tableau_planning_type_mime_autorise' => array_keys(StaticValue::$type_mime_autorise_image_planning),
						'longueur_autorisee_image_pub' => StaticDbValue::get('longueur_autorisee_image_pub'),
						'hauteur_autorisee_image_pub' =>  StaticDbValue::get('hauteur_autorisee_image_pub'),
						'taille_maximum_image_pub' => (StaticDbValue::get('taille_maximum_image_pub') / 1024),
						'tableau_correspondance_indice_jour' => StaticValue::$tableau_correspondance_indice_jour
					))
				))
			))
		);
	}
	
	private static function Layout() {
		$id_client = StackValue::getValue('id_client');
		$client = Loader::get('Client')->renvoi_client($id_client);
	
		return Loader::get('Template')->set('layout1.tpl', array(
			'head' => Loader::get('Template')->set('head.tpl', array(
				'titre_site' => StaticValue::$titre_site
			)),
			'content' => Loader::get('Template')->set('principal.tpl', array(
				'header' => Loader::get('Template')->set('header.tpl', array(
					'titre_site' => StaticValue::$titre_site,
					'client' => $client
				))
			)),
			'script' => Loader::get('Template')->set('script.tpl')
		));
	}
}