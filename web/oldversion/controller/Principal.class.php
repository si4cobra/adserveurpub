<?php
class Principal {
	public static function Index() {
		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'content', self::ListeFace());
		echo $template_layout->draw();
	}
	
	private static function Layout() {
		$id_client = StackValue::getValue('id_client');
		$client = Loader::get('Client')->renvoi_client($id_client);
		
		return Loader::get('Template')->set('layout1.tpl', array(
			'head' => Loader::get('Template')->set('head.tpl', array(
				'titre_site' => StaticValue::$titre_site
			)),
			'content' => Loader::get('Template')->set('principal.tpl', array(
				'header' => Loader::get('Template')->set('header.tpl', array(
					'titre_site' => StaticValue::$titre_site,
					'client' => $client
				))
			)),
			'script' => Loader::get('Template')->set('script.tpl')
		));
	}
	
	private static function ListeFace() {
		//récupération de l'identifiant du client
		$id_client = StackValue::getValue('id_client'); 
		
		//récupération de la liste statique
		$liste_face = Loader::get('RimGestionRim')->renvoi_liste_face($id_client); 
		
		//affectation du nom de la face
		$liste_face = MethodArray::affectColumnValueWithOtherColumn($liste_face, 'nom_vision_face', 'nom_face');
		$liste_face = MethodArray::affectColumnValueWithOtherColumn($liste_face, 'nom_vision_face2', 'nom_face2');
		
		foreach ($liste_face as &$face) {
			if ($face['nom_face2'] != NULL) {
				$face['nom_face'] = $face['nom_face'].'<br>'.$face['nom_face2'];
			}
		}
		
		//récupération des réseaux tournants 
		$liste_reseau_tournant = Loader::get('LeReseauTournantPere')->renvoi_liste_reseau_tournant($id_client);

		//echo"<pre>";print_r($liste_reseau_tournant);echo"</pre>";
		
		//echo"<pre>";print_r($liste_reseau_tournant);echo"</pre>";

		foreach ($liste_reseau_tournant as &$face) {	
		    //echo"<pre>";print_r($face);echo"</pre>";
			$sEmplacement =  Loader::get('LeReseauTournantPere')->posiion_client_rt($face['id_contratproduit'],date('d/m/Y'));
			$face['nom'].="<br>".$sEmplacement;
			//echo $sEmplacement;
		}
		
		$template = array();
		
		if (count($liste_face) > 0) {
			$template[] = Loader::get('Template')->set('liste1.tpl', array(
				'titre' => 'Actuellement vous êtes présent sur les Relais Informations Multimedia (RIM) suivants',
				'content' => Loader::get('Template')->set('liste_face.tpl', array(
					'liste_face' => $liste_face
				))
			));
		}
		
		if (count($liste_reseau_tournant) > 0) {
			$template[] = Loader::get('Template')->set('liste1.tpl', array(
				'titre' => 'Actuellement vous êtes présent sur les réseaux tournants suivants',
				'content' => Loader::get('Template')->set('liste_reseau_tournant.tpl', array(
					'liste_reseau_tournant' => $liste_reseau_tournant
				))
			));
		}
		
		//si ni de face ni de réseau tournant, alors ne rien afficher
		if (count($template) == 0) {
			$template = '';
		}
		
		//génération du template
		return $template;
	}
}