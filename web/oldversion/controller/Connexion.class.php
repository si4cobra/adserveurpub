<?php
class Connexion {
	public static function Index() {
		if (Request::hasPostParameter('log') && Request::hasPostParameter('password')) {
			$resultat = Loader::get('Client')->authentification_login_password(Request::getPostParameter('log'), Request::getPostParameter('password'));
			StackValue::setValue('id_message_connexion', 'login_incorrect');
		}
		//vérification de l'auto-connexion pour la connexion
		else if (Request::hasGetParameter('autoconnect')) {
			$resultat = Loader::get('Client')->authentification_autoconnect(Request::getGetParameter('autoconnect'));
		}
		//vérification du cookie de connexion
		else if (Request::hasCookieParameter('cookie_connect')) {
			$resultat = Loader::get('Client')->authentification_cookie_connect(Request::getCookieParameter('cookie_connect'));
			StackValue::setValue('id_message_connexion', 'connexion_simultanee');
		}
		//pas de tentative de connexion
		else {
			return false;
		}
		
		//test du réponse à la tentative de connexion
		if ($resultat != false) {
			//récupération de l'identifiant du client pour la connexion
			StackValue::setValue('id_client', $resultat['id_client']);
		
			//requete pour voir si le client est actif
			$client_est_actif = Loader::get('Client')->est_actif(StackValue::getValue('id_client'));
		
			//echec de la connexion si le client a été désactivé par les administrateurs
			if ($client_est_actif == false) {
				StackValue::setValue('id_message_connexion', 'client_desactive');
				return false;
			}
		
			//requete pour voir si le client possède des produits listing valides
			$resultat = Loader::get('ContratProduit')->renvoi_nombre_contratproduit_ref_produit_ref_sous_produit(
				StackValue::getValue('id_client'), StaticValue::$ref_produit_autorise, StaticValue::$ref_sous_produit_autorise
			);
				
			//test si le client possède des produits listing valides sur au moins un mobilier actif
			if ($resultat['nombre'] > 0) {
				//connexion validée dans le cas du login et mot de passe avec affectation d'un cookie
				if (Request::hasPostParameter('log') && Request::hasPostParameter('password')) {
					Request::regenerateSessionId();
					Request::setCookieParameter('cookie_connect', Request::getSessionId(), (time() + StaticDbValue::get('temps_duree_cookie_connexion')));
					$resultat = Loader::get('Client')->set_cookie_connect(StackValue::getValue('id_client'), Request::getSessionId());
					return $resultat;
				}
				//connexion validée pour l'auto-connexion avec affectation d'un cookie
				else if (Request::hasGetParameter('autoconnect')) {
					$resultat = Loader::get('Client')->get_cookie_connect(StackValue::getValue('id_client'));
		
					if ($resultat['cookie_connect'] == '' || $resultat['cookie_connect'] == NULL) {
						Request::setCookieParameter('cookie_connect', Request::getSessionId(), (time() + StaticDbValue::get('temps_duree_cookie_connexion')));
						$resultat = Loader::get('Client')->set_cookie_connect(StackValue::getValue('id_client'), Request::getSessionId());
						return $resultat;
					}
					
					Request::setCookieParameter('cookie_connect', $resultat['cookie_connect'], (time() + StaticDbValue::get('temps_duree_cookie_connexion')));
					return true;
				}
				//connexion validée dans le cas d'une connexion par cookie
				else if (Request::hasCookieParameter('cookie_connect')) {
					return true;
				}
			} else {
				StackValue::setValue('id_message_connexion', 'client_sans_pub');
			}
		}
		//suppression du cookie dans le cas de l'échec de la connexion
		else if (Request::hasCookieParameter('cookie_connect')) {
			Request::setCookieParameter('cookie_connect', '', (time() - StaticDbValue::get('temps_duree_cookie_connexion')));
		}
	}
}