<?php
class Selection {
	public static function Index() {
		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'content', self::Select());
		echo $template_layout->draw();
	}

	public static function ActiverPlanning() {
		if (!Request::hasGetParameter('planning')) {
			header('Location:./');
		}

		$id_client = StackValue::getValue('id_client');

		$planning = Loader::get('RimPlanning')->renvoi_planning($id_client, Request::getGetParameter('planning'));

		if (!$planning) {
			header('Location:./');
		}

		$plannings_chevauches = Loader::get('RimPlanning')->renvoi_plannings_chevauches($planning);

		$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_debut');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_fin');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchDay(array($planning), 'periodicite', ', ');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchSchedule(array($planning), 'heure_debut', 'heure_fin', 'horaire');
		$planning = array_shift($planning);

		if (count($plannings_chevauches) == 0) {
			$activation_planning = Loader::get('RimPlanning')->activer_planning(Request::getGetParameter('planning'));

			if ($activation_planning) {
				$message_info = array(
					'type_info' => 'alert-success',
					'titre' => 'Activation!',
					'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', a été activée.'
				);
			} else {
				$message_info = array(
					'type_info' => 'alert-error',
					'titre' => 'Erreur!',
					'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', n\'a pas pu être activée. Veuillez réessayer plus tard.'
				);
			}
		} else {
			$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDate($plannings_chevauches, 'date_debut');
			$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDate($plannings_chevauches, 'date_fin');
			$plannings_chevauches = MethodArray::affectColumnValueWithFrenchDay($plannings_chevauches, 'periodicite', ', ');
			$plannings_chevauches = MethodArray::affectColumnValueWithFrenchSchedule($plannings_chevauches, 'heure_debut', 'heure_fin', 'horaire');

			$description = array();

			foreach ($plannings_chevauches as $planning_chevauche) {
				$description[] = '- Conflit avec l\'affiche planifiée '.$planning_chevauche['date_debut'].' au '.$planning_chevauche['date_fin'].', '.$planning_chevauche['periodicite'].', '.$planning_chevauche['horaire'];
			}

			$description = implode('<br>', $description);

			$message_info = array(
				'type_info' => 'alert-error',
				'titre' => 'Erreur!',
				'description' => $description
			);
		}

		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'content', array(
			Loader::get('Template')->set('message2.tpl', array(
				'message_info' => $message_info
			)),
			self::Select()
		));
		echo $template_layout->draw();
	}

	public static function DesactiverPlanning() {
		if (!Request::hasGetParameter('planning')) {
			header('Location:./');
		}

		$id_client = StackValue::getValue('id_client');

		$planning = Loader::get('RimPlanning')->renvoi_planning($id_client, Request::getGetParameter('planning'));

		if (!$planning) {
			header('Location:./');
		}

		$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_debut');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_fin');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchDay(array($planning), 'periodicite', ', ');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchSchedule(array($planning), 'heure_debut', 'heure_fin', 'horaire');
		$planning = array_shift($planning);

		$desactivation_planning = Loader::get('RimPlanning')->desactiver_planning(Request::getGetParameter('planning'));

		if ($desactivation_planning) {
			$message_info = array(
				'type_info' => 'alert-info',
				'titre' => 'Désactivation!',
				'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', a été désactivée.'
			);
		} else {
			$message_info = array(
				'type_info' => 'alert-error',
				'titre' => 'Erreur!',
				'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', n\'a pas pu être désactivée. Veuillez réessayer plus tard.'
			);
		}

		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'content', array(
			Loader::get('Template')->set('message2.tpl', array(
				'message_info' => $message_info
			)),
			self::Select()
		));
		echo $template_layout->draw();
	}

	public static function SupprimerPlanning() {
		if (!Request::hasGetParameter('planning')) {
			header('Location:./');
		}

		$id_client = StackValue::getValue('id_client');

		$planning = Loader::get('RimPlanning')->renvoi_planning($id_client, Request::getGetParameter('planning'));

		if (!$planning) {
			header('Location:./');
		}

		$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_debut');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchDate(array($planning), 'date_fin');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchDay(array($planning), 'periodicite', ', ');
		$planning = array_shift($planning);
		$planning = MethodArray::affectColumnValueWithFrenchSchedule(array($planning), 'heure_debut', 'heure_fin', 'horaire');
		$planning = array_shift($planning);

		$suppression_planning = Loader::get('RimPlanning')->supprimer_planning(Request::getGetParameter('planning'));

		if ($suppression_planning) {
			$message_info = array(
				'type_info' => 'alert-info',
				'titre' => 'Suppression!',
				'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', a été supprimée.'
			);
		} else {
			$message_info = array(
				'type_info' => 'alert-error',
				'titre' => 'Erreur!',
				'description' => 'l\'affiche planifiée '.$planning['date_debut'].' au '.$planning['date_fin'].', '.$planning['periodicite'].', '.$planning['horaire'].', n\'a pas pu être supprimée. Veuillez réessayer plus tard.'
			);
		}

		$template_layout = self::Layout();
		$template_layout->add(array('content'), 'content', array(
			Loader::get('Template')->set('message2.tpl', array(
				'message_info' => $message_info
			)),
			self::Select()
		));
		echo $template_layout->draw();
	}

	private static function Layout() {
		$id_client = StackValue::getValue('id_client');
		$client = Loader::get('Client')->renvoi_client($id_client);

		return Loader::get('Template')->set('layout1.tpl', array(
			'head' => Loader::get('Template')->set('head.tpl', array(
				'titre_site' => StaticValue::$titre_site
			)),
			'content' => Loader::get('Template')->set('principal.tpl', array(
				'header' => Loader::get('Template')->set('header.tpl', array(
					'titre_site' => StaticValue::$titre_site,
					'client' => $client
				))
			)),
			'script' => Loader::get('Template')->set('script.tpl')
		));
	}

	private static function Select() {
		$id_client = StackValue::getValue('id_client');

		if (Request::hasGetParameter('face')) {
			$face = Loader::get('RimGestionRim')->renvoi_face($id_client, Request::getGetParameter('face'));

			if (!$face) {
				header('Location:./');
			}

			//affectation du nom de la face
			$face = MethodArray::affectColumnValueWithOtherColumn(array($face), 'nom_vision_face', 'nom_face');
			$face = array_shift($face);
			$face = MethodArray::affectColumnValueWithOtherColumn(array($face), 'nom_vision_face2', 'nom_face2');
			$face = array_shift($face);

			//affectation du numero du mobilier
			$face['numero_mobilier'] = 0 + $face['numero_mobilier'];

			$titre = $face['ville'].' - RIM No '.$face['numero_mobilier'].' - '.$face['adresse_mobilier'];

			$nombre_emplacement = $face['nombre_emplacement'];

			$emplacements = Loader::get('RimLienClientRim')->renvoi_emplacements_param_id_face($id_client, Request::getGetParameter('face'));

			//echo"<pre>";print_r($emplacements);echo"</pre>";

			$emplacements = MethodArray::affectColumnValueWithOtherColumn($emplacements, 'nom_emplacement', 'numero_emplacement');
			$emplacements = MethodArray::affectColumnValueWithOtherColumn($emplacements, 'numero_emplacement', 'nom');

			if ($nombre_emplacement == 0) {
				$info = 'Vous disposez d\'aucune face sur ce RIM';
			} else if ($nombre_emplacement == 1) {
				$info = 'Vous disposez d\'une face sur ce RIM';
			} else if ($nombre_emplacement > 1) {
				$info = 'Vous disposez de '.$nombre_emplacement.' faces sur ce RIM';
			} else {
				header('Location:./');
			}
		} else if (Request::hasGetParameter('reseau_tournant')) {
			$reseau_tournant = Loader::get('LeReseauTournantPere')->renvoi_reseau_tournant($id_client, Request::getGetParameter('reseau_tournant'));

			if (!$reseau_tournant) {
				header('Location:./');
			}

			$titre = 'Réseau tournant - '.$reseau_tournant['nom'];

			$nombre_emplacement = $reseau_tournant['nombre_emplacement'];

			$emplacements = Loader::get('RimLienClientRim')->renvoi_emplacements_param_reseau_tournant2($id_client, Request::getGetParameter('reseau_tournant'),Request::getGetParameter('id_contratproduit'));

			//echo"<pre>";print_r($emplacements);echo"</pre>";

			if ($nombre_emplacement == 0) {
				$info = 'Vous disposez d\'aucune face sur ce réseau tournant';
			} else if ($nombre_emplacement == 1) {
				$info = 'Vous disposez d\'une face sur le réseau tournant';
			} else if ($nombre_emplacement > 1) {
				$info = 'Vous disposez de '.$nombre_emplacement.' faces sur le réseau tournant';
			} else {
				header('Location:./');
			}
		} else {
			header('Location:./');
		}

		$numero_de_face = 0;

		foreach ($emplacements as &$emplacement) {
			$emplacement['numero_de_face'] = ++$numero_de_face;
			if (isset($emplacement['id_face'])) {
				$emplacement['selection'] = 'face='.$emplacement['id_face'];
			} else if (isset($emplacement['id_reseau_tournant'])) {
				$emplacement['selection'] = 'reseau_tournant='.$emplacement['id_reseau_tournant'];
			} else {
				header('Location:./');
			}
		}

		$tableau_id_emplacement = MethodArray::getColumn($emplacements, 'id');

		$affiches_tampons = Loader::get('ContratProduit')->renvoi_affiches_tampons_param_tableau_id_emplacement($tableau_id_emplacement);

		if ($affiches_tampons) {
			$affiches_tampons = MethodArray::changeKeyByColumn($affiches_tampons, 'id_emplacement');
			$affiches_tampons = MethodArray::prefixColumnValue($affiches_tampons, 'image', StaticValue::$adresse_image_site);
			//$affiche_tampon['extension'] = 'no_swf';
			foreach ($affiches_tampons as &$affiche_tampon) {

				//echo $affiche_tampon['image'];

				$fichier_ecran_info_image = getimagesize($affiche_tampon['image']);

				if ($fichier_ecran_info_image['mime'] == 'application/x-shockwave-flash') {
					$affiche_tampon['extension'] = 'swf';
				} else {
					$affiche_tampon['extension'] = 'no_swf';
				}
			}
			unset($affiche_tampon);
		}

		$plannings_actuels = Loader::get('RimPlanning')->renvoi_plannings_param_tableau_id_emplacement_date($tableau_id_emplacement, date('Y-m-d'), date('w'), date('H:i:s'));

		//echo"<pre>";print_r($plannings_actuels);echo"</pre>";

		if ($plannings_actuels) {
			$plannings_actuels = MethodArray::changeKeyByColumn($plannings_actuels, 'id_emplacement');
			$plannings_actuels = MethodArray::prefixColumnValue($plannings_actuels, 'image', StaticValue::$adresse_image_site);
			$plannings_actuels = MethodArray::affectColumnValueWithFrenchDate($plannings_actuels, 'date_debut');
			$plannings_actuels = MethodArray::affectColumnValueWithFrenchDate($plannings_actuels, 'date_fin');
			$plannings_actuels = MethodArray::affectColumnValueWithFrenchDay($plannings_actuels, 'periodicite');
			$plannings_actuels = MethodArray::affectColumnValueWithFrenchSchedule($plannings_actuels, 'heure_debut', 'heure_fin', 'horaire');
		}

		foreach ($emplacements as &$emplacement) {
			if (isset($affiches_tampons[$emplacement['id']])) {
				$emplacement['affiche_tampon'] = $affiches_tampons[$emplacement['id']];
			}
			if (isset($plannings_actuels[$emplacement['id']])) {
				$emplacement['planning_actuel'] = $plannings_actuels[$emplacement['id']];
			}
			$emplacement['class_icone'] = 'icon-file';
		}

		$info_fin = '*validée par notre service technique, votre affiche tampon est diffusée par défaut si vous ne programmez pas d\'autres visuels';

		//echo"<pre>";print_r($emplacements);echo"</pre>";

		return Loader::get('Template')->set('liste1.tpl', array(
			'titre' => $titre,
			'bouton_retour_libelle' => 'Retour',
			'bouton_retour_url' => './',
			'content' => array(
				Loader::get('Template')->set('info_face_reseau_tournant.tpl', array(
					'info' => $info
				)),



				//Loader::get('TemplateList')->set('liste2.tpl', $emplacements),
				Loader::get('TemplateList')->set('liste2.tpl', $emplacements),
				Loader::get('Template')->set('info_fin_face_reseau_tournant.tpl', array(
					'info' => $info_fin
				)),
			)
		));
	}
}