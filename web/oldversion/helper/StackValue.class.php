<?php
class StackValue {
	private static $values = array();
	
	public static function hasValue($title) {
		return isset(self::$values[$title]);
	}
	
	public static function getValue($title) {
		if (self::hasValue($title)) {
			return self::$values[$title];
		}
		
		return false;
	}
	
	public static function setValue($title, $value) {
		self::$values[$title] = $value;
	}
	
	public static function addValue($title, $value, $value_key) {
		self::$values[$title][$value_key] = $value;
	}
	
	public static function deleteValue($title, $value_key = false) {
		if (!$value_key) {
			if (self::hasValue($title)) {
				unset(self::$values[$title]);
				
				return true;
			}
			
			return false;
		}
		
		if (isset(self::$values[$title][$value_key])) {
			unset(self::$values[$title][$value_key]);
			
			return true;
		}
		
		return false;
	}
}