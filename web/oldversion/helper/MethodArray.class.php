<?php
class MethodArray {
	public static function getColumn($array, $column_name) {
		$table = array();
		
		if (is_array($array)) {
			foreach ($array as $value) {
				$table[] = $value[$column_name];
			}
		}
		
		return array_unique($table);
	}
	
	public static function changeKeyByColumn($array, $column_name) {
		if (is_array($array)) {
			$table = array();
			
			foreach ($array as $value) {
				$table[$value[$column_name]] = $value;
			}
			
			return $table;
		}
		
		return $array;
	}
	
	public static function affectTableUnderTable($array, $group_under_array_name, $under_array, $column_name) {
		foreach ($under_array as $key => $value) {
			$array[$value[$column_name]][$group_under_array_name][$key] = $value;
		}
		
		return $array;
	}
	
	public static function affectColumnValueWithOtherColumn($array, $column_name, $column_affected_name) {
		if (is_array($array)) {
			foreach ($array as &$value) {
				if ($value[$column_name] != '' && $value[$column_name] != NULL) {
					$value[$column_affected_name] = $value[$column_name];
				}
			}
			unset($value);
			reset($array);
		}
		
		return $array;
	}
	
	public static function frenchDate($date, $max_date_fin_value = 'indéterminée', $separator = '-') {
		$date = strtotime($date);
		
		if ($date) {
			$date = date('d'.$separator.'m'.$separator.'Y', $date);
			
			if ($date == date('d'.$separator.'m'.$separator.'Y', strtotime(StaticDbValue::get('max_date')))) {
				$date = $max_date_fin_value;
			}
		} else {
			$date = '';
		}
		
		return $date;
	}
	
	public static function frenchHour($time) {
		if ($time != '23:59:59') {
			$time = date('H', strtotime($time)).'h';
		} else {
			$time = '0h';
		}
		
		return $time;
	}
	
	public static function affectColumnValueWithFrenchDate($array, $column_name, $max_date_fin_value = 'indéterminée', $separator = '-') {
		if(is_array($array)){
			foreach ($array as &$value) {
				$value[$column_name] = self::frenchDate($value[$column_name], $max_date_fin_value, $separator);
			}
			unset($value);
		}
		
		return $array;
	}
	
	public static function affectColumnValueWithFrenchHour($array, $column_name) {
		if(is_array($array)){
			foreach ($array as &$value) {
				$value[$column_name] = self::frenchHour($value[$column_name]);
			}
			unset($value);
		}
		
		return $array;
	}
	
	public static function affectColumnValueWithFrenchHourFormulaire($array, $column_name) {
		if(is_array($array)){
			foreach ($array as &$value) {
				if ($value[$column_name] != '23:59:59') {
					$value[$column_name] = intval($value[$column_name] < 10) ? '0'.intval($value[$column_name]) : intval($value[$column_name]);
				}
			}
			unset($value);
		}
	
		return $array;
	}
	
	public static function affectColumnValueWithFrenchSchedule($array, $heure_debut, $heure_fin, $column_name) {

		if(is_array($array)){
			foreach ($array as &$value) {
				if ($value[$heure_debut] == '00:00:00' && $value[$heure_fin] == '23:59:59') {
					$value[$column_name] = 'Toute la journée';
				} else {
					$value[$column_name] = self::frenchHour($value[$heure_debut]).' à '.self::frenchHour($value[$heure_fin]);
				}
			}
			unset($value);
		}
		
		return $array;
	}
	
	public static function affectColumnValueWithFrenchDay($array, $column_name, $separator = '<br>') {
		if(is_array($array)){
			foreach ($array as $value) {
				$tableau_periodicite = str_split($value[$column_name]);
				
				if (count($tableau_periodicite) == 7) {
					$value[$column_name] = 'Tous les jours';
				} else {
					$tableau_jour = array();
					foreach ($tableau_periodicite as $indice) {
						$tableau_jour[] = StaticValue::$tableau_correspondance_indice_jour[$indice];
					}
					
					$value[$column_name] = implode($separator, $tableau_jour);
				}
			}
			unset($value);
		}
		
		return $array;
	}
	
	public static function prefixColumnValue($array, $column_name, $prefix, $suffix = '') {
		
		if(is_array($array)){


			//echo"<pre>";print_r($array);echo"</pre>";
			foreach ($array as &$value) {
				$value[$column_name] = $prefix.$value[$column_name].$suffix;
			}
			unset($value);
		}	
		
		return $array;
	}
	
	public static function addColumnWithStaticValue($array, $column_name, $static_value) {
		foreach ($array as &$value) {
			$value[$column_name] = $static_value;
		}
		unset($value);
		
		return $array;
	}
}