<?php
class Mailer extends PHPMailer {
	public function envoi_mail_modification_pub_param_liste_email($liste_email, $client, $affiche) {
		if ($liste_email) {
			$emails = explode(';', $liste_email);
			
			foreach ($emails as $email) {
				if ($this->Email_Is_Valide($email)) {
					$this->envoi_mail_html(
						$email,
						'Le client "'.$client['nom'].'" numéro "'.$client['id'].'" a créé/modifié une nouvelle affiche ADSERVER PUB<br>'.$affiche,
						'[ADSERVER][PUB][NOUVELLE AFFICHE] client "'.$client['nom'].'" numéro "'.$client['id'].'"',
						'adserver@cobranaja.com'
					);
				}
			}
		}
	}
	
	private function Email_Is_Valide($email) {
		$regex = "/^([~._a-z0-9-]+[~._a-z0-9-]*)@(([a-z0-9-]+\.)*([a-z0-9-]+)(\.[a-z]{2,3}))$/";
		return preg_match($regex, $email);
	}
	
	private function envoi_mail_html($adresse, $message, $sujet, $from) {
		$this->IsHTML(true);
		$this->From=$from;
		$this->AddAddress($adresse);
		$this->AddReplyTo($from);
		$this->Subject=$sujet;
		$this->Body=$message;
		$this->Send();
		$this->SmtpClose();
		$this->ClearAddresses();
	}
	
	public function envoi_mail_erreur_code($liste_email, $client, $erreur) {
		if ($liste_email) {
			$emails = explode(';', $liste_email);
				
			foreach ($emails as $email) {
				if ($this->Email_Is_Valide($email)) {
					$this->envoi_mail_html(
						$email,
						
						'[ADSERVER PUB]'.
						'<br>'.
						$erreur.
						'<br>'.
						'Client:<br>'.
						'&nbsp;&nbsp;Identifiant: '.$client['id'].'<br>'.
						'&nbsp;&nbsp;Nom: '.$client['nom'].'<br>'.
						'&nbsp;&nbsp;URL: <a href="http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'">http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'</a><br>',
							
						'[ADSERVER][PUB][ERREUR CODE]http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
							
						'adserver@cobranaja.com'
					);
				}
			}
		}
	}
}