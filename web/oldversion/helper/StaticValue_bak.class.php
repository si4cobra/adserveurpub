<?php
class StaticValue {
	public static $ref_produit_autorise = 'cprim_0001';
	public static $ref_sous_produit_autorise = 'option_ad_serveur_pub';
	public static $titre_site = 'AD-SERVEUR PUB';
	public static $adresse_image_site = 'https://www.adserveur-naja.fr/AdserveurPub/repimages/';
	public static $adresse_image_site2 = 'http://dircommairie.infoscommerces.mobi/repimages/';
	public static $chemin_local_image_site = '/home/3652088152/www/web/AdserveurPub/repimages/';
	public static $chemin_local_image_site2 = '/home/infoscommerces.mobi/dircommairie/web/repimages/';
	public static $type_mime_autorise_image_planning = array('png' => 'image/png', 'jpg' => 'image/jpeg', 'swf' => 'application/x-shockwave-flash','gif'=>'image/gif');
	public static $tableau_correspondance_indice_jour = array('1' => 'Lundi', '2' => 'Mardi', '3' => 'Mercredi', '4' => 'Jeudi', '5' => 'Vendredi', '6' => 'Samedi', '0' => 'Dimanche');
	public static $message_cle_valeur = array(
		'login_incorrect' => 'Votre nom d\'utilisateur et mot de passe sont incorrects.',
		'client_desactive' => 'Votre compte client a été bloqué.',
		'connexion_simultanee' => 'Connexion simultanée, vous avez été déconnecté.',
		'client_sans_pub' => 'Vous n\'avez pas de produit ADSERVER PUB.',
		'deconnexion' => 'Déconnexion réussie.',
		'formulaire_image_non_renseignee' => 'Veuillez renseigner votre affiche pour le planning.',
		'formulaire_image_type_non_autorise' => 'Le type de l\'image n\'est pas autorisé.', 
		'formulaire_dimension_image_pub_non_autorisee' => 'Les dimensions de l\'image ne sont pas autorisées.',
		'formulaire_taille_image_pub_non_autorisee' => 'Le poids de l\'image est trop important. Veuillez le réduire.',
		'formulaire_date_debut_non_rempli' => 'La date de début n\'a pas été renseignée.',
		'formulaire_date_debut_erronee' => 'La date de début est erronée. Veuillez retaper la date de début correctement.',
		'formulaire_date_fin_erronee' => 'La date de fin est erronée. Veuillez retaper la date de fin correctement.',
		'formulaire_date_conflit_debut_fin' => 'La date de fin est antérieure à la date de début.',
		'formulaire_periodicite_jour_corrompu' => 'La périodicité a été corrompue. Veuillez remplir le formulaire correctement.',
		'formulaire_periodicite_jour_non_rempli' => 'La périodicité n\'a pas été renseignée.',
		'formulaire_heure_debut_non_rempli' => 'L\'heure de début n\'a pas été renseignée.',
		'formulaire_heure_fin_non_rempli' => 'L\'heure de fin n\'a pas été renseignée.',
		'formulaire_heure_debut_erronee' => 'L\'heure de début est erronée. Veuillez retaper la date de début correctement.',
		'formulaire_heure_fin_erronee' => 'L\'heure de fin est erronée. Veuillez retaper la date de fin correctement.',
		'formulaire_heure_conflit_debut_fin' => 'L\'heure de fin est antérieure ou égale à l\'heure de début.',
		'formulaire_pas_de_nouveau_visuel' => 'Pas de nouveau visuel sélectionné'
	);
}