<?php
class RimGestionRim {
	function renvoi_face_par_mobilier($tableau_id_face) {
		return Query::fetch("
			SELECT DISTINCT
				rg.id_rim AS id,
				rg.nom_rim AS nom,
				rg.vision_rim AS nom_vision,
				rm.id_mobilier AS id_mobilier
			FROM rim_gestionrim rg
			JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_rim = rg.id_rim)
			JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_lerim = rlr.id_lerim)
			JOIN ric_mobilier rm ON (rm.supplogique_mobilier = 'N' AND rm.id_mobilier = rl.id_mobilier)
			WHERE rg.supplogique_rim = 'N'
			AND rg.id_rim IN (".implode(', ', $tableau_id_face).")
		");
	}
	
	function renvoi_face_mobilier_param_id_face($id_client, $id_face) {
	    
		return Query::fetch("
			SELECT DISTINCT
				rg.id_rim AS id,
				rg.nom_rim AS nom,
				rg.vision_rim AS nom_vision,
				rm.id_mobilier AS id_mobilier,
				rm.numero_mobilier AS numero_mobilier,
				rm.adresse_mobilier AS adresse_mobilier,
				rm.codepostal_mobillier AS codepostal_mobilier,
				v.ville AS ville_mobilier
			FROM rim_gestionrim rg
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_rim = rg.id_rim)
			JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_rim = rg.id_rim)
			JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_lerim = rlr.id_lerim)
			JOIN ric_mobilier rm ON (rm.supplogique_mobilier = 'N' AND rm.id_mobilier = rl.id_mobilier)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			LEFT JOIN ville_cedex v ON (v.supplogique_ville = 'N' AND v.id_code_postal = rm.id_ville)
			WHERE rg.supplogique_rim = 'N'
			AND rg.id_rim = '".$id_face."'
		");
	}
	
	public function renvoi_liste_face($id_client) {
	   
	    $requete = "SELECT DISTINCT
				v.ville AS ville,
				rm.numero_mobilier AS numero_mobilier,
				rm.adresse_mobilier AS adresse_mobilier,
				rg.vision_rim AS nom_vision_face,
				rg.nom_rim AS nom_face,
				count(rlc.id_lienclientrim) AS nombre_emplacement,
				nbrfacepub_mobilier AS nombre_max_emplacement,
				rg.id_rim AS id,
				rg2.nom_rim AS nom_face2,
				rg2.vision_rim AS nom_vision_face2,
				cp.id_contratproduit as id_cp
			FROM rim_gestionrim rg
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_rim = rg.id_rim)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_rim = rg.id_rim)
			JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_lerim = rlr.id_lerim)
			JOIN ric_mobilier rm ON (rm.supplogique_mobilier = 'N' AND rm.id_mobilier = rl.id_mobilier)
			LEFT JOIN ville_cedex v ON (v.supplogique_ville = 'N' AND v.id_code_postal = rm.id_ville)
			LEFT JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			LEFT JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement)
			LEFT JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			LEFT JOIN rim_lienrimface rlr2 ON (rlr2.supplogique_lienrimface = 'N' AND rlr2.id_lerim = rl.id_lerim)
			LEFT JOIN rim_gestionrim rg2 ON (rg2.supplogique_rim = 'N' AND rm.inversion_mobilier = 'Y' AND rg2.id_rim = rlr2.id_rim)
			WHERE rg.supplogique_rim = 'N'
			AND (rg2.id_rim IS NULL OR rg2.id_rim != rg.id_rim)
			AND lrt.id_reseau_tournant IS NULL and rlc.id_emplacement != 0
			GROUP BY v.ville, rm.numero_mobilier, rg.vision_rim, rg.nom_rim
			ORDER BY v.ville, rm.numero_mobilier, rg.vision_rim, rg.nom_rim";

	    //echo"<pre>";print_r($requete);echo"</pre>";
	    
		return Query::fetch($requete);
	}
	
	public function renvoi_face($id_client, $id_face) {

		$sql ="
			SELECT
				v.ville AS ville,
				rm.numero_mobilier AS numero_mobilier,
				rl.adrese_lerim AS adresse_mobilier,
				rg.vision_rim AS nom_vision_face,
				rg.nom_rim AS nom_face,
				rg.id_rim AS id,
				count(DISTINCT rlc.id_lienclientrim) AS nombre_emplacement,
				rg2.nom_rim AS nom_face2,
				rg2.vision_rim AS nom_vision_face2,
				v.ville AS ville
			FROM rim_gestionrim rg
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_rim = rg.id_rim)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_rim = rg.id_rim)
			JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_lerim = rlr.id_lerim)
			JOIN ric_mobilier rm ON (rm.supplogique_mobilier = 'N' AND rm.id_mobilier = rl.id_mobilier)
			LEFT JOIN ville_cedex v ON (v.supplogique_ville = 'N' AND v.id_code_postal = rm.id_ville)
			LEFT JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			LEFT JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement)
			LEFT JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			LEFT JOIN rim_lienrimface rlr2 ON (rlr2.supplogique_lienrimface = 'N' AND rlr2.id_lerim = rl.id_lerim)
			LEFT JOIN rim_gestionrim rg2 ON (rg2.supplogique_rim = 'N' AND rm.inversion_mobilier = 'Y' AND rg2.id_rim = rlr2.id_rim)
			WHERE rg.supplogique_rim = 'N'
			AND (rg2.id_rim IS NULL OR rg2.id_rim != rg.id_rim)
			AND rg.id_rim = '".$id_face."'
			AND lrt.id_reseau_tournant IS NULL
			GROUP BY rg.id_rim";

			//echo $sql;

		return Query::fetchOne($sql);
	}
}