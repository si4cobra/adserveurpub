<?php
class RimLienClientRim {
	function renvoi_emplacement_par_face($id_client) {
		return Query::fetch("
			SELECT DISTINCT
				rlc.id_lienclientrim AS id,
				rlc.id_contratproduit AS nom,
				e.nom_emplacment AS nom_emplacement,
				rg.id_rim AS id_face
			FROM rim_lienclientrim rlc
			JOIN rim_gestionrim rg ON (rg.supplogique_rim = 'N' AND rg.id_rim = rlc.id_rim)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			LEFT JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			LEFT JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement)
			LEFT JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			WHERE rlc.supplogique_lienclientrim = 'N'
			AND lrt.id_reseau_tournant IS NULL
			ORDER BY rlc.id_lienclientrim
		");
	}
	
	function renvoi_emplacement_par_reseau_tournant($id_client) {
		return Query::fetch("
			SELECT DISTINCT
				rlc.id_lienclientrim AS id,
				rlc.id_contratproduit AS nom,
				e.nom_emplacment AS nom_emplacement,
				lrt.id_reseau_tournant AS id_reseau_tournant
			FROM rim_lienclientrim rlc
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement)
			JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			WHERE rlc.supplogique_lienclientrim = 'N'
			ORDER BY rlc.id_lienclientrim
		");
	}
	
	function renvoi_emplacement_face_mobilier_param_id_emplacement($id_client, $id_emplacement) {
		return Query::fetchOne("
			SELECT DISTINCT
				rlc.id_lienclientrim AS id,
				rlc.id_contratproduit AS nom,
				e.nom_emplacment AS nom_emplacement,
				rg.id_rim AS id_face,
				rg.nom_rim AS nom_face,
				rg.vision_rim AS nom_vision_face,
				rm.id_mobilier AS id_mobilier,
				rm.numero_mobilier AS numero_mobilier,
				rm.adresse_mobilier AS adresse_mobilier,
				rm.codepostal_mobillier AS codepostal_mobilier,
				v.ville AS ville_mobilier
			FROM rim_lienclientrim rlc
			JOIN rim_gestionrim rg ON (rg.supplogique_rim = 'N' AND rg.id_rim = rlc.id_rim)
			JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_rim = rg.id_rim)
			JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_lerim = rlr.id_lerim)
			JOIN ric_mobilier rm ON (rm.supplogique_mobilier = 'N' AND rm.id_mobilier = rl.id_mobilier)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			LEFT JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			LEFT JOIN ville_cedex v ON (v.supplogique_ville = 'N' AND v.id_code_postal = rm.id_ville)
			WHERE rlc.supplogique_lienclientrim = 'N'
			AND rlc.id_lienclientrim = '".$id_emplacement."'
		");
	}
	
	function renvoi_emplacement_param_id_emplacement($id_client, $id_emplacement) {
		return Query::fetchOne("
			SELECT DISTINCT
				rlc.id_lienclientrim AS id,
				rlc.id_contratproduit AS nom,
				e.nom_emplacment AS nom_emplacement,
				e.numero_emplacement AS numero_emplacement,
				rg.id_rim AS id_face,
				rg.nom_rim AS nom_face,
				rg.vision_rim AS nom_vision_face,
				rm.id_mobilier AS id_mobilier,
				rm.numero_mobilier AS numero_mobilier,
				rl.adrese_lerim AS adresse_mobilier,
				rm.codepostal_mobillier AS codepostal_mobilier,
				v.ville AS ville_mobilier,
				lrtp.id_reseau_tournant_pere AS id_reseau_tournant,
				lrtp.nom_reseau_tournant_pere AS nom_reseau_tournant,
				lre.position_lien_emplacement AS position_dans_reseau_tournant,
				rg2.nom_rim AS nom_face2,
				rg2.vision_rim AS nom_vision_face2
			FROM rim_lienclientrim rlc
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			LEFT JOIN rim_gestionrim rg ON (rg.supplogique_rim = 'N' AND rg.id_rim = rlc.id_rim)
			LEFT JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_rim = rg.id_rim)
			LEFT JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_lerim = rlr.id_lerim)
			LEFT JOIN ric_mobilier rm ON (rm.supplogique_mobilier = 'N' AND rm.id_mobilier = rl.id_mobilier)
			LEFT JOIN ville_cedex v ON (v.supplogique_ville = 'N' AND v.id_code_postal = rm.id_ville)
			LEFT JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			LEFT JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement)
			LEFT JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			LEFT JOIN le_reseau_tournant_pere lrtp ON (lrtp.supplogique_le_reseau_tournant_pere = 'N' AND lrtp.id_reseau_tournant_pere = lrt.id_reseau_tournant_pere)
			LEFT JOIN rim_lienrimface rlr2 ON (rlr2.supplogique_lienrimface = 'N' AND rlr2.id_lerim = rl.id_lerim)
			LEFT JOIN rim_gestionrim rg2 ON (rg2.supplogique_rim = 'N' AND rm.inversion_mobilier = 'Y' AND rg2.id_rim = rlr2.id_rim)
			WHERE rlc.supplogique_lienclientrim = 'N'
			AND (rg2.id_rim IS NULL OR rg2.id_rim != rg.id_rim)
			AND rlc.id_lienclientrim = '".$id_emplacement."'
		");
	}
	
	function renvoi_emplacements_param_id_face($id_client, $id_face) {
		return Query::fetch("
			SELECT DISTINCT
				rlc.id_lienclientrim AS id,
				rlc.id_contratproduit AS nom,
				e.nom_emplacment AS nom_emplacement,
				e.numero_emplacement AS numero_emplacement,
				rg.id_rim AS id_face
			FROM rim_lienclientrim rlc
			JOIN rim_gestionrim rg ON (rg.supplogique_rim = 'N' AND rg.id_rim = '".$id_face."' AND rg.id_rim = rlc.id_rim)
			JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_rim = rg.id_rim)
			JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_lerim = rlr.id_lerim)
			JOIN ric_mobilier rm ON (rm.supplogique_mobilier = 'N' AND rm.id_mobilier = rl.id_mobilier)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			LEFT JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			WHERE rlc.supplogique_lienclientrim = 'N'
		");
	}
	
	function renvoi_emplacements_param_reseau_tournant($id_client, $id_reseau_tournant) {
		return Query::fetch("
			SELECT DISTINCT
				rlc.id_lienclientrim AS id,
				lrtp.id_reseau_tournant_pere AS id_reseau_tournant,
				rlc.id_contratproduit as id_contratproduit
			FROM rim_lienclientrim rlc
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement)
			JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			JOIN le_reseau_tournant_pere lrtp ON (lrtp.supplogique_le_reseau_tournant_pere = 'N' AND lrtp.id_reseau_tournant_pere = '".$id_reseau_tournant."' AND lrtp.id_reseau_tournant_pere = lrt.id_reseau_tournant_pere)
			WHERE rlc.supplogique_lienclientrim = 'N'
			ORDER BY rlc.id_lienclientrim
		");
	}

	function renvoi_emplacements_param_reseau_tournant2($id_client,$id_reseau_tournant,$id_contratproduit)
    {

        /*echo"<pre>";print_r("
			SELECT DISTINCT
				rlc.id_lienclientrim AS id,
				lrtp.id_reseau_tournant_pere AS id_reseau_tournant,
				rlc.id_contratproduit as id_contratproduit
			FROM rim_lienclientrim rlc
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement)
			JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			JOIN le_reseau_tournant_pere lrtp ON (lrtp.supplogique_le_reseau_tournant_pere = 'N' AND lrtp.id_reseau_tournant_pere = '".$id_reseau_tournant."' AND lrtp.id_reseau_tournant_pere = lrt.id_reseau_tournant_pere)
			WHERE rlc.supplogique_lienclientrim = 'N' and rlc.id_contratproduit = $id_contratproduit
			ORDER BY rlc.id_lienclientrim
		");echo"</pre>";*/

        return Query::fetch("
			SELECT DISTINCT
				rlc.id_lienclientrim AS id,
				lrtp.id_reseau_tournant_pere AS id_reseau_tournant,
				rlc.id_contratproduit as id_contratproduit
			FROM rim_lienclientrim rlc
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement)
			JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			JOIN le_reseau_tournant_pere lrtp ON (lrtp.supplogique_le_reseau_tournant_pere = 'N' AND lrtp.id_reseau_tournant_pere = '".$id_reseau_tournant."' AND lrtp.id_reseau_tournant_pere = lrt.id_reseau_tournant_pere)
			WHERE rlc.supplogique_lienclientrim = 'N' and rlc.id_contratproduit = $id_contratproduit
			ORDER BY rlc.id_lienclientrim
		");
    }

	function renvoi_emplacement_param_id_planning($id_client, $id_planning) {
		return Query::fetchOne("
			SELECT DISTINCT
				rlc.id_lienclientrim AS id,
				rlc.id_contratproduit AS nom,
				e.nom_emplacment AS nom_emplacement,
				e.numero_emplacement AS numero_emplacement,
				rg.id_rim AS id_face,
				rg.nom_rim AS nom_face,
				rg.vision_rim AS nom_vision_face,
				rm.id_mobilier AS id_mobilier,
				rm.numero_mobilier AS numero_mobilier,
				rl.adrese_lerim AS adresse_mobilier,
				rm.codepostal_mobillier AS codepostal_mobilier,
				v.ville AS ville_mobilier,
				lrtp.id_reseau_tournant_pere AS id_reseau_tournant,
				lrtp.nom_reseau_tournant_pere AS nom_reseau_tournant,
				lre.position_lien_emplacement AS position_dans_reseau_tournant,
				rg2.nom_rim AS nom_face2,
				rg2.vision_rim AS nom_vision_face2
			FROM rim_lienclientrim rlc
			JOIN rim_planning rp ON (rp.supplogique_planning = 'N' AND rp.id_planning = '".$id_planning."' AND rp.id_lienclientrim = rlc.id_lienclientrim)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			LEFT JOIN rim_gestionrim rg ON (rg.supplogique_rim = 'N' AND rg.id_rim = rlc.id_rim)
			LEFT JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_rim = rg.id_rim)
			LEFT JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_lerim = rlr.id_lerim)
			LEFT JOIN ric_mobilier rm ON (rm.supplogique_mobilier = 'N' AND rm.id_mobilier = rl.id_mobilier)
			LEFT JOIN ville_cedex v ON (v.supplogique_ville = 'N' AND v.id_code_postal = rm.id_ville)
			LEFT JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = rlc.id_emplacement)
			LEFT JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_emplacement = e.id_emplacement)
			LEFT JOIN le_reseau_tournant lrt ON (lrt.supplogique_reseau_tournant = 'N' AND lrt.id_reseau_tournant = lre.id_reseau_tournant)
			LEFT JOIN le_reseau_tournant_pere lrtp ON (lrtp.supplogique_le_reseau_tournant_pere = 'N' AND lrtp.id_reseau_tournant_pere = lrt.id_reseau_tournant_pere)
			LEFT JOIN rim_lienrimface rlr2 ON (rlr2.supplogique_lienrimface = 'N' AND rlr2.id_lerim = rl.id_lerim)
			LEFT JOIN rim_gestionrim rg2 ON (rg2.supplogique_rim = 'N' AND rm.inversion_mobilier = 'Y' AND rg2.id_rim = rlr2.id_rim)
			WHERE rlc.supplogique_lienclientrim = 'N'
			AND (rg2.id_rim IS NULL OR rg2.id_rim != rg.id_rim)
		");
	}
}