<?php
class RimPlanning {
	public function renvoi_plannings_param_id_emplacement($id_client, $id_emplacement) {
		return Query::fetch("
			SELECT
				rp.id_planning AS id,
				rp.datedebut_planning AS date_debut,
				rp.datefin_planning AS date_fin,
				rp.periodicitejoursemaine_planning AS periodicite,
				rp.desactivation_planning AS desactivation,
				rp.heuredebut_planning AS heure_debut,
				rp.heurefin_planning AS heure_fin,
				re.cheminfichier_ecran AS image,
				tf.extension_typefichier AS extension,
				rlc.id_lienclientrim AS id_emplacement
			FROM rim_planning rp
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_lienclientrim = ".$id_emplacement." AND rlc.id_lienclientrim = rp.id_lienclientrim)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN rim_ecran re ON (re.supplogique_ecran = 'N' AND re.id_ecran = rp.id_ecran AND re.id_client = c.id_client)
			JOIN typefichier tf ON (tf.supplogique_typefichier = 'N' AND tf.id_typefichier = re.id_typefichier)
			WHERE rp.supplogique_planning = 'N'
			ORDER BY rp.datedebut_planning DESC, rp.datefin_planning, rp.periodicitejoursemaine_planning, rp.heuredebut_planning, rp.heurefin_planning
		");
	}
	
	public function renvoi_plannings_param_id_face($id_client, $id_face) {
		return Query::fetch("
			SELECT
				rp.id_planning AS id,
				rp.datedebut_planning AS date_debut,
				rp.datefin_planning AS date_fin,
				rp.periodicitejoursemaine_planning AS periodicite,
				rp.desactivation_planning AS desactivation,
				rp.heuredebut_planning AS heure_debut,
				rp.heurefin_planning AS heure_fin,
				re.cheminfichier_ecran AS image,
				tf.extension_typefichier AS extension_image,
				rlc.id_lienclientrim AS id_emplacement
			FROM rim_planning rp
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_lienclientrim = rp.id_lienclientrim)
			JOIN rim_gestionrim rg ON (rg.supplogique_rim = 'N' AND rg.id_rim = '".$id_face."' AND rg.id_rim = rlc.id_rim)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN rim_ecran re ON (re.supplogique_ecran = 'N' AND re.id_ecran = rp.id_ecran AND re.id_client = c.id_client)
			JOIN typefichier tf ON (tf.supplogique_typefichier = 'N' AND tf.id_typefichier = re.id_typefichier)
			WHERE rp.supplogique_planning = 'N'
			ORDER BY rp.datedebut_planning, rp.datefin_planning, rp.periodicitejoursemaine_planning, rp.heuredebut_planning, rp.heurefin_planning
		");
	}
	
	function renvoi_planning($id_client, $id_planning) {
		return Query::fetchOne("
			SELECT
				rp.id_planning AS id,
				rp.datedebut_planning AS date_debut,
				rp.datefin_planning AS date_fin,
				rp.periodicitejoursemaine_planning AS periodicite,
				rp.desactivation_planning AS desactivation,
				rp.heuredebut_planning AS heure_debut,
				rp.heurefin_planning AS heure_fin,
				rlc.id_lienclientrim AS id_emplacement,
				re.id_ecran AS id_ecran,
				re.cheminfichier_ecran AS image,
				tf.extension_typefichier AS extension
			FROM rim_planning rp
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_lienclientrim = rp.id_lienclientrim)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN rim_ecran re ON (re.supplogique_ecran = 'N' AND re.id_ecran = rp.id_ecran AND re.id_client = c.id_client)
			JOIN typefichier tf ON (tf.supplogique_typefichier = 'N' AND tf.id_typefichier = re.id_typefichier)
			WHERE rp.supplogique_planning = 'N'
			AND rp.id_planning = '".$id_planning."'
		");
	}
	
	function renvoi_plannings_chevauches($planning) {
		return Query::fetch("
			SELECT
				rp.id_planning AS id,
				rp.datedebut_planning AS date_debut,
				rp.datefin_planning AS date_fin,
				rp.periodicitejoursemaine_planning AS periodicite,
				rp.heuredebut_planning AS heure_debut,
				rp.heurefin_planning AS heure_fin
			FROM rim_planning rp
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_lienclientrim = '".$planning['id_emplacement']."' AND rlc.id_lienclientrim = rp.id_lienclientrim)
			WHERE rp.supplogique_planning = 'N'
			AND rp.desactivation_planning = 'N'
			AND (('".$planning['date_debut']."' >= rp.datedebut_planning AND '".$planning['date_debut']."' <= rp.datefin_planning)
			OR ('".$planning['date_fin']."' >= rp.datedebut_planning AND '".$planning['date_fin']."' <= rp.datefin_planning)
			OR ('".$planning['date_debut']."' <= rp.datedebut_planning AND '".$planning['date_fin']."' >= rp.datedebut_planning)
			OR ('".$planning['date_debut']."' <= rp.datefin_planning AND '".$planning['date_fin']."' >= rp.datefin_planning))
			AND rp.periodicitejoursemaine_planning regexp '[".$planning['periodicite']."]'
			AND (('".$planning['heure_debut']."' >= rp.heuredebut_planning AND '".$planning['heure_debut']."' < rp.heurefin_planning)
			OR ('".$planning['heure_fin']."' > rp.heuredebut_planning AND '".$planning['heure_fin']."' <= rp.heurefin_planning)
			OR ('".$planning['heure_debut']."' <= rp.heuredebut_planning AND '".$planning['heure_fin']."' > rp.heuredebut_planning)
			OR ('".$planning['heure_debut']."' < rp.heurefin_planning AND '".$planning['heure_fin']."' >= rp.heurefin_planning))
			AND rp.id_planning != '".$planning['id']."'
		");
	}
	
	function activer_planning($planning) {
		return Query::fetchupdate("
			UPDATE rim_planning rp
			SET
				rp.datedebut_planning = '".$planning['date_debut']."',
				rp.datefin_planning = '".$planning['date_fin']."',
				rp.desactivation_planning = 'N',
				rp.datemodif_planning = NOW()
			WHERE rp.id_planning = '".$planning['id']."'
		");
	}
	
	function desactiver_planning($id_planning) {
		return Query::fetchupdate("
			UPDATE rim_planning rp
			SET
				rp.desactivation_planning = 'Y',
				rp.datemodif_planning = NOW()
			WHERE rp.id_planning = '".$id_planning."'
		");
	}
	
	function supprimer_planning($id_planning) {
		return Query::fetchupdate("
			UPDATE rim_planning rp
			SET
				rp.supplogique_planning = 'Y',
				rp.datemodif_planning = NOW()
			WHERE rp.id_planning = '".$id_planning."'
		");
	}
	
	function creer_planning($planning, $id_ecran) {

		$sQL="INSERT INTO rim_planning (
				id_planning,
				id_ecran,
				id_periodicite,
				id_rim,
				id_lienclientrim,
				datedebut_planning,
				datefin_planning,
				supplogique_planning,
				periodicitejoursemaine_planning,
				periodiciteheurprecise_planning,
				periodiciteheure_planning,
				nomdeecran_planning,
				typeperiodicite_planning,
				jouraffichage_planning,
				tempsaffichage_planning,
				tempsecouler_planning,
				verouiller_planning,
				desactivation_planning,
				datemodif_planning,
				position_planning,
				heuredebut_planning,
				heurefin_planning
			) VALUES (
				'',
				'".$id_ecran."',
				'',
				'',
				'".$planning['id_emplacement']."',
				'".$planning['date_debut']."',
				'".$planning['date_fin']."',
				'N',
				'".$planning['periodicite']."',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'N',
				'".$planning['desactivation']."',
				NOW(),
				'',
				'".$planning['heure_debut']."',
				'".$planning['heure_fin']."'
			)";
		
		//echo $sQL."<br>";
		//exit();

		return Query::fetchid($sQL);
	}
	
	function modifier_planning($planning) {
		return Query::fetchupdate("
			UPDATE rim_planning rp
			SET
				rp.datedebut_planning = '".$planning['date_debut']."',
				rp.datefin_planning = '".$planning['date_fin']."',
				rp.periodicitejoursemaine_planning = '".$planning['periodicite']."',
				rp.desactivation_planning = '".$planning['desactivation']."',
				rp.heuredebut_planning = '".$planning['heure_debut']."',
				rp.heurefin_planning = '".$planning['heure_fin']."',
				rp.id_ecran = '".$planning['id_ecran']."',
				rp.datemodif_planning = NOW()
			WHERE rp.id_planning = '".$planning['id']."'
		");
	}
	
	function renvoi_planning_param_date($id_emplacement, $date, $periodicite, $heure) {
		$requete_sql = "
			SELECT
				rp.id_planning AS id,
				re.cheminfichier_ecran AS image,
				tf.extension_typefichier AS extension
			FROM rim_planning rp
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_lienclientrim = '".$id_emplacement."' AND rlc.id_lienclientrim = rp.id_lienclientrim)
			JOIN rim_ecran re ON (re.supplogique_ecran = 'N' AND re.id_ecran = rp.id_ecran)
			JOIN typefichier tf ON (tf.supplogique_typefichier = 'N' AND tf.id_typefichier = re.id_typefichier)
			WHERE rp.supplogique_planning = 'N'
			AND rp.desactivation_planning = 'N'
			AND '".$date."' >= rp.datedebut_planning AND '".$date."' <= rp.datefin_planning
			AND rp.periodicitejoursemaine_planning regexp '[".$periodicite."]'
			AND '".$heure."' >= rp.heuredebut_planning AND '".$heure."' < rp.heurefin_planning";
		
		$reponse_requete = mysql_query($requete_sql, $this->link);
		
		$reponse = mysql_fetch_array($reponse_requete);
		
		if ($reponse) {
			$reponse['image'] = $this->adresse_image.$reponse['image'];
		}
		
		return $reponse;
	}
	
	function renvoi_plannings_param_tableau_id_emplacement_date($tableau_id_emplacement, $date, $periodicite, $heure) {
		return Query::fetch("
			SELECT
				rp.id_planning AS id,
				rp.datedebut_planning AS date_debut,
				rp.datefin_planning AS date_fin,
				rp.periodicitejoursemaine_planning AS periodicite,
				rp.desactivation_planning AS desactivation,
				rp.heuredebut_planning AS heure_debut,
				rp.heurefin_planning AS heure_fin,
				re.cheminfichier_ecran AS image,
				tf.extension_typefichier AS extension,
				rlc.id_lienclientrim AS id_emplacement
			FROM rim_planning rp
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_lienclientrim IN (".implode(', ', $tableau_id_emplacement).") AND rlc.id_lienclientrim = rp.id_lienclientrim)
			JOIN rim_ecran re ON (re.supplogique_ecran = 'N' AND re.id_ecran = rp.id_ecran)
			JOIN typefichier tf ON (tf.supplogique_typefichier = 'N' AND tf.id_typefichier = re.id_typefichier)
			WHERE rp.supplogique_planning = 'N'
			AND rp.desactivation_planning = 'N'
			AND '".$date."' >= rp.datedebut_planning AND '".$date."' <= rp.datefin_planning
			AND rp.periodicitejoursemaine_planning regexp '[".$periodicite."]'
			AND '".$heure."' >= rp.heuredebut_planning AND '".$heure."' < rp.heurefin_planning
			GROUP BY rlc.id_lienclientrim
		");
	}
}