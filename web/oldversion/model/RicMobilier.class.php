<?php
class RicMobilier {
	function renvoi_mobilier($tableau_id_mobilier) {
		return Query::fetch("
			SELECT DISTINCT
				rm.id_mobilier AS id,
				rm.numero_mobilier AS numero,
				rm.adresse_mobilier AS adresse,
				rm.codepostal_mobillier AS codepostal,
				v.ville AS ville
			FROM ric_mobilier rm
			JOIN ville_cedex v ON (v.supplogique_ville = 'N' AND v.id_code_postal = rm.id_ville)
			WHERE rm.supplogique_mobilier = 'N'
			AND rm.id_mobilier IN (".implode(', ', $tableau_id_mobilier).")
			ORDER BY rm.numero_mobilier
		");
	}
	
	function renvoi_mobilier_param_id_mobilier($id_mobilier) {
		return Query::fetch("
			SELECT DISTINCT
				rm.id_mobilier AS id,
				rm.numero_mobilier AS numero,
				rm.adresse_mobilier AS adresse,
				rm.codepostal_mobillier AS codepostal,
				v.ville AS ville
			FROM ric_mobilier rm
			JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_mobilier = rm.id_mobilier)
			JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_lerim = rl.id_lerim)
			JOIN rim_gestionrim rg ON (rg.supplogique_rim = 'N' AND rg.id_rim = rlr.id_rim)
			JOIN ville_cedex v ON (v.supplogique_ville = 'N' AND v.id_code_postal = rm.id_ville)
			WHERE rm.supplogique_mobilier = 'N'
			AND rm.id_mobilier = '".$id_mobilier."'
		");
	}
}