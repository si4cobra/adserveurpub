<?php
class LeReseauTournant {
	public function renvoi_liste_reseau_tournant($id_client) {
		return Query::fetch("
			SELECT DISTINCT
				lrt.id_reseau_tournant AS id,
				lrt.nom_reseau_tournant AS nom,
				count(rlc.id_lienclientrim) AS nombre_emplacement
			FROM le_reseau_tournant lrt
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_reseau_tournant = lrt.id_reseau_tournant)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = lre.id_emplacement)
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_emplacement = e.id_emplacement)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			WHERE lrt.supplogique_reseau_tournant = 'N'
			GROUP BY lrt.id_reseau_tournant
			ORDER BY lrt.nom_reseau_tournant
		");
	}
	
	public function renvoi_reseau_tournant($id_client, $id_reseau_tournant) {
		return Query::fetchOne("
			SELECT DISTINCT
				lrt.id_reseau_tournant AS id,
				lrt.nom_reseau_tournant AS nom,
				lrt.datedebut_reseau_tournant AS date_debut,
				lrt.periodicite_reseau_tournant AS periodicite_en_jour,
				count(rlc.id_lienclientrim) AS nombre_emplacement
			FROM le_reseau_tournant lrt
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_reseau_tournant = lrt.id_reseau_tournant)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = lre.id_emplacement)
			JOIN rim_lienclientrim rlc ON (rlc.supplogique_lienclientrim = 'N' AND rlc.id_emplacement = e.id_emplacement)
			JOIN contratproduit cp ON (cp.supplogique_contratpeoduit = 'N' AND cp.actif_contratproduit = 'Y' AND cp.id_contratproduit = rlc.id_contratproduit)
			JOIN produit p ON (p.supplogique_produit = 'N' AND p.ref_produit = '".StaticValue::$ref_produit_autorise."' AND p.id_produit = cp.id_produit)
			JOIN contratproduit cp2 ON (cp2.supplogique_contratpeoduit = 'N' AND cp2.actif_contratproduit = 'Y' AND cp2.idparent_contratproduit = cp.id_contratproduit)
			JOIN produit p2 ON (p2.supplogique_produit = 'N' AND p2.ref_produit = '".StaticValue::$ref_sous_produit_autorise."' AND p2.id_produit = cp2.id_produit)
			JOIN contrat co ON (co.supplogique_contrat = 'N' AND co.id_contrat = cp.id_contrat AND co.id_contrat = cp2.id_contrat)
			JOIN client c ON (c.supplogique_client = 'N' AND c.id_client = '".$id_client."' AND c.id_client = co.id_client)
			WHERE lrt.supplogique_reseau_tournant = 'N'
			AND lrt.id_reseau_tournant = '".$id_reseau_tournant."'
			GROUP BY lrt.id_reseau_tournant
		");
	}
	
	public function renvoi_mobiliers_position_dans_reseau_tournant($id_reseau_tournant) {
		return Query::fetch("
			SELECT DISTINCT
				lre.position_lien_emplacement AS position_dans_reseau_tournant,
				v.ville AS ville,
				v.code_postal AS code_postal,
				rm.numero_mobilier AS numero_mobilier,
				e.numero_emplacement AS numero_emplacement,
				e.nom_emplacment AS nom_emplacement
			FROM le_reseau_tournant lrt
			JOIN lien_resau_emplacement lre ON (lre.supplogique_lien_emplacement = 'N' AND lre.id_reseau_tournant = lrt.id_reseau_tournant)
			JOIN rim_emplacement e ON (e.suplogique_emplacement = 'N' AND e.id_emplacement = lre.id_emplacement)
			JOIN rim_gestionrim rg ON (rg.supplogique_rim = 'N' AND rg.id_rim = e.id_rim)
			JOIN rim_lienrimface rlr ON (rlr.supplogique_lienrimface = 'N' AND rlr.id_rim = rg.id_rim)
			JOIN rim_lerim rl ON (rl.supplogique_lerim = 'N' AND rl.id_lerim = rlr.id_lerim)
			JOIN ric_mobilier rm ON (rm.supplogique_mobilier = 'N' AND rm.id_mobilier = rl.id_mobilier)
			JOIN ville_cedex v ON (v.supplogique_ville = 'N' AND v.id_code_postal = rm.id_ville)
			WHERE lrt.supplogique_reseau_tournant = 'N'
			AND lrt.id_reseau_tournant = '".$id_reseau_tournant."'
			ORDER BY lre.position_lien_emplacement ASC
		");
	}
}