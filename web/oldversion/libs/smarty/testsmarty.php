<?// charge la librairie Smarty
require("Smarty.class.php");
// le fichier setup.php est un bon
// endroit pour charger les fichiers
// de librairies de l�application et vous pouvez
// faire cela juste ici. Par exemple :
// require(�livredor/livredor.lib.php�);
class testsmarty extends Smarty {
function testsmarty() {
// Constructeur de la classe. Appel� automatiquement
// � l�instanciation de la classe.
$this->Smarty();
$this->template_dir = "templates/";
$this->compile_dir = "templates_c/";
$this->config_dir = "configs/";
$this->cache_dir = "cache/";
$this->caching = false;
$this->assign("app_name","Guest Book");
}
}
?>