<?php
define('FPDF_FONTPATH','font/');
require('fpdf.php');

class PDF extends FPDF
{

var $secteur;
var $rue;
var $bafficheentete;
var $angle=0;

function SetSecteur($nomsecteur){
	$this->secteur=$nomsecteur;
}


function SetRue($nomrue){
	$this->rue=$nomrue;
}

function SetAffiche($baffiche){
	$this->bafficheentete=$baffiche;
}

function Header()
{
 
 if($this->bafficheentete){
    $this->SetFont('Arial','',10);
	$this->Cell(70,5,$this->secteur);
	$this->Cell(30,5,"Date d�but : ");
	$this->Cell(30,5,"",1,0);
	$this->Cell(30,5,"",0,0);
	$this->Cell(30,5,"Date fin : ");
	$this->Cell(30,5,"",1,0);
	$this->Ln();
	$this->Ln(); 
	$this->Cell(190,5,"");
	$this->Cell(10,5,"1: vu");
	$this->Cell(30,5,"2: argumentation");
	$this->Cell(20,5,"3: sign�");
	$this->Cell(20,5,"4: � revoir");
	$this->Ln(10);
	//$this->Cell(200,10,$this->rue);
	}	
}

/*function AcceptPageBreak(){
	$this->Cell(200,10,"saut");
	$this->Ln();
}*/
 
 

function EAN13($x,$y,$barcode,$h=16,$w=.35)
{
	$this->Barcode($x,$y,$barcode,$h,$w,13);
}

function UPC_A($x,$y,$barcode,$h=16,$w=.35)
{
	$this->Barcode($x,$y,$barcode,$h,$w,12);
}

function GetCheckDigit($barcode)
{
	//Calcule le chiffre de contr�le
	$sum=0;
	for($i=1;$i<=11;$i+=2)
		$sum+=3*$barcode{$i};
	for($i=0;$i<=10;$i+=2)
		$sum+=$barcode{$i};
	$r=$sum%10;
	if($r>0)
		$r=10-$r;
	return $r;
}

function TestCheckDigit($barcode)
{
	//V�rifie le chiffre de contr�le
	$sum=0;
	for($i=1;$i<=11;$i+=2)
		$sum+=3*$barcode{$i};
	for($i=0;$i<=10;$i+=2)
		$sum+=$barcode{$i};
	return ($sum+$barcode{12})%10==0;
}

function Barcode($x,$y,$barcode,$h,$w,$len)
{
	//Ajoute des 0 si n�cessaire
	$barcode=str_pad($barcode,$len-1,'0',STR_PAD_LEFT);
	if($len==12)
		$barcode='0'.$barcode;
	//Ajoute ou teste le chiffre de contr�le
	if(strlen($barcode)==12)
		$barcode.=$this->GetCheckDigit($barcode);
	elseif(!$this->TestCheckDigit($barcode))
		$this->Error('Incorrect check digit');
	//Convertit les chiffres en barres
	$codes=array(
		'A'=>array(
			'0'=>'0001101','1'=>'0011001','2'=>'0010011','3'=>'0111101','4'=>'0100011',
			'5'=>'0110001','6'=>'0101111','7'=>'0111011','8'=>'0110111','9'=>'0001011'),
		'B'=>array(
			'0'=>'0100111','1'=>'0110011','2'=>'0011011','3'=>'0100001','4'=>'0011101',
			'5'=>'0111001','6'=>'0000101','7'=>'0010001','8'=>'0001001','9'=>'0010111'),
		'C'=>array(
			'0'=>'1110010','1'=>'1100110','2'=>'1101100','3'=>'1000010','4'=>'1011100',
			'5'=>'1001110','6'=>'1010000','7'=>'1000100','8'=>'1001000','9'=>'1110100')
		);
	$parities=array(
		'0'=>array('A','A','A','A','A','A'),
		'1'=>array('A','A','B','A','B','B'),
		'2'=>array('A','A','B','B','A','B'),
		'3'=>array('A','A','B','B','B','A'),
		'4'=>array('A','B','A','A','B','B'),
		'5'=>array('A','B','B','A','A','B'),
		'6'=>array('A','B','B','B','A','A'),
		'7'=>array('A','B','A','B','A','B'),
		'8'=>array('A','B','A','B','B','A'),
		'9'=>array('A','B','B','A','B','A')
		);
	$code='101';
	$p=$parities[$barcode{0}];
	for($i=1;$i<=6;$i++)
		$code.=$codes[$p[$i-1]][$barcode{$i}];
	$code.='01010';
	for($i=7;$i<=12;$i++)
		$code.=$codes['C'][$barcode{$i}];
	$code.='101';
	//Dessine les barres
	for($i=0;$i<strlen($code);$i++)
	{
		if($code{$i}=='1')
			$this->Rect($x+$i*$w,$y,$w,$h,'F');
	}
	//Imprime le texte sous le code-barres
	$this->SetFont('','',8);
	$this->Text($x,$y+$h+11/$this->k,substr($barcode,-$len));
}

function Pointille($x1=0,$y1=0,$x2=210,$y2=297,$epaisseur=0.4,$nbPointilles=10)
    {
        $this->SetLineWidth($epaisseur);
        $longueur=abs($x1-$x2);
        $hauteur=abs($y1-$y2);
        if($longueur>$hauteur) {
            $Pointilles=($longueur/$nbPointilles)/2; // taille des pointilles
        }
        else {
            $Pointilles=($hauteur/$nbPointilles)/2;
        }
        for($i=$x1;$i<=$x2;$i+=$Pointilles+$Pointilles) {
            for($j=$i;$j<=($i+$Pointilles);$j++) {
                if($j<=($x2-1)) {
                    $this->Line($j,$y1,$j+1,$y1); // on trace le pointill� du haut, point par point
                    $this->Line($j,$y2,$j+1,$y2); // on trace le pointill� du bas, point par point
                }
            }
        }
        for($i=$y1;$i<=$y2;$i+=$Pointilles+$Pointilles) {
            for($j=$i;$j<=($i+$Pointilles);$j++) {
                if($j<=($y2-1)) {
                    $this->Line($x1,$j,$x1,$j+1); // on trace le pointill� du haut, point par point
                    $this->Line($x2,$j,$x2,$j+1); // on trace le pointill� du bas, point par point
                }
            }
        }
    }
	
	function trace_carre($iTaillecarre){	
		$this->Cell($iTaillecarre,$iTaillecarre,'',1,0);
		$this->Cell(1,$iTaillecarre,'',0,0);
	}
    
	function trace_saisie($iTaillecarre,$ilongueur){
	    
		$iX = $this->GetX();
		$iY = $this->GetY();
		$this->Line($iX,$iY+$iTaillecarre,$iX+$ilongueur,$iY+$iTaillecarre) ;
		$this->Line($iX,$iY,$iX,$iY+$iTaillecarre) ;
		for($k=5;$k<$ilongueur;$k=$k+5){
			$this->Line($iX+$k,$iY+2,$iX+$k,$iY+$iTaillecarre) ;
		}
		$this->Line($iX+$ilongueur,$iY,$iX+$ilongueur,$iY+$iTaillecarre) ;
	
	}
	
//function dessiner caract�re suivant produit
function caractere_produit($iX,$iY,$iproduit){

    if($iproduit=="1"){
		$this->Circle($iX+42,$iY+5,4,'F');
		$this->Circle($iX+42+75,$iY+5,4,'F');
		$this->Circle($iX+42+150,$iY+5,4,'F');
	}
	if($iproduit=="2"){
		$this->Rect($iX+40,$iY+5,6,6,'F');
		$this->Rect($iX+40+75,$iY+5,6,6,'F');
		$this->Rect($iX+40+150,$iY+5,6,6,'F');
    }
	if($iproduit=="3"){
		$this->Polygon(array($iX+40,$iY+10,$iX+48,$iY+10,$iX+44,$iY+2),'FD');
		$this->Polygon(array($iX+40+75,$iY+10,$iX+48+75,$iY+10,$iX+44+75,$iY+2),'FD');
		$this->Polygon(array($iX+40+150,$iY+10,$iX+48+150,$iY+10,$iX+44+150,$iY+2),'FD');
	
	}
}	

function Polygon($points, $style='D')
{
    //Draw a polygon
    if($style=='F')
        $op='f';
    elseif($style=='FD' or $style=='DF')
        $op='b';
    else
        $op='s';

    $h = $this->h;
    $k = $this->k;

    $points_string = '';
    for($i=0; $i<count($points); $i+=2){
        $points_string .= sprintf('%.2f %.2f', $points[$i]*$k, ($h-$points[$i+1])*$k);
        if($i==0)
            $points_string .= ' m ';
        else
            $points_string .= ' l ';
    }
    $this->_out($points_string . $op);
}
	
function Circle($x,$y,$r,$style='')
{
    $this->Ellipse($x,$y,$r,$r,$style);
}

function Ellipse($x,$y,$rx,$ry,$style='D')
{
    if($style=='F')
        $op='f';
    elseif($style=='FD' or $style=='DF')
        $op='B';
    else
        $op='S';
    $lx=4/3*(M_SQRT2-1)*$rx;
    $ly=4/3*(M_SQRT2-1)*$ry;
    $k=$this->k;
    $h=$this->h;
    $this->_out(sprintf('%.2f %.2f m %.2f %.2f %.2f %.2f %.2f %.2f c',
        ($x+$rx)*$k,($h-$y)*$k,
        ($x+$rx)*$k,($h-($y-$ly))*$k,
        ($x+$lx)*$k,($h-($y-$ry))*$k,
        $x*$k,($h-($y-$ry))*$k));
    $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c',
        ($x-$lx)*$k,($h-($y-$ry))*$k,
        ($x-$rx)*$k,($h-($y-$ly))*$k,
        ($x-$rx)*$k,($h-$y)*$k));
    $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c',
        ($x-$rx)*$k,($h-($y+$ly))*$k,
        ($x-$lx)*$k,($h-($y+$ry))*$k,
        $x*$k,($h-($y+$ry))*$k));
    $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c %s',
        ($x+$lx)*$k,($h-($y+$ry))*$k,
        ($x+$rx)*$k,($h-($y+$ly))*$k,
        ($x+$rx)*$k,($h-$y)*$k,
        $op));
}


function Rotate($angle,$x=-1,$y=-1)
{
    if($x==-1)
        $x=$this->x;
    if($y==-1)
        $y=$this->y;
    if($this->angle!=0)
        $this->_out('Q');
    $this->angle=$angle;
    if($angle!=0)
    {
        $angle*=M_PI/180;
        $c=cos($angle);
        $s=sin($angle);
        $cx=$x*$this->k;
        $cy=($this->h-$y)*$this->k;
        $this->_out(sprintf('q %.5f %.5f %.5f %.5f %.2f %.2f cm 1 0 0 1 %.2f %.2f cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
    }
}

function _endpage()
{
    if($this->angle!=0)
    {
        $this->angle=0;
        $this->_out('Q');
    }
    parent::_endpage();
}	

function RotatedText($x,$y,$txt,$angle)
{
    //Rotation du texte autour de son origine
    $this->Rotate($angle,$x,$y);
    $this->Text($x,$y,$txt);
    $this->Rotate(0);
}

}

?>
