<?php
/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 23/12/2015
 * Time: 15:31
 */

require_once 'class/class_fli.php';
require_once 'config/config.inc.php';

if( isset($_COOKIE[class_fli::get_nom_cookie()]) ) {
    unset($_COOKIE[class_fli::get_nom_cookie()]);
    setcookie(class_fli::get_nom_cookie(), "", time() - 3600);
}
$module_login = class_fli::get_module_login();
$controleur_login = class_fli::get_controleur_login();
$fonction_login = class_fli::get_fonction_login();

$module_defaut = class_fli::get_module_defaut();
$controleur_defaut = class_fli::get_controleur_defaut();
$fonction_defaut = class_fli::get_fonction_defaut();

if(!empty($module_login) && !empty($controleur_login) && !empty($fonction_login)){
    header('Location: '.class_fli::get_chemin_acces_absolu().'/'.$module_login.'-'.$controleur_login.'-'.$fonction_login);
}elseif(!empty($module_defaut) && !empty($controleur_defaut) && !empty($fonction_defaut)){
    header('Location: '.class_fli::get_chemin_acces_absolu().'/'.$module_defaut.'-'.$controleur_defaut.'-'.$fonction_defaut);
}