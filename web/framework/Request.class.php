<?php
/**
 * @author Sébastien Robert
 * @version 1
 * @name Request classe de récupération et de gestion des données utilisateurs GET, POST, FILE, COOKIE, SESSION
 */
class Request {
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @var session_start si la session a déjà été démarée
	 */
	private static $session_start = false;
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method hasGetParameter spécifie si un paramètre GET existe
	 * @param parameter_name le nom du parametre recherché
	 * @return si le paramètre GET nommé existe
	 */
	public static function hasGetParameter($parameter_name) {
		return isset($_GET[$parameter_name]);
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method hasPostParameter spécifie si un paramètre POST existe
	 * @param parameter_name le nom du parametre recherché
	 * @return si le paramètre POST nommé existe
	 */
	public static function hasPostParameter($parameter_name) {
		return isset($_POST[$parameter_name]);
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method hasFileParameter spécifie si un paramètre FILE existe
	 * @param parameter_name le nom du parametre recherché
	 * @return si le paramètre FILE nommé existe
	 */
	public static function hasFileParameter($parameter_name) {
		return isset($_FILES[$parameter_name]);
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method hasCookieParameter spécifie si un paramètre COOKIE existe
	 * @param parameter_name le nom du parametre recherché
	 * @return si le paramètre COOKIE nommé existe
	 */
	public static function hasCookieParameter($parameter_name) {
		return isset($_COOKIE[$parameter_name]);
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method hasSessionParameter spécifie si un paramètre SESSION existe
	 * @param parameter_name le nom du parametre recherché
	 * @return si le paramètre SESSION nommé existe
	 */
	public static function hasSessionParameter($parameter_name) {
		if (!self::$session_start) {
			self::$session_start = session_start();
		}
		
		if (self::$session_start) {
			return isset($_SESSION[$parameter_name]);
		}
		
		return false;
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method getGetParameter renvoi la valeur du paramètre GET nommé
	 * @param parameter_name le nom du parametre recherché
	 * @return la valeur du paramètre GET nommé
	 */
	public static function getGetParameter($parameter_name) {
		return self::securised($_GET[$parameter_name]);
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method getPostParameter renvoi la valeur du paramètre POST nommé
	 * @param parameter_name le nom du parametre recherché
	 * @return la valeur du paramètre POST nommé
	 */
	public static function getPostParameter($parameter_name) {
		return self::securised($_POST[$parameter_name]);
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method getFileParameter renvoi la valeur du paramètre FILE nommé
	 * @param parameter_name le nom du parametre recherché
	 * @return la valeur du paramètre FILE nommé
	 */
	public static function getFileParameter($parameter_name) {
		return self::securised($_FILES[$parameter_name]);
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method getCookieParameter renvoi la valeur du paramètre COOKIE nommé
	 * @param parameter_name le nom du parametre recherché
	 * @return la valeur du paramètre COOKIE nommé
	 */
	public static function getCookieParameter($parameter_name) {
		return self::securised($_COOKIE[$parameter_name]);
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method getSessionParameter renvoi la valeur du paramètre SESSION nommé
	 * @param parameter_name le nom du parametre recherché
	 * @return la valeur du paramètre SESSION nommé
	 */
	public static function getSessionParameter($parameter_name) {
		if (!self::$session_start) {
			self::$session_start = session_start();
		}
	
		if (self::$session_start) {
			return $_SESSION[$parameter_name];
		}
	
		return false;
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method getSessionId renvoi l'identifiant de session de l'utilisateur
	 * @return l'identifiant de session de l'utilisateur
	 */
	public static function getSessionId() {
		return session_id();
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method setCookieParameter crée ou modifie un cookie
	 * @param parameter_name le nom du cookie
	 * @param value la valeur du cookie créee ou modifiée
	 * @param time timestamp jusqu'à expiration du cookie
	 * @param path le domaine auquel est lié le cookie
	 * @return réussite de création ou modification de la valeur du cookie
	 */
	public static function setCookieParameter($parameter_name, $value, $time = 0, $path = '/') {
		return setcookie($parameter_name, $value, $time, $path);
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method setSessionParameter crée ou modifie une session
	 * @param parameter_name le nom de la session
	 * @param value la valeur de la session créee ou modifiée
	 * @return réussite de création ou modification de la valeur de la session
	 */
	public static function setSessionParameter($parameter_name, $value) {
		if (!self::$session_start) {
			self::$session_start = session_start();
		}
	
		if (self::$session_start) {
			$_SESSION[$parameter_name] = $value;
			
			return true;
		}
	
		return false;
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method regenerateSessionId régénère l'identifiant de session
	 */
	public static function regenerateSessionId() {
		if (!self::$session_start) {
			self::$session_start = session_start();
		}
		
		if (self::$session_start) {
			session_regenerate_id(true);
		}
	}
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method securised securise les variables utilisateurs
	 */
	private static function securised($value) {
		$search = array(NULL, '\x1a', '\n' , '\r', '\\', '\'', '\"', '\x00');
		$replace = array('\\'.NULL, '\\'.'\x1a', '\\'.'\n' , '\\'.'\r', '\\'.'\\', '\\'.'\'', '\\'.'\"', '\\'.'\x00');
		
		if (is_array($value)) {
			foreach ($value as &$v) {
				$v = htmlspecialchars($v);
				//$v = str_replace($search, $replace, htmlspecialchars($v));
			}
			unset($v);
		} else {
			$value = htmlspecialchars($value);
			//$value = str_replace($search, $replace, htmlspecialchars($value));
		}
		
		return $value;
	}
}