<?php
/**
 * @author Sébastien Robert
 * @version 1
 * @name QueryBase classe de gestion des requêtes SQL faites sur la base de données
 */
class QueryBase {
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @var host nom du serveur où se situe la base de données
	 */
	public static $host = '';
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @var base nom de la base de données
	 */
	public static $base = '';
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @var login login d'accès à la base de données
	 */
	public static $login = '';
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @var password mot de passe d'accès à la base de données
	 */
	public static $password = '';
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @var link lien de connexion à la base de données
	 */
	private static $link ;
	var  $db;

	private static $objbd = false;
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method getLink renvoi le lien de connexion à la base de données
	 * @return le lien de connexion à la base de données
	 */
	private static function getLink() {
		
			self::$link = mysqli_connect(self::$host, self::$login, self::$password);

			/*if (self::$link) {
				mysqli_select_db(self::$base, self::$link);
				mysqli_query('SET NAMES UTF8', self::$link);
			});*/
				mysqli_query(self::$link,'SET NAMES UTF8');
		

			return self::$link;
	}
	/*private static function getLink() {
		try {
			self::$objbd = new PDO('mysql:host=' .self::$host. ';dbname=' . self::$base, self::$login,
				self::$password, array(
					PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
					PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING

				));

			$req = self::$objbd->prepare("SET sql_mode = ''");
			$req->execute();

		} catch( PDOException $e ) {
			die('<h1>Impossible de se connecter &agrave; la base de donn&eacute;e ' . $bdd . ' !!</h1>');
		}
	}*/
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method fetch renvoi les résultats d'une requête SQL passée en paramètre
	 * @param sql la requête SQL
	 * @return les résultats d'une requête SQL
	 */
	public static function fetch($sql) {
		//$link = self::getLink();
		
		if($bdd = mysqli_connect(self::$host, self::$login, self::$password)){
			echo "nickel";

		}else{
			echo "erreur";
		}

		$query_return = mysqli_query($bdd,$sql) ;

		echo"<pre>";print_r($query_return);echo"</pre>";
		
		/*if ($query_return === true || $query_return === false) {
			return $query_return;
		}*/
		
		$results = array();
		
		while ($result =  mysqli_fetch_assoc($query_return,MYSQLI_ASSOC)) {
			$results[] = $result;
		}
		
		return $results;
	}
	/*public static function fetch($sql,$data=array())
	{
		$req = self::$objbd->prepare($sql);
		$req->execute($data);
		return $req->fetchAll(PDO::FETCH_OBJ);
	}*/
	
	
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method fetchOne renvoi le résultat unique d'une requête SQL passée en paramètre
	 * @param sql la requête SQL
	 * @return le résultat unique d'une requête SQL
	 */
	public static function fetchOne($sql) {
		
		$results = self::fetch($sql);

		echo "toto ".$sql;

		echo"<pre>";print_r($results);echo"</pre>";
		
		if (is_array($results)) {
			return current($results);
		}
		
		return $results;
	}
}