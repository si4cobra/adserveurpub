<?php
class QueryBasePdo{
	/**
	 * methode de connexion
	 * @author Danon Gnakouri
	 * @modified by Yannick Vendrell
	 * @param $login - login de connexion
	 * @param $motpass - mot de passe
	 * @param $bdd - nom de la base de donnée
	 * @since 1.0
	 * @return la connexion
	 */			
            var $db;
	public static function connectToBD($login,$motpass,$bdd){
	

       /*$principalbase="94.247.232.194";
       $bdd="advnaja";
       $login="teikhos";
       $motpass="TE7saD1cqyEkRF7Rcz729Elm";
        */
        $principalbase="advnajasrv";
       $bdd="advnaja";
       $login="advnaja";
       $motpass="MEafApTbhcj2";

		 try {
            $labdd = new PDO('mysql:host=' . $principalbase. ';dbname=' . $bdd, $login,
                $motpass, array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING

                ));

                $req = $labdd->prepare("SET sql_mode = ''");
                $req->execute();

        } catch( PDOException $e ) {
            die('<h1>Impossible de se connecter &agrave; la base de donn&eacute;e ' . $bdd . ' !!</h1>');
        }

        return $labdd;
	}

    public static function fetch($sql,$data=array()) {

       $labdd = self::connectToBD("","","");

        $req = $labdd->prepare($sql);
        $req->execute($data);
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function fetchOne($sql) {
       
         $results = self::fetch($sql);
         //echo $sql;
        //echo"<pre>";print_r($results);echo"</pre>";
        if (is_array($results)) {
            return current($results);
        }
        
        return $results;
    }
    
    public static function fetchid($sql,$data=array()) {

       $labdd = self::connectToBD("","","");

        $req = $labdd->prepare($sql);
        $req->execute($data);
        $id= $labdd->lastInsertId();
        return $id;
   }

     public static function fetchupdate($sql,$data=array()) {

       $labdd = self::connectToBD("","","");

        $req = $labdd->prepare($sql);
        $bresult =$req->execute($data);
       
        return $bresult;
   }

    public function tl_query($sql, $data = array())
    {
        $req = $this->db->prepare($sql);
        $req->execute($data);
        return $req->fetchAll(PDO::FETCH_OBJ);
    }

    public function renvoi_info_requete($sql, $data = array())
    {

        $req = $this->db->prepare($sql);
        $req->execute($data);
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

      public function renvoi_info_requete_number($sql, $data = array())
    {

        $req = $this->db->prepare($sql);
        $req->execute($data);
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Renvoi la liste des table de la base de donnée
     * @return array
     */
    public function renvoi_liste_table()
    {   $aTabRetour =array();
        $sql ="SHOW TABLES";
        $data=array();
        $req = $this->db->prepare($sql);
        $req->execute($data);
        $aTmp= $req->fetchAll(PDO::FETCH_ASSOC);

       
        if(!empty($aTmp)) {
            foreach( $aTmp as $item ) {
                $aTabRetour[] = $item['Tables_in_'.$sDatabase];
            }
        }

        return $aTabRetour;
    }

    public function renvoi_liste_champ_table($sNomTable){

        $sSqlChamp ="SHOW COLUMNS FROM ".$sNomTable;
        $req = $this->db->prepare($sSqlChamp);
        $req->execute();
        $aTmp= $req->fetchAll(PDO::FETCH_ASSOC);

        return $aTmp;
    }


    public function renvoi_nombreLigne_requete($sql, $data = array())
    {
        $req = $this->db->prepare($sql);
        $req->execute($data);
        return $req->rowCount();
    }

    /**
     * @param $sql
     */
    public function tl_exec($sql)
    {
        $req = $this->db->prepare($sql);
        $req->execute();
    }

    /**
     * Methode qui execute une requete en direct
     * @param $sql
     */
    public function execute_requete($sql)
    {
        //echo $sql."<br>";
        $req = $this->db->prepare($sql);
        return $req->execute();
    }

      /**
     * Methode qui execute une requete en direct
     * @param $sql
     */
    public function executionRequete($sql,$bdd="",$action="",$id="",$commentaire="")
    {
        $req = $this->db->prepare($sql);
      
         //$this->enregistrement_log($bdd,$action,$id,$commentaire);

        return $req->execute();
    }


 
    /**
     * Methode qui execute une requete en direct
     * @param $sql
     */
    public function executionRequeteId($sql)
    {
        //echo $sql."<br>";
        $req = $this->db->prepare($sql);
       
        $bresult = $req->execute();
        
        if(preg_match('/\binsert/i',$sql)){
            $id= $this->db->lastInsertId();
             //$this->enregistrement_log($bdd,$action,$id,$commentaire);
            return $id;
        }
        else
            return $bresult;

        
    }



    public function insert_update_requete($sql,$aTab=array()){
        $req = $this->db->prepare($sql);
        //echo"<pre>";print_r($aTab);echo"</pre>";
        //echo $sql."<br>";
        if(!empty($aTab)) {
            foreach( $aTab as $key => $value ) {
                $req->bindValue(':' . $key, $value);
            }
        }


        return $req->execute();
    }

    /**
 * methode de renvoi liste déroulante sur une requete   
 * @author Danon Gnakouri 
 * @since 3.2
 * @return la connexion
 */

 
    public function liste_deroulante($sLarequete,$bblanc=true,$atableauchoix=array(),$atableaudebut=array(),$valeurvide='',$textvide='',$bSelect=false){
    
        $aTableauretour = array();
        if(empty($atableauchoix))
            $atableauchoix=array();
        //echo "vide ".$valeurvide;
        $aTableauResult = $this->renvoi_info_requete_number($sLarequete);
        if($bblanc){
            $i=1;
            $aTableauretour[0]=array($valeurvide,$textvide);
        }else
            $i=0;
         //ici on ajoute des données avant
         if(!empty($atableaudebut)){
            foreach($atableaudebut as $valeur){
                $aTableauretour[$i]=array(str_replace("\"","'",$valeur[0]),$valeur[1]);
                $i++;
            }
         }
         
         if(!empty($aTableauResult)){
            

            foreach ($aTableauResult as $valeur) {
                # code...
                $aTabKeys = array_keys($valeur);
                //on verifie ici que l'option à afficher n'est pasd dans le tableauchoix  pour filtrer
                if(!in_array($valeur[0],$atableauchoix)){
                    $bcolor =false;
                    if($valeur[2]!="")
                         $bcolor=true; 
                               
                    $aTableauretour[$i] = array(str_replace("\"","'",$valeur[$aTabKeys[0]]),$valeur[$aTabKeys[1]],$bcolor,$bSelect);
                    $i++;
                }
            }
        }
        
        //echo $sLarequete."<br>";

        //echo"<pre>";print_r($aTabKeys);echo"</pre>";

        //echo"<pre>";print_r($aTableauResult);echo"</pre>";

        //echo"<pre>";print_r($aTableauretour);echo"</pre>";

        return $aTableauretour;
    
    }


}