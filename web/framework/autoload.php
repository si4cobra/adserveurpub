<?php
/**
 * @author Sébastien Robert
 * @version 1
 * @method autoload fonction d'autochargement de classes PHP, il n'est plus utile d'inclure des fichiers avec les commandes suivantes (include, include_once, require, require_once), cette fonction le fait dynamiquement
 * @param class classe à inclure
 * @see http://fr.php.net/autoload
 */
function autoload($class) {
	$directory_table = array('framework', 'model', 'controller', 'helper', 'libs');
	
	foreach ($directory_table as $directory) {
		$path = $directory.'/'.$class.'.class.php';
		
		if (file_exists($path)) {
			include $path;
			break;
		}
	}
}

/**
 * enregistrement de la fonction autoload en tant que fonction d'autochargement de classes PHP
 */
spl_autoload_register('autoload');