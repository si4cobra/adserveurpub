<?php
/**
 * @author Sébastien Robert
 * @version 1
 * @name Loader classe de récupération et de gestion des données utilisateurs GET, POST, FILE, COOKIE, SESSION
 */
class Loader {
	/**
	 * @author Sébastien Robert
	 * @version 1
	 * @method get instanciation d'une classe
	 * @param class_name le nom de la classe a instanciée
	 * @return une instance de la classe nommée
	 */
	public static function get($class_name) {
		return new $class_name;
	}
}