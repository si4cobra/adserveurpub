<?php
class StaticDbValue {
	private static $static_db_value_table = array();

	public static function get($libelle_variable) {
		if (!isset(self::$static_db_value_table[$libelle_variable])) {
			$resultat = Loader::get('Variables')->renvoi_variable($libelle_variable);
	
			if ($resultat) {
				self::$static_db_value_table[$libelle_variable] = $resultat['valeur'];
			} else {
				return false;
			}
		}
	
		return self::$static_db_value_table[$libelle_variable];
	}
} 