<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Debug</title>

        <!-- Bootstrap -->
        <link href="public/css/simple-sidebar.css" rel="stylesheet">
        <link href="public/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) 1.11.2 -->
        <script src="public/js/jquery-1.11.2.min.js"></script>
        <style>
            .containerKeyValue{
                display: flex;
                justify-content: space-between;
            }
            .key{

            }
            .value{

            }
        </style>
    </head>
    <body>
    {foreach from=$aDebug item=aDebugParc}
        <div class="containerKeyValue">
        {foreach from=$aDebugParc item=$aDebugParcParc key=$key}
            {if $key!='importance'}
                <div class="key"><b>{$key}</b></div><div class="value">{$aDebugParcParc}</div><br/>
            {/if}
        {/foreach}
        </div>
    {/foreach}
    </body>
</html>