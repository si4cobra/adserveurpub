{strip}
    {if isset($pagedirection)}
        {if $pagedirection|is_array}
            {foreach from=$pagedirection item=tabTpl}
                {foreach from=$tabTpl item=tpl}
                    {$tpl}
                {/foreach}
            {/foreach}
        {else}
            {$pagedirection}
        {/if}
    {/if}
{/strip}