<div style="color:#313131;margin: 0 auto;padding: 15px;margin-top: 10px;margin: 0 auto;text-align: center;padding-top: 10px;">

{if isset($aForm)}

		{if isset($aForm)}
		<div style="display: inline-block;background-color: #fff;padding: 15px;border: 1px solid #D0D1D5;">
		<table border="0" cellpadding="0" cellspacing="0" id="id-form" style="background-color:#fff;" class="table table-condensed table-hover">
			<tbody>
			{*{$aForm|@print_r}*}
			{foreach from=$aForm item=objForm}

				{if isset($objForm.type_champ) and ((isset($objForm.aff_form) and $objForm.aff_form eq 'ok') or ($objForm.type_champ eq 'category' or $objForm.type_champ eq 'bouton'))}
					{if $objForm.type_champ eq 'text'}
						<tr>
							<th valign="top" style="border:0;text-align: inherit;">{$objForm.text_label}</th>
							<td style="border:0;text-align: inherit;">
							{$objForm.valeur_variable}
							</td>
						</tr>
					{elseif $objForm.type_champ eq 'color'}
						<tr>
							<th valign="top" style="border:0;text-align: inherit;">{$objForm.text_label}</th>
							<td style="border:0;text-align: inherit;">
								<input
										type="color"
										class="inp-form"
										id="id_{$objForm.nom_variable}"
										name="{$objForm.nom_variable}"
										{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
									{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
										{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								>

							</td>
						</tr>

					{elseif $objForm.type_champ eq 'category'}
					<tr>
						<th colspan="2" style="border:0;">
							<h1>{$objForm.text_label}</h1>
						</th>
					</tr>
					{elseif $objForm.type_champ eq 'date'}
					<tr>
						<th valign="top" style="border:0;">{$objForm.text_label}</th>
						<td style="border:0;text-align: left;">
							<input

								{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}{$objForm.valeur_variable|date_format:"%Y-%m-%d"}{/if}

						</td>
					</tr>
					{elseif $objForm.type_champ eq 'textarea'}
					<tr>
						<th valign="top" style="border:0;text-align: inherit">{$objForm.text_label}</th>
						<td style="border:0;text-align: inherit;">
							{$objForm.valeur_variable}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'checkbox'}
					<tr>
						<th style="vertical-align:top;border:0;text-align: inherit">{$objForm.text_label}</th>
						<td style="padding-top:5px;border:0;text-align: inherit;">
							{if $objForm.tags eq 'ok'}
								<select name="{$objForm.nom_variable}[]" class="js-example-responsive" multiple="multiple" style="width: 100%;">
									{foreach from=$objForm.lesitem key=valeur_checkbox item=nom_checkbox}
								  		<option value="{$valeur_checkbox}" {if is_array($objForm.valeur_variable) and in_array($valeur_checkbox, $objForm.valeur_variable)}selected{/if}>{$nom_checkbox}</option>
								  	{/foreach}						  
								</select>								
							{else}
							<table style="display:inline-block;">
								<tbody>
									{foreach from=$objForm.lesitem key=valeur_checkbox item=nom_checkbox}
									<tr>
										<td style="padding-right:10px;">
											<input
												type="checkbox"
												name="{$objForm.nom_variable}[]"
												value="{$valeur_checkbox}"
												{if is_array($objForm.valeur_variable) and in_array($valeur_checkbox, $objForm.valeur_variable)}checked{/if}
											>
										</td>
										<td style="padding-right:20px;">{$nom_checkbox}</td>
									</tr>
									{/foreach}
								</tbody>
							</table>
							{/if}
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'select'}

					<tr>
						<th style="border:0;text-align: inherit">{$objForm.text_label}</th>
						<td style="border:0;text-align: inherit;">
								{if is_array($objForm.lesitem)}
								{foreach from=$objForm.lesitem key=id_valeur_possible item=valeur_possible_bdd}
								 {if $objForm.valeur_variable eq $id_valeur_possible}{$valeur_possible_bdd}{/if}
								{/foreach}
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'radio'}
					<tr>
						<th valign="top" style="border:0;text-align: inherit">{$objForm.text_label}</th>
						<td style="padding-top:8px;border:0;text-align: inherit;">
							<div style="display:inline-block;">
								<table>
									<tbody>

										<tr>
											<td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
                                                {foreach from=$objForm.lesitem key=valeur_radio item=nom_radio}
													{if $valeur_radio eq $objForm.valeur_variable}{$nom_radio}{/if}
                                                {/foreach}
											</td>

										</tr>

									</tbody>
								</table>
							</div>

						</td>
					</tr>
					{elseif $objForm.type_champ eq 'file'}
					<tr>
						<th style="border:0;">{$objForm.text_label}</th>
						<td style="border:0;text-align: left;">
							<div style="border-radius:6px;border:1px solid #ACACAC;padding:3px;display:inline-block;vertical-align:middle;">
								<input
									type="file"
									id="id_{$objForm.nom_variable}"
									name="{$objForm.nom_variable}"
									{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
									{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
									{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
										{foreach from=$objForm.tableau_attribut key=cle item=valeur}
											{if $cle!='size'}{$cle}="{$valeur}"{/if}
										{/foreach}
									{/if}
									{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
										{foreach from=$objForm.fonction_javascript key=cle item=valeur}
											{if $cle!='size'}{$cle}="{$valeur}"{/if}
										{/foreach}
									{/if}
								>
							</div>
							{if isset($objForm.file_aff_modif_form) and $objForm.file_aff_modif_form eq 'ok'}
								{if $objForm.valeur_variable neq ''}
									{if preg_match("/\.png$/", $objForm.valeur_variable) or
										preg_match("/\.jpeg$/", $objForm.valeur_variable) or 
										preg_match("/\.gif$/", $objForm.valeur_variable)
									}
									<div style="
										border-radius:6px;
										border:1px solid #ACACAC;
										padding:3px;
										display:inline-block;
										vertical-align:middle;
										{if isset($objForm.file_aff_modif_form_couleur_fond)}background-color:{$objForm.file_aff_modif_form_couleur_fond}{/if};"
									>
										<img
											src="{$objForm.file_visu}{$objForm.valeur_variable}"
											style="display:inline-block;vertical-align:middle;"
											{if isset($objForm.file_aff_modif_form_taille)}width="{$objForm.file_aff_modif_form_taille}"{/if}
										>
									</div>
									{elseif $objForm.valeur_variable neq '' and preg_match("/\.swf$/", $objForm.valeur_variable)}
									<div style="
										border-radius:6px;
										border:1px solid #ACACAC;
										padding:3px;
										display:inline-block;
										vertical-align:middle;
										{if isset($objForm.file_aff_modif_form_couleur_fond)}background-color:{$objForm.file_aff_modif_form_couleur_fond}{/if};"
									>
										<object
											type="application/x-shockwave-flash"
											data="{$objForm.file_visu}{$objForm.valeur_variable}"
											{if isset($objForm.file_aff_modif_form_taille)}width="{$objForm.file_aff_modif_form_taille}"{/if}
										>
											<param name="movie" value="{$objForm.file_visu}{$objForm.valeur_variable}" />
											<param name="wmode" value="transparent" />
											<p>swf non affichable</p>
										</object>
									</div>
									{else}
									<a target="_blank" href="{$objForm.file_visu}{$objForm.valeur_variable}">Voir le fichier</a>
									{/if}
								{/if}
							{/if}
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'time'}
					<tr>
						<th style="border:0;">{$objForm.text_label}</th>
						<td style="border:0;text-align: left;">
							<input
								type="time"
								class="timepicker inp-form"
								id="id_{$objForm.nom_variable}"
								name="{$objForm.nom_variable}"
								{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
								{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
								{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
							>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'hour'}
					<tr>
						<th style="border:0;">{$objForm.text_label}</th>
						<td style="border:0;text-align: left;">
							<input
								type="text"
								class="hourpicker inp-form"
								id="id_{$objForm.nom_variable}"
								name="{$objForm.nom_variable}"
								{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
								{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
								{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
							>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'hidden'}
					<tr>
						<td style="padding:0;border:0;"></td>
						<td style="padding:0;border:0;">
							<input
								type="hidden"
								{if isset($objForm.nom_variable)}name="{$objForm.nom_variable}"{/if}
								{if isset($objForm.valeur_variable)}value="{$objForm.valeur_variable}"{/if}
							>
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'password'}
						{if $objForm.double_password eq 'ok'}
							<tr>
								<th valign="top">{$objForm.text_label}</th>
								<td>
									<input
											type="password"
											class="inp-form"
											id="id_{$objForm.nom_variable}"
											name="{$objForm.nom_variable}"
											{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
										{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
											{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
									{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
										{foreach from=$objForm.tableau_attribut key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
										{foreach from=$objForm.fonction_javascript key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									>
									{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
										<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
									{/if}
								</td>
							</tr>
							<tr>
								<th valign="top">{$objForm.text_label} 2</th>
								<td>
									<input
											type="password"
											class="inp-form"
											id="id_{$objForm.nom_variable}_2"
											name="{$objForm.nom_variable}_2"
											{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
										{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
											{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
									{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
										{foreach from=$objForm.tableau_attribut key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
										{foreach from=$objForm.fonction_javascript key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									>
									{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
										<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
									{/if}
								</td>
							</tr>
						{else}
							<tr>
								<th valign="top">{$objForm.text_label}</th>
								<td>
									<input
											type="password"
											class="inp-form"
											id="id_{$objForm.nom_variable}"
											name="{$objForm.nom_variable}"
											{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
										{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
											{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
									{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
										{foreach from=$objForm.tableau_attribut key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
										{foreach from=$objForm.fonction_javascript key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									>
									{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
										<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
									{/if}
								</td>
							</tr>
						{/if}
					{elseif $objForm.type_champ eq 'email'}
					<tr>
						<th valign="top" style="border:0;">{$objForm.text_label}</th>
						<td style="border:0;text-align: left;">
							<input
								type="email"
								class="inp-form"
								id="id_{$objForm.nom_variable}"
								name="{$objForm.nom_variable}"
								{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
								{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
								{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
							>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'telephone'}
					<tr>
						<th style="border:0;" valign="top">{$objForm.text_label}</th>
						<td style="border:0;text-align: left;">
							<input
								type="tel"
								class="inp-form"
								id="id_{$objForm.nom_variable}"
								name="{$objForm.nom_variable}"
								{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
								{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
								{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
							>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{/if}
				{/if}
			{/foreach}
			</tbody>
		</table>

			</div>
		{/if}
	</form>
{/if}
</div>